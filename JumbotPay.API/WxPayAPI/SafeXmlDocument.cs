﻿using System;
using System.Xml;
namespace JumbotPay.API.WxPayAPI
{
    public class SafeXmlDocument : XmlDocument
    {
        public SafeXmlDocument()
        {
            this.XmlResolver = null;
        }
    }
}
