﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// 有关程序集的常规信息通过下列属性集
// 控制。更改这些属性值可修改
// 与程序集关联的信息。
[assembly: AssemblyTitle("JumbotPay.API")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("jumbot.net")]
[assembly: AssemblyProduct("JumbotPay.API")]
[assembly: AssemblyCopyright("jumbot.net (C) 2018")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(true)]
[assembly: AssemblyDelaySign(false)] 
[assembly: AssemblyVersion("1.0.0903")]
