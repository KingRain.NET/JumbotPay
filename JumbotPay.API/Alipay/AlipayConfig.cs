﻿using System.Web;
using System.Text;
using System.IO;
using System.Net;
using System;
using System.Collections.Generic;

namespace JumbotPay.API.Alipay
{
    /// <summary>
    /// 类名：Config
    /// 功能：基础配置类
    /// 详细：设置帐户有关信息及返回路径
    /// 版本：3.2
    /// 日期：2011-03-17
    /// 说明：
    /// 以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
    /// 该代码仅供学习和研究支付宝接口使用，只是提供一个参考。
    /// 
    /// 如何获取安全校验码和合作身份者ID
    /// 1.用您的签约支付宝账号登录支付宝网站(www.alipay.com)
    /// 2.点击“商家服务”(https://b.alipay.com/order/myOrder.htm)
    /// 3.点击“查询合作者身份(PID)”、“查询安全校验码(Key)”
    /// </summary>
    public class Config
    {
        #region 字段
        private string partner = "";
        private string key = "";
        private string seller_email = "";
        private string return_url = "";
        private string notify_url = "";
        private string input_charset = "";
        private string sign_type = "";
        private string transport = "";
        #endregion

        public Config(string account_alipay)
        {

            string strXmlFile = HttpContext.Current.Server.MapPath("~/_data/config/payment_alipay_" + account_alipay + ".config");
            JumbotPay.DBUtility.XmlControl XmlTool = new JumbotPay.DBUtility.XmlControl(strXmlFile);
            seller_email = XmlTool.GetText("Root/seller_email");                     //商家签约时的支付宝帐号，即收款的支付宝帐号
            partner = XmlTool.GetText("Root/partner"); 		//partner合作伙伴id（必须填写）
            key = XmlTool.GetText("Root/key"); //partner 的对应交易安全校验码（必须填写）
            XmlTool.Dispose();

            return_url = JumbotPay.Utils.App.Url + JumbotPay.Utils.App.Path + "alipay/return_url.aspx";
            notify_url = JumbotPay.Utils.App.Url + JumbotPay.Utils.App.Path + "alipay/notify_url.aspx";


            input_charset = "utf-8";
            sign_type = "MD5";
            transport = "https";
        }

        #region 属性

        /// <summary>
        /// 获取或设置合作者身份ID
        /// </summary>
        public string Partner
        {
            get { return partner; }
            set { partner = value; }
        }

        /// <summary>
        /// 获取或设置交易安全检验码
        /// </summary>
        public string Key
        {
            get { return key; }
            set { key = value; }
        }

        /// <summary>
        /// 获取或设置签约支付宝账号或卖家支付宝帐户
        /// </summary>
        public string Seller_email
        {
            get { return seller_email; }
            set { seller_email = value; }
        }

        /// <summary>
        /// 获取页面跳转同步通知页面路径
        /// </summary>
        public string Return_url
        {
            get { return return_url; }
        }

        /// <summary>
        /// 获取服务器异步通知页面路径
        /// </summary>
        public string Notify_url
        {
            get { return notify_url; }
        }

        /// <summary>
        /// 获取字符编码格式
        /// </summary>
        public string Input_charset
        {
            get { return input_charset; }
        }

        /// <summary>
        /// 获取签名方式
        /// </summary>
        public string Sign_type
        {
            get { return sign_type; }
        }

        /// <summary>
        /// 获取访问模式
        /// </summary>
        public string Transport
        {
            get { return transport; }
        }
        #endregion
    }
}