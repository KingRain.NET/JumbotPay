﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Configuration;
namespace JumbotPay.Common
{
    public class DomainLocation : IHttpModule
    {
        public void Dispose()
        {
        }
        public void Init(HttpApplication context)
        {
            context.AuthorizeRequest += (new EventHandler(Process301));
        }
        public void Process301(object sender, EventArgs e)
        {
            HttpApplication app = (HttpApplication)sender;
            HttpRequest request = app.Context.Request;
            string lRequestedPath = request.Url.DnsSafeHost.ToString();
            if (request.Url.ToString().ToLower().StartsWith("http://www.paperyy.com"))
            {
                if (!request.RawUrl.ToString().ToLower().Contains("/report/html2zip"))
                {
                    app.Response.StatusCode = 301;
                    app.Response.AddHeader("Location", lRequestedPath.Replace(lRequestedPath, "http://www.jumbot.net" + request.RawUrl.ToString().Trim()));
                    app.Response.End();
                }
                else
                    CYQ.Data.Log.WriteLogToTxt("301原始URL：" + request.Url.ToString());
            }

        }
    }
}