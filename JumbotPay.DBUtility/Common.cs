﻿/*
 * 程序名称: JumbotPay
 * 
 * 程序版本: 1.x
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Text.RegularExpressions;
namespace JumbotPay.DBUtility
{
    /// <summary>
    /// 枚举，作为Web中常用的用户操作类型。常用于权限相关的判断。
    /// </summary>
    public enum OperationType : byte { Add, Modify, Delete, Audit, Enable, Copy };
}