﻿using System;

namespace JumbotPay.Utils.fastJSON
{
    internal class Getters
    {
        public string Name;
        public JSON.GenericGetter Getter;
    }
}
