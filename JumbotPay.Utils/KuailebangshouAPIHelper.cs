﻿/*
 * 程序名称: JumbotPay
 * 
 * 程序版本: 1.x
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Web;
using System.IO;
namespace JumbotPay.Utils
{
    public static class KuaiLeBangShouAPIHelper
    {
        /// <summary>
        /// 获取订购信息 
        /// </summary>
        /// <param name="_ScretKey"></param>
        /// <param name="_ThirdFag"></param>
        /// <param name="_TID"></param>
        /// <returns></returns>
        public static string BuyerInfo(string _ScretKey, string _ThirdFag, string _Nick)
        {
            long _long = JumbotPay.Utils.DateTimeHelp.ConvertDateTimeInt(System.DateTime.Now) / 1000;
            string _time = _long.ToString();
            string _pin = JumbotPay.Utils.MD5.Lower32(_ScretKey + _Nick + _time);
            string _posturl = "http://v.dingdan.kuailebangshou.com/app/info";
            StringBuilder builder = new StringBuilder();
            builder.Append("third_flag=" + _ThirdFag);
            builder.Append("&nick=" + _Nick);
            builder.Append("&time=" + _time);
            builder.Append("&pin=" + _pin);
            string _postData = builder.ToString();
            string _json = JumbotPay.Utils.HttpHelper.Post_Http(_posturl, _postData, Encoding.UTF8);
            return _json;
        }
        /// <summary>
        /// 获取单笔交易信息 
        /// </summary>
        /// <param name="_ScretKey"></param>
        /// <param name="_ThirdFag"></param>
        /// <param name="_TID"></param>
        /// <returns></returns>
        public static string TradeGet(string _ScretKey, string _ThirdFag, string _Nick, string _TID)
        {
            long _long = JumbotPay.Utils.DateTimeHelp.ConvertDateTimeInt(System.DateTime.Now) / 1000;
            string _time = _long.ToString();
            string _pin = JumbotPay.Utils.MD5.Lower32(_ScretKey + _Nick + _time);
            string _posturl = "http://v.dingdan.kuailebangshou.com/tid/trade_info";
            StringBuilder builder = new StringBuilder();
            builder.Append("third_flag=" + _ThirdFag);
            builder.Append("&nick=" + _Nick);
            builder.Append("&time=" + _time);
            builder.Append("&pin=" + _pin);
            builder.Append("&tid=" + _TID);
            string _postData = builder.ToString();
            string _json = JumbotPay.Utils.HttpHelper.Post_Http(_posturl, _postData, Encoding.UTF8);
            return _json;
        }
        /// <summary>
        /// 发货
        /// </summary>
        /// <param name="_ScretKey"></param>
        /// <param name="_ThirdFag"></param>
        /// <param name="_Nick"></param>
        /// <param name="_TID"></param>
        /// <returns></returns>
        public static bool LogisticsDummySend(string _ScretKey, string _ThirdFag, string _Nick, string _TID)
        {
            try
            {
                long _long = JumbotPay.Utils.DateTimeHelp.ConvertDateTimeInt(System.DateTime.Now) / 1000;
                string _time = _long.ToString();
                string _pin = JumbotPay.Utils.MD5.Lower32(_ScretKey + _Nick + _time);
                string _posturl = "http://v.dingdan.kuailebangshou.com/tid/dummy_send";
                StringBuilder builder = new StringBuilder();
                builder.Append("third_flag=" + _ThirdFag);
                builder.Append("&nick=" + _Nick);
                builder.Append("&time=" + _time);
                builder.Append("&pin=" + _pin);
                builder.Append("&tid=" + _TID);
                string _postData = builder.ToString();
                string _json = JumbotPay.Utils.HttpHelper.Post_Http(_posturl, _postData, Encoding.UTF8);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// 授权的错误代码
        /// </summary>
        /// <param name="error_code"></param>
        /// <returns></returns>
        public static string UserSellerGetCodeToChinese(string error_code)
        {
            switch (error_code)
            {
                case "session-is-blank":
                    return "授权已过期";
                case "session-expired":
                    return "授权已过期";
                case "sessionkey-not-generated-by-server":
                    return "授权已过期";
                case "isv.user-not-exist:invalid-nick":
                    return "用户不存在";
                case "isv.shop-service-error:SHOP_IS_NOT_EXIST":
                    return "您还没有店铺，不能授权开店";
                case "isv.shop-not-exist:invalid-shop":
                    return "您还没有店铺，不能授权开店";
                default:
                    return error_code;
            }
        }

        /// <summary>
        /// 获取订单信息的错误代码
        /// </summary>
        /// <param name="error_code"></param>
        /// <returns></returns>
        public static string TradeGetCodeToChinese(string error_code)
        {
            switch (error_code)
            {
                case "session-is-blank":
                    return "授权已过期";
                case "session-expired":
                    return "授权已过期";
                case "sessionkey-not-generated-by-server":
                    return "授权已过期";
                case "isv.invalid-parameter:tid":
                    return "tid无效，格式不对、非法值、越界等";
                case "isv.trade-not-exist":
                    return "订单不存在";
                case "isv.invalid-permission":
                    return "无权获取该订单详情";//必须是此交易的买家或卖家才能获取交易详细信息
                default:
                    return error_code;
            }
        }
        /// <summary>
        /// 发货API的错误代码
        /// </summary>
        /// <param name="error_code"></param>
        /// <returns></returns>
        public static string LogisticsDummySendCodeToChinese(string error_code)
        {
            switch (error_code)
            {
                case "session-expired":
                    return "授权已过期";
                case "sessionkey-not-generated-by-server":
                    return "授权已过期";
                case "isv.logistics-dummy-service-error:B01":
                    return "物流订单不存在";
                case "isv.logistics-dummy-service-error:B02":
                    return "没有权限进行发货";
                case "isv.logistics-dummy-service-error:B04":
                    return "物流订单状态不对";
                case "isv.logistics-dummy-service-error:P01":
                    return "参数为空";
                case "isv.invalid-parameter":
                    return "参数无效，格式不对、非法值、越界等";
                case "isv.logistics-dummy-service-error:B98":
                    return "发货类型不匹配";
                case "isv.logistics-dummy-service-error:P03":
                    return "没有用户ID";
                case "isv.logistics-dummy-service-error:B55":
                    return "该交易状态不正确，不能发货";
                case "isv.logistics-dummy-service-error:B27":
                    return "已生成发货单";
                case "isv.logistics-dummy-service-error:S01":
                    return "系统异常";
                case "isv.logistics-dummy-service-error:CD21":
                    return "已拆单订单不能进行无需物流发货";
                case "isv.logistics-dummy-service-error:B54":
                    return "不支持该种发货方式";
                case "isv.logistics-offline-service-error:B150":
                    return "发货异常，请稍等后重试";
                default:
                    return error_code;
            }
        }
    }
}
