﻿/*
 * 程序名称: JumbotPay
 * 
 * 程序版本: 1.x
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using System.Text;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
namespace JumbotPay.Utils
{
    /// <summary>
    /// api接口
    /// </summary>
    public static class apiHelper
    {
        private static string SiteUrl = System.Configuration.ConfigurationManager.AppSettings["JumbotPay:SiteUrl"];
        #region 关键词推荐与纠正
        /// <summary>
        /// 关键词推荐与纠正
        /// </summary>
        /// <param name="_name"></param>
        /// <param name="_indexname"></param>
        /// <returns></returns>
        public static string GetRecommendKeywords(ref string _error_code, string _data)
        {
            string _posturl = SiteUrl + "/Service_API.aspx";
            string _postData = "";
            string _json = "";
            _postData = "url=" + System.Web.HttpUtility.UrlEncode("/api/writing/keywords/recommend");
            _postData += "&post=" + System.Web.HttpUtility.UrlEncode(_data);
            _json = JumbotPay.Utils.HttpHelper.Post_Http(_posturl, _postData, Encoding.UTF8);
            return _json;
        }
        #endregion

        #region 选题推荐
        /// <summary>
        /// 选题推荐
        /// </summary>
        /// <param name="_name"></param>
        /// <param name="_indexname"></param>
        /// <returns></returns>
        public static string GetRecommendTitles(ref string _error_code, string _data)
        {
            string _posturl = SiteUrl + "/Service_API.aspx";
            string _postData = "";
            string _json = "";
            _postData = "url=" + System.Web.HttpUtility.UrlEncode("/api/writing/title/recommend");
            _postData += "&post=" + System.Web.HttpUtility.UrlEncode(_data);
            _json = JumbotPay.Utils.HttpHelper.Post_Http(_posturl, _postData, Encoding.UTF8);
            return _json;
        }
        #endregion

        #region 论文基本元数据更新保存
        /// <summary>
        /// 论文基本元数据更新保存
        /// </summary>
        /// <param name="_name"></param>
        /// <param name="_indexname"></param>
        /// <returns></returns>
        public static string SetArticleInfo(ref string _error_code,string _articleid, string _data)
        {
            string _posturl = SiteUrl + "/Service_API.aspx";
            string _postData = "";
            _postData = "url=" + System.Web.HttpUtility.UrlEncode("/api/writing/" + _articleid);
            _postData += "&post=" + System.Web.HttpUtility.UrlEncode(_data);
            DateTime beforDT = System.DateTime.Now;
            string _json = JumbotPay.Utils.HttpHelper.Post_Http(_posturl, _postData, Encoding.UTF8);
            //DateTime afterDT = System.DateTime.Now;
            //TimeSpan ts = afterDT.Subtract(beforDT);
            //try
            //{
            //    string _savefile = "~/_data/log/sapi_" + DateTime.Now.ToString("yyyyMMdd") + ".log";
            //    System.IO.StreamWriter sw = new System.IO.StreamWriter(HttpContext.Current.Server.MapPath(_savefile), true, System.Text.Encoding.UTF8);
            //    sw.WriteLine(System.DateTime.Now.ToString());
            //    sw.WriteLine("访问地址：\r\n{0}", _posturl);
            //    sw.WriteLine("提交数据：\r\n{0}", _postData);
            //    sw.WriteLine("返回数据：\r\n{0}", _json);
            //    sw.WriteLine("总共花费：{0}ms.", ts.TotalMilliseconds);
            //    sw.Close();
            //    sw.Dispose();
            //}
            //catch { }
            return _json;
        }
        #endregion

        #region 提纲推荐
        /// <summary>
        /// 提纲推荐
        /// </summary>
        /// <param name="_name"></param>
        /// <param name="_indexname"></param>
        /// <returns></returns>
        public static string GetRecommendOutlines(ref string _error_code, string _data)
        {
            string _posturl = SiteUrl + "/Service_API.aspx";
            string _postData = "";
            string _json = "";
            _postData = "url=" + System.Web.HttpUtility.UrlEncode("/api/writing/outline/recommend");
            _postData += "&post=" + System.Web.HttpUtility.UrlEncode(_data);
            _json = JumbotPay.Utils.HttpHelper.Post_Http(_posturl, _postData, Encoding.UTF8);
            return _json;
        }
        #endregion

        #region 短句提醒
        /// <summary>
        /// 短句提醒
        /// </summary>
        /// <param name="_name"></param>
        /// <param name="_indexname"></param>
        /// <returns></returns>
        public static string GetSuggest(ref string _error_code, string _articleid, string _data)
        {
            string _posturl = SiteUrl + "/Service_API.aspx";
            string _postData = "";
            string _json = "";
            _postData = "url=" + System.Web.HttpUtility.UrlEncode("/api/writing/" + _articleid + "/suggest");
            _postData += "&post=" + System.Web.HttpUtility.UrlEncode(_data);
            _json = JumbotPay.Utils.HttpHelper.Post_Http(_posturl, _postData, Encoding.UTF8);
            return _json;
        }
        #endregion
        #region 写作上下文获取接口
        /// <summary>
        /// 写作上下文获取接口
        /// </summary>
        /// <param name="_error_code"></param>
        /// <param name="_docid"></param>
        /// <param name="_index"></param>
        /// <param name="_action"></param>
        /// <returns></returns>
        public static string GetSuggestContext(ref string _error_code, string _docid, int _index, string _action)
        {
            string _data="{}";
            string _posturl = SiteUrl + "/Service_API.aspx";
            string _postData = "";
            string _json = "";
            _postData = "url=" + System.Web.HttpUtility.UrlEncode("/api/writing/" + _docid + "/" + _index + "/" + _action);
            _postData += "&post=" + System.Web.HttpUtility.UrlEncode(_data);
            _json = JumbotPay.Utils.HttpHelper.Post_Http(_posturl, _postData, Encoding.UTF8);
            return _json;
        }
        #endregion
        #region 资料搜集
        /// <summary>
        /// 资料搜集
        /// </summary>
        /// <param name="_name"></param>
        /// <param name="_indexname"></param>
        /// <returns></returns>
        public static string GetRecommendDatas(ref string _error_code, string _data)
        {
            string _posturl = SiteUrl + "/Service_API.aspx";
            string _postData = "";
            string _json = "";
            _postData = "url=" + System.Web.HttpUtility.UrlEncode("/api/writing/data/search");
            _postData += "&post=" + System.Web.HttpUtility.UrlEncode(_data);
            _json = JumbotPay.Utils.HttpHelper.Post_Http(_posturl, _postData, Encoding.UTF8);
            return _json;
        }
        #endregion

        #region 资料搜索分组统计
        /// <summary>
        /// 资料搜索分组统计
        /// </summary>
        /// <param name="_name"></param>
        /// <param name="_indexname"></param>
        /// <returns></returns>
        public static string GetRecommendDataAggs(ref string _error_code, string _data)
        {
            string _posturl = SiteUrl + "/Service_API.aspx";
            string _postData = "";
            string _json = "";
            _postData = "url=" + System.Web.HttpUtility.UrlEncode("/api/writing/data/search/aggs");
            _postData += "&post=" + System.Web.HttpUtility.UrlEncode(_data);
            _json = JumbotPay.Utils.HttpHelper.Post_Http(_posturl, _postData, Encoding.UTF8);
            return _json;
        }
        #endregion

        #region 参考文献
        /// <summary>
        /// 参考文献
        /// </summary>
        /// <param name="_name"></param>
        /// <param name="_indexname"></param>
        /// <returns></returns>
        public static string GetRecommendReference(ref string _error_code, string _data)
        {
            string _posturl = SiteUrl + "/Service_API.aspx";
            string _postData = "";
            string _json = "";
            _postData = "url=" + System.Web.HttpUtility.UrlEncode("/api/writing/reference/recommend");
            _postData += "&post=" + System.Web.HttpUtility.UrlEncode(_data);
            _json = JumbotPay.Utils.HttpHelper.Post_Http(_posturl, _postData, Encoding.UTF8);
            return _json;
        }
        #endregion

        #region 参考文献-格式化
        /// <summary>
        /// 参考文献-格式化
        /// </summary>
        /// <param name="_name"></param>
        /// <param name="_indexname"></param>
        /// <returns></returns>
        public static string GetReferenceFormat(ref string _error_code, string _data)
        {
            string _posturl = SiteUrl + "/Service_API.aspx";
            string _postData = "";
            string _json = "";
            _postData = "url=" + System.Web.HttpUtility.UrlEncode("/api/writing/reference/format");
            _postData += "&post=" + System.Web.HttpUtility.UrlEncode(_data);
            _json = JumbotPay.Utils.HttpHelper.Post_Http(_posturl, _postData, Encoding.UTF8);
            return _json;
        }
        #endregion

        #region 关键词被引与发文统计
        /// <summary>
        /// 关键词被引与发文统计
        /// </summary>
        /// <param name="_name"></param>
        /// <param name="_indexname"></param>
        /// <returns>
        /// {"publish":{"2004":278,"2006":249,"2007":249,"2008":317,"2009":372,"2010":415,"2011":518,"2012":609,"2013":569,"2014":328},"reference":{"2004":1232.0,"2006":1245.0,"2007":1392.0,"2008":768.0,"2009":893.0,"2010":660.0,"2011":467.0,"2012":356.0,"2013":135.0,"2014":37.0}}
        /// </returns>
        public static string GetAnalyze(ref string _error_code, string _data)
        {
            string _posturl = SiteUrl + "/Service_API.aspx";
            string _postData = "";
            string _json = "";
            _postData = "url=" + System.Web.HttpUtility.UrlEncode("/api/writing/title/analyze");
            _postData += "&post=" + System.Web.HttpUtility.UrlEncode(_data);
            _json = JumbotPay.Utils.HttpHelper.Post_Http(_posturl, _postData, Encoding.UTF8);
            return _json;
        }
        #endregion
        
    }
}