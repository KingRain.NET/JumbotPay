﻿/*
 * 程序名称: JumbotPay
 * 
 * 程序版本: 1.x
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */

using System.Diagnostics;
using System.Web;
namespace JumbotPay.Utils
{
    public static class wkhtmltopdfHelper
    {
        /// <summary>
        /// HTML生成PDF
        /// </summary>
        /// <param name="url">地址</param>
        /// <param name="pdfPath">PDF存放路径</param>
        public static bool HtmlToPdf(string url, string pdfPath)
        {
                if (string.IsNullOrEmpty(url) || string.IsNullOrEmpty(pdfPath))
                    return false;
                Process p = new Process();
                string exe = HttpContext.Current.Server.MapPath("~/Bin/tools/wkhtmltopdf.exe");
                if (!System.IO.File.Exists(exe))
                    return false;
                p.StartInfo.FileName = exe;
                p.StartInfo.Arguments = " \"" + url + "\" \"" + pdfPath+"\"";
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.RedirectStandardInput = true;
                p.StartInfo.RedirectStandardOutput = true;
                p.StartInfo.RedirectStandardError = true;
                p.StartInfo.CreateNoWindow = true;
                p.Start();
                p.WaitForExit();
                System.Threading.Thread.Sleep(500);

                if (!System.IO.File.Exists(pdfPath))
                    return false;
                else
                    return true;
        }
    }
}
