﻿/*
 * 程序名称: JumbotPay
 * 
 * 程序版本: 1.x
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Web;
namespace JumbotPay.Utils
{
    /// <summary>
    /// ActionValidator操作类
    /// </summary>
    public static class ActionValidator
    {
        #region 防止暴力访问
        /// <summary>
        /// 1分钟验证
        /// </summary>
        private const int DURATION = 2;
        public enum ActionTypeEnum
        {
            Normal = 200,
            Postback = 200
        }
        public static bool IsValid(ActionTypeEnum actionType)
        {
            HttpContext context = HttpContext.Current;
            if (context.Request.Browser.Crawler) return false;
            string key = actionType.ToString() + context.Request.UserHostAddress;

            int hit = (Int32)(context.Cache[key] ?? 0);
            if (hit > (Int32)actionType) return false;
            else hit++;

            if (hit == 1)
            {
                context.Cache.Add(key, hit, null, DateTime.Now.AddMinutes(DURATION), System.Web.Caching.Cache.NoSlidingExpiration, System.Web.Caching.CacheItemPriority.Normal, null);

            }
            else
            {
                context.Cache[key] = hit;
            }
            return true;
        }
        #endregion
    }
}
