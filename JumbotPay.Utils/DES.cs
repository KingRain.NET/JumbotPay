﻿/*
 * 程序名称: JumbotPay
 * 
 * 程序版本: 1.x
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Security.Cryptography;
using System.Text;

namespace JumbotPay.Utils
{
    public static class DES
    {
        /// <summary>
        /// des加密(得到64位小写字符串
        /// </summary>
        /// <param name="strText"></param>
        /// <param name="strKey"></param>
        /// <returns></returns>
        public static String DESEncrypt(string strText, string strKey)
        {
            DESCryptoServiceProvider des = new DESCryptoServiceProvider();
            des.Mode = CipherMode.ECB;
            byte[] inputByteArray = Encoding.UTF8.GetBytes(strText);
            des.Key = Encoding.UTF8.GetBytes(strKey.Substring(0, 8));
            System.IO.MemoryStream ms = new System.IO.MemoryStream();
            CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(), CryptoStreamMode.Write);
            cs.Write(inputByteArray, 0, inputByteArray.Length);
            cs.FlushFinalBlock();
            byte[] inArray = ms.ToArray();
            cs.Close();
            ms.Close();
            StringBuilder ret = new StringBuilder();
            foreach (byte b in ms.ToArray())
            {
                ret.AppendFormat("{0:x2}", b);
            }
            return ret.ToString();
        }
        ///// <summary>
        ///// des解密(方法有问题
        ///// </summary>
        ///// <param name="strText"></param>
        ///// <param name="strDesKey"></param>
        ///// <returns></returns>
        //public static String DESDecrypt(String strText, string strDesKey)
        //{
        //    strText = strText.ToUpper();
        //    DESCryptoServiceProvider des = new DESCryptoServiceProvider();
        //    Int32 x;
        //    Byte[] inputByteArray = new Byte[strText.Length / 2];
        //    for (x = 0; x < strText.Length / 2; x++)
        //        inputByteArray[x] = (Byte)(Convert.ToInt32(strText.Substring(x * 2, 2), 16));

        //    des.Key = Encoding.UTF8.GetBytes(strDesKey.Substring(0, 8));
        //    des.IV = Encoding.UTF8.GetBytes(strDesKey.Substring(0, 8));
        //    System.IO.MemoryStream ms = new System.IO.MemoryStream();

        //    using (CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(), CryptoStreamMode.Write))
        //    {
        //        cs.Write(inputByteArray, 0, inputByteArray.Length);
        //        //cs.FlushFinalBlock();
        //        cs.Close();
        //    }
        //    string str = Encoding.UTF8.GetString(ms.ToArray());
        //    ms.Close();
        //    return str;
        //}
    }
}
