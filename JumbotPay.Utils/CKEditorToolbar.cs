﻿/*
 * 程序名称: JumbotPay
 * 
 * 程序版本: 1.x
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.IO;
namespace JumbotPay.Utils
{
    /// <summary>
    /// CKEditorToolbar
    /// </summary>
    public static class CKEditorToolbar
    {
        /// <summary>
        /// 简单模式
        /// </summary>
        public static object[] Simple
        {
            get
            {
                return new object[]{
                new object[] {"Source", "-", "JustifyLeft", "JustifyCenter", "JustifyRight", "-","Styles","FontSize",},
				new object[] { "Bold", "Italic", "-", "NumberedList", "BulletedList", "-", "Link", "Unlink"},
			};
            }
        }
        /// <summary>
        /// 最简模式
        /// </summary>
        public static object[] Basic
        {
            get
            {
                return new object[]{
				new object[] { "Bold","Italic","-","OrderedList","UnorderedList","-","Link","Unlink"}
                };
            }
        }
    }
}
