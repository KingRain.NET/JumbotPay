﻿/*
 * 程序名称: JumbotPay
 * 
 * 程序版本: 1.x
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
namespace JumbotPay.Utils
{
    /// <summary>
    /// 语言包
    /// </summary>
    public class LanguageHelp
    {
        public LanguageHelp()
        { }
        /// <summary>
        /// 绑定语言包(V6之后深入开发)
        /// </summary>
        /// <param name="_lng">如cn表示中文，en表示英文</param>
        /// <returns></returns>
        public Dictionary<string, object> GetEntity(string _lng)
        {
            string json = JumbotPay.Utils.DirFile.ReadFile("~/_data/languages/" + _lng + ".js");
            json = JumbotPay.Utils.Strings.GetHtml(json, "//<!--语言包begin", "//-->语言包end");
            return (Dictionary<string, object>)JumbotPay.Utils.fastJSON.JSON.Instance.ToObject(json);
        }
    }
}
