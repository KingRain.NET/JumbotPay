
CREATE TABLE [dbo].[jpay_app](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[appid] [nvarchar](20) NOT NULL,
	[appkey] [nvarchar](40) NOT NULL,
	[appname] [nvarchar](40) NOT NULL,
	[applogo] [nvarchar](150) NULL,
	[appurl] [nvarchar](150) NULL,
	[notify_url] [nvarchar](150) NOT NULL,
	[return_url] [nvarchar](150) NULL,
	[state] [int] NOT NULL,
	[account_alipay] [nvarchar](50) NULL,
	[account_wxpay] [nvarchar](50) NULL,
 CONSTRAINT [PK_pay_app] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


CREATE TABLE [dbo].[jpay_order](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[appid] [nvarchar](20) NULL,
	[order_id] [nvarchar](40) NOT NULL,
	[pay_method] [nvarchar](10) NOT NULL,
	[pay_amount] [numeric](18, 2) NOT NULL,
	[order_time] [datetime] NOT NULL,
	[pay_status] [int] NOT NULL,
	[product_name] [nvarchar](50) NULL,
	[product_desc] [nvarchar](200) NULL,
	[out_trade_no] [nvarchar](40) NULL,
	[pay_order_id] [nvarchar](40) NULL,
	[NoticeTimes] [int] NOT NULL,
	[notified] [int] NOT NULL,
	[pay_time] [datetime] NULL,
	[MustCheckPayState] [int] NOT NULL,
 CONSTRAINT [PK_pay_order] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[jpay_payment_account](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[account_name] [nvarchar](40) NOT NULL,
	[account_alipay] [nvarchar](50) NULL,
	[account_wxpay] [nvarchar](50) NULL
) ON [PRIMARY]
GO


CREATE NONCLUSTERED INDEX [idx_appid] ON [dbo].[jpay_order]
(
	[appid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [idx_MustCheckPayState] ON [dbo].[jpay_order]
(
	[MustCheckPayState] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


CREATE NONCLUSTERED INDEX [idx_order_id] ON [dbo].[jpay_order]
(
	[order_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [idx_order_time] ON [dbo].[jpay_order]
(
	[order_time] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [idx_out_trade_no] ON [dbo].[jpay_order]
(
	[out_trade_no] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [idx_pay_method] ON [dbo].[jpay_order]
(
	[pay_method] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [idx_pay_status] ON [dbo].[jpay_order]
(
	[pay_status] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[jpay_app] ADD DEFAULT ('') FOR [appid]
GO
ALTER TABLE [dbo].[jpay_app] ADD DEFAULT ('') FOR [appkey]
GO
ALTER TABLE [dbo].[jpay_app] ADD DEFAULT ('') FOR [appname]
GO
ALTER TABLE [dbo].[jpay_app] ADD DEFAULT ('') FOR [notify_url]
GO
ALTER TABLE [dbo].[jpay_app] ADD DEFAULT ((0)) FOR [state]
GO
ALTER TABLE [dbo].[jpay_order] ADD DEFAULT ('') FOR [order_id]
GO
ALTER TABLE [dbo].[jpay_order] ADD DEFAULT ('') FOR [pay_method]
GO
ALTER TABLE [dbo].[jpay_order] ADD DEFAULT ((0)) FOR [pay_amount]
GO
ALTER TABLE [dbo].[jpay_order] ADD DEFAULT (getdate()) FOR [order_time]
GO
ALTER TABLE [dbo].[jpay_order] ADD DEFAULT ((0)) FOR [pay_status]
GO
ALTER TABLE [dbo].[jpay_order] ADD DEFAULT ((0)) FOR [NoticeTimes]
GO
ALTER TABLE [dbo].[jpay_order] ADD DEFAULT ((0)) FOR [notified]
GO
ALTER TABLE [dbo].[jpay_order] ADD DEFAULT ((0)) FOR [MustCheckPayState]
GO
ALTER TABLE [dbo].[jpay_payment_account] ADD DEFAULT ('') FOR [account_name]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'后台通知回调页面' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'jpay_app', @level2type=N'COLUMN',@level2name=N'notify_url'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'前台跳转回调页面' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'jpay_app', @level2type=N'COLUMN',@level2name=N'return_url'
GO
