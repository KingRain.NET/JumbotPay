﻿/*
 * 程序名称: JumbotPay
 * 
 * 程序版本: 1.x
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Collections.Generic;
namespace JumbotPay.Entity
{ 
    /// <summary>
    /// 站点信息
    /// </summary>
    public class Site
    {
        public Site()
        { }
        private string m_Name;
        private string m_Url;
        private string m_Dir;


        /// <summary>
        /// 网站全称
        /// </summary>
        public string Name
        {
            set { m_Name = value; }
            get { return m_Name; }
        }
        /// <summary>
        /// 网站地址
        /// </summary>
        public string Url
        {
            set { m_Url = value; }
            get { return m_Url; }
        }
        /// <summary>
        /// 安装目录
        /// </summary>
        public string Dir
        {
            set { m_Dir = value; }
            get { return m_Dir; }
        }
    }
}
