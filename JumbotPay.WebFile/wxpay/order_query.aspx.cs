﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using JumbotPay.API.WxPayAPI;
namespace JumbotPay.WebFile.WxPay
{
    public partial class _order_query : JumbotPay.UI.BasicPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string _order_id = q("order_id");
            string _out_trade_no = q("out_trade_no");
            object[] value = new JumbotPay.DAL.OrderDAL().GetOrderFields("wxpay", _order_id, _out_trade_no, "appid,out_trade_no,account_wxpay");
            if (value != null)
            {
                string appid = value[0].ToString();
                _out_trade_no = value[1].ToString();
                string _account_wxpay = value[2].ToString();
                try
                {
                    string result = new OrderQuery().Run("", _out_trade_no, _account_wxpay);//不要暴露流水号
                    if (result.Contains("trade_state=SUCCESS"))
                    {
                        if (new JumbotPay.DAL.OrderDAL().UpdateOrder("wxpay", _out_trade_no, ""))
                        {
                        }
                        Response.Write(JsonResult(1, "success"));
                    }
                    else
                    {
                        Response.Write(JsonResult(0, JumbotPay.Utils.Strings.GetHtml(result, "return_msg=", "\r")));
                    }
                }
                catch (WxPayException ex)
                {
                    Response.Write(JsonResult(0, ex.Message));
                }
                catch (Exception ex)
                {
                    Response.Write(JsonResult(0, ex.Message));
                }
            }else
                Response.Write(JsonResult(0, "请确认订单号或商户单号"));
        }
    }
}