﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Drawing;
using System.Text;
using JumbotPay.API.WxPayAPI;
namespace JumbotPay.WebFile.WxPay
{
    public partial class _default2 : JumbotPay.UI.BasicPage
    {
        public string Order_ID = "";
        public string Pay_Amount = "0.00";
        public string Product_Name = "";
        public string Product_Desc = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            NativePay nativePay = new NativePay();
            string appid = q("appid");//appid
            string order_id = q("order_id");//订单号
            Order_ID = order_id;//订单号
            string sign = q("sign");
            string key = new JumbotPay.DAL.AppDAL().GetKey(appid);
            if (key == "")
            {
                Response.Write("无效的appid");
                Response.End();
            }
            string[] array0 ={
			    "order_id="+order_id,
                 "appid="+appid
				};
            if (!JumbotPay.Utils.Strings.CheckSign(array0, key, sign))
            {
                Response.Write("验签失败");
                Response.End();
            }
            object[] _value = new JumbotPay.DAL.OrderDAL().GetOrderFields("wxpay", order_id, "", "product_name,product_desc,pay_amount,account_wxpay");
            Product_Name = _value[0].ToString();                            //商品名称
            Product_Desc = _value[1].ToString();                                 //商品描述
            Pay_Amount = _value[2].ToString();

            string _account_wxpay = _value[3].ToString();
            string url1 = nativePay.GetPrePayUrl(order_id, _account_wxpay);

            //将url生成二维码图片
            Image1.ImageUrl = "MakeQRCode.aspx?data=" + HttpUtility.UrlEncode(url1);
            CYQ.Data.Log.WriteLogToTxt("\r\n" + Request.UrlReferrer + "\r\n" + Request.Url.ToString() + "\r\n");
        }
    }
}