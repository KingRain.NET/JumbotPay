﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Drawing;
using System.Text;
using JumbotPay.API.WxPayAPI;
namespace JumbotPay.WebFile.WxPay
{
    public partial class _default : JumbotPay.UI.BasicPage
    {
        public string Order_ID = "";
        public string APP_Name = "";
        public string APP_Logo = "";
        public string APP_Url = "";
        public string Pay_Amount = "0.00";
        public string Product_Name = "";
        public string Product_Desc = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            NativePay nativePay = new NativePay();
            string appid = q("appid");//appid
            string order_id = q("order_id");//订单号
            Order_ID = order_id;//订单号
            string sign = q("sign");

            doh.Reset();
            doh.ConditionExpress = "appid=@appid and state=1";
            doh.AddConditionParameter("@appid", appid);
            object[] value = doh.GetFields("jpay_app", "appkey,appname,applogo,appurl");

            if (value == null)
            {
                Response.Write("无效的appid");
                Response.End();
            }
            string key = value[0].ToString();
            APP_Name = value[1].ToString();
            APP_Logo = value[2].ToString();
            APP_Url = value[3].ToString();
            string[] array0 ={
			    "order_id="+order_id,
                 "appid="+appid
				};
            if (!JumbotPay.Utils.Strings.CheckSign(array0, key, sign))
            {
                Response.Write("验签失败");
                Response.End();
            }
            object[] _value = new JumbotPay.DAL.OrderDAL().GetOrderFields("wxpay", order_id, "", "product_name,product_desc,pay_amount,pay_status,account_wxpay");
            Product_Name = _value[0].ToString();                            //商品名称
            Product_Desc = _value[1].ToString();                                 //商品描述
            Pay_Amount = _value[2].ToString();
            int pay_status = Str2Int(_value[3].ToString());
            string _account_wxpay = _value[4].ToString();

            if (pay_status == 1)
            {
                Response.Write("该订单已经支付，请勿重复操作。");
                Response.End();
            }
            else
            {
                string url1 = nativePay.GetPrePayUrl(order_id, _account_wxpay);

                //将url生成二维码图片
                Image1.ImageUrl = "MakeQRCode.aspx?data=" + HttpUtility.UrlEncode(url1);
                CYQ.Data.Log.WriteLogToTxt("\r\n" + Request.UrlReferrer + "\r\n" + Request.Url.ToString() + "\r\n");
            }
        }
    }
}