﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Specialized;
using System.Collections.Generic;
namespace JumbotPay.WebFile.WxPay
{
    /// <summary>
    /// 创建该页面文件时，请留心该页面文件是可以对其进行美工处理的，原因在于支付完成以后，当前窗口会从支付宝的页面跳转回这个页面。
    /// 该页面称作“返回页”，是同步被支付宝服务器所调用，可当作是支付完成后的提示信息页，如“您的某某某订单，多少金额已支付成功”。
    /// </summary>
    public partial class _return_url : JumbotPay.UI.BasicPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string _order_id = q("order_id");
            string _out_trade_no = q("out_trade_no");
            object[] value = new JumbotPay.DAL.OrderDAL().GetOrderFields("wxpay", _order_id, _out_trade_no, "appid,out_trade_no,pay_status,account_wxpay");
            string appid = value[0].ToString();
            _out_trade_no = value[1].ToString();
            string _pay_status = value[2].ToString();
            string account_wxpay = value[3].ToString();

            doh.Reset();
            doh.ConditionExpress = "appid=@appid";
            doh.AddConditionParameter("@appid", appid);
            object[] value2 = doh.GetFields("jpay_app", "return_url");
            if (value2 != null)
            {
                string _return_url = value2[0].ToString();
                string key = new JumbotPay.DAL.AppDAL().GetKey(appid);
                string[] array1 = { "order_id=" + _order_id, "pay_status=" + (_pay_status=="1"?"0":"-1"), "pay_order_id=" + _out_trade_no };
                string sign1 = JumbotPay.Utils.Strings.GetSign(array1, key);
                Response.Redirect(_return_url + "?" + JumbotPay.Utils.Strings.GetParaStr(array1, sign1));
            }
            else
            {
                Response.Write("app无效");
            }



        }
    }
}