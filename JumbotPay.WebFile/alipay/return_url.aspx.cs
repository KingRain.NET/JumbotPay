﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Specialized;
using System.Collections.Generic;
using JumbotPay.API.Alipay;
namespace JumbotPay.WebFile.Alipay
{
    /// <summary>
    /// 创建该页面文件时，请留心该页面文件是可以对其进行美工处理的，原因在于支付完成以后，当前窗口会从支付宝的页面跳转回这个页面。
    /// 该页面称作“返回页”，是同步被支付宝服务器所调用，可当作是支付完成后的提示信息页，如“您的某某某订单，多少金额已支付成功”。
    /// </summary>
    public partial class _return_url : JumbotPay.UI.BasicPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SortedDictionary<string, string> sPara = GetRequestGet();

            if (sPara.Count > 0)//判断是否有带返回参数
            {
                string appid = Request.QueryString["extra_common_param"];
                string _return_url = "";
                string _account_alipay = "";
                string key = "";
                doh.Reset();
                doh.ConditionExpress = "appid=@appid";
                doh.AddConditionParameter("@appid", appid);
                object[] value = doh.GetFields("jpay_app", "return_url,account_alipay");
                if (value != null)
                {
                    _return_url = value[0].ToString();
                    key = new JumbotPay.DAL.AppDAL().GetKey(appid);
                    _account_alipay = value[1].ToString();
                }
                else
                {
                    Response.Write("app无效");
                    Response.End();
                }

                Notify aliNotify = new Notify(_account_alipay);
                bool verifyResult = aliNotify.Verify(sPara, Request.QueryString["notify_id"], Request.QueryString["sign"]);

                if (verifyResult)//验证成功
                {
                }
                else//验证失败
                {
                    CYQ.Data.Log.WriteLogToTxt(Request.Url.ToString() + "<br />验证失败");
                    //Response.Write("验证失败");
                }
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////
                //请在这里加上商户的业务逻辑程序代码

                //——请根据您的业务逻辑来编写程序（以下代码仅作参考）——
                //获取支付宝的通知返回参数，可参考技术文档中页面跳转同步通知参数列表
                string pay_order_id = Request.QueryString["trade_no"];              //支付宝交易号
                string out_trade_no = Request.QueryString["out_trade_no"];	        //获取订单号
                string total_fee = Request.QueryString["total_fee"];            //获取总金额
                string subject = Request.QueryString["subject"];                //商品名称、订单名称
                string body = Request.QueryString["body"];                      //商品描述、订单备注、描述
                string buyer_email = Request.QueryString["buyer_email"];        //买家支付宝账号
                string trade_status = Request.QueryString["trade_status"];      //交易状态


                int pay_status = 1;//默认支付失败
                if (Request.QueryString["trade_status"] == "TRADE_FINISHED" || Request.QueryString["trade_status"] == "TRADE_SUCCESS")
                {
                    if (new JumbotPay.DAL.OrderDAL().UpdateOrder("alipay", out_trade_no, pay_order_id))
                    {
                    }
                    pay_status = 0;
                }
                else
                {
                    Response.Write("trade_status=" + Request.QueryString["trade_status"]);
                }

                doh.Reset();
                doh.ConditionExpress = "out_trade_no=@out_trade_no";
                doh.AddConditionParameter("@out_trade_no", out_trade_no);
                string order_id = doh.GetField("jpay_order", "order_id").ToString();//根据流水号反推业务订单号
                string[] array1 = { "order_id=" + order_id, "pay_status=" + pay_status, "pay_order_id=" + out_trade_no };
                string sign1 = JumbotPay.Utils.Strings.GetSign(array1, key);
                Response.Redirect(_return_url + "?" + JumbotPay.Utils.Strings.GetParaStr(array1, sign1));

                //——请根据您的业务逻辑来编写程序（以上代码仅作参考）——

                /////////////////////////////////////////////////////////////////////////////////////////////////////////////

            }
            else
            {
                Response.Write("无返回参数");
            }
        }

        /// <summary>
        /// 获取支付宝GET过来通知消息，并以“参数名=参数值”的形式组成数组
        /// </summary>
        /// <returns>request回来的信息组成的数组</returns>
        public SortedDictionary<string, string> GetRequestGet()
        {
            int i = 0;
            SortedDictionary<string, string> sArray = new SortedDictionary<string, string>();
            NameValueCollection coll;
            //Load Form variables into NameValueCollection variable.
            coll = Request.QueryString;

            // Get names of all forms into a string array.
            String[] requestItem = coll.AllKeys;

            for (i = 0; i < requestItem.Length; i++)
            {
                sArray.Add(requestItem[i], Request.QueryString[requestItem[i]]);
            }

            return sArray;
        }
    }
}