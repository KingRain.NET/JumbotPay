﻿
/*	SWFObject v2.2 <http://code.google.com/p/swfobject/> 
	is released under the MIT License <http://www.opensource.org/licenses/mit-license.php> 
*/
var swfobject=function(){var D="undefined",r="object",S="Shockwave Flash",W="ShockwaveFlash.ShockwaveFlash",q="application/x-shockwave-flash",R="SWFObjectExprInst",x="onreadystatechange",O=window,j=document,t=navigator,T=false,U=[h],o=[],N=[],I=[],l,Q,E,B,J=false,a=false,n,G,m=true,M=function(){var aa=typeof j.getElementById!=D&&typeof j.getElementsByTagName!=D&&typeof j.createElement!=D,ah=t.userAgent.toLowerCase(),Y=t.platform.toLowerCase(),ae=Y?/win/.test(Y):/win/.test(ah),ac=Y?/mac/.test(Y):/mac/.test(ah),af=/webkit/.test(ah)?parseFloat(ah.replace(/^.*webkit\/(\d+(\.\d+)?).*$/,"$1")):false,X=!+"\v1",ag=[0,0,0],ab=null;if(typeof t.plugins!=D&&typeof t.plugins[S]==r){ab=t.plugins[S].description;if(ab&&!(typeof t.mimeTypes!=D&&t.mimeTypes[q]&&!t.mimeTypes[q].enabledPlugin)){T=true;X=false;ab=ab.replace(/^.*\s+(\S+\s+\S+$)/,"$1");ag[0]=parseInt(ab.replace(/^(.*)\..*$/,"$1"),10);ag[1]=parseInt(ab.replace(/^.*\.(.*)\s.*$/,"$1"),10);ag[2]=/[a-zA-Z]/.test(ab)?parseInt(ab.replace(/^.*[a-zA-Z]+(.*)$/,"$1"),10):0}}else{if(typeof O.ActiveXObject!=D){try{var ad=new ActiveXObject(W);if(ad){ab=ad.GetVariable("$version");if(ab){X=true;ab=ab.split(" ")[1].split(",");ag=[parseInt(ab[0],10),parseInt(ab[1],10),parseInt(ab[2],10)]}}}catch(Z){}}}return{w3:aa,pv:ag,wk:af,ie:X,win:ae,mac:ac}}(),k=function(){if(!M.w3){return}if((typeof j.readyState!=D&&j.readyState=="complete")||(typeof j.readyState==D&&(j.getElementsByTagName("body")[0]||j.body))){f()}if(!J){if(typeof j.addEventListener!=D){j.addEventListener("DOMContentLoaded",f,false)}if(M.ie&&M.win){j.attachEvent(x,function(){if(j.readyState=="complete"){j.detachEvent(x,arguments.callee);f()}});if(O==top){(function(){if(J){return}try{j.documentElement.doScroll("left")}catch(X){setTimeout(arguments.callee,0);return}f()})()}}if(M.wk){(function(){if(J){return}if(!/loaded|complete/.test(j.readyState)){setTimeout(arguments.callee,0);return}f()})()}s(f)}}();function f(){if(J){return}try{var Z=j.getElementsByTagName("body")[0].appendChild(C("span"));Z.parentNode.removeChild(Z)}catch(aa){return}J=true;var X=U.length;for(var Y=0;Y<X;Y++){U[Y]()}}function K(X){if(J){X()}else{U[U.length]=X}}function s(Y){if(typeof O.addEventListener!=D){O.addEventListener("load",Y,false)}else{if(typeof j.addEventListener!=D){j.addEventListener("load",Y,false)}else{if(typeof O.attachEvent!=D){i(O,"onload",Y)}else{if(typeof O.onload=="function"){var X=O.onload;O.onload=function(){X();Y()}}else{O.onload=Y}}}}}function h(){if(T){V()}else{H()}}function V(){var X=j.getElementsByTagName("body")[0];var aa=C(r);aa.setAttribute("type",q);var Z=X.appendChild(aa);if(Z){var Y=0;(function(){if(typeof Z.GetVariable!=D){var ab=Z.GetVariable("$version");if(ab){ab=ab.split(" ")[1].split(",");M.pv=[parseInt(ab[0],10),parseInt(ab[1],10),parseInt(ab[2],10)]}}else{if(Y<10){Y++;setTimeout(arguments.callee,10);return}}X.removeChild(aa);Z=null;H()})()}else{H()}}function H(){var ag=o.length;if(ag>0){for(var af=0;af<ag;af++){var Y=o[af].id;var ab=o[af].callbackFn;var aa={success:false,id:Y};if(M.pv[0]>0){var ae=c(Y);if(ae){if(F(o[af].swfVersion)&&!(M.wk&&M.wk<312)){w(Y,true);if(ab){aa.success=true;aa.ref=z(Y);ab(aa)}}else{if(o[af].expressInstall&&A()){var ai={};ai.data=o[af].expressInstall;ai.width=ae.getAttribute("width")||"0";ai.height=ae.getAttribute("height")||"0";if(ae.getAttribute("class")){ai.styleclass=ae.getAttribute("class")}if(ae.getAttribute("align")){ai.align=ae.getAttribute("align")}var ah={};var X=ae.getElementsByTagName("param");var ac=X.length;for(var ad=0;ad<ac;ad++){if(X[ad].getAttribute("name").toLowerCase()!="movie"){ah[X[ad].getAttribute("name")]=X[ad].getAttribute("value")}}P(ai,ah,Y,ab)}else{p(ae);if(ab){ab(aa)}}}}}else{w(Y,true);if(ab){var Z=z(Y);if(Z&&typeof Z.SetVariable!=D){aa.success=true;aa.ref=Z}ab(aa)}}}}}function z(aa){var X=null;var Y=c(aa);if(Y&&Y.nodeName=="OBJECT"){if(typeof Y.SetVariable!=D){X=Y}else{var Z=Y.getElementsByTagName(r)[0];if(Z){X=Z}}}return X}function A(){return !a&&F("6.0.65")&&(M.win||M.mac)&&!(M.wk&&M.wk<312)}function P(aa,ab,X,Z){a=true;E=Z||null;B={success:false,id:X};var ae=c(X);if(ae){if(ae.nodeName=="OBJECT"){l=g(ae);Q=null}else{l=ae;Q=X}aa.id=R;if(typeof aa.width==D||(!/%$/.test(aa.width)&&parseInt(aa.width,10)<310)){aa.width="310"}if(typeof aa.height==D||(!/%$/.test(aa.height)&&parseInt(aa.height,10)<137)){aa.height="137"}j.title=j.title.slice(0,47)+" - Flash Player Installation";var ad=M.ie&&M.win?"ActiveX":"PlugIn",ac="MMredirectURL="+O.location.toString().replace(/&/g,"%26")+"&MMplayerType="+ad+"&MMdoctitle="+j.title;if(typeof ab.flashvars!=D){ab.flashvars+="&"+ac}else{ab.flashvars=ac}if(M.ie&&M.win&&ae.readyState!=4){var Y=C("div");X+="SWFObjectNew";Y.setAttribute("id",X);ae.parentNode.insertBefore(Y,ae);ae.style.display="none";(function(){if(ae.readyState==4){ae.parentNode.removeChild(ae)}else{setTimeout(arguments.callee,10)}})()}u(aa,ab,X)}}function p(Y){if(M.ie&&M.win&&Y.readyState!=4){var X=C("div");Y.parentNode.insertBefore(X,Y);X.parentNode.replaceChild(g(Y),X);Y.style.display="none";(function(){if(Y.readyState==4){Y.parentNode.removeChild(Y)}else{setTimeout(arguments.callee,10)}})()}else{Y.parentNode.replaceChild(g(Y),Y)}}function g(ab){var aa=C("div");if(M.win&&M.ie){aa.innerHTML=ab.innerHTML}else{var Y=ab.getElementsByTagName(r)[0];if(Y){var ad=Y.childNodes;if(ad){var X=ad.length;for(var Z=0;Z<X;Z++){if(!(ad[Z].nodeType==1&&ad[Z].nodeName=="PARAM")&&!(ad[Z].nodeType==8)){aa.appendChild(ad[Z].cloneNode(true))}}}}}return aa}function u(ai,ag,Y){var X,aa=c(Y);if(M.wk&&M.wk<312){return X}if(aa){if(typeof ai.id==D){ai.id=Y}if(M.ie&&M.win){var ah="";for(var ae in ai){if(ai[ae]!=Object.prototype[ae]){if(ae.toLowerCase()=="data"){ag.movie=ai[ae]}else{if(ae.toLowerCase()=="styleclass"){ah+=' class="'+ai[ae]+'"'}else{if(ae.toLowerCase()!="classid"){ah+=" "+ae+'="'+ai[ae]+'"'}}}}}var af="";for(var ad in ag){if(ag[ad]!=Object.prototype[ad]){af+='<param name="'+ad+'" value="'+ag[ad]+'" />'}}aa.outerHTML='<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"'+ah+">"+af+"</object>";N[N.length]=ai.id;X=c(ai.id)}else{var Z=C(r);Z.setAttribute("type",q);for(var ac in ai){if(ai[ac]!=Object.prototype[ac]){if(ac.toLowerCase()=="styleclass"){Z.setAttribute("class",ai[ac])}else{if(ac.toLowerCase()!="classid"){Z.setAttribute(ac,ai[ac])}}}}for(var ab in ag){if(ag[ab]!=Object.prototype[ab]&&ab.toLowerCase()!="movie"){e(Z,ab,ag[ab])}}aa.parentNode.replaceChild(Z,aa);X=Z}}return X}function e(Z,X,Y){var aa=C("param");aa.setAttribute("name",X);aa.setAttribute("value",Y);Z.appendChild(aa)}function y(Y){var X=c(Y);if(X&&X.nodeName=="OBJECT"){if(M.ie&&M.win){X.style.display="none";(function(){if(X.readyState==4){b(Y)}else{setTimeout(arguments.callee,10)}})()}else{X.parentNode.removeChild(X)}}}function b(Z){var Y=c(Z);if(Y){for(var X in Y){if(typeof Y[X]=="function"){Y[X]=null}}Y.parentNode.removeChild(Y)}}function c(Z){var X=null;try{X=j.getElementById(Z)}catch(Y){}return X}function C(X){return j.createElement(X)}function i(Z,X,Y){Z.attachEvent(X,Y);I[I.length]=[Z,X,Y]}function F(Z){var Y=M.pv,X=Z.split(".");X[0]=parseInt(X[0],10);X[1]=parseInt(X[1],10)||0;X[2]=parseInt(X[2],10)||0;return(Y[0]>X[0]||(Y[0]==X[0]&&Y[1]>X[1])||(Y[0]==X[0]&&Y[1]==X[1]&&Y[2]>=X[2]))?true:false}function v(ac,Y,ad,ab){if(M.ie&&M.mac){return}var aa=j.getElementsByTagName("head")[0];if(!aa){return}var X=(ad&&typeof ad=="string")?ad:"screen";if(ab){n=null;G=null}if(!n||G!=X){var Z=C("style");Z.setAttribute("type","text/css");Z.setAttribute("media",X);n=aa.appendChild(Z);if(M.ie&&M.win&&typeof j.styleSheets!=D&&j.styleSheets.length>0){n=j.styleSheets[j.styleSheets.length-1]}G=X}if(M.ie&&M.win){if(n&&typeof n.addRule==r){n.addRule(ac,Y)}}else{if(n&&typeof j.createTextNode!=D){n.appendChild(j.createTextNode(ac+" {"+Y+"}"))}}}function w(Z,X){if(!m){return}var Y=X?"visible":"hidden";if(J&&c(Z)){c(Z).style.visibility=Y}else{v("#"+Z,"visibility:"+Y)}}function L(Y){var Z=/[\\\"<>\.;]/;var X=Z.exec(Y)!=null;return X&&typeof encodeURIComponent!=D?encodeURIComponent(Y):Y}var d=function(){if(M.ie&&M.win){window.attachEvent("onunload",function(){var ac=I.length;for(var ab=0;ab<ac;ab++){I[ab][0].detachEvent(I[ab][1],I[ab][2])}var Z=N.length;for(var aa=0;aa<Z;aa++){y(N[aa])}for(var Y in M){M[Y]=null}M=null;for(var X in swfobject){swfobject[X]=null}swfobject=null})}}();return{registerObject:function(ab,X,aa,Z){if(M.w3&&ab&&X){var Y={};Y.id=ab;Y.swfVersion=X;Y.expressInstall=aa;Y.callbackFn=Z;o[o.length]=Y;w(ab,false)}else{if(Z){Z({success:false,id:ab})}}},getObjectById:function(X){if(M.w3){return z(X)}},embedSWF:function(ab,ah,ae,ag,Y,aa,Z,ad,af,ac){var X={success:false,id:ah};if(M.w3&&!(M.wk&&M.wk<312)&&ab&&ah&&ae&&ag&&Y){w(ah,false);K(function(){ae+="";ag+="";var aj={};if(af&&typeof af===r){for(var al in af){aj[al]=af[al]}}aj.data=ab;aj.width=ae;aj.height=ag;var am={};if(ad&&typeof ad===r){for(var ak in ad){am[ak]=ad[ak]}}if(Z&&typeof Z===r){for(var ai in Z){if(typeof am.flashvars!=D){am.flashvars+="&"+ai+"="+Z[ai]}else{am.flashvars=ai+"="+Z[ai]}}}if(F(Y)){var an=u(aj,am,ah);if(aj.id==ah){w(ah,true)}X.success=true;X.ref=an}else{if(aa&&A()){aj.data=aa;P(aj,am,ah,ac);return}else{w(ah,true)}}if(ac){ac(X)}})}else{if(ac){ac(X)}}},switchOffAutoHideShow:function(){m=false},ua:M,getFlashPlayerVersion:function(){return{major:M.pv[0],minor:M.pv[1],release:M.pv[2]}},hasFlashPlayerVersion:F,createSWF:function(Z,Y,X){if(M.w3){return u(Z,Y,X)}else{return undefined}},showExpressInstall:function(Z,aa,X,Y){if(M.w3&&A()){P(Z,aa,X,Y)}},removeSWF:function(X){if(M.w3){y(X)}},createCSS:function(aa,Z,Y,X){if(M.w3){v(aa,Z,Y,X)}},addDomLoadEvent:K,addLoadEvent:s,getQueryParamValue:function(aa){var Z=j.location.search||j.location.hash;if(Z){if(/\?/.test(Z)){Z=Z.split("?")[1]}if(aa==null){return L(Z)}var Y=Z.split("&");for(var X=0;X<Y.length;X++){if(Y[X].substring(0,Y[X].indexOf("="))==aa){return L(Y[X].substring((Y[X].indexOf("=")+1)))}}}return""},expressInstallCallback:function(){if(a){var X=c(R);if(X&&l){X.parentNode.replaceChild(l,X);if(Q){w(Q,true);if(M.ie&&M.win){l.style.display="block"}}if(E){E(B)}}a=false}}}}();




//<!--网站参数begin

var site = new Object();
site.Name = '';
site.Name2 = '';
site.Url = '';
site.SiteID = '';
site.Dir = '/';
site.CookieDomain = '';
site.CookiePrev = 'JumbotPayV1';
site.AllowReg = true;
site.CheckReg = true;


//-->网站参数end



/*=================以上信息请勿手工修改=================*/

//浏览器兼容访问DOM 
function thisMovie(movieName) {  
	if (navigator.appName.indexOf("Microsoft") != -1) {      
		return window[movieName];
	}    
	else {       
		return document[movieName];   
	} 
}
var _ppyy_Host = function(){
    if(document.URL.substr(0,7)=='http://')
        return 'http://'+window.location.host;
    else
        return 'https://'+window.location.host;
}
var _ppyy_DialogUrl = site.Dir + "statics/dialog/";


/*获得元素*/
function $i(el)
{
    if(typeof el=='string')return document.getElementById(el);
    else if(typeof el=='object')return el;
    return null;
}
/*获得元素数组*/
function $A(els){var _els=[];if(els instanceof Array){for(var i=0;i!=els.length;i++){_els[_els.length]=$i(els[i]);}}else if(typeof els=='object'&&typeof els['length']!='undefined'&&els['length']>0){for(var i=0;i!=els.length;i++){_els[_els.length]=$i(els[i]);}}else{_els[0]=$i(els);}return _els;}

var JumbotPay=new Object();
var hexcase = 0;//1为大写
var chrsz   = 8;  
    
JumbotPay.MD5=function(s){
	//if(s.length == 32)
		//return s;
	return _ppyy_binl2hex(_ppyy_core_md5(_ppyy_str2binl(s), s.length * chrsz));
}   

function _ppyy_core_md5(x, len)   
{   
 
  x[len >> 5] |= 0x80 << ((len) % 32);   
  x[(((len + 64) >>> 9) << 4) + 14] = len;   
  
  var a =  1732584193;   
  var b = -271733879;   
  var c = -1732584194;   
  var d =  271733878;   
  
  for(var i = 0; i < x.length; i += 16)   
  {   
    var olda = a;   
    var oldb = b;   
    var oldc = c;   
    var oldd = d;   
  
    a = md5_ff(a, b, c, d, x[i+ 0], 7 , -680876936);   
    d = md5_ff(d, a, b, c, x[i+ 1], 12, -389564586);   
    c = md5_ff(c, d, a, b, x[i+ 2], 17,  606105819);   
    b = md5_ff(b, c, d, a, x[i+ 3], 22, -1044525330);   
    a = md5_ff(a, b, c, d, x[i+ 4], 7 , -176418897);   
    d = md5_ff(d, a, b, c, x[i+ 5], 12,  1200080426);   
    c = md5_ff(c, d, a, b, x[i+ 6], 17, -1473231341);   
    b = md5_ff(b, c, d, a, x[i+ 7], 22, -45705983);   
    a = md5_ff(a, b, c, d, x[i+ 8], 7 ,  1770035416);   
    d = md5_ff(d, a, b, c, x[i+ 9], 12, -1958414417);   
    c = md5_ff(c, d, a, b, x[i+10], 17, -42063);   
    b = md5_ff(b, c, d, a, x[i+11], 22, -1990404162);   
    a = md5_ff(a, b, c, d, x[i+12], 7 ,  1804603682);   
    d = md5_ff(d, a, b, c, x[i+13], 12, -40341101);   
    c = md5_ff(c, d, a, b, x[i+14], 17, -1502002290);   
    b = md5_ff(b, c, d, a, x[i+15], 22,  1236535329);   
  
    a = md5_gg(a, b, c, d, x[i+ 1], 5 , -165796510);   
    d = md5_gg(d, a, b, c, x[i+ 6], 9 , -1069501632);   
    c = md5_gg(c, d, a, b, x[i+11], 14,  643717713);   
    b = md5_gg(b, c, d, a, x[i+ 0], 20, -373897302);   
    a = md5_gg(a, b, c, d, x[i+ 5], 5 , -701558691);   
    d = md5_gg(d, a, b, c, x[i+10], 9 ,  38016083);   
    c = md5_gg(c, d, a, b, x[i+15], 14, -660478335);   
    b = md5_gg(b, c, d, a, x[i+ 4], 20, -405537848);   
    a = md5_gg(a, b, c, d, x[i+ 9], 5 ,  568446438);   
    d = md5_gg(d, a, b, c, x[i+14], 9 , -1019803690);   
    c = md5_gg(c, d, a, b, x[i+ 3], 14, -187363961);   
    b = md5_gg(b, c, d, a, x[i+ 8], 20,  1163531501);   
    a = md5_gg(a, b, c, d, x[i+13], 5 , -1444681467);   
    d = md5_gg(d, a, b, c, x[i+ 2], 9 , -51403784);   
    c = md5_gg(c, d, a, b, x[i+ 7], 14,  1735328473);   
    b = md5_gg(b, c, d, a, x[i+12], 20, -1926607734);   
  
    a = md5_hh(a, b, c, d, x[i+ 5], 4 , -378558);   
    d = md5_hh(d, a, b, c, x[i+ 8], 11, -2022574463);   
    c = md5_hh(c, d, a, b, x[i+11], 16,  1839030562);   
    b = md5_hh(b, c, d, a, x[i+14], 23, -35309556);   
    a = md5_hh(a, b, c, d, x[i+ 1], 4 , -1530992060);   
    d = md5_hh(d, a, b, c, x[i+ 4], 11,  1272893353);   
    c = md5_hh(c, d, a, b, x[i+ 7], 16, -155497632);   
    b = md5_hh(b, c, d, a, x[i+10], 23, -1094730640);   
    a = md5_hh(a, b, c, d, x[i+13], 4 ,  681279174);   
    d = md5_hh(d, a, b, c, x[i+ 0], 11, -358537222);   
    c = md5_hh(c, d, a, b, x[i+ 3], 16, -722521979);   
    b = md5_hh(b, c, d, a, x[i+ 6], 23,  76029189);   
    a = md5_hh(a, b, c, d, x[i+ 9], 4 , -640364487);   
    d = md5_hh(d, a, b, c, x[i+12], 11, -421815835);   
    c = md5_hh(c, d, a, b, x[i+15], 16,  530742520);   
    b = md5_hh(b, c, d, a, x[i+ 2], 23, -995338651);   
  
    a = md5_ii(a, b, c, d, x[i+ 0], 6 , -198630844);   
    d = md5_ii(d, a, b, c, x[i+ 7], 10,  1126891415);   
    c = md5_ii(c, d, a, b, x[i+14], 15, -1416354905);   
    b = md5_ii(b, c, d, a, x[i+ 5], 21, -57434055);   
    a = md5_ii(a, b, c, d, x[i+12], 6 ,  1700485571);   
    d = md5_ii(d, a, b, c, x[i+ 3], 10, -1894986606);   
    c = md5_ii(c, d, a, b, x[i+10], 15, -1051523);   
    b = md5_ii(b, c, d, a, x[i+ 1], 21, -2054922799);   
    a = md5_ii(a, b, c, d, x[i+ 8], 6 ,  1873313359);   
    d = md5_ii(d, a, b, c, x[i+15], 10, -30611744);   
    c = md5_ii(c, d, a, b, x[i+ 6], 15, -1560198380);   
    b = md5_ii(b, c, d, a, x[i+13], 21,  1309151649);   
    a = md5_ii(a, b, c, d, x[i+ 4], 6 , -145523070);   
    d = md5_ii(d, a, b, c, x[i+11], 10, -1120210379);   
    c = md5_ii(c, d, a, b, x[i+ 2], 15,  718787259);   
    b = md5_ii(b, c, d, a, x[i+ 9], 21, -343485551);   
  
    a = _ppyy_safe_add(a, olda);   
    b = _ppyy_safe_add(b, oldb);   
    c = _ppyy_safe_add(c, oldc);   
    d = _ppyy_safe_add(d, oldd);   
  }   
  return Array(a, b, c, d);   
  
}   
  
 
function _ppyy_md5_cmn(q, a, b, x, s, t)   
{   
  return _ppyy_safe_add(_ppyy_bit_rol(_ppyy_safe_add(_ppyy_safe_add(a, q), _ppyy_safe_add(x, t)), s),b);   
}   
function md5_ff(a, b, c, d, x, s, t)   
{   
  return _ppyy_md5_cmn((b & c) | ((~b) & d), a, b, x, s, t);   
}   
function md5_gg(a, b, c, d, x, s, t)   
{   
  return _ppyy_md5_cmn((b & d) | (c & (~d)), a, b, x, s, t);   
}   
function md5_hh(a, b, c, d, x, s, t)   
{   
  return _ppyy_md5_cmn(b ^ c ^ d, a, b, x, s, t);   
}   
function md5_ii(a, b, c, d, x, s, t)   
{   
  return _ppyy_md5_cmn(c ^ (b | (~d)), a, b, x, s, t);   
}   
 
function _ppyy_core_hmac_md5(key, data)   
{   
  var bkey = _ppyy_str2binl(key);   
  if(bkey.length > 16) bkey = _ppyy_core_md5(bkey, key.length * chrsz);   
  
  var ipad = Array(16), ōpad = Array(16);   
  for(var i = 0; i < 16; i++)   
  {   
    ipad[i] = bkey[i] ^ 0x36363636;   
    opad[i] = bkey[i] ^ 0x5C5C5C5C;   
  }   
  
  var hash = _ppyy_core_md5(ipad.concat(_ppyy_str2binl(data)), 512 + data.length * chrsz);   
  return _ppyy_core_md5(opad.concat(hash), 512 + 128);   
}   
  
  
function _ppyy_safe_add(x, y)   
{   
  var lsw = (x & 0xFFFF) + (y & 0xFFFF);   
  var msw = (x >> 16) + (y >> 16) + (lsw >> 16);   
  return (msw << 16) | (lsw & 0xFFFF);   
}   
 
function _ppyy_bit_rol(num, cnt)   
{   
  return (num << cnt) | (num >>> (32 - cnt));   
}   
  
  
function _ppyy_str2binl(str)   
{   
  var bin = Array();   
  var mask = (1 << chrsz) - 1;   
  for(var i = 0; i < str.length * chrsz; i += chrsz)   
    bin[i>>5] |= (str.charCodeAt(i / chrsz) & mask) << (i%32);   
  return bin;   
}   
   

  
function _ppyy_binl2hex(binarray)   
{   
  var hex_tab = hexcase ? "0123456789ABCDEF" : "0123456789abcdef";   
  var str = "";   
  for(var i = 0; i < binarray.length * 4; i++)   
  {   
    str += hex_tab.charAt((binarray[i>>2] >> ((i%4)*8+4)) & 0xF) +   
           hex_tab.charAt((binarray[i>>2] >> ((i%4)*8  )) & 0xF);   
  }   
  return str;   
}   
  
JumbotPay.Cookie={//
    set:function(name,value,expires,path,domain){
        if(typeof expires=="undefined"){
            expires=new Date(new Date().getTime()+24*3600*100);
        }
        document.cookie=name+"="+_ppyy_UrlEncode(value)+((expires)?"; expires="+expires.toGMTString():"")+((path)?"; path="+path:"; path=/")+((domain!=null && domain.length>0)?";domain="+domain:"");
    },
    get:function(name, subname){
        var re = new RegExp((subname ? name + "=(.*?&)*?" + subname + "=(.*?)(&|;)" : name + "=([^;]*)(;|$)"), "i");
        return _ppyy_UrlDecode(re.test(document.cookie) ? (subname ? RegExp["$2"] : RegExp["$1"]) : ""); 
    }, 
    clear:function(name,path,domain){
        if(this.get(name)){
            document.cookie=name+"="+((path)?"; path="+path:"; path=/")+((domain)?"; domain="+domain:"")+";expires=Fri, 02-Jan-1970 00:00:00 GMT";
        }
    }
};
//追加/删除事件
JumbotPay.Event={
    add:function(obj, evType, fn){
        if (obj.addEventListener){obj.addEventListener(evType, fn, false);return true;}
        else if (obj.attachEvent){var r = obj.attachEvent("on"+evType, fn);return r;}
        else {return false;}
    },
    remove:function(obj, evType, fn, useCapture){
        if (obj.removeEventListener){obj.removeEventListener(evType, fn, useCapture);return true;}
        else if (obj.detachEvent){var r = obj.detachEvent("on"+evType, fn);return r;}
        else {alert("Handler could not be removed");}
    }
};
//追加onload事件
JumbotPay.addOnloadEvent=function(fnc) {
    if ( typeof window.addEventListener != "undefined" )
        window.addEventListener( "load", fnc, false );
    else if ( typeof window.attachEvent != "undefined" )
    {
        window.attachEvent( "onload", fnc );
    }
    else
    {
        if ( window.onload != null )
        {
            var oldOnload = window.onload;
            window.onload = function (e) {
                oldOnload(e);
                window[fnc]();
            };
        } else
            window.onload = fnc;
    }
};
JumbotPay.isFunction=function(variable) {return (typeof(variable) == 'function' ? true : false);};
JumbotPay.isUndefined=function(variable) {return (typeof(variable) == 'undefined' ? true : false);};
/*字节长度*/
JumbotPay.Length=function(variable) {
    var len = 0;
    var val = variable;
    for (var i = 0; i < val.length; i++) 
    {
        if (val.charCodeAt(i) >= 0x4e00 && val.charCodeAt(i) <= 0x9fa5){ 
            len += 2;
        }else {
            len++;
        }
    }
    return len;
};
JumbotPay.Eval=function(data) {
    JumbotPay.Loading.hide();
    try {
        eval(data);
    }
    catch(e) {
        alert(data);    
    }
};
/////////////////////////////
//弹出消息框
/////////////////////////////
JumbotPay.Message=function(errstr, success, returnFunc){
    var MSG = $.message;
    MSG.lays(200, 24,_ppyy_GetScrollTop()+2);
    //MSG.anim('fade', 'slow');
    MSG.anim('fade', 'slow', site.Dir + '_libs/jquery.messager/');
    if(returnFunc) MSG.doafter(returnFunc);
    MSG.show(success, errstr, 1500);
    new _ppyy_Dialog().reset();//消息显示完毕完再执行
};
/////////////////////////////
//弹出提示框
/////////////////////////////
JumbotPay.Alert=function(errstr, success, returnFunc){
    var oDialog = new _ppyy_Dialog('2', '', 360, 180, success, true);
    oDialog.init();
    oDialog.event(errstr,'');
    if (returnFunc == null)
        oDialog.button('dialogSubmit', '');
    else
        oDialog.button('dialogSubmit', returnFunc);
};
/////////////////////////////
//弹出确认框
//例如:
//1、JumbotPay.Confirm("是否操作", act, null) //函数不加()
//2、JumbotPay.Confirm("是否操作", "alert('yes')", "alert('no')")
/////////////////////////////
JumbotPay.Confirm=function(errstr, returnSubmitFunc, returnCancelFunc)
{
    var oDialog = new _ppyy_Dialog('2', '', 360, 180, null, true);
    oDialog.init();
    oDialog.event(errstr,'');
    oDialog.button('dialogSubmit', returnSubmitFunc);
    if (returnCancelFunc == null)
        oDialog.button('dialogCancel', '');
    else
        oDialog.button('dialogCancel', returnCancelFunc);
};

/////////////////////////////
//弹出加载层
/////////////////////////////
JumbotPay.loading = {
    show: function (msgstr, width, height) {
        if (width == null) width = 460;
        if (height == null) height = 150;
        var oDialog = new _ppyy_Dialog('0', '友情提示', width, height, null, false);
        oDialog.init(true);
        oDialog.html("<div style='text-align:center;padding-top:20px;margin-top:20px;font:16px \"微软雅黑\";'>" + msgstr + "<br /><br /><img src='" + _ppyy_DialogUrl + "loading.gif' align='absmiddle'></div>");
    },
    hide: function (callReturnFunc) {
        new _ppyy_Dialog2().reset(callReturnFunc);
    }
};
/////////////////////////////
//弹出模拟窗口
/////////////////////////////
JumbotPay.Popup={
    show:function(url, width, height, showCloseBox, showTitle, returnFunc)
    {
        new _ppyy_Dialog().reset();
        if(showTitle==null) showTitle="&nbsp;";
        var oDialog = new _ppyy_Dialog('2', showTitle, width, height, null, showCloseBox);
        if (url.indexOf("?") == -1)
            oDialog.open(url+"?windowCode="+(new Date().getTime()), returnFunc, "auto");
        else
            oDialog.open(url+"&windowCode="+(new Date().getTime()), returnFunc, "auto");
    },
    hide:function(callReturnFunc){
        new _ppyy_Dialog().reset(callReturnFunc);
    }
};
/////////////////////////////
//弹出加载层
/////////////////////////////
JumbotPay.Loading={
    show:function(msgstr, width, height)
    {
        if(width==null) width=280;
        if(height==null) height=100;
        var oDialog = new _ppyy_Dialog('0', '友情提示', width, height, null, false);
        oDialog.init(true);
        oDialog.html("<div style='text-align:center;padding-top:20px;'>"+msgstr+"<br /><br /><img src='" + _ppyy_DialogUrl + "loading.gif' align='absmiddle'></div>");
    },
    hide:function(callReturnFunc){
        new _ppyy_Dialog().reset(callReturnFunc);
    }
};
JumbotPay.Event.add(window,"load",_ppyy_OperatorPlus);
JumbotPay.Event.add(window,"scroll",_ppyy_OperatorPlus);
JumbotPay.Event.add(window,"resize",_ppyy_OperatorPlus);


JumbotPay.Core = {
    WriteUserInfo: function () {
        $.ajax({
            type: "get",
            dataType: "json",
            url: _ppyy_Host() + site.Dir + "ajax/common.aspx?oper=ajaxGetLoginbar&clienttime=" + Math.random(),
            error: function (XmlHttpRequest, textStatus, errorThrown) { alert(XmlHttpRequest.responseText); },
            success: function (d) {
                if (d.result == "1") {//成功登录
                    var _pmBar = '', _cartBar = '';
                    if (d.newmessage > 0)
                        _pmBar = '<li class="ico pm1"><a href="' + site.Dir + 'user/message_list.aspx" title="' + d.newmessage + '条未读消息" target="_blank">短消息<span style="color:red;">(' + d.newmessage + ')</span></a></li>';
                    else
                        _pmBar = '<li class="ico pm0"><a href="' + site.Dir + 'user/message_list.aspx" title="0条未读消息" target="_blank">短消息</a></li>';
                    if (d.newcart > 0)
                        _cartBar = '<li class="ico cart1"><a href="' + site.Dir + 'user/maimai_cart.aspx" title="' + d.newcart + '件待购商品" target="_blank">购物车<span style="color:red;">(' + d.newcart + ')</span></a></li>';
                    else
                        _cartBar = '<li class="ico cart0"><a href="' + site.Dir + 'user/maimai_cart.aspx" title="0件待购商品" target="_blank">购物车</a></li>';
                    var _ajaxGetLoginbar = '\
						<ul>\
							<li class="ico user"><a href="' + site.Dir + 'user/default.aspx" title="进入个人中心" target="_blank"><b>' + d.username + '</b></a></li>\
							<li><a href="' + site.Dir + 'passport/logout">[退出]</a></li>\
							<li class="ico points"><a href="' + site.Dir + 'user/bobi_buypoints" target="_blank" title="当前账户剩余' + d.points + '元，点击马上充值">账户</a></li>' + _pmBar + _cartBar + '\
						</ul>\
					';
                    $("#user_status").html(_ajaxGetLoginbar);
                }
                $("#user_status").show();

            }
        });
    },
    ShowWeather: function (obj) {
        $.ajax({
            type: "get",
            dataType: "json",
            data: "clienttime=" + Math.random(),
            url: _ppyy_Host() + site.Dir + "plus/weather/json.aspx",
            error: function (XmlHttpRequest, textStatus, errorThrown) { if (XmlHttpRequest.responseText != "") { alert(XmlHttpRequest.responseText); } },
            success: function (d) {
                $('#' + obj).html(d.cityname + " <br /><span style='vertical-align:text-bottom;'><img alt='" + d.weather + "' title='" + d.weather + "' src='" + site.Dir + "plus/weather/icon/" + d.img + ".gif' /></span> " + d.temperature);
            }
        });
    },
    AddDigg: function (cType, id) {
        $.ajax({
            type: "get",
            dataType: "json",
            data: "oper=ajaxDiggAdd&id=" + id + "&cType=" + cType + "&clienttime=" + Math.random(),
            url: _ppyy_Host() + site.Dir + "digg/ajax.aspx",
            error: function (XmlHttpRequest, textStatus, errorThrown) { if (XmlHttpRequest.responseText != "") { alert(XmlHttpRequest.responseText); } },
            success: function (d) {
                $("#ajaxDigg_" + id).text(d.returnval);
                $("#DiggSpan" + id).html("谢谢支持");
            }
        });
    }

};

////////////////////////////////////////////////////////////////////////////////////////////
//标题栏跑马灯
////////////////////////////////////////////////////////////////////////////////////////////
var _ppyy_ScrollTitle__Oldtitle        = top.document.title;
var _ppyy_ScrollTitle__i        = 0;
var _ppyy_ScrollTitle__Speed        = 200;
var _ppyy_ScrollTitle__Timer        = function(message){
    if(_ppyy_ScrollTitle__i == message.length)
    {
        top.document.title = _ppyy_ScrollTitle__Oldtitle;
        _ppyy_ScrollTitle__i = 0;
        return;
    }
    else{
        top.document.title = message.substring(_ppyy_ScrollTitle__i);
        _ppyy_ScrollTitle__i++;
        setTimeout("_ppyy_ScrollTitle__Timer('"+message+"')",_ppyy_ScrollTitle__Speed);
    }
}

////////////////////////////////////////////////////////////////////////////////////////////

var _ppyy_HideSelects            = false;
var _ppyy_DialogIsShown            = false;
var _ppyy_WindowMask            = null;
////////////////////////////////////////////////////////////////////////////////////////////
//以下为弹出窗口的类
////////////////////////////////////////////////////////////////////////////////////////////
function _ppyy_Dialog(styletype, title, width, height, iswhat, showCloseBox){
    //半透明边框宽度
    var shadowBorderBoth=0;
    var oWidth = width;
    var oHeight = height;
    if(oWidth==-1 || oWidth>_ppyy_GetViewportWidth()-15)
    {
        oWidth=_ppyy_GetViewportWidth()-15;
        shadowBorderBoth = 0;
    }
    if(oWidth<-1)
    {
        oWidth=_ppyy_GetViewportWidth()+oWidth;
        shadowBorderBoth = 0;
    }
    if(oHeight==-1 || oHeight>_ppyy_GetViewportHeight()-44)
    {
        oHeight=_ppyy_GetViewportHeight()-44;
        shadowBorderBoth = 0;
    }
    if(oHeight<-1)
    {
        oHeight=_ppyy_GetViewportHeight()+oHeight;
        shadowBorderBoth = 0;
    }
    var sTitle = "友情提示";
    if (iswhat == "0")
        sTitle = "错误提示";
    else if (iswhat == "1")
        sTitle = "成功提示";
    else
        if (title!='') sTitle = title;
    var src = "";
    var path = _ppyy_DialogUrl + styletype + "/";
    var gReturnFunc;
    var gReturnVal = null;
    var sButtonFunc = '<input id="dialogSubmit" class="dialogSubmit' + styletype + '" type="button" value="确 认" onclick="new _ppyy_Dialog().reset();" /> <input id="dialogCancel" class="dialogCancel' + styletype + '" type="button" value="取 消" onclick="new _ppyy_Dialog().reset();" />';
    var sClose = '';
    if (showCloseBox == null || showCloseBox == true)
        sClose = '<img alt="关闭" style="cursor:pointer;" id="dialogBoxClose" onclick="new _ppyy_Dialog().reset();" src="' + path + 'dialogCloseOut.gif" border="0" onmouseover="this.src=\'' + path + 'dialogCloseOver.gif\';" onmouseout="this.src=\'' + path + 'dialogCloseOut.gif\';" align="absmiddle" />';
    var sSuccess = '';
    if (iswhat != null)
        sSuccess = '<td width="80" align="center" valign="middle"><img id="dialogBoxFace" class="dialogBoxFace' + styletype + '" src="' + path + iswhat + '.gif" valign="absmiddle" /></td>';
    else
        sSuccess = '<td width="80" align="center" valign="middle"><img id="dialogBoxFace" class="dialogBoxFace' + styletype + '" src="' + path + '0.gif" valign="absmiddle" /></td>';
    var sBody = '\
        <table id="dialogBodyBox" class="dialogBodyBox' + styletype + '" border="0" align="center" cellpadding="0" cellspacing="0" width="100%" height="100%" >\
            <tr height="' + (oHeight - 60) + '">\
                <td width="10"></td>' + sSuccess + '<td id="dialogMsg" class="dialogMsg' + styletype + '"></td>\
                <td width="10"></td>\
            </tr>\
            <tr height="30"><td id="dialogFunc" class="dialogFunc' + styletype + '" colspan="4">' + sButtonFunc + '</td></tr>\
        </table>\
    ';
    var sBox = '\
        <div style="display:none;" id="dialogBox" class="dialogBox' + styletype + '">\
            <div id="dialogTitleDiv" class="dialogTitleDiv' + styletype + '" style="width:' + oWidth + 'px;">\
                <span id="dialogBoxTitle" class="dialogBoxTitle' + styletype + '">' + sTitle + '</span>\
                <span id="dialogBoxClose" class="dialogBoxClose' + styletype + '">' + sClose + '</span>\
            </div>\
            <div id="dialogHeight" style="width:' + oWidth + 'px;height:' + oHeight + 'px;">\
                <div id="dialogBody" class="dialogBody' + styletype + '" style="height:' + oHeight + 'px;">' + sBody + '</div>\
            </div>\
        </div>\
        <div id="dialogBoxShadow" style="display:none;"></div>\
    ';
    this.init = function(_showTitleBar){
        document.body.oncontextmenu=function(){return false;};
        document.body.onselectstart=function(){return false;};
        document.body.ondragstart=function(){return false;};
        document.body.onsource=function(){return false;};
	//document.body.style.overflow='hidden';//屏蔽滚动条
        $i('dialogFrame') ? $i('dialogFrame').src='' : function(){};
        $i('dialogCase') ? $i('dialogCase').parentNode.removeChild($i('dialogCase')) : function(){};
        $i('windowMask') ? $i('windowMask').parentNode.removeChild($i('windowMask')) : function(){};
        var oDiv = document.createElement('span');
        oDiv.id = "dialogCase";
        oDiv.innerHTML = sBox;
        document.body.appendChild(oDiv);
        var oMask = document.createElement('div');
        oMask.id = 'windowMask';
        document.body.appendChild(oMask);
        _ppyy_WindowMask = $i("windowMask");
        _ppyy_WindowMask.style.display="block";
        var brsVersion = parseInt(window.navigator.appVersion.charAt(0), 10);
        if (brsVersion <= 6 && window.navigator.userAgent.indexOf("MSIE") > -1) {
            _ppyy_HideSelects = true;
        }
        if (_ppyy_HideSelects == true) {
            HideSelectBoxes();
        }
        if (_showTitleBar == true || _showTitleBar == null)
            $i("dialogTitleDiv").style.display = "block";
        else
            $i("dialogTitleDiv").style.display = "none";
        _ppyy_OperatorPlus();
    }
    //this.show = function(){$i('dialogBox') ? function(){} : this.init();_ppyy_DialogIsShown=true;this.middle('dialogBox');}
    this.show = function(){$i('dialogBox') ? function(){} : this.init();_ppyy_DialogIsShown=true;this.middle('dialogBox');this.shadow();this.middle('dialogBoxShadow');_ppyy_OperatorPlus();}
    this.html = function(_sHtml){
        this.show();
        $i('dialogBody').innerHTML = _sHtml;
    }
    this.button = function(_sId, _sFuc){
        if($i(_sId)){
            $i(_sId).style.display = '';
            if($i(_sId).addEventListener){
                if($i(_sId).act){$i(_sId).removeEventListener('click', function(){eval($i(_sId).act);}, false);}
                $i(_sId).act = _sFuc;
                $i(_sId).addEventListener('click', function(){eval(_sFuc);try{this.reset();}catch(e){}}, false);
            }else{
                if($i(_sId).act){$i(_sId).detachEvent('onclick', function(){eval($i(_sId).act);});}
                $i(_sId).act = _sFuc;
                $i(_sId).attachEvent('onclick', function(){eval(_sFuc);});
            }
        }
    }
    this.shadow = function(){
        if(shadowBorderBoth>0){
            var oShadow = $i('dialogBoxShadow');
            var oDialogDiv = $i('dialogBox');
            oShadow.style.position = "absolute";
            oShadow.style.background = "#000";
            oShadow.style.display = "";
            oShadow.style.opacity = "0.25";
            oShadow.style.filter = "alpha(opacity=25)";
            oShadow.style.width = (oDialogDiv.offsetWidth + shadowBorderBoth)+"px";
            oShadow.style.height = (oDialogDiv.offsetHeight + shadowBorderBoth)+"px";
        }
    }
    this.open = function(_sUrl, _returnFunc, _sMode){
        this.show();
        gReturnFunc = _returnFunc;
        //if(!_sMode || _sMode == "no" || _sMode == "yes"){
            $i("dialogBody").innerHTML = "<iframe id='dialogFrame' name='dialogFrame' src='" + site.Dir + "_data/loading.html' width='" + oWidth + "' height='" + oHeight + "' frameborder='no' border='0' marginwidth='0' marginheight='0' scrolling='" + _sMode + "'></iframe>";
            //$i("dialogFrame").src = _sUrl;
            setTimeout("dialogFrame.location.href='"+_sUrl+"';", 100);//为了避免一直加载
        //}
    }
    this.reset = function(callReturnFunc){$i('dialogCase') ? this.dispose(callReturnFunc) : function(){};}
    this.dispose = function(callReturnFunc){
        _ppyy_DialogIsShown = false;
        document.body.oncontextmenu = function () { return true; };
        document.body.onselectstart = function () { return true; };
        document.body.ondragstart = function () { return true; };
        document.body.onsource = function () { return true; };
        //document.body.style.overflow='';//恢复滚动条
        $i('dialogFrame') ? $i('dialogFrame').src = '' : function () { };
        $i('dialogCase').parentNode.removeChild($i('dialogCase'));
        $i('windowMask').parentNode.removeChild($i('windowMask'));
        _ppyy_WindowMask = null;
        if (callReturnFunc == true && gReturnFunc != null) {
            gReturnVal = window.dialogFrame.returnVal;
            window.setTimeout('gReturnFunc(gReturnVal);', 1);
        }
        else if (callReturnFunc != null) {
            eval(callReturnFunc);
        }
        if (_ppyy_HideSelects == true) {
            ShowSelectBoxes();
            _ppyy_HideSelects = false;
        }
        //$i('dialogBoxShadow').style.display = "none";
    }
    this.event = function(_sMsg, _sSubmit, _sCancel, _sClose){
        this.show();
        $i('dialogFunc').innerHTML = sButtonFunc;
        $i('dialogBoxClose').innerHTML = sClose;
        $i('dialogBodyBox') == null ? $i('dialogBody').innerHTML = sBody : function(){};
        $i('dialogMsg') ? $i('dialogMsg').innerHTML = _sMsg  : function(){};
        _sSubmit ? this.button('dialogSubmit', _sSubmit) | $i('dialogSubmit').focus() : $i('dialogSubmit').style.display = "none";
        _sCancel ? this.button('dialogCancel', _sCancel) : $i('dialogCancel').style.display = "none";
        _sClose ? this.button('dialogBoxClose', _sClose) : function(){};
    }
    this.set = function(_oAttr, _sVal){
        var oDialogDiv = $i('dialogBox');
        var oHeight = $i('dialogHeight');
        if(_sVal != ''){
            switch(_oAttr){
                case 'title':
                    $i('dialogBoxTitle').innerHTML = _sVal;
                    title = _sVal;
                    break;
                case 'width':
                    oDialogDiv.style.width = _sVal;
                    width = _sVal;
                    this.middle('dialogBox');
                    this.shadow();
                    this.middle('dialogBoxShadow');
                    _ppyy_OperatorPlus();
                    break;
                case 'height':
                    oHeight.style.height = _sVal;
                    height = _sVal;
                    this.middle('dialogBox');
                    this.shadow();
                    this.middle('dialogBoxShadow');
                    _ppyy_OperatorPlus();
                    break;
                case 'src':
                    if(parseInt(_sVal) > 0){
                        $i('dialogBoxFace') ? $i('dialogBoxFace').src = path + _sVal + '.png' : function(){};
                    }else{
                        $i('dialogBoxFace') ? $i('dialogBoxFace').src = _sVal : function(){};
                    }
                    src = _sVal;
                    break;
                case 'url':
                    this.open(_sVal);
                    break;
            }
        }
    }
    this.middle = function(_sId){    
        var theWidth;
        var theHeight;
        if (document.documentElement && document.documentElement.clientWidth) { 
            theWidth = document.documentElement.clientWidth+document.documentElement.scrollLeft*2;
            theHeight = document.documentElement.clientHeight+document.documentElement.scrollTop*2; 
        } else if (document.body) { 
            theWidth = document.body.clientWidth;
            theHeight = document.body.clientHeight; 
        }else if(window.innerWidth){
            theWidth = window.innerWidth;
            theHeight = window.innerHeight;
        }
        $i(_sId).style.display = '';
        $i(_sId).style.position = "absolute";
        $i(_sId).style.left = (theWidth / 2) - ($i(_sId).offsetWidth / 2)+"px";
        if(document.all||$i("user_page_top")){
            $i(_sId).style.top = (theHeight / 2 + document.body.scrollTop) - ($i(_sId).offsetHeight / 2)+"px";
        }else{
            var sClientHeight = parent ? parent.document.body.clientHeight : document.body.clientHeight;
            var sScrollTop = parent ? parent.document.body.scrollTop : document.body.scrollTop;
            var sTop = -80 + (sClientHeight / 2 + sScrollTop) - ($i(_sId).offsetHeight / 2);
            $i(_sId).style.top = (theHeight / 2 + document.body.scrollTop) - ($i(_sId).offsetHeight / 2)+"px";
        }
    }
    BtnOver=function(obj,path){obj.style.backgroundImage = "url("+path+"button2.gif)";}
    BtnOut=function(obj,path){obj.style.backgroundImage = "url("+path+"button1.gif)";}
    ShowSelectBoxes=function(){var x = document.getElementsByTagName("SELECT");for (i=0;x && i < x.length; i++){x[i].style.visibility = "visible";}}
    HideSelectBoxes=function(){var x = document.getElementsByTagName("SELECT");for (i=0;x && i < x.length; i++) {x[i].style.visibility = "hidden";}}

}


JumbotPay.alert = function (errstr, success, returnFunc) {
    var oDialog = new _ppyy_Dialog2('3', '', 460, 120, null, true);
    oDialog.init();
    oDialog.event(errstr, '');
    if (returnFunc == null)
        oDialog.button('dialogSubmit', '');
    else
        oDialog.button('dialogSubmit', returnFunc);
};

JumbotPay.confirm = function (errstr, returnSubmitFunc, returnCancelFunc) {
    var oDialog = new _ppyy_Dialog2('3', '', 460, 120, null, true);
    oDialog.init();
    oDialog.event(errstr, '');
    oDialog.button('dialogSubmit', returnSubmitFunc);
    if (returnCancelFunc == null)
        oDialog.button('dialogCancel', '');
    else
        oDialog.button('dialogCancel', returnCancelFunc);
};

////////////////////////////////////////////////////////////////////////////////////////////
//以下为弹出窗口的类
////////////////////////////////////////////////////////////////////////////////////////////
function _ppyy_Dialog2(styletype, title, width, height, iswhat, showCloseBox){
    //半透明边框宽度
    var shadowBorderBoth=0;
    var oWidth = width;
    var oHeight = height;
    if(oWidth==-1 || oWidth>_ppyy_GetViewportWidth()-15)
    {
        oWidth=_ppyy_GetViewportWidth()-15;
        shadowBorderBoth = 0;
    }
    if(oWidth<-1)
    {
        oWidth=_ppyy_GetViewportWidth()+oWidth;
        shadowBorderBoth = 0;
    }
    if(oHeight==-1 || oHeight>_ppyy_GetViewportHeight()-44)
    {
        oHeight=_ppyy_GetViewportHeight()-44;
        shadowBorderBoth = 0;
    }
    if(oHeight<-1)
    {
        oHeight=_ppyy_GetViewportHeight()+oHeight;
        shadowBorderBoth = 0;
    }
    var sTitle = "友情提示";
    if (iswhat == "0")
        sTitle = "错误提示";
    else if (iswhat == "1")
        sTitle = "成功提示";
    else
        if (title!='') sTitle = title;
    var src = "";
    var path = _ppyy_DialogUrl + styletype + "/";
    var gReturnFunc;
    var gReturnVal = null;
    var sButtonFunc = ' <input id="dialogCancel" class="dialogCancel' + styletype + '" type="button" value="取 消" onclick="new _ppyy_Dialog2().reset();" /><input id="dialogSubmit" class="dialogSubmit' + styletype + '" type="button" value="确 认" onclick="new _ppyy_Dialog2().reset();" />';
    var sClose = '';
    if (showCloseBox == null || showCloseBox == true)
        sClose = '<img alt="关闭" style="cursor:pointer;" id="dialogBoxClose" onclick="new _ppyy_Dialog2().reset();" src="' + path + 'dialogCloseOut.gif" border="0" onmouseover="this.src=\'' + path + 'dialogCloseOver.gif\';" onmouseout="this.src=\'' + path + 'dialogCloseOut.gif\';" align="absmiddle" />';
    var sSuccess = '';
    if (iswhat != null)
        sSuccess = '<td width="80" align="center" valign="middle"><img id="dialogBoxFace" class="dialogBoxFace' + styletype + '" src="' + path + iswhat + '.gif" valign="absmiddle" /></td>';
    else
        sSuccess = '<td width="80" align="center" valign="middle"><img id="dialogBoxFace" class="dialogBoxFace' + styletype + '" src="' + path + '0.gif" valign="absmiddle" /></td>';
    var sBody = '\
        <table id="dialogBodyBox" class="dialogBodyBox' + styletype + '" border="0" align="center" cellpadding="0" cellspacing="0" width="100%" height="100%" >\
            <tr>\
                <td width="10"></td><td id="dialogMsg" class="dialogMsg' + styletype + '"></td>\
                <td width="10"></td>\
            </tr>\
            <tr height="30"><td id="dialogFunc" class="dialogFunc' + styletype + '" colspan="4">' + sButtonFunc + '</td></tr>\
        </table>\
    ';
    var sBox = '\
        <div style="display:none;" id="dialogBox" class="dialogBox' + styletype + '">\
            <div id="dialogTitleDiv" class="dialogTitleDiv' + styletype + '" style="width:' + oWidth + 'px;">\
                <span id="dialogBoxTitle" class="dialogBoxTitle' + styletype + '">' + sTitle + '</span>\
                <span id="dialogBoxClose" class="dialogBoxClose' + styletype + '">' + sClose + '</span>\
            </div>\
            <div id="dialogHeight">\
                <div id="dialogBody" class="dialogBody' + styletype + '">' + sBody + '</div>\
            </div><div class="dialogBoxShadow"></div><div id="dialogBoxShadow"></div>\
        </div>\
    ';
    this.init = function(_showTitleBar){
        document.body.oncontextmenu=function(){return false;};
        document.body.onselectstart=function(){return false;};
        document.body.ondragstart=function(){return false;};
        document.body.onsource=function(){return false;};
	//document.body.style.overflow='hidden';//屏蔽滚动条
        $i('dialogFrame') ? $i('dialogFrame').src='' : function(){};
        $i('dialogCase') ? $i('dialogCase').parentNode.removeChild($i('dialogCase')) : function(){};
        $i('windowMask') ? $i('windowMask').parentNode.removeChild($i('windowMask')) : function(){};
        var oDiv = document.createElement('span');
        oDiv.id = "dialogCase";
        oDiv.innerHTML = sBox;
        document.body.appendChild(oDiv);
        var oMask = document.createElement('div');
        oMask.id = 'windowMask';
        document.body.appendChild(oMask);
        _ppyy_WindowMask = $i("windowMask");
        _ppyy_WindowMask.style.display="block";
        var brsVersion = parseInt(window.navigator.appVersion.charAt(0), 10);
        if (brsVersion <= 6 && window.navigator.userAgent.indexOf("MSIE") > -1) {
            _ppyy_HideSelects = true;
        }
        if (_ppyy_HideSelects == true) {
            HideSelectBoxes();
        }
        if (_showTitleBar == true || _showTitleBar == null)
            $i("dialogTitleDiv").style.display = "block";
        else
            $i("dialogTitleDiv").style.display = "none";
        _ppyy_OperatorPlus();
    }
    //this.show = function(){$i('dialogBox') ? function(){} : this.init();_ppyy_DialogIsShown=true;this.middle('dialogBox');}
    this.show = function(){$i('dialogBox') ? function(){} : this.init();_ppyy_DialogIsShown=true;this.middle('dialogBox');this.shadow();this.middle('dialogBoxShadow');_ppyy_OperatorPlus();}
    this.html = function(_sHtml){
        this.show();
        $i('dialogBody').innerHTML = _sHtml;
    }
    this.button = function(_sId, _sFuc){
        if($i(_sId)){
            $i(_sId).style.display = '';
            if($i(_sId).addEventListener){
                if($i(_sId).act){$i(_sId).removeEventListener('click', function(){eval($i(_sId).act);}, false);}
                $i(_sId).act = _sFuc;
                $i(_sId).addEventListener('click', function(){eval(_sFuc);try{this.reset();}catch(e){}}, false);
            }else{
                if($i(_sId).act){$i(_sId).detachEvent('onclick', function(){eval($i(_sId).act);});}
                $i(_sId).act = _sFuc;
                $i(_sId).attachEvent('onclick', function(){eval(_sFuc);});
            }
        }
    }
    this.shadow = function(){
        if(shadowBorderBoth>0){
            var oShadow = $i('dialogBoxShadow');
            var oDialogDiv = $i('dialogBox');
//            oShadow.style.position = "absolute";
//            oShadow.style.background = "#000";
//            oShadow.style.display = "";
//            oShadow.style.opacity = "0.25";
//            oShadow.style.filter = "alpha(opacity=25)";
//            oShadow.style.width = (oDialogDiv.offsetWidth + shadowBorderBoth)+"px";
//            oShadow.style.height = (oDialogDiv.offsetHeight + shadowBorderBoth)+"px";
        }
    }
    this.open = function(_sUrl, _returnFunc, _sMode){
        this.show();
        gReturnFunc = _returnFunc;
        //if(!_sMode || _sMode == "no" || _sMode == "yes"){
            $i("dialogBody").innerHTML = "<iframe id='dialogFrame' name='dialogFrame' src='" + site.Dir + "_data/loading.html' width='" + oWidth + "' height='" + oHeight + "' frameborder='no' border='0' marginwidth='0' marginheight='0' scrolling='" + _sMode + "'></iframe>";
            //$i("dialogFrame").src = _sUrl;
            setTimeout("dialogFrame.location.href='"+_sUrl+"';", 100);//为了避免一直加载
        //}
    }
    this.reset = function(callReturnFunc){$i('dialogCase') ? this.dispose(callReturnFunc) : function(){};}
    this.dispose = function(callReturnFunc){
        _ppyy_DialogIsShown = false;
        document.body.oncontextmenu = function () { return true; };
        document.body.onselectstart = function () { return true; };
        document.body.ondragstart = function () { return true; };
        document.body.onsource = function () { return true; };
        //document.body.style.overflow='';//恢复滚动条
        $i('dialogFrame') ? $i('dialogFrame').src = '' : function () { };
        $i('dialogCase').parentNode.removeChild($i('dialogCase'));
        $i('windowMask').parentNode.removeChild($i('windowMask'));
        _ppyy_WindowMask = null;
        if (callReturnFunc == true && gReturnFunc != null) {
            gReturnVal = window.dialogFrame.returnVal;
            window.setTimeout('gReturnFunc(gReturnVal);', 1);
        }
        else if (callReturnFunc != null) {
            eval(callReturnFunc);
        }
        if (_ppyy_HideSelects == true) {
            ShowSelectBoxes();
            _ppyy_HideSelects = false;
        }
        //$i('dialogBoxShadow').style.display = "none";
    }
    this.event = function(_sMsg, _sSubmit, _sCancel, _sClose){
        this.show();
        $i('dialogFunc').innerHTML = sButtonFunc;
        $i('dialogBoxClose').innerHTML = sClose;
        $i('dialogBodyBox') == null ? $i('dialogBody').innerHTML = sBody : function(){};
        $i('dialogMsg') ? $i('dialogMsg').innerHTML = _sMsg  : function(){};
        _sSubmit ? this.button('dialogSubmit', _sSubmit) | $i('dialogSubmit').focus() : $i('dialogSubmit').style.display = "none";
        _sCancel ? this.button('dialogCancel', _sCancel) : $i('dialogCancel').style.display = "none";
        _sClose ? this.button('dialogBoxClose', _sClose) : function(){};
    }
    this.set = function(_oAttr, _sVal){
        var oDialogDiv = $i('dialogBox');
        var oHeight = $i('dialogHeight');
        if(_sVal != ''){
            switch(_oAttr){
                case 'title':
                    $i('dialogBoxTitle').innerHTML = _sVal;
                    title = _sVal;
                    break;
                case 'width':
                    oDialogDiv.style.width = _sVal;
                    width = _sVal;
                    this.middle('dialogBox');
                    this.shadow();
                    this.middle('dialogBoxShadow');
                    _ppyy_OperatorPlus();
                    break;
                case 'height':
                    oHeight.style.height = _sVal;
                    height = _sVal;
                    this.middle('dialogBox');
                    this.shadow();
                    this.middle('dialogBoxShadow');
                    _ppyy_OperatorPlus();
                    break;
                case 'src':
                    if(parseInt(_sVal) > 0){
                        $i('dialogBoxFace') ? $i('dialogBoxFace').src = path + _sVal + '.png' : function(){};
                    }else{
                        $i('dialogBoxFace') ? $i('dialogBoxFace').src = _sVal : function(){};
                    }
                    src = _sVal;
                    break;
                case 'url':
                    this.open(_sVal);
                    break;
            }
        }
    }
    this.middle = function(_sId){    
        var theWidth;
        var theHeight;
        if (document.documentElement && document.documentElement.clientWidth) { 
            theWidth = document.documentElement.clientWidth+document.documentElement.scrollLeft*2;
            theHeight = document.documentElement.clientHeight+document.documentElement.scrollTop*2; 
        } else if (document.body) { 
            theWidth = document.body.clientWidth;
            theHeight = document.body.clientHeight; 
        }else if(window.innerWidth){
            theWidth = window.innerWidth;
            theHeight = window.innerHeight;
        }
        $i(_sId).style.display = '';
        $i(_sId).style.position = "absolute";
        $i(_sId).style.left = (theWidth / 2) - ($i(_sId).offsetWidth / 2)+"px";
        if(document.all||$i("user_page_top")){
            $i(_sId).style.top = (theHeight / 2 + document.body.scrollTop) - ($i(_sId).offsetHeight / 2)+"px";
        }else{
            var sClientHeight = parent ? parent.document.body.clientHeight : document.body.clientHeight;
            var sScrollTop = parent ? parent.document.body.scrollTop : document.body.scrollTop;
            var sTop = -80 + (sClientHeight / 2 + sScrollTop) - ($i(_sId).offsetHeight / 2);
            $i(_sId).style.top = (theHeight / 2 + document.body.scrollTop) - ($i(_sId).offsetHeight / 2)+"px";
        }
    }
    BtnOver=function(obj,path){obj.style.backgroundImage = "url("+path+"button2.gif)";}
    BtnOut=function(obj,path){obj.style.backgroundImage = "url("+path+"button1.gif)";}
    ShowSelectBoxes=function(){var x = document.getElementsByTagName("SELECT");for (i=0;x && i < x.length; i++){x[i].style.visibility = "visible";}}
    HideSelectBoxes=function(){var x = document.getElementsByTagName("SELECT");for (i=0;x && i < x.length; i++) {x[i].style.visibility = "hidden";}}

}


///////////////////////////////////////////////////////////////////////////
function _ppyy_OperatorPlus() {
    if (_ppyy_DialogIsShown == true) {
        var oDialogDiv = $i("dialogBox");
        var oShadow = $i("dialogBoxShadow");
        var oWidth = oDialogDiv.offsetWidth;
        var oHeight = oDialogDiv.offsetHeight;
        var theBody = document.getElementsByTagName("BODY")[0];
        var scTop = parseInt(_ppyy_GetScrollTop(),10);
        var scLeft = parseInt(theBody.scrollLeft,10);
        var fullHeight = _ppyy_GetViewportHeight();
        var fullWidth = _ppyy_GetViewportWidth();
        oDialogDiv.style.top = (scTop + ((fullHeight - oHeight) / 2)) + "px";
        oDialogDiv.style.left = (scLeft + ((fullWidth - oWidth) / 2)) + "px";
        oShadow.style.top = (scTop + ((fullHeight - oShadow.offsetHeight) / 2)) + "px";
        oShadow.style.left = (scLeft + ((fullWidth - oShadow.offsetWidth) / 2)) + "px";
        if (_ppyy_WindowMask != null) {
            var popHeight = theBody.scrollHeight;
            var popWidth = theBody.scrollWidth;
            if (fullHeight > theBody.scrollHeight) popHeight = fullHeight;
            if (fullWidth > theBody.scrollWidth) popWidth = fullWidth;
            //if (fullHeight > theBody.scrollHeight) popWidth = popWidth -17;
            _ppyy_WindowMask.style.height = popHeight + "px";
            _ppyy_WindowMask.style.width = popWidth + "px";

            //_ppyy_WindowMask.style.height = $(document).height(); 
            //_ppyy_WindowMask.style.width = $(document).width(); 
        }
    }
}
function _ppyy_GetViewportHeight() {
    if (window.innerHeight!=window.undefined)//FF
    {
        return window.innerHeight;
    }
    if (document.compatMode=='CSS1Compat')//IE
    {
        return document.documentElement.clientHeight;
    }
    if (document.body)//other
    {
        return document.body.clientHeight; 
    }
    return window.undefined; 
}
function _ppyy_GetViewportWidth() {
    var offset = 17;
    var width = null;
    if (window.innerWidth!=window.undefined)//FF
    {
        //return window.innerWidth-offset; 
        return window.innerWidth; 
    }
    if (document.compatMode=='CSS1Compat')//IE
    {
        return document.documentElement.clientWidth; 
    }
    if (document.body)//other
    {
        return document.body.clientWidth; 
    }
    return window.undefined; 
}
function _ppyy_GetScrollTop() {
    if (self.pageYOffset){return self.pageYOffset;}
    else if (document.documentElement && document.documentElement.scrollTop){return document.documentElement.scrollTop;}
    else if (document.body){return document.body.scrollTop;}
}
function _ppyy_GetScrollLeft() {
    if (self.pageXOffset){return self.pageXOffset;}
    else if (document.documentElement && document.documentElement.scrollLeft){return document.documentElement.scrollLeft;}
    else if (document.body){return document.body.scrollLeft;}
}
function _ppyy_SetDialogTitle(){
    //var _title = window.document.title + ' - 按Esc键可关闭窗口';
    var _title = window.document.title;
    try {
        $i('dialogBoxTitle').innerHTML = _title;
    }
    catch(e) {
        try {
            parent.$i('dialogBoxTitle').innerHTML = _title;
        }
        catch(e) {
        }    
    }
    $(document).keyup(function(e) {
        var key = window.event?e.keyCode:e.which;
        if (key == 27) {
            parent.JumbotPay.Popup.hide(); 
        }
    }); 
}
function _ppyy_SetDialogSize(w,h){
    try {
        if(w>0) $i('dialogBox').style.width = w;
        if(h>0) $i('dialogHeight').style.height = h;
        _ppyy_OperatorPlus();
    }
    catch(e) {
        try {
            if(w>0) parent.$i('dialogBox').style.width = w;
            if(h>0) parent.$i('dialogHeight').style.height = h;
            parent._ppyy_OperatorPlus();
        }
        catch(e) {
        }    
    }    
}
/* Url编码 */ 
function _ppyy_UrlEncode(unzipStr){ 
    var zipstr=""; 
    var strSpecial="!\"#$%&'()*+,/:;<=>?[]^`{|}~%"; 
    var tt= ""; 
    for(var i=0;i<unzipStr.length;i++){ 
        var chr = unzipStr.charAt(i); 
        var c=_ppyy_StringToAscii(chr); 
        tt += chr+":"+c+"n"; 
        if(parseInt("0x"+c) > 0x7f){ 
            zipstr+=encodeURI(unzipStr.substr(i,1)); 
        }else{ 
        if(chr==" ") 
            zipstr+="+"; 
        else if(strSpecial.indexOf(chr)!=-1) 
            zipstr+="%"+c.toString(16); 
        else 
            zipstr+=chr; 
        } 
    } 
    return zipstr; 
} 
/* Url解码 */ 
function _ppyy_UrlDecode(zipStr){ 
    var uzipStr=""; 
    for(var i=0;i<zipStr.length;i++){ 
        var chr = zipStr.charAt(i); 
        if(chr == "+"){ 
            uzipStr+=" "; 
        }else if(chr=="%"){ 
            var asc = zipStr.substring(i+1,i+3); 
            if(parseInt("0x"+asc)>0x7f){ 
                uzipStr+=decodeURI("%"+asc.toString()+zipStr.substring(i+3,i+9).toString()); 
                i+=8; 
            }else{ 
                uzipStr+=_ppyy_AsciiToString(parseInt("0x"+asc)); 
                i+=2; 
            } 
        }else{ 
            uzipStr+= chr; 
        } 
    } 
    return uzipStr; 
} 
var _ppyy_StringToAscii = function(str){return str.charCodeAt(0).toString(16);}
var _ppyy_AsciiToString = function(asccode){return String.fromCharCode(asccode);}

////////////////////////////////////////////////////////////////////////////////////////////
function _ppyy_SetUrlRefresh(url)
{
    if(url.indexOf("?") > 0)
            return url+"&clienttime="+Math.random();   
    else
            return url+"?clienttime="+Math.random();   
}
/*刷新验证码*/
var isReturn = false;
function _ppyy_GetRefreshCode(obj, h) {
    isReturn = false;
    if (!h) {
        $i(obj).src = _ppyy_SetUrlRefresh(site.Dir + "plus/getcode.aspx");
    }
    else {
        $i(obj).src = _ppyy_SetUrlRefresh(site.Dir + "plus/getcode.aspx?h=" + h);
    }
    isReturn = true;
    try {
        var timer1 = setInterval(function () {
            if (isReturn == true) {
                clearInterval(timer1);
                ajaxGetValidateCode();
            }
        }, 500);

    }
    catch (e) { }
}
/*当前网站访问者信息*/
var user = new Object();
if(JumbotPay.Cookie.get(site.CookiePrev + "user", "id") == ""){//游客
    user.id = "0";
    user.name = "guest";
    user.userkey = "666666";
    user.groupid = "0";
    user.adminid = "0";
    user.groupname = "游客组";
    user.cookies = "88888888";
}
else{
    user.id = JumbotPay.Cookie.get(site.CookiePrev + "user", "id");
    user.name = JumbotPay.Cookie.get(site.CookiePrev + "user", "name");
    user.userkey = JumbotPay.Cookie.get(site.CookiePrev + "user", "userkey");
    user.groupid = JumbotPay.Cookie.get(site.CookiePrev + "user", "groupid");
    user.adminid = JumbotPay.Cookie.get(site.CookiePrev + "user", "adminid");
    user.groupname = JumbotPay.Cookie.get(site.CookiePrev + "user", "groupname");
    user.cookies = JumbotPay.Cookie.get(site.CookiePrev + "user", "cookies");
}

var _ppyy_StuHover = function() {
    var cssRule;
    var newSelector;
    for (var i = 0; i < document.styleSheets.length; i++)
        for (var x = 0; x < document.styleSheets[i].rules.length ; x++)
            {
            cssRule = document.styleSheets[i].rules[x];
            if (cssRule.selectorText.indexOf("LI:hover") != -1)
            {
                 newSelector = cssRule.selectorText.replace(/LI:hover/gi, "LI.iehover");
                document.styleSheets[i].addRule(newSelector , cssRule.style.cssText);
            }
        }
    var topnavbar = $i("topnavbar");
    if(topnavbar != null)
    {
        var getElm = topnavbar.getElementsByTagName("LI");
        for (var i=0; i<getElm.length; i++) {
            getElm[i].onmouseover=function() {
                this.className+=" iehover";
            }
            getElm[i].onmouseout=function() {
                this.className=this.className.replace(new RegExp(" iehover\\b"), "");
            }
        }
    }
}
/*HTML标签小写*/
function HTML2LowerCase(html)
{
    return html.replace(/(<\/?)([a-z\d\:]+)((\s+.+?)?>)/gi,function(s,a,b,c){return a+b.toLowerCase()+c;});
}
/*获取指定字符串的长度*/
function GetLength(id)
{
    var srcjo = $("#"+id);
    sType = srcjo.get(0).type;
    var len = 0;
    switch(sType)
    {
        case "text":
        case "hidden":
        case "password":
        case "textarea":
        case "file":
            var val = srcjo.val();
            for (var i = 0; i < val.length; i++) 
            {
                if (val.charCodeAt(i) >= 0x4e00 && val.charCodeAt(i) <= 0x9fa5){ 
                    len += 2;
                }else {
                    len++;
                }
            }
            break;
        case "checkbox":
        case "radio": 
            len = $("input[type='"+sType+"'][name='"+srcjo.attr("name")+"']:checked").length;
            break;
        case "select-one":
            len = srcjo.get(0).options ? srcjo.get(0).options.selectedIndex : -1;
            break;
        case "select-more":
            break;
    }
    return len;
}
function InsertUnit(text, obj) {
    if(!obj) {
        obj = 'jstemplate';
    }
    var o = $i(obj);
    o.focus();
    if(!JumbotPay.isUndefined(o.selectionStart)) {
        var opn = o.selectionStart + 0;
        o.value = o.value.substr(0, o.selectionStart) + text + o.value.substr(o.selectionEnd);
    } else if(document.selection && document.selection.createRange) {
        var sel = document.selection.createRange();
        sel.text = text.replace(/\r?\n/g, '\r\n');
        //sel.moveStart('character', -strlen(text));
    } else {
        o.value += text;
    }
}
function JoinSelect(selectName)
{
    var selectIDs="";
    $("input[name='" + selectName + "']").each(function(){
           if($(this).is(":checked")){
            if(selectIDs=="")
                    selectIDs = $(this).attr("value");
            else
                selectIDs += ","+$(this).attr("value");
           }
    })
    return selectIDs;
}

function ajaxAddMessage(userid,username){
    window.open(site.Dir + 'user/message_send.aspx?touserid='+userid+'&tousername='+encodeURIComponent(username));
}
function ajaxAddFriend(userid){
    $.ajax({
        type:        "post",
        dataType:    "json",
        data:        "id="+userid+"&clienttime="+Math.random(),
        url:        site.Dir + "user/ajax.aspx?oper=ajaxAddFriend",
        error:        function(XmlHttpRequest,textStatus, errorThrown){if(XmlHttpRequest.responseText!=""){alert(XmlHttpRequest.responseText);}},
        success:    function(d){
            switch (d.result)
            {
            case '0':
                alert(d.returnval);
                break;
            case '1':
                alert(d.returnval);
                break;
            }
        }
    });
}




/*显示个人主页*/
function ShowUserPage(userid){
    //alert(userid);
}


/*========================================================================================*/
function UrlSearch(){ //重复时只取最后一个
    var name,value; 
    var str=window.location.href; //取得整个地址栏
    var num=str.indexOf("?") 
    str=str.substr(num+1); //取得所有参数
    var arr=str.split("&"); //各个参数放到数组里
    for(var i=0;i < arr.length;i++){ 
        num=arr[i].indexOf("="); 
        if(num>0){ 
            name=arr[i].substring(0,num);
            value=arr[i].substr(num+1);
            this[name]=value;
        } 
    }
    this["getall"]=str;
}
var RQ=new UrlSearch(); //实例化
function formatStr(s){
    if(typeof(s) == "string")
        return s;
    else
        return "";
}
function joinValue(parameter){
    eval("var temp=RQ."+parameter);
    if((typeof(temp) == "string") && (typeof(temp) != null))
    {
        return "&"+parameter+"="+temp.replace(/(^\s*)|(\s*$)/g, "");
    }
    else
        return "";
}
function q(pname){
    var query = location.search.substring(1);
    var qq = "";
    params = query.split("&");
    if(params.length>0){
        for(var n in params){
            var pairs = params[n].split("=");
            if(pairs[0]==pname){
                qq = pairs[1];
                break;
            }
        }
    }
    return qq;
}
function anchor(){
    var str=window.location.href; //取得整个地址栏
    var num=str.indexOf("#") 
    str=str.substr(num+1);
    return str;
}
/*获取当前页页码*/
function thispage(){
    var r = /^[1-9][0-9]*$/;
    if(q('page')=='')return 1;
    if(r.test(q('page')))
        return parseInt(q('page'));
    else
        return 1;
}
/*全选*/
function CheckAll(form)
{
    var f;
    if(form==null)
        f = document.getElementsByTagName('FORM')[0];
    else
        f = $i(form);
    for (var i=0;i<f.elements.length;i++)
    {
        var e = f.elements[i];
        if (e.name != 'chkall' && e.type == "checkbox")
            e.checked = $i("chkall").checked;
    }
}
/*全不选*/
function CheckNo(form)
{
    var f;
    if(form==null)
        f = document.getElementsByTagName('FORM')[0];
    else
        f = $i(form);
    for (var i=0;i<f.elements.length;i++)
    {
        var e = f.elements[i];
        if (e.type == "checkbox")
            e.checked = false;
    }
}
function WinFullOpen(url){
    var newwin=window.open(url,"","scrollbars");
    if(document.all){
        newwin.moveTo(0,0);
        newwin.resizeTo(screen.width,screen.height);
    }
}
function WindowOpen(url,iWidth,iHeight,name)
{
    if(name==null) name='';
    var iTop = (window.screen.availHeight-30-iHeight)/2;
    var iLeft = (window.screen.availWidth-10-iWidth)/2;
    window.open(url,name,'height='+iHeight+',,innerHeight='+iHeight+',width='+iWidth+',innerWidth='+iWidth+',top='+iTop+',left='+iLeft+',toolbar=no,menubar=no,scrollbars=auto,resizeable=no,location=no,status=no');
}
/*字符串格式化*/
String.prototype.Trim = function(){return this.replace(/(^\s*)|(\s*$)/g, "");}
String.prototype.LTrim = function(){return this.replace(/(^\s*)/g, "");}
String.prototype.RTrim = function(){return this.replace(/(\s*$)/g, "");}

/*日期格式化(2009-06-30+++)*/
function formatDate(strDate, format,leftsecond) {
    if (strDate == "")
        return "";
    if (typeof (leftsecond) != undefined) {
        var d_days = parseInt(leftsecond / 86400);
        var d_hours = parseInt(leftsecond / 3600);
        var d_minutes = parseInt(leftsecond / 60);
        if (d_days > 0 && d_days < 4) 
            return d_days + "天前";
        else if (d_days <= 0 && d_hours > 0)
            return d_hours + "小时前";
        else if (d_hours <= 0 && d_minutes > 0)
            return d_minutes + "分钟前";
        else if (leftsecond < 60)
            return leftsecond + "秒前";
        else
            return parseDate(strDate).format(format);
    }
    return parseDate(strDate).format(format);
} 
Date.prototype.format = function(format)
{
    if(format == null) format = "yyyy-MM-dd HH:mm:ss";
    var o = 
    {
        "M+" : this.getMonth()+1, //month
        "d+" : this.getDate(),    //day
        "H+" : this.getHours(),   //24小时制hour
        "h+" : (this.getHours()>12)? (this.getHours()-12) : this.getHours(),   //12小时制hour
        "m+" : this.getMinutes(), //minute
        "s+" : this.getSeconds(), //second
        "q+" : Math.floor((this.getMonth()+3)/3), //quarter
        "S" : this.getMilliseconds() //millisecond
    }
        
    if(/(y+)/.test(format)) 
        format=format.replace(RegExp.$1,(this.getFullYear()+"").substr(4 - RegExp.$1.length));
    for(var k in o)
        if(new RegExp("("+ k +")").test(format))
            format = format.replace(RegExp.$1,RegExp.$1.length==1 ? o[k] : ("00"+ o[k]).substr((""+ o[k]).length));
    return format;
}
function parseDate(str){
    if(typeof str == 'string'){   
        var results = str.match(/^ *(\d{4})-(\d{1,2})-(\d{1,2}) *$/);   
        if(results && results.length>3)   
            return new Date(parseInt(results[1],10),parseInt(results[2],10) -1,parseInt(results[3],10));    
        results = str.match(/^ *(\d{4})-(\d{1,2})-(\d{1,2}) +(\d{1,2}):(\d{1,2}):(\d{1,2}) *$/);   
        if(results && results.length>6)   
            return new Date(parseInt(results[1],10),parseInt(results[2],10) -1,parseInt(results[3],10),parseInt(results[4],10),parseInt(results[5],10),parseInt(results[6],10));    
        results = str.match(/^ *(\d{4})-(\d{1,2})-(\d{1,2}) +(\d{1,2}):(\d{1,2}):(\d{1,2})\.(\d{1,9}) *$/);   
        if(results && results.length>7)   
            return new Date(parseInt(results[1],10),parseInt(results[2],10) -1,parseInt(results[3],10),parseInt(results[4],10),parseInt(results[5],10),parseInt(results[6],10),parseInt(results[7],10));    
    }   
    return null;   
}
var DateStringFromNow = function(lasttime, thistime){
	//return thistime;
	var d_minutes, d_hours, d_days;       
	var timeNow = parseInt(parseDate(thistime).getTime()/1000);       
	var d;       
	d = timeNow - parseInt(parseDate(lasttime).getTime()/1000);       
	d_days = parseInt(d/86400);       
	d_hours = parseInt(d/3600);       
	d_minutes = parseInt(d/60);       
	if(d_days>0 && d_days<4){
		return d_days+"天前";
	}else if(d_days<=0 && d_hours>0){
		return d_hours+"小时前";
	}else if(d_hours<=0 && d_minutes>0){
		return d_minutes+"分钟前";
	}else{
		return lasttime;
	}
}

/**
 * 将数值四舍五入(保留2位小数)后格式化成金额形式
 *
 * @param num 数值(Number或者String)
 * @return 金额格式的字符串,如'1,234,567.45'
 * @type String
 */
function formatCurrency(num) {
    num = num.toString().replace(/\$|\,/g,'');
    if(isNaN(num))
    num = "0";
    sign = (num == (num = Math.abs(num)));
    num = Math.floor(num*100+0.50000000001);
    cents = num%100;
    num = Math.floor(num/100).toString();
    if(cents<10)
        cents = "0" + cents;
    for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
        num = num.substring(0,num.length-(4*i+3))+','+ num.substring(num.length-(4*i+3));
    return (((sign)?'':'-') + num + '.' + cents);
}
function formatFloat(src,pos){     
	return Math.round(src*Math.pow(10, pos))/Math.pow(10, pos);
}
function formatSimilarity(_similarity,useSpan) {
    var _str;
    if (_similarity < 0.1)
        _str = useSpan ? '<span style="color:green;">' + formatFloat(_similarity * 100, 1) + '%</span>' : formatFloat(_similarity * 100, 1) + '%';
    else if (_similarity < 0.15)
        _str = useSpan ? '<span style="color:#FF8000;">' + formatFloat(_similarity * 100, 1) + '%</span>' : formatFloat(_similarity * 100, 1) + '%';
    else
        _str = useSpan ? '<span style="color:red;">' + formatFloat(_similarity * 100, 1) + '%</span>' : formatFloat(_similarity * 100, 1) + '%';
    return _str;
}
/*预览HTML代码*/
function PreviewHTML(txt)
{
    var win = window.open("", "win");
    win.document.open("text/html", "replace"); 
    win.document.write(txt); 
    win.document.close();
}
function formatIsPass(ispass) {
    if(ispass == "-1")return "已删";
    return ispass == "1" ? "已审" : "未审";
}
function formatIsImg(isimg) {
    return isimg == "1" ? "有缩略图" : "无缩略图";
}
function formatIsTop(istop) {
    return istop == "1" ? "推荐" : "不推荐";
}
function formatIsFocus(isfocus) {
    return isfocus == "1" ? "焦点" : "非焦点";
}
function formatIsHead(ishead) {
    return ishead == "1" ? "置顶" : "非置顶";
}
function formatIsRunning(isrunning) {
	return isrunning == "1" ? "采集中" : "等待中";
}
/*格式化列表*/
function FormatListValue(id)
{
    var _val = $('#'+id).val();
    if(_val=='') return;
    _val =_val.replace(/[?]/g,"");
    _val =_val.replace(/[。，、.；;]/g,",");
    _val =_val.replace(/ /g,",");
    _val =_val.replace(/[,]+/g,",");
    $('#'+id).val(_val);
}

/*递归显示栏目li*/
function set_class_display(prev, classcode){
    $('#'+prev+classcode).show();
    $('.p'+classcode).show();//显示子类    
    if(classcode.length>4)//显示父级
        set_class_display(prev, classcode.substr(0,classcode.length-4));
}
/*{begin第三方接口*/
function _ppyy_GetOAuthBar(){
    var _oauthbar="";
    var _oauths = ___JSON_OAuths.table;
    for(var i=0;i<_oauths.length;i++){
        if(_oauths[i].enabled==1)
            _oauthbar += '<li><a href="'+site.Dir+'app/'+_oauths[i].code+'/oauth.aspx?type=login&mode=thirdlogin"><img src="'+site.Dir+'statics/passport/'+_oauths[i].code+'16.gif" title="使用'+_oauths[i].title+'登录"  alt="使用'+_oauths[i].title+'登录" /></a></li>';
    }
    return _oauthbar;
}
//发送微博
function _ppyy_SendWeiboUrl(msg, url, oauth){
	if(url.substr(0,7)!='http://' && url.substr(0,8)!='https://')
		url=site.Url+url;
	return site.Dir+'app/'+oauth+'/weibo.aspx?msg='+encodeURIComponent(msg+' '+url);
}
//绑定链接
function _ppyy_SetOAuthBar(){
    var _oauths = ___JSON_OAuths.table;
    var _enablednum=0;
    for(var i=0;i<_oauths.length;i++){
        if(_oauths[i].enabled==1){
            _enablednum++;
            $('#oauth_'+_oauths[i].code).attr("title","使用"+_oauths[i].title+"登录").attr("href",site.Dir+"app/"+_oauths[i].code+"/oauth.aspx?type=login&mode=thirdlogin");
        }
        else
            $('#oauth_'+_oauths[i].code).hide();
    }
    if(_enablednum>0) $('.oauth-navbar').show();
}
/*end}第三方接口*/

// 显示当前日期，时间
function setCurrentDateTime(o){
    var d = new Date();
    var da = d.getDate();
    var mo = d.getMonth() + 1;
    var y = d.getFullYear();
    var h = d.getHours();
    if(h<10){h='0'+h}
    var m = d.getMinutes();
    if(m<10){m='0'+m}
    var s = d.getSeconds();
    if(s<10){s='0'+s}
    var week = ['天','一','二','三','四','五','六'];
    if(typeof(o) != 'object'){o=$i(o)}
    o.innerHTML = y+'年'+mo+'月'+da+'日 星期'+week[d.getDay()]+'<br />'+h+':'+m+':'+s;
    window.setTimeout(function(){setCurrentDateTime(o)}, 1000);    
}
function CheckSearchData()
{
    var type = $("#search_channeltype").val();
    if($("#search_keywords").val()=="")
    {
        alert("请输入关键字");
        return;
    }
    window.open(site.Dir + 'search/default.aspx?type='+type+'&k='+encodeURIComponent($("#search_keywords").val()));
}
function BindModuleRadio(spanId,selecdType)
{
    var data = ___JSON_Modules;
    var html = "";
    html += "<span><input id=\"RaChannelType_all\" type=\"radio\" name=\"type\" value=\"all\"";
    if(selecdType == "all")
        html += " checked=\"checked\"";
    html += " /><label for=\"RaChannelType_all\">全部</label></span>";
    for (i=0;i<data.table.length;i++) {
        html += "<span><input id=\"RaChannelType_" + data.table[i].type + "\" type=\"radio\" name=\"type\" value=\"" + data.table[i].type + "\"";
        if(data.table[i].type == selecdType)
            html += " checked=\"checked\"";
        html += " /><label for=\"RaChannelType_" + data.table[i].type + "\">" + data.table[i].title + "</label></span>";
    }

    $("#"+spanId).html(html);
}
var ___JSON_Modes = {
    recordcount: 2, 
    table: [
        {no: 0, title: '普通检索'},
        {no: 1, title: '智能检索'}
    ]
}
function BindModeRadio(spanId,selecdMode)
{
    var data = ___JSON_Modes;
    var html = "";
    for (i=0;i<data.table.length;i++) {
        html += "<span style=\"padding-right:6px;\"><input id=\"RaSearchMode_" + data.table[i].no + "\" type=\"radio\" name=\"mode\" value=\"" + data.table[i].no + "\"";
        if(data.table[i].no == selecdMode)
            html += " checked=\"checked\"";
        html += " /><label for=\"RaSearchMode_" + data.table[i].no + "\">&nbsp;" + data.table[i].title + "</label></span>";
    }
    $("#"+spanId).html(html);
}


function addFavorite(ccid,cType,id)
{
    $.ajax({
        type:        "get",
        dataType:    "json",
        data:        "id="+id+"&ccid="+ccid+"&cType="+cType+"&clienttime="+Math.random(),
        url: _ppyy_Host() + site.Dir + "ajax/common.aspx?oper=ajaxAddFavorite",
        error:        function(XmlHttpRequest,textStatus, errorThrown){if(XmlHttpRequest.responseText!=""){alert(XmlHttpRequest.responseText);}},
		success:	function(d){
			switch (d.result)
			{
			case '0':
				alert(d.returnval);
				break;
			case '1':
				alert(d.returnval);
				break;
			}
		}
    });
}

function replaceContentTags(taglist,bodyid)
{
    try{
        var elms1 = $("#"+bodyid+" a");
        for (i = 0; i < elms1.length; i++){elms1[i].title="";}
        var elms2 = $("#"+bodyid+" img");
        for (i = 0; i < elms2.length; i++){elms2[i].alt="";}
            if (taglist.length == 0) return;
            var keys = taglist.split(",");
            var element = $i(bodyid);
            for (var i = 0; i < keys.length; i++) {
                    highlightWord(element, keys[i], site.Dir + 'search/default.aspx?k=');
            }
    }
    catch(e){}

}
function highlightWord(node, word, linkurl) {
    // Iterate into this nodes childNodes 
    if (node.hasChildNodes) {
        var hi_cn;
        for (hi_cn = 0; hi_cn < node.childNodes.length; hi_cn++) {
            highlightWord(node.childNodes[hi_cn], word, linkurl);
        }
    }
    // And do this node itself 
    if (node.nodeType == 3) { // text node 
        tempNodeVal = node.nodeValue.toLowerCase();
        tempWordVal = word.toLowerCase();
        if (tempNodeVal.indexOf(tempWordVal) > -1) {
            pn = node.parentNode;
            if (pn.className != "highlight") {
                nv = node.nodeValue;
                ni = tempNodeVal.indexOf(tempWordVal);
                before = document.createTextNode(nv.substr(0, ni));
                docWordVal = nv.substr(ni, word.length);
                after = document.createTextNode(nv.substr(ni + word.length));
                hiwordtext = document.createTextNode(docWordVal);
                hiword = document.createElement("A");
                hiword.className = "highlight";
                if (linkurl)
                    hiword.href = linkurl + encodeURIComponent(tempWordVal);
                hiword.appendChild(hiwordtext);
                pn.insertBefore(before, node);
                pn.insertBefore(hiword, node);
                pn.insertBefore(after, node);
                pn.removeChild(node);
            }
        }
    }

}
function SearchHighlight(idVal, keyword) {
    var pucl = document.getElementById(idVal);
    if ("" == keyword) return;
    var temp = pucl.innerHTML;
    var htmlReg = new RegExp("\<.*?\>", "i");
    var arrA = new Array();
    //替换HTML标签 
    for (var i = 0; true; i++) {
        var m = htmlReg.exec(temp);
        if (m) {
            arrA[i] = m;
        }
        else {
            break;
        }
        temp = temp.replace(m, "{[(" + i + ")]}");
    }
    words = unescape(keyword.replace(/\+/g, ' ')).split(/\s+/);
    //替换关键字 
    for (w = 0; w < words.length; w++) {
        var r = new RegExp("(" + words[w].replace(/[(){}.+*?^$|\\\[\]]/g, "\\$&") + ")", "ig");
        temp = temp.replace(r, "<em class='highlight'>$1</em>");
    }
    //恢复HTML标签 
    for (var i = 0; i < arrA.length; i++) {
        temp = temp.replace("{[(" + i + ")]}", arrA[i]);
    }
    pucl.innerHTML = temp;
} 
/*选项卡*/
function $SetTab(name,cursel,n){	
	for(i=1;i<=n;i++){
		var menu=$('#menu_'+name+i);
		var cont=$('#cont_'+name+i);
		if(i==cursel){
			menu.addClass('current');
			cont.show();
		}else{
			menu.removeClass('current');
			cont.hide();
		}
	}
}

function jTab(Id, tId, EclassName, iBeHavior){
    if(!document.getElementById(Id))return;
    if(iBeHavior==null)iBeHavior='mouseover';
    if(EclassName==null)EclassName='more';
    var self=this;
    var links=document.getElementById(Id).getElementsByTagName('a');
    if(links.length==0)return;

    this.init=function(){
        for(var i=0;i<links.length;i++){
            eval("links[i].on"+iBeHavior+"=function(e){return self.itab(this);};");
            links[i].onclick=function(){
                return (this.href.indexOf('javascript:')>-1 || this.href.indexOf('#')<0 || this.className==EclassName);
            };
            links[i].onfocus=function(e){
                this.blur();
            };
        }
        self.itab(links[0]);
    };
    this.itab=function(o){
        if(o.href.indexOf('javascript:')>-1 || o.href.indexOf('#')<0 || o.className==EclassName){return true;}
        for(var i=0;i<links.length;i++){
            if(links[i].className!=EclassName)links[i].className='';
        }
        o.className='s';
        var url=o.href.substring(o.href.indexOf('#')+1);
        this.showDiv(url);
        return false;
    };
    this.showDiv=function(tDiv){
        if(document.getElementById(tId) && document.getElementById(tDiv)){
            document.getElementById(tId).innerHTML=document.getElementById(tDiv).innerHTML;
            jTab_img_border(document.getElementById(tId));
            //jTab_blank_link(document.getElementById(tId));
            jTab_set_className(document.getElementById(tId));
        }
    };
    this.createDiv=function(id){
        var div=document.createElement('div');
        div.style.display='none';
        div.id=id;
        document.body.appendChild(div);
        return div;
    };
    this.init();
}

function jTab_img_border(obj){
    var li = obj.getElementsByTagName('li');
    var img = null;
    var bc = '#333';
    for(var i=0;i<li.length;i++){
        img = li[i].getElementsByTagName('img');
        for(var j=0;j<img.length;j++){
            bc = img[j].style.borderColor;
            img[j].onmouseover=function(){this.style.borderColor='#f60';};
            img[j].onmouseout=function(){this.style.borderColor=bc;};
        }
    }
}

function jTab_blank_link(obj){
    obj=obj==null?document:obj;
    var links = obj.getElementsByTagName('a');
    for(var j=0;j<links.length;j++){
        links[j].setAttribute('target','_blank');
    }
}

function jTab_set_className(obj){
    obj=obj==null?document:obj;
    this.initialize=function(){
        var ename='';
        var links=obj.getElementsByTagName('a');
        for(var i=0;i<links.length;i++){
            ename=links[i].className;
            if(ename=='new' || ename=='hot'){
                links[i].style.position='relative';
                this.createDiv(links[i],ename);
            }
        }
    };
    this.createDiv=function(ilink, en){
        var a=document.createElement('div');
        a.className='icon_'+en;
        a.style.left=parseInt(ilink.offsetLeft-15)+'px';
        a.style.top=parseInt(ilink.offsetTop-15)+'px';
        ilink.parentNode.appendChild(a);
        return a;
    };
    this.initialize();
}

/*图片滚动*/
var $core={isIE:navigator.appVersion.indexOf("MSIE")!=-1?true:false,addEvent:function(l,i,I){if(l.attachEvent){l.attachEvent("on"+i,I)}else{l.addEventListener(i,I,false)}},delEvent:function(l,i,I){if(l.detachEvent){l.detachEvent("on"+i,I)}else{l.removeEventListener(i,I,false)}},readCookie:function(O){var o="",l=O+"=";if(document.cookie.length>0){var i=document.cookie.indexOf(l);if(i!=-1){i+=l.length;var I=document.cookie.indexOf(";",i);if(I==-1)I=document.cookie.length;o=unescape(document.cookie.substring(i,I))}};return o},writeCookie:function(i,l,o,c){var O="",I="";if(o!=null){O=new Date((new Date).getTime()+o*3600000);O="; expires="+O.toGMTString()};if(c!=null){I=";domain="+c};document.cookie=i+"="+escape(l)+O+I},readStyle:function(I,l){if(I.style[l]){return I.style[l]}else if(I.currentStyle){return I.currentStyle[l]}else if(document.defaultView&&document.defaultView.getComputedStyle){var i=document.defaultView.getComputedStyle(I,null);return i.getPropertyValue(l)}else{return null}}};

function ScrollPic(scrollContId,arrLeftId,arrRightId,dotListId){this.scrollContId=scrollContId;this.arrLeftId=arrLeftId;this.arrRightId=arrRightId;this.dotListId=dotListId;this.dotClassName="dotItem";this.dotOnClassName="dotItemOn";this.dotObjArr=[];this.pageWidth=0;this.frameWidth=0;this.speed=10;this.space=10;this.pageIndex=0;this.autoPlay=true;this.autoPlayTime=5;var _autoTimeObj,_scrollTimeObj,_state="ready";this.stripDiv=document.createElement("DIV");this.listDiv01=document.createElement("DIV");this.listDiv02=document.createElement("DIV");if(!ScrollPic.childs){ScrollPic.childs=[]};this.ID=ScrollPic.childs.length;ScrollPic.childs.push(this);this.initialize=function(){if(!this.scrollContId){throw new Error("必须指定scrollContId.");return};this.scrollContDiv=$i(this.scrollContId);if(!this.scrollContDiv){throw new Error("scrollContId不是正确的对象.(scrollContId = \""+this.scrollContId+"\")");return};this.scrollContDiv.style.width=this.frameWidth+"px";this.scrollContDiv.style.overflow="hidden";this.listDiv01.innerHTML=this.listDiv02.innerHTML=this.scrollContDiv.innerHTML;this.scrollContDiv.innerHTML="";this.scrollContDiv.appendChild(this.stripDiv);this.stripDiv.appendChild(this.listDiv01);this.stripDiv.appendChild(this.listDiv02);this.stripDiv.style.overflow="hidden";this.stripDiv.style.zoom="1";this.stripDiv.style.width="32766px";this.listDiv01.style.cssFloat="left";this.listDiv02.style.cssFloat="left";$core.addEvent(this.scrollContDiv,"mouseover",Function("ScrollPic.childs["+this.ID+"].stop()"));$core.addEvent(this.scrollContDiv,"mouseout",Function("ScrollPic.childs["+this.ID+"].play()"));if(this.arrLeftId){this.arrLeftObj=$i(this.arrLeftId);if(this.arrLeftObj){$core.addEvent(this.arrLeftObj,"mousedown",Function("ScrollPic.childs["+this.ID+"].rightMouseDown()"));$core.addEvent(this.arrLeftObj,"mouseup",Function("ScrollPic.childs["+this.ID+"].rightEnd()"));$core.addEvent(this.arrLeftObj,"mouseout",Function("ScrollPic.childs["+this.ID+"].rightEnd()"))}};if(this.arrRightId){this.arrRightObj=$i(this.arrRightId);if(this.arrRightObj){$core.addEvent(this.arrRightObj,"mousedown",Function("ScrollPic.childs["+this.ID+"].leftMouseDown()"));$core.addEvent(this.arrRightObj,"mouseup",Function("ScrollPic.childs["+this.ID+"].leftEnd()"));$core.addEvent(this.arrRightObj,"mouseout",Function("ScrollPic.childs["+this.ID+"].leftEnd()"))}};if(this.dotListId){this.dotListObj=$i(this.dotListId);if(this.dotListObj){var pages=Math.round(this.listDiv01.offsetWidth/this.frameWidth+0.4),i,tempObj;for(i=0;i<pages;i++){tempObj=document.createElement("span");this.dotListObj.appendChild(tempObj);this.dotObjArr.push(tempObj);if(i==this.pageIndex){tempObj.className=this.dotClassName}else{tempObj.className=this.dotOnClassName};tempObj.title="第"+(i+1)+"页";$core.addEvent(tempObj,"click",Function("ScrollPic.childs["+this.ID+"].pageTo("+i+")"))}}};if(this.autoPlay){this.play()}};this.leftMouseDown=function(){if(_state!="ready"){return};_state="floating";_scrollTimeObj=setInterval("ScrollPic.childs["+this.ID+"].moveLeft()",this.speed)};this.rightMouseDown=function(){if(_state!="ready"){return};_state="floating";_scrollTimeObj=setInterval("ScrollPic.childs["+this.ID+"].moveRight()",this.speed)};this.moveLeft=function(){if(this.scrollContDiv.scrollLeft+this.space>=this.listDiv01.scrollWidth){this.scrollContDiv.scrollLeft=this.scrollContDiv.scrollLeft+this.space-this.listDiv01.scrollWidth}else{this.scrollContDiv.scrollLeft+=this.space};this.accountPageIndex()};this.moveRight=function(){if(this.scrollContDiv.scrollLeft-this.space<=0){this.scrollContDiv.scrollLeft=this.listDiv01.scrollWidth+this.scrollContDiv.scrollLeft-this.space}else{this.scrollContDiv.scrollLeft-=this.space};this.accountPageIndex()};this.leftEnd=function(){if(_state!="floating"){return};_state="stoping";clearInterval(_scrollTimeObj);var fill=this.pageWidth-this.scrollContDiv.scrollLeft%this.pageWidth;this.move(fill)};this.rightEnd=function(){if(_state!="floating"){return};_state="stoping";clearInterval(_scrollTimeObj);var fill=-this.scrollContDiv.scrollLeft%this.pageWidth;this.move(fill)};this.move=function(num,quick){var thisMove=num/5;if(!quick){if(thisMove>this.space){thisMove=this.space};if(thisMove<-this.space){thisMove=-this.space}};if(Math.abs(thisMove)<1&&thisMove!=0){thisMove=thisMove>=0?1:-1}else{thisMove=Math.round(thisMove)};var temp=this.scrollContDiv.scrollLeft+thisMove;if(thisMove>0){if(this.scrollContDiv.scrollLeft+thisMove>=this.listDiv01.scrollWidth){this.scrollContDiv.scrollLeft=this.scrollContDiv.scrollLeft+thisMove-this.listDiv01.scrollWidth}else{this.scrollContDiv.scrollLeft+=thisMove}}else{if(this.scrollContDiv.scrollLeft-thisMove<=0){this.scrollContDiv.scrollLeft=this.listDiv01.scrollWidth+this.scrollContDiv.scrollLeft-thisMove}else{this.scrollContDiv.scrollLeft+=thisMove}};num-=thisMove;if(Math.abs(num)==0){_state="ready";if(this.autoPlay){this.play()};this.accountPageIndex();return}else{this.accountPageIndex();setTimeout("ScrollPic.childs["+this.ID+"].move("+num+","+quick+")",this.speed)}};this.next=function(){if(_state!="ready"){return};_state="stoping";this.move(this.pageWidth,true)};this.play=function(){if(!this.autoPlay){return};clearInterval(_autoTimeObj);_autoTimeObj=setInterval("ScrollPic.childs["+this.ID+"].next()",this.autoPlayTime*1000)};this.stop=function(){clearInterval(_autoTimeObj)};this.pageTo=function(num){if(_state!="ready"){return};_state="stoping";var fill=num*this.frameWidth-this.scrollContDiv.scrollLeft;this.move(fill,true)};this.accountPageIndex=function(){this.pageIndex=Math.round(this.scrollContDiv.scrollLeft/this.frameWidth);if(this.pageIndex>Math.round(this.listDiv01.offsetWidth/this.frameWidth+0.4)-1){this.pageIndex=0};var i;for(i=0;i<this.dotObjArr.length;i++){if(i==this.pageIndex){this.dotObjArr[i].className=this.dotClassName}else{this.dotObjArr[i].className=this.dotOnClassName}}}};

/*内容滚动*/
function jScrollText(content,btnPrevious,btnNext,autoStart,timeout,isSmoothScroll)
{
    this.Speed = 10;
    this.Timeout = timeout;
    this.stopscroll =false;//是否停止滚动的标志位
    this.isSmoothScroll= isSmoothScroll;//是否平滑连续滚动
    this.LineHeight = 20;//默认高度。可以在外部根据需要设置
    this.NextButton = this.$(btnNext);
    this.PreviousButton = this.$(btnPrevious);
    this.ScrollContent = this.$(content);
    if(!this.ScrollContent) return;
    this.ScrollContent.innerHTML += this.ScrollContent.innerHTML;//为了平滑滚动再加一遍

    if(this.PreviousButton)

    {
        this.PreviousButton.onclick = this.GetFunction(this,"Previous"); 
        this.PreviousButton.onmouseover = this.GetFunction(this,"MouseOver");
        this.PreviousButton.onmouseout = this.GetFunction(this,"MouseOut");
    }
    if(this.NextButton){
        this.NextButton.onclick = this.GetFunction(this,"Next");
        this.NextButton.onmouseover = this.GetFunction(this,"MouseOver");
        this.NextButton.onmouseout = this.GetFunction(this,"MouseOut");
    }
    this.ScrollContent.onmouseover = this.GetFunction(this,"MouseOver");
    this.ScrollContent.onmouseout = this.GetFunction(this,"MouseOut");

    if(autoStart)
    {
        this.Start();
    }
}

jScrollText.prototype = {

    $:function(element)
    {
        return document.getElementById(element);
    },
    Previous:function()
    {
        this.stopscroll = true;
        this.Scroll("up");
    },
    Next:function()
    {
        this.stopscroll = true;
        this.Scroll("down");
    },
    Start:function()
    {
        if(this.isSmoothScroll)
        {
            this.AutoScrollTimer = setInterval(this.GetFunction(this,"SmoothScroll"), this.Timeout);
        }
        else
        {        
            this.AutoScrollTimer = setInterval(this.GetFunction(this,"AutoScroll"), this.Timeout);
        }
    },
    Stop:function()
    {
        clearTimeout(this.AutoScrollTimer);
        this.DelayTimerStop = 0;
    },
    MouseOver:function()
    {    
        this.stopscroll = true;
    },
    MouseOut:function()
    {
        this.stopscroll = false;
    },
    AutoScroll:function()
    {
        if(this.stopscroll) 
        {
            return;
        }
        this.ScrollContent.scrollTop++;
        if(parseInt(this.ScrollContent.scrollTop) % this.LineHeight != 0)
        {
            this.ScrollTimer = setTimeout(this.GetFunction(this,"AutoScroll"), this.Speed);
        }
        else
        {
            if(parseInt(this.ScrollContent.scrollTop) >= parseInt(this.ScrollContent.scrollHeight) / 2)
            {
                this.ScrollContent.scrollTop = 0;
            }
            clearTimeout(this.ScrollTimer);
            //this.AutoScrollTimer = setTimeout(this.GetFunction(this,"AutoScroll"), this.Timeout);
        }
    },
    SmoothScroll:function()
    {
        if(this.stopscroll) 
        {
            return;
        }
        this.ScrollContent.scrollTop++;
        if(parseInt(this.ScrollContent.scrollTop) >= parseInt(this.ScrollContent.scrollHeight) / 2)
        {
            this.ScrollContent.scrollTop = 0;
        }
    },
    Scroll:function(direction)
    {

        if(direction=="up")
        {
            this.ScrollContent.scrollTop--;
        }
        else
        {
            this.ScrollContent.scrollTop++;
        }
        if(parseInt(this.ScrollContent.scrollTop) >= parseInt(this.ScrollContent.scrollHeight) / 2)
        {
            this.ScrollContent.scrollTop = 0;
        }
        else if(parseInt(this.ScrollContent.scrollTop)<=0)
        {
            this.ScrollContent.scrollTop = parseInt(this.ScrollContent.scrollHeight) / 2;
        }
        
        if(parseInt(this.ScrollContent.scrollTop) % this.LineHeight != 0)
        {
            this.ScrollTimer = setTimeout(this.GetFunction(this,"Scroll",direction), this.Speed);
        }
    },
    GetFunction:function(variable,method,param)
    {
        return function()
        {
            variable[method](param);
        }
    }
}

var isIE=!-[1,];
//JS图片播放器
function renderPicPlayer(id){
    var interv=4000; //切换间隔时间
    var intervSpeed=10; //切换速度
    var cpic=0;
    var tpic=1;
    var timer, timer1, timer2;
    
    var list=$i(id + '-list');
    if(list){list=list.getElementsByTagName('li')}
    var change = $i(id + '-change');
    if(!list || !list.length || list.length < 2 || !change){return}
    
    var lis = cls = '';
    var picnum = list.length;
    for(var i=0;i<picnum;i++){cls=i==0?' class="active"':'';lis+='<li'+cls+'>'+(i+1)+'</li>'}
    change.innerHTML = lis;
    change = change.getElementsByTagName('li');
    var div = list[0].getElementsByTagName('div')[0];
    var img_fit_with = div.offsetWidth, img_fit_height = div.offsetHeight;
    for(var i=0;i<picnum;i++){
        change[i].index = i;
        var img = list[i].getElementsByTagName('img');
        if(img && img[0]){
            //img[0].onload = function(){resizeImage(this, img_fit_with, img_fit_height, true)}
        }
        if(i>0){
            list[i].opacity = 0;
            alpha(list[i]);
        }else{
            list[i].opacity = 100;
        }
        change[i].onmouseover = function(){
            list[cpic].opacity = 0;
            alpha(list[cpic]);
            setActive(cpic);
            cpic = tpic = this.index;
            list[tpic].opacity = 100;
            alpha(list[tpic]);
            setActive(tpic,true);
            tpic = tpic == (picnum - 1) ? 0 : tpic + 1;
            window.clearInterval(timer);
            timer = window.setInterval(loop, interv);
        }
    }
    function setActive(n,f){change[n].className=f?'active':''}
    if(picnum < 2){return}
    //控制图层透明度
    function alpha(o){if(isIE){o.style.filter="alpha(opacity="+o.opacity+")";}else{o.style.opacity=(o.opacity/100)}o.style.display=o.opacity>0?'':'none'}
    //渐显
    var fadeon=function(){setActive(tpic,true);var o=list[tpic];o.opacity+=5;alpha(o);if(o.opacity<100){window.clearTimeout(timer1);timer1=setTimeout(fadeon,intervSpeed)}else{cpic=tpic;tpic=tpic==(picnum-1)?0:tpic+1;}}
    //渐隐
    var fadeout=function(){setActive(cpic);var o=list[cpic];o.opacity-=10;alpha(o);if(o.opacity>0){window.clearTimeout(timer2);timer2=setTimeout(fadeout,intervSpeed)}else{o.opacity=0;}}
    //循环
    var loop = function(){fadeout();setTimeout(fadeon,intervSpeed+50)}
    timer = window.setInterval(loop, interv);
}
/*加入收藏夹*/
function _ppyy_addFavorite(A,C)
{
	if(window.sidebar||window.opera)
	{
		return true;
	}
	try{window.external.AddFavorite(A,C);}
	catch(B)
	{
		alert("你可以按Ctrl+D键收藏本页！");
	}
	return false;
}
function _ppyy_GetPageContent(pageSize, currentPage)
{
    var totalCount = $("#NewData .liRepeat").length;
    var _html = "";
    if (totalCount > pageSize) {

        if (totalCount % pageSize == 0)
            pageFoot = Math.floor(totalCount / pageSize);
        else
            pageFoot = Math.floor(totalCount / pageSize) + 1;
        var liRoot = (currentPage - 1) * pageSize + 1;
        var liFoot = currentPage * pageSize;
        if (liFoot > totalCount) liFoot = totalCount;
        for (var i = 1; i <= pageFoot; i++) {
            if (i == currentPage)
                _html += "<a class='on' href='javascript:;'>" + i + "</a>";
            else
                _html += "<a href='javascript:;' onclick='_ppyy_GetPageContent(" + pageSize + "," + i + ")'>" + i + "</a>";
        }
        $("#PageBar").html(_html);
        for (var j = 1; j <= totalCount; j++) {
            if (j >= liRoot && j <= liFoot)
                $("#NewData .liRepeat").eq(j - 1).show();
            else
                $("#NewData .liRepeat").eq(j - 1).hide();
        }
    }
    else
        $("#PageBar").html(_html);
}
function _ppyy_SearchBar(type){
    if (!type || type == '') type = 'all';
    var _document = "<div class=\"search_bar\">";
    _document +="    <div id=\"search_bar_date\" class=\"search_bar_left\"></div>";
    _document +="    <script type=\"text/javascript\">setCurrentDateTime('search_bar_date');<\/script>";
    _document +="    <form id=\"searchform\" target=\"_blank\" action=\"" + site.Dir + "search/default.aspx\"><input type=\"hidden\" name=\"channelid\" value=\"0\" />";
    _document +="<ul class=\"tab\" id=\"sotypetab\">";
    _document +="<li class=\"new\"><span id=\"ajaxChannelType\"></span><script>BindModuleRadio('ajaxChannelType','"+type+"');</script></li>";
    _document +="</ul>";
    _document +="        <div class=\"sokey\">";
    _document +="            <input type=\"text\" name=\"k\" id=\"search_keywords\" class=\"keywords\" value=\"\" />";
    _document +="            <input type=\"submit\" id=\"searchsubmit\" class=\"submit\" value=\"搜索\" />";
    _document +="        </div>";
    _document +="    </form>";
    _document +="    <div id=\"search_bar_weather\" class=\"search_bar_right\"></div><script>JumbotPay.Core.ShowWeather(\"search_bar_weather\");</script>";
    _document +="</div>";
    return _document;
}


function print(str) {
    return str;
}

/*字符串正则*/
String.prototype.isTOrderID = function () { return /^[0-9]{15,18}$/.exec(this); }
String.prototype.isMobile = function () { return /^(13|15|17|18)[0-9]{9}$/.exec(this); }
String.prototype.isEmail = function () { return /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/.exec(this); }
String.prototype.Substr = function(index,num) { if(this.length>num){return this.substr(index,num)+'...';}return this; }


String.prototype.noSpace = function () { return this.replace(/[ ]/g, "").replace(/[ ]/g, "").replace(/[ 　]/g, "").replace(/[	]/g, "").replace(/[\r\n\t\v]/g, ""); }

String.prototype.reSpace = function () {
    var newstr = this || ''; 
    var reg = new RegExp(String.fromCharCode(56256), "g");
    newstr = newstr.replace(reg, " ");

    return newstr.replace(/[ ]/g, " ").replace(/[\v]/g, " ").replace(/[ ]/g, " ").replace(/[ 　]/g, " ").replace(/[	]/g, " ");
}


function Num2Chines(n) {
    if (n < 10000)
        return n;
    else if (n < 100000000)
        return (n / 10000) + '万';
    else
        return (n / 100000000) + '亿';
}

function $IsBlank(obj) {
    return (obj.val() == '' || obj.val() == obj.attr('myplaceholder'));
}



function $Popup(url, id, is_dialog, config) {
    var features = '';
    if (+is_dialog) {
        var 
            screenX = typeof window.screenX != 'undefined'
                ? window.screenX
                : window.screenLeft,
            screenY = typeof window.screenY != 'undefined'
                ? window.screenY
                : window.screenTop,
            outerWidth = typeof window.outerWidth != 'undefined'
                ? window.outerWidth
                : document.documentElement.clientWidth,
            outerHeight = typeof window.outerHeight != 'undefined'
                ? window.outerHeight
                : (document.documentElement.clientHeight - 22), // 22= IE toolbar height
            width = config && +config.width || 400,
            height = config && +config.height || 300,
            left = parseInt(screenX + ((outerWidth - width) / 2), 10),
            top = parseInt(screenY + ((outerHeight - height) / 2.5), 10);
        if (config.display == 'popup') {
            top = 100;
        }
        features = (
                'width=' + width +
                    ',height=' + height +
                    ',left=' + left +
                    ',top=' + top
            );


    }
    return window.open(url, ('ppyy_' + id), features);
}




/*school list*/
eval(function (p, a, c, k, e, r) { e = function (c) { return (c < a ? '' : e(parseInt(c / a))) + ((c = c % a) > 35 ? String.fromCharCode(c + 29) : c.toString(36)) }; if (!''.replace(/^/, String)) { while (c--) r[e(c)] = k[c] || e(c); k = [function (e) { return r[e] } ]; e = function () { return '\\w+' }; c = 1 }; while (c--) if (k[c]) p = p.replace(new RegExp('\\b' + e(c) + '\\b', 'g'), k[c]); return p } ('ho 89=[{1:"aZ",2:"\\h\\u\\8\\0"},{1:"k3",2:"\\w\\H\\2C\\Z\\8\\0"},{1:"dw",2:"\\6o\\C\\8\\0"},{1:"9h",2:"\\h\\u\\17\\T\\8\\0"},{1:"eE",2:"\\h\\u\\9\\4\\8\\0"},{1:"FY",2:"\\h\\u\\1g\\28\\1g\\F\\8\\0"},{1:"ef",2:"\\h\\u\\l\\9\\8\\0"},{1:"gv",2:"\\h\\u\\a\\6\\8\\0"},{1:"s1",2:"\\h\\1P\\9\\4\\8\\0"},{1:"Lt",2:"\\h\\u\\1n\\9\\8\\0"},{1:"9o",2:"\\h\\u\\9\\A\\8\\0"},{1:"bF",2:"\\h\\u\\2X\\2R\\0\\3"},{1:"eO",2:"\\h\\u\\2v\\z\\8\\0"},{1:"jn",2:"\\h\\u\\7S\\7a\\0\\3"},{1:"yO",2:"\\h\\u\\G\\25\\8\\0"},{1:"8b",2:"\\h\\u\\1c\\2k\\1n\\9\\0\\3"},{1:"aY",2:"\\h\\u\\z\\1q\\a\\6\\0\\3"},{1:"bx",2:"\\w\\H\\L\\4\\8\\0"},{1:"cT",2:"\\h\\u\\L\\0\\3"},{1:"dr",2:"\\h\\u\\J\\4\\8\\0"},{1:"e8",2:"\\h\\u\\63\\3q\\k\\0\\3"},{1:"gn",2:"\\3e\\1A\\k\\a\\8\\0"},{1:"jf",2:"\\h\\u\\w\\k\\Y\\8\\0"},{1:"jW",2:"\\h\\u\\f\\g\\8\\0"},{1:"sm",2:"\\3e\\1A\\f\\g\\8\\0"},{1:"wO",2:"\\3e\\1A\\1r\\1f\\0\\3"},{1:"He",2:"\\h\\u\\U\\H\\1o\\8\\0"},{1:"8O",2:"\\h\\u\\3k\\3H\\U\\H\\1o\\0\\3"},{1:"9B",2:"\\h\\u\\1o\\aw\\8\\0"},{1:"aS",2:"\\w\\H\\2j\\2l\\8\\0"},{1:"b7",2:"\\w\\42\\O\\y\\8\\0"},{1:"cW",2:"\\5D\\U\\y\\19\\V\\47\\8\\0"},{1:"eC",2:"\\h\\u\\2H\\3N\\0\\3"},{1:"eX",2:"\\3e\\1A\\y\\19\\V\\47\\8\\0"},{1:"fn",2:"\\U\\17\\0\\3"},{1:"fW",2:"\\w\\H\\2C\\Z\\34\\m\\8\\0"},{1:"hd",2:"\\H\\3f\\4F\\6g\\0\\3"},{1:"ju",2:"\\h\\u\\1r\\1f\\8\\0"},{1:"jB",2:"\\w\\42\\33\\2O\\0\\3"},{1:"k6",2:"\\w\\H\\33\\2O\\0\\3"},{1:"km",2:"\\w\\42\\21\\7\\0\\3"},{1:"sx",2:"\\w\\42\\3B\\5s\\0\\3"},{1:"zj",2:"\\w\\H\\3B\\41\\0\\3"},{1:"GC",2:"\\h\\u\\z\\3c\\0\\3"},{1:"JF",2:"\\h\\u\\7c\\7F\\0\\3"},{1:"8n",2:"\\w\\42\\Z\\1i\\8\\0"},{1:"8V",2:"\\w\\H\\1S\\1F\\8\\0"},{1:"9A",2:"\\C\\h\\z\\1H\\8\\0"},{1:"9K",2:"\\w\\C\\48\\1q\\0\\3"},{1:"aN",2:"\\h\\u\\I\\P\\a\\6\\8\\0"},{1:"87",2:"\\w\\H\\49\\4\\8\\0\\4a\\h\\u\\4c"},{1:"co",2:"\\w\\H\\1c\\2k\\8\\0\\4a\\h\\u\\4c"},{1:"cy",2:"\\w\\H\\4n\\4v\\8\\0\\4a\\h\\u\\4c"},{1:"d5",2:"\\h\\u\\3D\\2t\\8\\0"},{1:"dR",2:"\\h\\u\\D\\15\\0\\3"},{1:"eB",2:"\\w\\H\\1e\\30\\1S\\3A\\0\\3"},{1:"eU",2:"\\3e\\61\\9\\0\\3"},{1:"f1",2:"\\w\\H\\4M\\2S\\4F\\6g\\0\\3"},{1:"g8",2:"\\h\\u\\1t\\22\\0\\3"},{1:"gN",2:"\\3e\\1A\\f\\g\\8\\0\\a\\1J\\0\\3"},{1:"hf",2:"\\h\\u\\9\\A\\8\\0\\3w\\C\\0\\3"},{1:"hA",2:"\\h\\u\\2v\\z\\8\\0\\i0\\i4\\0\\3"},{1:"iJ",2:"\\h\\u\\9\\4\\8\\0\\j7\\53\\0\\3"},{1:"jk",2:"\\h\\u\\16\\1W\\0\\3"},{1:"jx",2:"\\h\\u\\3k\\3H\\U\\H\\1o\\0\\3\\w\\7O\\4R\\4e\\1T\\l\\0\\3"},{1:"k9",2:"\\w\\H\\a\\0\\3\\8\\0"},{1:"ke",2:"\\h\\u\\9\\4\\5\\4\\6\\7\\0\\3"},{1:"l4",2:"\\h\\u\\I\\P\\5\\4\\6\\7\\0\\3"},{1:"lE",2:"\\h\\u\\z\\1q\\a\\6\\5\\4\\0\\3"},{1:"lI",2:"\\h\\u\\u\\h\\5\\4\\6\\7\\0\\3"},{1:"lS",2:"\\h\\u\\17\\T\\5\\4\\6\\7\\0\\3"},{1:"mm",2:"\\h\\u\\1e\\30\\1S\\3A\\0\\3"},{1:"mx",2:"\\h\\u\\L\\4\\5\\4\\0\\3"},{1:"mW",2:"\\h\\u\\1S\\1F\\5\\4\\0\\3"},{1:"qu",2:"\\h\\u\\O\\V\\5\\4\\0\\3"},{1:"r0",2:"\\h\\u\\h\\8\\1P\\5w\\2p\\2P\\5\\4\\6\\7\\0\\3"},{1:"u4",2:"\\h\\u\\y\\V\\5\\4\\0\\3"},{1:"uh",2:"\\h\\u\\y\\19\\6\\7\\5\\4\\0\\3"},{1:"wu",2:"\\h\\u\\3B\\41\\S\\7\\5\\4\\0\\3"},{1:"xq",2:"\\h\\u\\7W\\5H\\5\\4\\0\\3"},{1:"zz",2:"\\h\\u\\26\\23\\5\\4\\6\\7\\0\\3"},{1:"H9",2:"\\h\\u\\a\\6\\y\\4N\\1T\\l\\0\\3"},{1:"Hh",2:"\\h\\u\\a\\6\\5\\4\\0\\3"},{1:"IY",2:"\\h\\u\\3Q\\5N\\5\\4\\0\\3"},{1:"LI",2:"\\h\\u\\y\\19\\1T\\l\\5\\4\\0\\3"},{1:"Pe",2:"\\h\\u\\4M\\2S\\2Y\\8f\\5\\4\\0\\3"},{1:"8j",2:"\\h\\u\\8k\\5g\\1T\\l\\5\\4\\0\\3"},{1:"8o",2:"\\h\\u\\S\\7\\2j\\2l\\5\\4\\0\\3"},{1:"8D",2:"\\h\\u\\1r\\1f\\5\\4\\0\\3"},{1:"8N",2:"\\h\\u\\17\\T\\2i\\5X\\5\\4\\0\\3"},{1:"8W",2:"\\h\\u\\2a\\1m\\5\\4\\0\\3"},{1:"9z",2:"\\b\\3M\\8\\0"},{1:"9J",2:"\\F\\10\\8\\0"},{1:"9N",2:"\\F\\10\\a\\6\\8\\0"},{1:"9O",2:"\\F\\10\\9\\4\\8\\0"},{1:"9P",2:"\\w\\H\\Z\\1g\\8\\0"},{1:"9Q",2:"\\F\\10\\l\\9\\8\\0"},{1:"9S",2:"\\F\\10\\L\\0\\3"},{1:"9T",2:"\\F\\10\\k\\a\\8\\0"},{1:"9X",2:"\\F\\10\\w\\k\\Y\\8\\0"},{1:"9Y",2:"\\F\\10\\f\\g\\8\\0"},{1:"a0",2:"\\F\\10\\5\\4\\6\\7\\f\\g\\8\\0"},{1:"a5",2:"\\F\\10\\U\\H\\1o\\8\\0"},{1:"a6",2:"\\F\\10\\A\\4\\8\\0"},{1:"a7",2:"\\F\\10\\O\\y\\8\\0"},{1:"ac",2:"\\F\\10\\1r\\1f\\0\\3"},{1:"ae",2:"\\F\\10\\33\\2O\\0\\3"},{1:"am",2:"\\F\\10\\21\\7\\0\\3"},{1:"an",2:"\\F\\10\\D\\G\\8\\0"},{1:"as",2:"\\F\\10\\F\\at\\0\\3"},{1:"au",2:"\\F\\10\\U\\H\\1o\\8\\0\\1p\\n\\U\\2E\\0\\3"},{1:"aT",2:"\\F\\10\\1r\\1f\\0\\3\\2i\\2S\\3J\\14\\1n\\S\\7\\0\\3"},{1:"b5",2:"\\F\\10\\A\\4\\8\\0\\69\\1J\\0\\3"},{1:"bm",2:"\\F\\10\\k\\a\\8\\0\\3s\\4U\\k\\0\\3"},{1:"c6",2:"\\b\\3M\\8\\0\\1p\\n\\0\\3"},{1:"c9",2:"\\F\\10\\f\\g\\8\\0\\10\\ce\\0\\3"},{1:"cm",2:"\\F\\10\\l\\9\\8\\0\\w\\3d\\I\\P\\0\\3"},{1:"cr",2:"\\h\\u\\a\\6\\8\\0\\F\\10\\0\\3"},{1:"cx",2:"\\F\\10\\8\\0\\3L\\cC\\0\\3"},{1:"cS",2:"\\F\\10\\O\\y\\8\\0\\3v\\e\\0\\3"},{1:"cX",2:"\\F\\10\\15\\5\\4\\8\\0"},{1:"d0",2:"\\F\\10\\w\\1J\\5\\4\\6\\7\\0\\3"},{1:"d3",2:"\\F\\10\\1p\\n\\5\\4\\0\\3"},{1:"d4",2:"\\F\\10\\9\\v\\5\\4\\6\\7\\0\\3"},{1:"dd",2:"\\F\\10\\1e\\30\\5\\4\\0\\3"},{1:"de",2:"\\F\\10\\4k\\n\\5\\4\\6\\7\\0\\3"},{1:"ds",2:"\\F\\10\\z\\1q\\I\\P\\5\\4\\6\\7\\0\\3"},{1:"dv",2:"\\F\\10\\1R\\z\\5\\4\\6\\7\\0\\3"},{1:"dy",2:"\\F\\10\\26\\23\\5\\4\\6\\7\\0\\3"},{1:"dM",2:"\\F\\10\\34\\m\\16\\1w\\5\\4\\0\\3"},{1:"dY",2:"\\F\\10\\2w\\9\\5\\4\\6\\7\\0\\3"},{1:"e9",2:"\\F\\10\\A\\1Y\\5\\4\\0\\3"},{1:"et",2:"\\F\\10\\H\\6B\\3N\\27\\3q\\7m\\eK\\5\\4\\0\\3"},{1:"eN",2:"\\F\\10\\k\\0\\q\\t\\s\\a\\0\\r"},{1:"fi",2:"\\F\\10\\3M\\fm\\4o\\5\\4\\6\\7\\0\\3"},{1:"fp",2:"\\F\\10\\S\\7\\5\\4\\0\\3"},{1:"fu",2:"\\F\\10\\17\\T\\5\\4\\0\\3"},{1:"fG",2:"\\F\\10\\3h\\1B\\5\\4\\6\\7\\0\\3"},{1:"g9",2:"\\F\\10\\1c\\2k\\5\\4\\6\\7\\0\\3"},{1:"gj",2:"\\F\\10\\D\\15\\5\\4\\0\\3"},{1:"gk",2:"\\F\\10\\1U\\2A\\5\\4\\6\\7\\0\\3"},{1:"gw",2:"\\F\\10\\9\\S\\21\\7\\5\\4\\0\\3"},{1:"gM",2:"\\F\\10\\D\\15\\G\\24\\1T\\l\\5\\4\\6\\7\\0\\3"},{1:"gZ",2:"\\F\\10\\1m\\2H\\9\\v\\5\\4\\6\\7\\0\\3"},{1:"h1",2:"\\F\\10\\n\\2i\\5\\4\\0\\3"},{1:"hc",2:"\\F\\10\\o\\52\\3c\\3a\\5\\4\\0\\3"},{1:"hq",2:"\\x\\h\\8\\0"},{1:"hK",2:"\\x\\h\\9\\v\\8\\0"},{1:"hR",2:"\\1c\\1G\\1O\\y\\19\\0\\3"},{1:"ia",2:"\\x\\h\\9\\4\\8\\0"},{1:"ib",2:"\\C\\h\\l\\9\\8\\0"},{1:"in",2:"\\x\\h\\a\\6\\8\\0"},{1:"ir",2:"\\x\\h\\G\\25\\9\\v\\0\\3"},{1:"iC",2:"\\x\\h\\L\\4\\8\\0"},{1:"iE",2:"\\x\\h\\k\\a\\8\\0"},{1:"iG",2:"\\x\\h\\h\\1P\\0\\3"},{1:"iH",2:"\\6p\\1J\\k\\0\\3"},{1:"iT",2:"\\x\\h\\f\\g\\8\\0"},{1:"iX",2:"\\2Y\\3Y\\0\\3"},{1:"ja",2:"\\x\\h\\Z\\1i\\f\\g\\0\\3"},{1:"je",2:"\\40\\j\\f\\g\\0\\3"},{1:"jm",2:"\\4y\\2J\\f\\g\\0\\3"},{1:"jv",2:"\\5v\\1y\\0\\3"},{1:"jG",2:"\\1c\\1G\\1O\\0\\3"},{1:"jV",2:"\\6Z\\79\\0\\3"},{1:"k5",2:"\\5E\\2x\\0\\3"},{1:"kb",2:"\\6b\\d\\f\\g\\0\\3"},{1:"kr",2:"\\1c\\1G\\1O\\1U\\2A\\8\\0"},{1:"kF",2:"\\4C\\j\\8\\0"},{1:"lj",2:"\\x\\h\\a\\6\\f\\g\\0\\3"},{1:"lp",2:"\\40\\j\\0\\3"},{1:"lq",2:"\\C\\h\\a\\6\\0\\3"},{1:"lx",2:"\\w\\H\\2C\\Z\\18\\2R\\16\\1W\\67\\62\\0\\3"},{1:"m7",2:"\\x\\h\\1r\\1f\\0\\3"},{1:"m9",2:"\\x\\h\\1B\\2Q\\0\\3"},{1:"mn",2:"\\h\\C\\1g\\F\\9\\4\\0\\3"},{1:"mv",2:"\\3p\\mJ\\a\\6\\0\\3"},{1:"mO",2:"\\x\\h\\y\\V\\8\\0"},{1:"mT",2:"\\w\\42\\2K\\1F\\16\\1w\\0\\3"},{1:"mX",2:"\\x\\h\\2j\\2l\\0\\3"},{1:"nd",2:"\\x\\h\\9\\v\\6\\7\\0\\3"},{1:"nh",2:"\\x\\h\\21\\7\\0\\3"},{1:"ny",2:"\\x\\h\\a\\6\\0\\3"},{1:"ot",2:"\\x\\h\\U\\H\\1o\\0\\3"},{1:"oC",2:"\\x\\h\\8\\0\\9\\A\\0\\3"},{1:"oI",2:"\\C\\h\\l\\9\\8\\0\\2w\\9\\0\\3"},{1:"p6",2:"\\x\\h\\a\\6\\8\\0\\l\\9\\0\\3"},{1:"pi",2:"\\x\\h\\f\\g\\8\\0\\7W\\C\\0\\3"},{1:"px",2:"\\x\\h\\y\\V\\8\\0\\y\\19\\1T\\l\\0\\3"},{1:"pz",2:"\\x\\h\\k\\a\\8\\0\\3s\\4U\\0\\3"},{1:"pD",2:"\\C\\h\\z\\1H\\8\\0\\a\\6\\0\\3"},{1:"pF",2:"\\x\\h\\9\\v\\8\\0\\a\\I\\0\\3"},{1:"q1",2:"\\x\\h\\9\\4\\8\\0\\D\\15\\0\\3"},{1:"qd",2:"\\4C\\j\\8\\0\\4E\\3L\\0\\3"},{1:"qJ",2:"\\1c\\1G\\1O\\1U\\2A\\8\\0\\R\\1P\\0\\3"},{1:"r8",2:"\\1c\\1G\\1O\\y\\19\\0\\3\\C\\I\\0\\3"},{1:"rb",2:"\\x\\h\\L\\4\\8\\0\\26\\23\\a\\6\\0\\3"},{1:"rH",2:"\\C\\h\\l\\9\\8\\0\\7j\\40\\0\\3"},{1:"sj",2:"\\w\\H\\4n\\4v\\8\\0\\13\\D\\0\\3"},{1:"st",2:"\\4C\\u\\l\\9\\0\\3"},{1:"sw",2:"\\h\\u\\w\\k\\Y\\8\\0\\i\\1P\\0\\3"},{1:"sU",2:"\\h\\u\\17\\T\\8\\0\\n\\1p\\0\\3"},{1:"t7",2:"\\x\\h\\w\\k\\0\\3"},{1:"t8",2:"\\58\\1G\\2T\\0\\3"},{1:"uk",2:"\\x\\h\\9\\v\\6\\7\\q\\t\\s\\a\\0\\r"},{1:"ul",2:"\\x\\h\\9\\4\\5\\4\\6\\7\\0\\3"},{1:"uq",2:"\\6Z\\79\\5\\4\\6\\7\\0\\3"},{1:"uv",2:"\\1c\\1G\\1O\\5\\4\\6\\7\\0\\3"},{1:"ux",2:"\\58\\1G\\2T\\5\\4\\6\\7\\0\\3"},{1:"uC",2:"\\6p\\1J\\1c\\2k\\q\\t\\s\\a\\0\\r"},{1:"uM",2:"\\5E\\2x\\5\\4\\6\\7\\0\\3"},{1:"ve",2:"\\x\\h\\2p\\2P\\5\\4\\6\\7\\0\\3"},{1:"vf",2:"\\x\\h\\1c\\2k\\5\\4\\6\\7\\0\\3"},{1:"vC",2:"\\x\\h\\G\\vL\\5\\4\\6\\7\\0\\3"},{1:"vN",2:"\\x\\h\\1S\\1F\\5\\4\\0\\3"},{1:"vT",2:"\\6b\\d\\5\\4\\6\\7\\0\\3"},{1:"wk",2:"\\x\\h\\2U\\27\\5\\4\\6\\7\\0\\3"},{1:"ww",2:"\\1c\\1G\\1O\\1U\\3l\\5\\4\\6\\7\\0\\3"},{1:"x5",2:"\\2Y\\3Y\\5\\4\\6\\7\\0\\3"},{1:"xe",2:"\\xg\\xi\\2c\\5\\4\\6\\7\\0\\3"},{1:"xs",2:"\\1c\\1G\\1O\\9\\v\\5\\4\\0\\3"},{1:"xL",2:"\\1c\\1G\\1O\\D\\15\\y\\19\\5\\4\\0\\3"},{1:"yq",2:"\\40\\j\\5\\4\\6\\7\\0\\3"},{1:"yz",2:"\\5v\\1y\\5\\4\\6\\7\\0\\3"},{1:"yA",2:"\\40\\j\\9\\4\\5\\4\\6\\7\\0\\3"},{1:"yI",2:"\\5E\\2x\\k\\0\\q\\t\\s\\a\\0\\r"},{1:"yJ",2:"\\x\\h\\5d\\S\\7\\5\\4\\0\\3"},{1:"ze",2:"\\x\\h\\1L\\1Q\\5\\4\\0\\3"},{1:"zZ",2:"\\1c\\1G\\1O\\O\\y\\5\\4\\0\\3"},{1:"Aa",2:"\\x\\h\\17\\T\\5\\4\\6\\7\\0\\3"},{1:"AF",2:"\\x\\h\\1n\\9\\k\\Y\\5\\4\\6\\7\\0\\3"},{1:"AG",2:"\\1c\\1G\\1O\\I\\P\\9\\v\\5\\4\\0\\3"},{1:"AZ",2:"\\x\\h\\U\\H\\1o\\5\\4\\0\\3"},{1:"B9",2:"\\2Y\\3Y\\z\\1H\\5\\4\\6\\7\\0\\3"},{1:"Bb",2:"\\x\\h\\1R\\z\\5\\4\\6\\7\\0\\3"},{1:"Bd",2:"\\4k\\n\\1c\\2k\\5\\4\\0\\3"},{1:"Bj",2:"\\4y\\2J\\5\\4\\6\\7\\0\\3"},{1:"Bp",2:"\\40\\j\\a\\6\\5\\4\\6\\7\\0\\3"},{1:"Bx",2:"\\1c\\1G\\1O\\2v\\z\\5\\4\\6\\7\\0\\3"},{1:"BA",2:"\\x\\h\\34\\m\\16\\1W\\5\\4\\0\\3"},{1:"BD",2:"\\1c\\1G\\1O\\9\\A\\5\\4\\0\\3"},{1:"BI",2:"\\1c\\1G\\1O\\l\\9\\5\\4\\0\\3"},{1:"BM",2:"\\1c\\1G\\1O\\a\\6\\I\\P\\5\\4\\0\\3"},{1:"BR",2:"\\x\\h\\2K\\1F\\16\\1w\\5\\4\\0\\3"},{1:"Dg",2:"\\6b\\d\\k\\0\\q\\t\\s\\a\\0\\r"},{1:"Dj",2:"\\x\\h\\48\\1q\\5\\4\\6\\7\\0\\3"},{1:"Dq",2:"\\1c\\1G\\1O\\k\\0\\q\\t\\s\\a\\0\\r"},{1:"Dr",2:"\\1c\\1G\\1O\\y\\19\\5\\4\\0\\3"},{1:"Dt",2:"\\7j\\w\\5\\4\\0\\3"},{1:"DC",2:"\\1c\\1G\\1O\\2C\\Z\\k\\0\\q\\t\\s\\a\\0\\r"},{1:"E7",2:"\\1c\\1G\\1O\\a\\6\\9\\v\\5\\4\\0\\3"},{1:"Es",2:"\\x\\h\\4M\\2S\\4F\\6g\\5\\4\\0\\3"},{1:"EL",2:"\\1c\\1G\\1O\\a\\6\\5\\4\\0\\3"},{1:"Fr",2:"\\4y\\2J\\i\\1P\\5\\4\\6\\7\\0\\3"},{1:"Fz",2:"\\FB\\3y\\5\\4\\0\\3"},{1:"Ga",2:"\\7f\\1n\\a\\6\\5\\4\\0\\3"},{1:"GM",2:"\\4y\\2J\\4C\\u\\5\\4\\6\\7\\0\\3"},{1:"H8",2:"\\6p\\1J\\2B\\l\\5\\4\\0\\3"},{1:"Ha",2:"\\1c\\1G\\1O\\1C\\1z\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"Hm",2:"\\4y\\2J\\2a\\1m\\5\\4\\0\\3"},{1:"IW",2:"\\x\\h\\7I\\2A\\2i\\5X\\5\\4\\6\\7\\0\\3"},{1:"J5",2:"\\2Y\\3Y\\1C\\1z\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"Ji",2:"\\x\\h\\9\\S\\21\\7\\5\\4\\0\\3"},{1:"Ju",2:"\\4k\\n\\l\\9\\5\\4\\0\\3"},{1:"Jy",2:"\\j\\c\\8\\0"},{1:"KB",2:"\\2N\\2M\\a\\6\\8\\0"},{1:"LL",2:"\\w\\h\\8\\0"},{1:"LP",2:"\\2N\\2M\\l\\9\\8\\0"},{1:"LU",2:"\\j\\c\\L\\4\\8\\0"},{1:"Mr",2:"\\j\\c\\k\\a\\8\\0"},{1:"Mw",2:"\\13\\3A\\k\\0\\3"},{1:"Mz",2:"\\j\\c\\f\\g\\8\\0"},{1:"MO",2:"\\2N\\2M\\f\\g\\0\\3"},{1:"On",2:"\\j\\c\\8\\3Z\\8\\0"},{1:"8a",2:"\\4P\\w\\0\\3"},{1:"8c",2:"\\13\\3A\\0\\3"},{1:"8d",2:"\\2i\\D\\0\\3"},{1:"8e",2:"\\6K\\d\\f\\g\\0\\3"},{1:"8g",2:"\\j\\c\\O\\y\\8\\0"},{1:"8h",2:"\\j\\c\\w\\k\\0\\3"},{1:"8i",2:"\\6O\\6W\\0\\3"},{1:"8l",2:"\\2N\\2M\\0\\3"},{1:"8m",2:"\\j\\c\\29\\2d\\a\\6\\0\\3"},{1:"8p",2:"\\j\\c\\8\\0\\A\\1Y\\0\\3"},{1:"8q",2:"\\2N\\2M\\l\\9\\8\\0\\26\\23\\a\\6\\0\\3"},{1:"8r",2:"\\j\\c\\L\\4\\8\\0\\I\\P\\0\\3"},{1:"8s",2:"\\j\\c\\f\\g\\8\\0\\26\\23\\14\\l\\0\\3"},{1:"8t",2:"\\w\\h\\8\\0\\I\\P\\A\\1Y\\0\\3"},{1:"8u",2:"\\2N\\2M\\a\\6\\8\\0\\C\\a\\0\\3"},{1:"8v",2:"\\j\\c\\k\\a\\8\\0\\4P\\8w\\0\\3"},{1:"8x",2:"\\j\\c\\O\\y\\8\\0\\C\\A\\0\\3"},{1:"8y",2:"\\j\\c\\9\\A\\0\\3"},{1:"8z",2:"\\2N\\2M\\9\\4\\0\\3"},{1:"8A",2:"\\j\\c\\2j\\2l\\0\\3"},{1:"8B",2:"\\j\\c\\9\\v\\6\\7\\0\\3"},{1:"8C",2:"\\j\\c\\5d\\O\\1S\\45\\1Y\\s\\a\\0\\r"},{1:"8E",2:"\\j\\c\\16\\1w\\q\\t\\s\\a\\0\\r"},{1:"8F",2:"\\13\\3A\\5\\4\\6\\7\\0\\3"},{1:"8G",2:"\\j\\c\\S\\7\\5\\4\\0\\3"},{1:"8H",2:"\\4P\\D\\5\\4\\6\\7\\0\\3"},{1:"8I",2:"\\j\\c\\G\\25\\5\\4\\6\\7\\0\\3"},{1:"8J",2:"\\j\\c\\Y\\a\\5\\4\\0\\3"},{1:"8K",2:"\\j\\c\\9\\v\\5\\4\\6\\7\\0\\3"},{1:"8L",2:"\\j\\c\\17\\T\\5\\4\\6\\7\\0\\3"},{1:"8M",2:"\\8\\3Z\\5F\\6c\\5\\4\\6\\7\\0\\3"},{1:"8P",2:"\\j\\c\\1R\\z\\5\\4\\6\\7\\0\\3"},{1:"8Q",2:"\\j\\c\\3B\\5s\\5\\4\\0\\3"},{1:"8R",2:"\\j\\c\\O\\V\\5\\4\\6\\7\\0\\3"},{1:"8S",2:"\\j\\c\\J\\4\\5\\4\\6\\7\\0\\3"},{1:"8T",2:"\\j\\c\\1y\\22\\5\\4\\6\\7\\0\\3"},{1:"8U",2:"\\E\\2s\\5\\4\\6\\7\\0\\3"},{1:"8X",2:"\\3s\\8Y\\5\\4\\6\\7\\0\\3"},{1:"8Z",2:"\\j\\c\\5\\4\\6\\7\\0\\3"},{1:"90",2:"\\j\\c\\5F\\6c\\5\\4\\6\\7\\0\\3"},{1:"91",2:"\\j\\c\\1B\\2Q\\5\\4\\0\\3"},{1:"92",2:"\\2N\\2M\\D\\15\\5\\4\\6\\7\\0\\3"},{1:"93",2:"\\j\\c\\I\\P\\5\\4\\6\\7\\0\\3"},{1:"94",2:"\\j\\c\\1r\\1f\\5\\4\\0\\3"},{1:"95",2:"\\j\\c\\16\\1w\\5\\4\\0\\3"},{1:"96",2:"\\j\\c\\H\\3f\\A\\1Y\\5\\4\\0\\3"},{1:"97",2:"\\98\\m\\5\\4\\6\\7\\0\\3"},{1:"99",2:"\\2N\\2M\\1L\\1Q\\5\\4\\0\\3"},{1:"9a",2:"\\j\\c\\1L\\1Q\\5\\4\\0\\3"},{1:"9b",2:"\\j\\c\\1T\\l\\5\\4\\0\\3"},{1:"9c",2:"\\j\\c\\z\\1H\\5\\4\\6\\7\\0\\3"},{1:"9d",2:"\\6K\\d\\5\\4\\6\\7\\0\\3"},{1:"9e",2:"\\j\\c\\3Z\\14\\5\\4\\6\\7\\0\\3"},{1:"9f",2:"\\4P\\w\\5\\4\\6\\7\\0\\3"},{1:"9g",2:"\\j\\c\\C\\7q\\A\\V\\5\\4\\0\\3"},{1:"9i",2:"\\j\\c\\2i\\D\\L\\4\\5\\4\\6\\7\\0\\3"},{1:"9j",2:"\\2i\\D\\1C\\1z\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"9k",2:"\\j\\c\\9l\\4o\\5\\4\\6\\7\\0\\3"},{1:"9m",2:"\\j\\c\\y\\V\\5\\4\\0\\3"},{1:"9n",2:"\\7r\\d\\5\\4\\6\\7\\0\\3"},{1:"9p",2:"\\2i\\D\\5\\4\\6\\7\\0\\3"},{1:"9q",2:"\\j\\c\\2w\\9\\5\\4\\6\\7\\0\\3"},{1:"9r",2:"\\4P\\w\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"9s",2:"\\E\\2s\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"9t",2:"\\j\\c\\1e\\30\\5\\4\\0\\3"},{1:"9u",2:"\\2i\\D\\2B\\l\\5\\4\\0\\3"},{1:"9v",2:"\\2i\\D\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"9w",2:"\\7r\\d\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"9x",2:"\\6O\\6W\\5\\4\\6\\7\\0\\3"},{1:"9y",2:"\\1x\\1D\\1E\\8\\0"},{1:"9C",2:"\\1x\\1D\\1E\\a\\6\\8\\0"},{1:"9D",2:"\\1x\\1D\\1E\\9\\4\\8\\0"},{1:"9E",2:"\\1x\\1D\\1E\\L\\4\\8\\0"},{1:"9F",2:"\\1x\\1D\\1E\\k\\a\\8\\0"},{1:"9G",2:"\\1x\\1D\\1E\\f\\g\\8\\0"},{1:"9H",2:"\\1x\\1D\\1E\\Z\\1i\\8\\0"},{1:"9I",2:"\\5J\\5h\\0\\3"},{1:"9L",2:"\\1x\\1D\\1E\\O\\y\\8\\0"},{1:"9M",2:"\\5e\\7Z\\86\\1h\\0\\3"},{1:"9R",2:"\\5W\\B\\f\\g\\0\\3"},{1:"9U",2:"\\x\\9V\\0\\3"},{1:"9W",2:"\\5e\\3q\\6Q\\3V\\Z\\1i\\0\\3"},{1:"9Z",2:"\\1x\\1D\\1E\\8\\0\\3W\\4\\0\\3"},{1:"a1",2:"\\1x\\1D\\1E\\f\\g\\8\\0\\a2\\1J\\0\\3"},{1:"a3",2:"\\1x\\1D\\1E\\S\\7\\0\\3"},{1:"a4",2:"\\4L\\1h\\60\\3E\\29\\2d\\6\\7\\0\\3"},{1:"a8",2:"\\1x\\1D\\1E\\G\\25\\5\\4\\6\\7\\0\\3"},{1:"a9",2:"\\1x\\1D\\1E\\aa\\d\\5\\4\\0\\3"},{1:"ab",2:"\\4G\\3y\\5\\4\\6\\7\\0\\3"},{1:"ad",2:"\\2o\\m\\5\\4\\6\\7\\0\\3"},{1:"af",2:"\\5e\\3q\\6Q\\3V\\5\\4\\0\\3"},{1:"ag",2:"\\4G\\3y\\2w\\9\\5\\4\\6\\7\\0\\3"},{1:"ah",2:"\\1x\\1D\\1E\\z\\1q\\I\\P\\5\\4\\6\\7\\0\\3"},{1:"ai",2:"\\1x\\1D\\1E\\1R\\z\\5\\4\\6\\7\\0\\3"},{1:"aj",2:"\\1x\\1D\\1E\\1n\\9\\5\\4\\0\\3"},{1:"ak",2:"\\1x\\1D\\1E\\A\\V\\5\\4\\0\\3"},{1:"al",2:"\\39\\J\\7n\\ao\\5\\4\\0\\3"},{1:"ap",2:"\\1x\\1D\\1E\\16\\1W\\5\\4\\0\\3"},{1:"aq",2:"\\1x\\1D\\1E\\1r\\1f\\5\\4\\0\\3"},{1:"ar",2:"\\4f\\2e\\1W\\7A\\5\\4\\0\\3"},{1:"av",2:"\\T\\12\\5\\4\\0\\3"},{1:"ax",2:"\\a\\1h\\ay\\S\\7\\5\\4\\0\\3"},{1:"az",2:"\\1x\\1D\\1E\\17\\T\\5\\4\\6\\7\\0\\3"},{1:"aA",2:"\\4G\\3y\\61\\1U\\5\\4\\6\\7\\0\\3"},{1:"aB",2:"\\4f\\n\\5\\4\\6\\7\\0\\3"},{1:"aC",2:"\\1x\\1D\\1E\\a\\6\\5\\4\\0\\3"},{1:"aD",2:"\\1x\\1D\\1E\\h\\1P\\5\\4\\6\\7\\0\\3"},{1:"aE",2:"\\5J\\5h\\5\\4\\6\\7\\0\\3"},{1:"aF",2:"\\1x\\1D\\1E\\y\\V\\U\\1o\\5\\4\\0\\3"},{1:"aG",2:"\\4G\\3y\\1U\\2A\\5\\4\\6\\7\\0\\3"},{1:"aH",2:"\\4f\\2e\\1W\\7A\\k\\0\\q\\t\\s\\a\\0\\r"},{1:"aI",2:"\\4L\\1h\\60\\3E\\5\\4\\0\\3"},{1:"aJ",2:"\\1x\\1D\\1E\\9\\4\\5\\4\\0\\3"},{1:"aK",2:"\\5e\\7Z\\86\\1h\\5\\4\\6\\7\\0\\3"},{1:"aL",2:"\\aM\\5y\\4E\\aO\\1o\\5\\4\\0\\3"},{1:"aP",2:"\\1x\\1D\\1E\\2U\\27\\5\\4\\0\\3"},{1:"aQ",2:"\\5J\\5h\\9\\4\\5\\4\\6\\7\\0\\3"},{1:"aR",2:"\\6h\\6i\\aU\\5\\4\\6\\7\\0\\3"},{1:"aV",2:"\\1x\\1D\\1E\\21\\7\\5\\4\\0\\3"},{1:"aW",2:"\\1x\\1D\\1E\\Z\\1i\\1C\\1z\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"aX",2:"\\4L\\1h\\60\\3E\\1m\\4w\\3d\\3j\\5\\4\\0\\3"},{1:"b0",2:"\\b1\\2e\\b2\\5\\4\\0\\3"},{1:"b3",2:"\\12\\B\\8\\0"},{1:"b4",2:"\\8\\1I\\l\\9\\8\\0"},{1:"b6",2:"\\1Z\\E\\9\\4\\8\\0"},{1:"b8",2:"\\1Z\\E\\1g\\28\\1g\\F\\8\\0"},{1:"b9",2:"\\1Z\\E\\l\\9\\8\\0"},{1:"ba",2:"\\i\\h\\8\\0"},{1:"bb",2:"\\12\\B\\a\\6\\8\\0"},{1:"bc",2:"\\12\\B\\9\\v\\6\\7\\8\\0"},{1:"bd",2:"\\12\\B\\1c\\2k\\1n\\9\\8\\0"},{1:"be",2:"\\1Z\\E\\1n\\9\\8\\0"},{1:"bf",2:"\\8\\1I\\17\\T\\8\\0"},{1:"bg",2:"\\8\\1I\\n\\2E\\8\\0"},{1:"bh",2:"\\8\\1I\\9\\4\\8\\0"},{1:"bi",2:"\\1Z\\E\\G\\25\\8\\0"},{1:"bj",2:"\\12\\B\\9\\4\\8\\0"},{1:"bk",2:"\\1Z\\E\\L\\4\\8\\0"},{1:"bl",2:"\\8\\1I\\n\\2m\\8\\0"},{1:"bn",2:"\\w\\H\\k\\a\\8\\0"},{1:"bo",2:"\\12\\B\\k\\0\\3"},{1:"bp",2:"\\8\\1I\\k\\a\\8\\0"},{1:"bq",2:"\\12\\B\\w\\k\\Y\\8\\0"},{1:"br",2:"\\1Z\\E\\Y\\a\\8\\0"},{1:"bs",2:"\\1Z\\E\\k\\0\\3"},{1:"bt",2:"\\12\\B\\f\\g\\8\\0"},{1:"bu",2:"\\1Z\\E\\f\\g\\8\\0"},{1:"bv",2:"\\4k\\n\\8\\0"},{1:"bw",2:"\\6q\\j\\f\\g\\0\\3"},{1:"by",2:"\\8\\1I\\U\\H\\1o\\8\\0"},{1:"bz",2:"\\i\\h\\O\\y\\8\\0"},{1:"bA",2:"\\w\\H\\bB\\2E\\16\\1W\\0\\3"},{1:"bC",2:"\\1Z\\E\\1r\\1f\\0\\3"},{1:"bD",2:"\\1Z\\E\\33\\2O\\0\\3"},{1:"bE",2:"\\3F\\bG\\21\\7\\0\\3"},{1:"bH",2:"\\12\\B\\5D\\U\\y\\V\\0\\3"},{1:"bI",2:"\\1Z\\E\\8\\0"},{1:"bJ",2:"\\8\\1I\\8\\0"},{1:"bK",2:"\\12\\B\\a\\6\\0\\3"},{1:"bL",2:"\\12\\B\\16\\1W\\0\\3"},{1:"bM",2:"\\1Z\\E\\9\\v\\0\\3"},{1:"bN",2:"\\12\\i\\0\\3"},{1:"bO",2:"\\8\\1I\\Z\\1i\\8\\0"},{1:"bP",2:"\\8\\1I\\l\\9\\8\\0\\D\\15\\0\\3"},{1:"bQ",2:"\\1Z\\E\\9\\4\\8\\0\\9\\v\\0\\3"},{1:"bR",2:"\\1Z\\E\\1g\\28\\1g\\F\\8\\0\\h\\1P\\a\\6\\0\\3"},{1:"bS",2:"\\1Z\\E\\9\\0\\3"},{1:"bT",2:"\\8\\1I\\9\\4\\8\\0\\S\\7\\3J\\I\\P\\9\\v\\0\\3"},{1:"bU",2:"\\8\\1I\\a\\6\\0\\3"},{1:"bV",2:"\\1Z\\E\\D\\15\\G\\24\\0\\3"},{1:"bW",2:"\\w\\H\\k\\a\\8\\0\\3s\\4U\\k\\Y\\0\\3"},{1:"bX",2:"\\8\\1I\\k\\a\\8\\0\\w\\j\\0\\3"},{1:"bY",2:"\\12\\B\\k\\0\\3\\k\\bZ\\0\\3"},{1:"c0",2:"\\12\\B\\f\\g\\8\\0\\n\\C\\0\\3"},{1:"c1",2:"\\12\\B\\l\\9\\0\\3"},{1:"c2",2:"\\8\\1I\\O\\y\\0\\3"},{1:"c3",2:"\\1Z\\E\\D\\15\\0\\3"},{1:"c4",2:"\\12\\B\\a\\6\\8\\0\\I\\P\\6\\7\\0\\3"},{1:"c5",2:"\\12\\B\\1c\\2k\\1n\\9\\8\\0\\3S\\C\\2U\\27\\0\\3"},{1:"c7",2:"\\8\\1I\\S\\7\\0\\3"},{1:"c8",2:"\\12\\B\\w\\k\\Y\\8\\0\\4t\\J\\0\\3"},{1:"ca",2:"\\12\\B\\cb\\cc\\k\\0\\3"},{1:"cd",2:"\\1Z\\E\\1n\\9\\8\\0\\a\\3n\\0\\3"},{1:"cf",2:"\\8\\1I\\i\\2p\\I\\P\\0\\3"},{1:"cg",2:"\\12\\B\\O\\V\\0\\3"},{1:"ch",2:"\\12\\B\\2j\\2l\\0\\3"},{1:"ci",2:"\\4N\\2T\\l\\9\\0\\3"},{1:"cj",2:"\\ck\\E\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"cl",2:"\\6r\\3S\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"cn",2:"\\51\\d\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"cp",2:"\\4N\\2T\\5\\4\\6\\7\\0\\3"},{1:"cq",2:"\\1U\\4p\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"cs",2:"\\8\\1I\\5\\4\\6\\7\\0\\3"},{1:"ct",2:"\\12\\B\\L\\4\\5\\4\\6\\7\\0\\3"},{1:"cu",2:"\\6r\\3S\\5\\4\\6\\7\\0\\3"},{1:"cv",2:"\\12\\E\\5\\4\\6\\7\\0\\3"},{1:"cw",2:"\\3r\\11\\q\\t\\s\\a\\0\\r"},{1:"cz",2:"\\12\\B\\5d\\17\\T\\q\\t\\s\\a\\0\\r"},{1:"cA",2:"\\12\\B\\45\\1Y\\q\\t\\s\\a\\0\\r"},{1:"cB",2:"\\6u\\51\\5\\4\\6\\7\\0\\3"},{1:"cD",2:"\\1Z\\E\\1g\\28\\5\\4\\6\\7\\0\\3"},{1:"cE",2:"\\12\\B\\1r\\1f\\2i\\2S\\5\\4\\6\\7\\0\\3"},{1:"cF",2:"\\12\\B\\5\\4\\0\\3"},{1:"cG",2:"\\12\\B\\J\\4\\5\\4\\6\\7\\0\\3"},{1:"cH",2:"\\1Z\\E\\5\\4\\6\\7\\0\\3"},{1:"cI",2:"\\12\\B\\l\\9\\5\\4\\0\\3"},{1:"cJ",2:"\\8\\1I\\A\\1Y\\5\\4\\0\\3"},{1:"cK",2:"\\12\\B\\1B\\2Q\\5\\4\\0\\3"},{1:"cL",2:"\\12\\B\\7I\\2A\\17\\T\\5\\4\\0\\3"},{1:"cM",2:"\\12\\B\\o\\cN\\5\\4\\0\\3"},{1:"cO",2:"\\12\\B\\1R\\z\\5\\4\\6\\7\\0\\3"},{1:"cP",2:"\\12\\B\\y\\19\\5\\4\\6\\7\\0\\3"},{1:"cQ",2:"\\12\\B\\1c\\1n\\5\\4\\6\\7\\0\\3"},{1:"cR",2:"\\4k\\n\\4l\\7o\\5\\4\\0\\3"},{1:"cU",2:"\\8\\1I\\2p\\2P\\5\\4\\0\\3"},{1:"cV",2:"\\8\\1I\\59\\57\\5\\4\\0\\3"},{1:"cY",2:"\\12\\B\\A\\V\\5\\4\\0\\3"},{1:"cZ",2:"\\8\\1I\\7v\\d1\\5\\4\\6\\7\\0\\3"},{1:"d2",2:"\\12\\B\\2R\\7w\\6w\\6y\\5\\4\\6\\7\\0\\3"},{1:"d6",2:"\\12\\x\\1c\\2k\\5\\4\\6\\7\\0\\3"},{1:"d7",2:"\\12\\B\\4n\\4v\\9\\v\\5\\4\\0\\3"},{1:"d8",2:"\\12\\B\\1U\\2A\\5\\4\\6\\7\\0\\3"},{1:"d9",2:"\\12\\B\\G\\25\\5\\4\\0\\3"},{1:"da",2:"\\8\\1I\\1g\\2i\\5\\4\\6\\7\\0\\3"},{1:"db",2:"\\8\\1I\\2R\\7w\\6w\\6y\\5\\4\\6\\7\\0\\3"},{1:"dc",2:"\\8\\1I\\2z\\2y\\5\\4\\6\\7\\0\\3"},{1:"df",2:"\\12\\B\\26\\23\\2X\\1Y\\5\\4\\6\\7\\0\\3"},{1:"dg",2:"\\12\\B\\3h\\1B\\5\\4\\6\\7\\0\\3"},{1:"dh",2:"\\12\\B\\9\\v\\5\\4\\0\\3"},{1:"di",2:"\\12\\B\\D\\15\\G\\24\\5\\4\\6\\7\\0\\3"},{1:"dj",2:"\\12\\B\\k\\Y\\5\\4\\0\\3"},{1:"dk",2:"\\1U\\4p\\2a\\1m\\5\\4\\0\\3"},{1:"dl",2:"\\1Z\\E\\h\\2p\\I\\P\\5\\4\\6\\7\\0\\3"},{1:"dm",2:"\\12\\B\\1S\\1F\\5\\4\\0\\3"},{1:"dn",2:"\\12\\B\\Z\\1i\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"do",2:"\\12\\B\\2w\\9\\5\\4\\0\\3"},{1:"dp",2:"\\12\\B\\1y\\22\\5\\4\\0\\3"},{1:"dq",2:"\\12\\B\\3V\\4T\\5b\\1f\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"dt",2:"\\1t\\J\\8\\0"},{1:"du",2:"\\4i\\6E\\8\\0"},{1:"dx",2:"\\13\\1V\\l\\9\\8\\0"},{1:"dz",2:"\\i\\h\\z\\1H\\8\\0"},{1:"dA",2:"\\13\\1V\\9\\4\\8\\0"},{1:"dB",2:"\\1t\\J\\G\\25\\8\\0"},{1:"dC",2:"\\1t\\J\\1n\\9\\0\\3"},{1:"dD",2:"\\1t\\J\\L\\4\\8\\0"},{1:"dE",2:"\\13\\1V\\w\\k\\Y\\8\\0"},{1:"dF",2:"\\i\\h\\f\\g\\8\\0"},{1:"dG",2:"\\h\\C\\8\\0"},{1:"dH",2:"\\T\\1n\\f\\g\\0\\3"},{1:"dI",2:"\\1t\\J\\f\\g\\8\\0"},{1:"dJ",2:"\\1t\\J\\9\\v\\6\\7\\f\\g\\0\\3"},{1:"dK",2:"\\13\\1V\\f\\g\\8\\0"},{1:"dL",2:"\\4d\\D\\f\\g\\0\\3"},{1:"dN",2:"\\1t\\J\\O\\y\\8\\0"},{1:"dO",2:"\\1t\\J\\1r\\1f\\0\\3"},{1:"dP",2:"\\1t\\J\\S\\7\\0\\3"},{1:"dQ",2:"\\1t\\J\\C\\4Q\\U\\H\\1o\\0\\3"},{1:"dS",2:"\\1t\\J\\9\\A\\0\\3"},{1:"dT",2:"\\13\\1V\\9\\v\\0\\3"},{1:"dU",2:"\\1t\\J\\L\\4\\a\\6\\0\\3"},{1:"dV",2:"\\1t\\J\\16\\1W\\0\\3"},{1:"dW",2:"\\13\\1V\\8\\0"},{1:"dX",2:"\\13\\1V\\4g\\C\\0\\3"},{1:"dZ",2:"\\13\\1V\\9\\4\\8\\0\\2C\\14\\I\\P\\0\\3"},{1:"e0",2:"\\13\\1V\\l\\9\\8\\0\\4g\\z\\I\\P\\0\\3"},{1:"e1",2:"\\13\\1V\\O\\y\\0\\3"},{1:"e2",2:"\\1t\\J\\G\\25\\8\\0\\D\\G\\0\\3"},{1:"e3",2:"\\13\\1V\\G\\25\\0\\3"},{1:"e4",2:"\\13\\1V\\a\\6\\0\\3"},{1:"e5",2:"\\1t\\J\\2S\\e6\\0\\3"},{1:"e7",2:"\\1t\\J\\f\\g\\8\\0\\37\\2D\\0\\3"},{1:"ea",2:"\\13\\1V\\8\\0\\1L\\1Q\\0\\3"},{1:"eb",2:"\\i\\h\\f\\g\\8\\0\\2C\\14\\0\\3"},{1:"ec",2:"\\1t\\J\\k\\Y\\0\\3"},{1:"ed",2:"\\12\\27\\5\\4\\6\\7\\0\\3"},{1:"ee",2:"\\R\\4h\\5\\4\\8\\0"},{1:"eg",2:"\\13\\1V\\2z\\2y\\9\\4\\q\\t\\s\\a\\0\\r"},{1:"eh",2:"\\13\\1V\\1B\\2Q\\q\\t\\s\\a\\0\\r"},{1:"ei",2:"\\13\\1V\\k\\0\\q\\t\\s\\a\\0\\r"},{1:"ej",2:"\\1t\\J\\17\\T\\5\\4\\6\\7\\0\\3"},{1:"ek",2:"\\13\\1V\\i\\1P\\5\\4\\0\\3"},{1:"el",2:"\\1t\\J\\2K\\1F\\16\\1w\\5\\4\\0\\3"},{1:"em",2:"\\1t\\J\\z\\1q\\I\\P\\5\\4\\6\\7\\0\\3"},{1:"en",2:"\\1t\\J\\9\\4\\5\\4\\6\\7\\0\\3"},{1:"eo",2:"\\1t\\J\\9\\v\\5\\4\\0\\3"},{1:"ep",2:"\\13\\1V\\5\\4\\6\\7\\0\\3"},{1:"eq",2:"\\4d\\D\\k\\0\\q\\t\\s\\a\\0\\r"},{1:"er",2:"\\13\\1V\\I\\P\\6\\7\\5\\4\\0\\3"},{1:"es",2:"\\4V\\2M\\5\\4\\6\\7\\0\\3"},{1:"eu",2:"\\1t\\J\\1U\\2A\\5\\4\\6\\7\\0\\3"},{1:"ev",2:"\\4d\\D\\5\\4\\6\\7\\0\\3"},{1:"ew",2:"\\13\\4d\\j\\5\\4\\6\\7\\0\\3"},{1:"ex",2:"\\1t\\J\\a\\6\\5\\4\\6\\7\\0\\3"},{1:"ey",2:"\\4i\\6E\\5\\4\\6\\7\\0\\3"},{1:"ez",2:"\\1t\\J\\D\\15\\5\\4\\6\\7\\0\\3"},{1:"eA",2:"\\1j\\1k\\e\\8\\0"},{1:"eD",2:"\\1v\\1h\\1p\\9\\4\\8\\0"},{1:"eF",2:"\\1v\\1h\\1p\\l\\9\\8\\0"},{1:"eG",2:"\\1v\\1h\\1p\\9\\v\\8\\0"},{1:"eH",2:"\\1j\\1k\\e\\a\\6\\8\\0"},{1:"eI",2:"\\i\\h\\1c\\2k\\8\\0"},{1:"eJ",2:"\\5H\\4j\\3E\\8\\0"},{1:"eL",2:"\\1j\\1k\\e\\eM\\4Y\\L\\6D\\8\\0"},{1:"eP",2:"\\i\\h\\L\\4\\8\\0"},{1:"eQ",2:"\\i\\h\\J\\4\\8\\0"},{1:"eR",2:"\\1v\\1h\\1p\\k\\a\\8\\0"},{1:"eS",2:"\\1j\\1k\\e\\w\\k\\Y\\8\\0"},{1:"eT",2:"\\6z\\53\\e\\k\\0\\3"},{1:"eV",2:"\\1v\\1h\\1p\\f\\g\\8\\0"},{1:"eW",2:"\\2u\\2u\\1v\\1h\\8\\0"},{1:"eY",2:"\\6z\\53\\e\\f\\g\\0\\3"},{1:"eZ",2:"\\1v\\1h\\1p\\0\\3"},{1:"f0",2:"\\8\\K\\f\\g\\0\\3"},{1:"f2",2:"\\f3\\1n\\0\\3"},{1:"f4",2:"\\1v\\1h\\1p\\A\\4\\8\\0"},{1:"f5",2:"\\1v\\1h\\1p\\1r\\1f\\0\\3"},{1:"f6",2:"\\1v\\1h\\1p\\1B\\2Q\\0\\3"},{1:"f7",2:"\\2u\\2u\\1v\\1h\\k\\0\\3"},{1:"f8",2:"\\1j\\1k\\e\\9\\4\\0\\3"},{1:"f9",2:"\\1j\\1k\\e\\i\\1P\\0\\3"},{1:"fa",2:"\\1v\\1h\\1p\\I\\P\\9\\v\\0\\3"},{1:"fb",2:"\\1j\\1k\\e\\9\\v\\0\\3"},{1:"fc",2:"\\2u\\2u\\1v\\1h\\9\\v\\0\\3"},{1:"fd",2:"\\1j\\1k\\e\\U\\H\\1o\\0\\3"},{1:"fe",2:"\\1j\\1k\\e\\O\\y\\0\\3"},{1:"ff",2:"\\1v\\1h\\1p\\1c\\2k\\0\\3"},{1:"fg",2:"\\1j\\1k\\e\\9\\A\\0\\3"},{1:"fh",2:"\\1v\\1h\\1p\\43\\i\\l\\9\\0\\3"},{1:"fj",2:"\\1v\\1h\\1p\\fk\\4Q\\0\\3"},{1:"fl",2:"\\1j\\1k\\e\\9\\v\\0\\3\\2G\\7z\\1L\\1Q\\0\\3"},{1:"fo",2:"\\1v\\1h\\1p\\o\\2g\\0\\3"},{1:"fq",2:"\\1v\\1h\\1p\\C\\1J\\0\\3"},{1:"fr",2:"\\1j\\x\\0\\3"},{1:"fs",2:"\\2u\\2u\\1v\\1h\\q\\t\\f\\g\\s\\a\\0\\r"},{1:"ft",2:"\\6v\\1V\\5\\4\\0\\3"},{1:"fv",2:"\\6z\\53\\e\\8\\0"},{1:"fw",2:"\\1j\\1k\\e\\5\\4\\0\\3"},{1:"fx",2:"\\1j\\1k\\e\\G\\25\\5\\4\\6\\7\\0\\3"},{1:"fy",2:"\\1j\\1k\\e\\S\\7\\5\\4\\0\\3"},{1:"fz",2:"\\8\\K\\5\\4\\0\\3"},{1:"fA",2:"\\1j\\1k\\e\\J\\4\\5\\4\\6\\7\\0\\3"},{1:"fB",2:"\\1j\\1k\\e\\L\\4\\5\\4\\6\\7\\0\\3"},{1:"fC",2:"\\1j\\1k\\e\\L\\4\\9\\v\\5\\4\\0\\3"},{1:"fD",2:"\\1j\\1k\\e\\L\\6D\\5\\4\\0\\3"},{1:"fE",2:"\\1j\\1k\\e\\2K\\1F\\16\\1w\\5\\4\\0\\3"},{1:"fF",2:"\\5q\\fH\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"fI",2:"\\1v\\1h\\1p\\z\\1H\\5\\4\\6\\7\\0\\3"},{1:"fJ",2:"\\1v\\1h\\1p\\1U\\2A\\5\\4\\6\\7\\0\\3"},{1:"fK",2:"\\8\\2o\\m\\4p\\5\\4\\0\\3"},{1:"fL",2:"\\1j\\1k\\e\\L\\4\\y\\19\\5\\4\\0\\3"},{1:"fM",2:"\\1v\\1h\\1p\\5\\4\\6\\7\\0\\3"},{1:"fN",2:"\\1v\\1h\\1p\\2j\\2l\\5\\4\\0\\3"},{1:"fO",2:"\\1j\\1k\\e\\1m\\2H\\a\\6\\5\\4\\0\\3"},{1:"fP",2:"\\1j\\1k\\e\\A\\4\\5\\4\\0\\3"},{1:"fQ",2:"\\1j\\1k\\e\\34\\m\\16\\1w\\5\\4\\0\\3"},{1:"fR",2:"\\1j\\1k\\e\\I\\P\\6\\7\\5\\4\\0\\3"},{1:"fS",2:"\\1v\\1h\\1p\\e\\b\\5\\4\\6\\7\\0\\3"},{1:"fT",2:"\\1j\\1k\\e\\L\\6D\\a\\6\\5\\4\\0\\3"},{1:"fU",2:"\\1j\\1k\\e\\1L\\1Q\\5\\4\\6\\7\\0\\3"},{1:"fV",2:"\\1j\\1k\\e\\20\\e\\21\\7\\5\\4\\0\\3"},{1:"fX",2:"\\1j\\1k\\e\\1m\\4w\\9\\v\\5\\4\\0\\3"},{1:"fY",2:"\\1j\\1k\\e\\5F\\6c\\5\\4\\6\\7\\0\\3"},{1:"fZ",2:"\\g0\\2x\\x\\5\\4\\0\\3"},{1:"g1",2:"\\1j\\1k\\e\\Z\\1i\\5\\4\\0\\3"},{1:"g2",2:"\\8\\K\\k\\0\\q\\t\\s\\a\\0\\r"},{1:"g3",2:"\\1j\\1k\\e\\17\\T\\5\\4\\6\\7\\0\\3"},{1:"g4",2:"\\1v\\1h\\1p\\29\\2d\\5\\4\\6\\7\\0\\3"},{1:"g5",2:"\\1j\\1k\\e\\1C\\1z\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"g6",2:"\\1v\\1h\\1p\\a\\0\\6\\7\\5\\4\\0\\3"},{1:"g7",2:"\\1j\\1k\\e\\7N\\3C\\5\\4\\0\\3"},{1:"ga",2:"\\5H\\4j\\3E\\5\\4\\0\\3"},{1:"gb",2:"\\1j\\1k\\e\\2B\\l\\q\\t\\s\\a\\0\\r"},{1:"gc",2:"\\1v\\1h\\1p\\9\\v\\6\\7\\5\\4\\0\\3"},{1:"gd",2:"\\2u\\2u\\1v\\1h\\l\\9\\5\\4\\0\\3"},{1:"ge",2:"\\1v\\1h\\1p\\1C\\1z\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"gf",2:"\\1j\\1k\\e\\gg\\gh\\1r\\1f\\5\\4\\0\\3"},{1:"gi",2:"\\7Q\\7R\\8\\0"},{1:"gl",2:"\\3Z\\19\\8\\0"},{1:"gm",2:"\\N\\n\\17\\T\\8\\0"},{1:"go",2:"\\C\\i\\l\\9\\8\\0"},{1:"gp",2:"\\N\\n\\l\\9\\8\\0"},{1:"gq",2:"\\N\\n\\n\\2E\\8\\0"},{1:"gr",2:"\\i\\C\\8\\0"},{1:"gs",2:"\\N\\n\\z\\1H\\0\\3"},{1:"gt",2:"\\N\\n\\29\\2d\\6\\7\\0\\3"},{1:"gu",2:"\\N\\n\\3K\\38\\k\\0\\3"},{1:"gx",2:"\\N\\n\\n\\2m\\8\\0"},{1:"gy",2:"\\N\\n\\w\\k\\Y\\8\\0"},{1:"gz",2:"\\C\\i\\f\\g\\8\\0"},{1:"gA",2:"\\N\\n\\f\\g\\8\\0"},{1:"gB",2:"\\N\\n\\U\\H\\1o\\8\\0"},{1:"gC",2:"\\N\\n\\O\\y\\8\\0"},{1:"gD",2:"\\N\\n\\5D\\U\\y\\V\\8\\0"},{1:"gE",2:"\\N\\n\\n\\4F\\0\\3"},{1:"gF",2:"\\C\\i\\1S\\1F\\8\\0"},{1:"gG",2:"\\N\\n\\1r\\1f\\0\\3"},{1:"gH",2:"\\N\\n\\33\\2O\\0\\3"},{1:"gI",2:"\\N\\n\\3B\\5s\\0\\3"},{1:"gJ",2:"\\N\\n\\8\\0"},{1:"gK",2:"\\N\\n\\9\\v\\6\\7\\8\\0"},{1:"gL",2:"\\N\\n\\4q\\I\\5g\\2Z\\0\\3"},{1:"gO",2:"\\N\\n\\z\\1R\\0\\3"},{1:"gP",2:"\\N\\n\\1B\\2Q\\0\\3"},{1:"gQ",2:"\\N\\n\\gR\\2D\\0\\3"},{1:"gS",2:"\\N\\n\\1S\\1F\\0\\3"},{1:"gT",2:"\\N\\n\\3k\\3H\\9\\4\\8\\0"},{1:"gU",2:"\\N\\n\\A\\0\\3"},{1:"gV",2:"\\N\\n\\G\\4Q\\0\\3"},{1:"gW",2:"\\N\\n\\2o\\gX\\0\\3"},{1:"gY",2:"\\N\\n\\3a\\88\\S\\7\\0\\3"},{1:"h0",2:"\\N\\n\\U\\H\\1o\\8\\0\\6t\\2D\\y\\19\\2C\\14\\0\\3"},{1:"h2",2:"\\N\\n\\f\\g\\8\\0\\F\\C\\0\\3"},{1:"h3",2:"\\N\\n\\a\\6\\8\\0"},{1:"h4",2:"\\N\\n\\h5\\h6\\8\\0"},{1:"h7",2:"\\N\\n\\1L\\1Q\\q\\t\\s\\a\\0\\r"},{1:"h8",2:"\\N\\n\\34\\m\\q\\t\\s\\a\\0\\r"},{1:"h9",2:"\\N\\n\\i\\n\\5\\4\\6\\7\\0\\3"},{1:"ha",2:"\\N\\n\\9\\A\\5\\4\\6\\7\\0\\3"},{1:"hb",2:"\\N\\n\\6M\\6s\\7S\\7a\\q\\t\\s\\a\\0\\r"},{1:"he",2:"\\N\\n\\3g\\3K\\5\\4\\0\\3"},{1:"hg",2:"\\N\\n\\D\\15\\1T\\l\\5\\4\\6\\7\\0\\3"},{1:"hh",2:"\\N\\n\\17\\T\\5\\4\\6\\7\\0\\3"},{1:"hi",2:"\\N\\n\\n\\2E\\5\\4\\6\\7\\0\\3"},{1:"hj",2:"\\N\\n\\z\\1q\\I\\P\\5\\4\\6\\7\\0\\3"},{1:"hk",2:"\\N\\n\\hl\\7R\\5\\4\\0\\3"},{1:"hm",2:"\\N\\n\\Z\\43\\5\\4\\6\\7\\0\\3"},{1:"hn",2:"\\N\\n\\6T\\C\\5\\4\\6\\7\\0\\3"},{1:"hp",2:"\\N\\n\\55\\37\\5\\4\\6\\7\\0\\3"},{1:"hr",2:"\\N\\n\\4q\\2D\\5\\4\\6\\7\\0\\3"},{1:"hs",2:"\\N\\n\\9\\S\\21\\7\\5\\4\\0\\3"},{1:"ht",2:"\\N\\n\\19\\4g\\5\\4\\6\\7\\0\\3"},{1:"hu",2:"\\N\\n\\9\\A\\U\\H\\1o\\5\\4\\0\\3"},{1:"hv",2:"\\N\\n\\a\\0\\6\\7\\5\\4\\0\\3"},{1:"hw",2:"\\N\\n\\L\\J\\5\\4\\6\\7\\0\\3"},{1:"hx",2:"\\N\\n\\hy\\1J\\5\\4\\6\\7\\0\\3"},{1:"hz",2:"\\N\\n\\w\\6Y\\5\\4\\6\\7\\0\\3"},{1:"hB",2:"\\N\\n\\G\\5h\\5\\4\\6\\7\\0\\3"},{1:"hC",2:"\\N\\n\\z\\3c\\S\\7\\5\\4\\0\\3"},{1:"hD",2:"\\N\\n\\w\\C\\5\\4\\6\\7\\0\\3"},{1:"hE",2:"\\N\\n\\9\\5g\\1T\\l\\5\\4\\0\\3"},{1:"hF",2:"\\N\\n\\1r\\1f\\5\\4\\0\\3"},{1:"hG",2:"\\N\\n\\3K\\38\\5\\4\\6\\7\\0\\3"},{1:"hH",2:"\\N\\n\\Z\\1g\\5\\4\\6\\7\\0\\3"},{1:"hI",2:"\\b\\u\\8\\0"},{1:"hJ",2:"\\1b\\d\\8\\0"},{1:"hL",2:"\\i\\b\\8\\0"},{1:"hM",2:"\\b\\u\\1g\\28\\1g\\F\\8\\0"},{1:"hN",2:"\\b\\u\\l\\9\\8\\0"},{1:"hO",2:"\\e\\1b\\a\\6\\8\\0"},{1:"87",2:"\\w\\H\\49\\4\\8\\0"},{1:"hP",2:"\\b\\u\\9\\4\\8\\0"},{1:"hQ",2:"\\2V\\d\\8\\0"},{1:"hS",2:"\\b\\u\\2v\\z\\8\\0"},{1:"hT",2:"\\x\\n\\8\\0"},{1:"hU",2:"\\e\\b\\8\\0"},{1:"hV",2:"\\b\\u\\J\\4\\8\\0"},{1:"hW",2:"\\e\\1b\\8\\0"},{1:"hX",2:"\\b\\u\\I\\P\\9\\v\\8\\0"},{1:"hY",2:"\\b\\T\\8\\0"},{1:"hZ",2:"\\5a\\D\\9\\0\\3"},{1:"i1",2:"\\b\\u\\L\\4\\8\\0"},{1:"i2",2:"\\b\\u\\k\\a\\8\\0"},{1:"i3",2:"\\3P\\d\\k\\0\\3"},{1:"i5",2:"\\b\\u\\w\\k\\Y\\8\\0"},{1:"i6",2:"\\w\\H\\Y\\a\\8\\0"},{1:"i7",2:"\\b\\u\\f\\g\\8\\0"},{1:"i8",2:"\\e\\1b\\f\\g\\8\\0"},{1:"i9",2:"\\2L\\6l\\f\\g\\0\\3"},{1:"ic",2:"\\5a\\D\\f\\g\\0\\3"},{1:"id",2:"\\b\\u\\O\\y\\8\\0"},{1:"ie",2:"\\e\\1b\\16\\1w\\0\\3"},{1:"if",2:"\\b\\u\\1r\\1f\\0\\3"},{1:"ig",2:"\\b\\u\\S\\7\\0\\3"},{1:"ih",2:"\\1b\\d\\a\\6\\0\\3"},{1:"ii",2:"\\2V\\ij\\l\\9\\0\\3"},{1:"ik",2:"\\2L\\6l\\9\\0\\3"},{1:"il",2:"\\2V\\d\\9\\0\\3"},{1:"im",2:"\\3x\\d\\8\\0"},{1:"io",2:"\\20\\e\\0\\3"},{1:"ip",2:"\\b\\u\\9\\v\\0\\3"},{1:"iq",2:"\\b\\u\\5u\\2Z\\0\\3"},{1:"is",2:"\\b\\u\\it\\1O\\0\\3"},{1:"iu",2:"\\e\\1b\\l\\9\\0\\3"},{1:"iv",2:"\\2L\\n\\9\\0\\3"},{1:"iw",2:"\\3P\\d\\9\\v\\0\\3"},{1:"ix",2:"\\b\\u\\3V\\4T\\5b\\1f\\f\\g\\0\\3"},{1:"iy",2:"\\b\\T\\l\\9\\0\\3"},{1:"iz",2:"\\b\\u\\iA\\J\\16\\1W\\0\\3"},{1:"iB",2:"\\i\\b\\8\\0\\1X\\6t\\0\\3"},{1:"iD",2:"\\2W\\d\\0\\3"},{1:"iF",2:"\\3z\\39\\2N\\p\\0\\3"},{1:"iI",2:"\\1B\\4A\\a\\6\\0\\3"},{1:"iK",2:"\\w\\H\\49\\4\\8\\0\\3P\\n\\0\\3"},{1:"iL",2:"\\b\\u\\8\\0\\1B\\4A\\0\\3"},{1:"iM",2:"\\b\\u\\l\\9\\8\\0\\iN\\1B\\0\\3"},{1:"iO",2:"\\b\\u\\1g\\28\\1g\\F\\8\\0\\1B\\D\\0\\3"},{1:"iP",2:"\\w\\H\\2j\\2l\\8\\0\\b\\o\\0\\3"},{1:"iQ",2:"\\b\\u\\l\\9\\8\\0\\2W\\d\\a\\6\\0\\3"},{1:"iR",2:"\\b\\u\\f\\g\\8\\0\\2W\\d\\0\\3"},{1:"iS",2:"\\b\\u\\9\\4\\8\\0\\7x\\e\\0\\3"},{1:"iU",2:"\\b\\u\\f\\g\\8\\0\\w\\h\\0\\3"},{1:"iV",2:"\\b\\u\\k\\a\\8\\0\\38\\2D\\0\\3"},{1:"iW",2:"\\b\\u\\w\\k\\Y\\8\\0\\7y\\J\\0\\3"},{1:"iY",2:"\\b\\u\\I\\P\\9\\v\\8\\0\\1p\\e\\0\\3"},{1:"iZ",2:"\\1b\\d\\8\\0\\14\\5w\\0\\3"},{1:"j0",2:"\\1b\\d\\8\\0\\29\\2d\\6\\7\\0\\3"},{1:"j1",2:"\\1b\\d\\a\\6\\0\\3\\F\\4h\\0\\3"},{1:"j2",2:"\\e\\1b\\8\\0\\u\\e\\0\\3"},{1:"j3",2:"\\3x\\d\\8\\0\\o\\4A\\0\\3"},{1:"j4",2:"\\e\\1b\\f\\g\\8\\0\\a\\14\\0\\3"},{1:"j5",2:"\\b\\u\\2v\\z\\8\\0\\T\\2D\\0\\3"},{1:"j6",2:"\\b\\u\\O\\y\\8\\0\\4W\\j\\0\\3"},{1:"j8",2:"\\e\\1b\\a\\6\\8\\0\\1b\\d\\l\\9\\0\\3"},{1:"j9",2:"\\2V\\d\\8\\0\\6f\\1J\\0\\3"},{1:"jb",2:"\\b\\T\\8\\0\\4t\\J\\0\\3"},{1:"jc",2:"\\b\\u\\5u\\2Z\\0\\3\\1B\\5u\\0\\3"},{1:"jd",2:"\\4B\\6a\\0\\3"},{1:"jg",2:"\\e\\1b\\3k\\3H\\f\\g\\0\\3"},{1:"jh",2:"\\c\\17\\22\\2H\\7x\\8\\0"},{1:"ji",2:"\\2G\\j\\jj\\68\\8\\0"},{1:"jl",2:"\\Z\\3m\\2h\\2D\\5\\4\\6\\7\\0\\3"},{1:"jo",2:"\\3z\\39\\5\\4\\6\\7\\0\\3"},{1:"jp",2:"\\e\\1b\\G\\25\\5\\4\\6\\7\\0\\3"},{1:"jq",2:"\\b\\u\\9\\4\\5\\4\\6\\7\\0\\3"},{1:"jr",2:"\\e\\1b\\9\\v\\5\\4\\6\\7\\0\\3"},{1:"js",2:"\\1b\\d\\9\\S\\21\\7\\5\\4\\6\\7\\0\\3"},{1:"jt",2:"\\1I\\1d\\4H\\5\\4\\6\\7\\0\\3"},{1:"jw",2:"\\4I\\e\\15\\q\\t\\s\\a\\0\\r"},{1:"jy",2:"\\b\\T\\5\\4\\8\\0"},{1:"jz",2:"\\1b\\d\\5\\4\\8\\0"},{1:"jA",2:"\\2q\\5y\\5\\4\\9\\0\\3"},{1:"jC",2:"\\3x\\d\\15\\5\\4\\8\\0"},{1:"jD",2:"\\1I\\1d\\4H\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"jE",2:"\\e\\1b\\y\\V\\5\\4\\6\\7\\0\\3"},{1:"jF",2:"\\4J\\d\\5\\4\\6\\7\\0\\3"},{1:"jH",2:"\\jI\\p\\5\\4\\6\\7\\0\\3"},{1:"jJ",2:"\\2W\\d\\5\\4\\6\\7\\0\\3"},{1:"jK",2:"\\2V\\d\\I\\P\\5\\4\\6\\7\\0\\3"},{1:"jL",2:"\\e\\1b\\3D\\2t\\5\\4\\6\\7\\0\\3"},{1:"jM",2:"\\e\\1b\\n\\2E\\5\\4\\6\\7\\0\\3"},{1:"jN",2:"\\29\\F\\5\\4\\6\\7\\0\\3"},{1:"jO",2:"\\3z\\39\\a\\6\\5\\4\\0\\3"},{1:"jP",2:"\\5a\\D\\2a\\1m\\5\\4\\6\\7\\0\\3"},{1:"jQ",2:"\\3x\\d\\3d\\3j\\3N\\27\\5\\4\\6\\7\\0\\3"},{1:"jR",2:"\\b\\T\\a\\6\\5\\4\\0\\3"},{1:"jS",2:"\\1b\\d\\y\\V\\5\\4\\6\\7\\0\\3"},{1:"jT",2:"\\1b\\d\\9\\4\\5\\4\\6\\7\\0\\3"},{1:"jU",2:"\\1b\\d\\81\\5Z\\I\\P\\5\\4\\6\\7\\0\\3"},{1:"jX",2:"\\1b\\d\\2a\\1m\\5\\4\\6\\7\\0\\3"},{1:"jY",2:"\\3z\\39\\A\\4\\5\\4\\6\\7\\0\\3"},{1:"jZ",2:"\\b\\T\\1g\\2i\\5\\4\\6\\7\\0\\3"},{1:"k0",2:"\\b\\u\\17\\T\\5\\4\\6\\7\\0\\3"},{1:"k1",2:"\\2L\\m\\I\\P\\5\\4\\6\\7\\0\\3"},{1:"k2",2:"\\e\\1b\\L\\4K\\a\\6\\5\\4\\0\\3"},{1:"k4",2:"\\2V\\d\\3X\\3U\\2X\\2R\\5\\4\\6\\7\\0\\3"},{1:"k7",2:"\\1b\\d\\L\\4\\5\\4\\6\\7\\0\\3"},{1:"k8",2:"\\1b\\d\\9\\4\\5c\\4o\\5\\4\\6\\7\\0\\3"},{1:"ka",2:"\\2N\\p\\3W\\6N\\5\\4\\6\\7\\0\\3"},{1:"kc",2:"\\kd\\2F\\5\\4\\6\\7\\0\\3"},{1:"kf",2:"\\b\\u\\a\\6\\5\\4\\0\\3"},{1:"kg",2:"\\5w\\1J\\5\\4\\6\\7\\0\\3"},{1:"kh",2:"\\ki\\j\\5\\4\\6\\7\\0\\3"},{1:"kj",2:"\\3z\\39\\b\\2m\\5\\4\\6\\7\\0\\3"},{1:"kk",2:"\\e\\b\\3c\\3a\\S\\7\\5\\4\\0\\3"},{1:"kl",2:"\\1B\\6P\\5\\4\\6\\7\\0\\3"},{1:"kn",2:"\\2V\\d\\2w\\9\\5\\4\\6\\7\\0\\3"},{1:"ko",2:"\\2V\\d\\9\\v\\5\\4\\6\\7\\0\\3"},{1:"kp",2:"\\e\\1b\\L\\J\\5\\4\\6\\7\\0\\3"},{1:"kq",2:"\\e\\1b\\3C\\3b\\Y\\3b\\5\\4\\6\\7\\0\\3"},{1:"ks",2:"\\G\\i\\5\\4\\6\\7\\0\\3"},{1:"kt",2:"\\b\\u\\1U\\2A\\5\\4\\6\\7\\0\\3"},{1:"ku",2:"\\3P\\d\\9\\4\\5\\4\\6\\7\\0\\3"},{1:"kv",2:"\\e\\1b\\I\\P\\5\\4\\6\\7\\0\\3"},{1:"kw",2:"\\4B\\6a\\5\\4\\6\\7\\0\\3"},{1:"kx",2:"\\b\\u\\I\\P\\5\\4\\6\\7\\0\\3"},{1:"ky",2:"\\e\\n\\5\\4\\6\\7\\0\\3"},{1:"kz",2:"\\2V\\d\\1R\\z\\5\\4\\6\\7\\0\\3"},{1:"kA",2:"\\e\\6l\\5\\4\\6\\7\\0\\3"},{1:"kB",2:"\\3z\\39\\D\\15\\5\\4\\6\\7\\0\\3"},{1:"kC",2:"\\3z\\39\\9\\S\\5\\4\\6\\7\\0\\3"},{1:"kD",2:"\\1B\\j\\5\\4\\6\\7\\0\\3"},{1:"kE",2:"\\1b\\d\\3K\\5U\\5\\4\\6\\7\\0\\3"},{1:"kG",2:"\\5a\\D\\9\\4\\5\\4\\6\\7\\0\\3"},{1:"kH",2:"\\e\\1b\\O\\y\\5\\4\\6\\7\\0\\3"},{1:"kI",2:"\\3x\\d\\9\\4\\5\\4\\6\\7\\0\\3"},{1:"kJ",2:"\\1b\\d\\4H\\8\\55\\3Q\\a\\6\\5\\4\\0\\3"},{1:"kK",2:"\\2G\\j\\kL\\1d\\a\\6\\5\\4\\0\\3"},{1:"kM",2:"\\b\\u\\3a\\88\\S\\7\\5\\4\\0\\3"},{1:"kN",2:"\\e\\1b\\D\\15\\5\\4\\0\\3"},{1:"kO",2:"\\b\\u\\D\\15\\5\\4\\0\\3"},{1:"kP",2:"\\b\\u\\1R\\z\\5\\4\\6\\7\\0\\3"},{1:"kQ",2:"\\p\\b\\29\\2d\\6\\7\\0\\3"},{1:"kR",2:"\\p\\b\\I\\P\\0\\3"},{1:"kS",2:"\\p\\b\\17\\T\\9\\v\\0\\3"},{1:"kT",2:"\\13\\2q\\Z\\1S\\5\\4\\6\\7\\0\\3"},{1:"kU",2:"\\p\\b\\9\\4\\5\\4\\6\\7\\0\\3"},{1:"kV",2:"\\kW\\5y\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"kX",2:"\\p\\b\\I\\P\\5\\4\\6\\7\\0\\3"},{1:"kY",2:"\\p\\b\\45\\1Y\\q\\t\\s\\a\\0\\r"},{1:"kZ",2:"\\p\\b\\3h\\1B\\5\\4\\6\\7\\0\\3"},{1:"l0",2:"\\13\\2q\\1g\\28\\5\\4\\6\\7\\0\\3"},{1:"l1",2:"\\p\\b\\8\\l2\\2j\\2l\\5\\4\\6\\7\\0\\3"},{1:"l3",2:"\\6V\\d\\5\\4\\6\\7\\0\\3"},{1:"l5",2:"\\p\\b\\1U\\2A\\5\\4\\6\\7\\0\\3"},{1:"l6",2:"\\p\\b\\a\\6\\5\\4\\0\\3"},{1:"l7",2:"\\p\\b\\1m\\2H\\1R\\z\\5\\4\\6\\7\\0\\3"},{1:"l8",2:"\\p\\b\\17\\T\\5\\4\\6\\7\\0\\3"},{1:"l9",2:"\\p\\b\\A\\1Y\\5\\4\\6\\7\\0\\3"},{1:"la",2:"\\p\\b\\1r\\1f\\5\\4\\0\\3"},{1:"lb",2:"\\p\\b\\9\\v\\5\\4\\6\\7\\0\\3"},{1:"lc",2:"\\2Y\\ld\\5\\4\\0\\3"},{1:"le",2:"\\p\\b\\U\\V\\5\\4\\0\\3"},{1:"lf",2:"\\p\\b\\lg\\lh\\9\\v\\5\\4\\0\\3"},{1:"li",2:"\\5O\\E\\5\\4\\6\\7\\0\\3"},{1:"lk",2:"\\p\\b\\2K\\1F\\16\\1w\\5\\4\\0\\3"},{1:"ll",2:"\\13\\2q\\A\\V\\1L\\1Q\\5\\4\\6\\7\\0\\3"},{1:"lm",2:"\\p\\b\\3d\\3j\\1m\\2H\\5\\4\\6\\7\\0\\3"},{1:"ln",2:"\\p\\b\\2v\\z\\5\\4\\6\\7\\0\\3"},{1:"lo",2:"\\31\\5i\\5\\4\\6\\7\\0\\3"},{1:"lr",2:"\\ls\\d\\5\\4\\6\\7\\0\\3"},{1:"lt",2:"\\lu\\lv\\5\\4\\6\\7\\0\\3"},{1:"lw",2:"\\58\\1G\\71\\1g\\28\\9\\4\\5\\4\\6\\7\\0\\3"},{1:"ly",2:"\\13\\2q\\3d\\3j\\2Y\\2B\\5\\4\\6\\7\\0\\3"},{1:"lz",2:"\\p\\b\\S\\7\\5\\4\\0\\3"},{1:"lA",2:"\\p\\b\\1R\\z\\5\\4\\6\\7\\0\\3"},{1:"lB",2:"\\13\\2q\\5\\4\\6\\7\\0\\3"},{1:"lC",2:"\\6f\\1n\\5\\4\\6\\7\\0\\3"},{1:"lD",2:"\\74\\E\\5\\4\\6\\7\\0\\3"},{1:"lF",2:"\\2V\\1J\\5\\4\\6\\7\\0\\3"},{1:"lG",2:"\\13\\2q\\b\\1P\\5\\4\\0\\3"},{1:"lH",2:"\\75\\31\\5\\4\\0\\3"},{1:"lJ",2:"\\p\\b\\1n\\9\\5\\4\\6\\7\\0\\3"},{1:"lK",2:"\\p\\b\\D\\G\\5\\4\\6\\7\\0\\3"},{1:"lL",2:"\\p\\b\\1c\\2k\\1n\\9\\5\\4\\6\\7\\0\\3"},{1:"lM",2:"\\p\\b\\w\\k\\Y\\q\\t\\s\\a\\0\\r"},{1:"lN",2:"\\5O\\E\\k\\0\\q\\t\\s\\a\\0\\r"},{1:"lO",2:"\\p\\b\\Z\\1i\\5\\4\\0\\3"},{1:"lP",2:"\\31\\c\\Z\\1i\\5\\4\\6\\7\\0\\3"},{1:"lQ",2:"\\p\\b\\O\\y\\9\\4\\5\\4\\6\\7\\0\\3"},{1:"lR",2:"\\76\\E\\5\\4\\6\\7\\0\\3"},{1:"lT",2:"\\p\\b\\9\\S\\21\\7\\5\\4\\0\\3"},{1:"lU",2:"\\p\\b\\4J\\lV\\5\\4\\6\\7\\0\\3"},{1:"lW",2:"\\p\\b\\l\\9\\5\\4\\6\\7\\0\\3"},{1:"lX",2:"\\p\\b\\2p\\2P\\5\\4\\0\\3"},{1:"lY",2:"\\p\\b\\2z\\2y\\9\\v\\5\\4\\0\\3"},{1:"lZ",2:"\\13\\2q\\z\\1H\\5\\4\\6\\7\\0\\3"},{1:"m0",2:"\\p\\b\\1y\\22\\1y\\z\\5\\4\\6\\7\\0\\3"},{1:"m1",2:"\\p\\b\\26\\23\\2H\\m2\\5\\4\\6\\7\\0\\3"},{1:"m3",2:"\\p\\b\\q\\m4\\1U\\3l\\5\\4\\6\\7\\0\\3"},{1:"m5",2:"\\p\\b\\1U\\3l\\a\\6\\5\\4\\6\\7\\0\\3"},{1:"m6",2:"\\p\\b\\m\\5j\\6\\7\\5\\4\\0\\3"},{1:"m8",2:"\\p\\b\\z\\5M\\5\\4\\6\\7\\0\\3"},{1:"ma",2:"\\p\\b\\U\\H\\1o\\5\\4\\0\\3"},{1:"mb",2:"\\76\\E\\k\\0\\q\\t\\s\\a\\0\\r"},{1:"mc",2:"\\p\\b\\1A\\15\\5\\4\\0\\3"},{1:"md",2:"\\p\\b\\z\\1q\\a\\6\\5\\4\\0\\3"},{1:"me",2:"\\p\\b\\a\\6\\9\\4\\5\\4\\6\\7\\0\\3"},{1:"mf",2:"\\p\\b\\q\\1h\\mg\\1L\\1Q\\5\\4\\0\\3"},{1:"mh",2:"\\p\\b\\9\\A\\5\\4\\0\\3"},{1:"mi",2:"\\p\\b\\20\\4Y\\9\\4\\5\\4\\6\\7\\0\\3"},{1:"mj",2:"\\13\\2q\\2a\\1m\\5\\4\\0\\3"},{1:"mk",2:"\\p\\b\\3C\\3b\\Y\\3b\\5\\4\\0\\3"},{1:"ml",2:"\\p\\b\\7b\\5l\\1B\\mo\\5\\4\\6\\7\\0\\3"},{1:"mp",2:"\\p\\b\\1t\\22\\2z\\2y\\5\\4\\6\\7\\0\\3"},{1:"mq",2:"\\p\\b\\1C\\1z\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"mr",2:"\\31\\b\\1C\\1z\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"ms",2:"\\p\\b\\4M\\2S\\2C\\2E\\5\\4\\0\\3"},{1:"mt",2:"\\w\\j\\8\\0"},{1:"mu",2:"\\7d\\b\\8\\0"},{1:"mw",2:"\\5m\\3y\\8\\0"},{1:"my",2:"\\C\\b\\l\\9\\8\\0"},{1:"mz",2:"\\C\\b\\L\\4\\8\\0"},{1:"mA",2:"\\o\\i\\n\\2m\\8\\0"},{1:"mB",2:"\\o\\d\\k\\a\\8\\0"},{1:"mC",2:"\\o\\i\\k\\0\\3"},{1:"mD",2:"\\o\\d\\w\\k\\Y\\8\\0"},{1:"mE",2:"\\o\\i\\Y\\0\\3"},{1:"mF",2:"\\C\\b\\f\\g\\8\\0"},{1:"mG",2:"\\mH\\4F\\0\\3"},{1:"mI",2:"\\5n\\d\\0\\3"},{1:"mK",2:"\\mL\\j\\f\\g\\0\\3"},{1:"mM",2:"\\4p\\b\\f\\g\\0\\3"},{1:"mN",2:"\\7k\\K\\0\\3"},{1:"mP",2:"\\3w\\29\\0\\3"},{1:"mQ",2:"\\o\\d\\1r\\1f\\0\\3"},{1:"mR",2:"\\o\\d\\21\\7\\0\\3"},{1:"mS",2:"\\7l\\n\\33\\2O\\0\\3"},{1:"mU",2:"\\o\\i\\6\\7\\f\\g\\0\\3"},{1:"mV",2:"\\5o\\5p\\8\\0"},{1:"mY",2:"\\o\\i\\O\\y\\8\\0"},{1:"mZ",2:"\\o\\i\\4d\\1d\\0\\3"},{1:"n0",2:"\\o\\d\\8\\0"},{1:"n1",2:"\\o\\d\\1g\\n\\0\\3"},{1:"n2",2:"\\o\\i\\16\\1w\\0\\3"},{1:"n3",2:"\\n4\\n5\\L\\4\\9\\v\\0\\3"},{1:"n6",2:"\\n7\\n8\\8\\0"},{1:"n9",2:"\\o\\i\\1B\\2Q\\0\\3"},{1:"na",2:"\\z\\1q\\a\\6\\8\\0\\w\\j\\0\\3"},{1:"nb",2:"\\o\\i\\1c\\2k\\1n\\9\\0\\3"},{1:"nc",2:"\\i\\5L\\l\\9\\0\\3"},{1:"ne",2:"\\o\\i\\9\\4\\8\\0"},{1:"nf",2:"\\o\\i\\U\\1o\\U\\V\\8\\0"},{1:"ng",2:"\\7p\\j\\a\\0\\6\\7\\0\\3"},{1:"ni",2:"\\o\\i\\3Q\\5w\\0\\3"},{1:"nj",2:"\\b\\1P\\k\\a\\8\\0"},{1:"nk",2:"\\o\\i\\i\\2p\\0\\3"},{1:"nl",2:"\\C\\b\\l\\9\\8\\0\\o\\d\\0\\3"},{1:"nm",2:"\\o\\d\\8\\0\\C\\2p\\2p\\2P\\0\\3"},{1:"nn",2:"\\w\\j\\8\\0\\b\\1P\\0\\3"},{1:"no",2:"\\o\\i\\U\\1o\\U\\V\\8\\0\\b\\H\\A\\0\\3"},{1:"np",2:"\\o\\i\\O\\y\\8\\0\\C\\A\\0\\3"},{1:"nq",2:"\\o\\i\\n\\2m\\8\\0\\nr\\1B\\0\\3"},{1:"ns",2:"\\C\\b\\L\\4\\8\\0\\3v\\e\\0\\3"},{1:"nt",2:"\\o\\i\\6\\7\\f\\g\\0\\3\\F\\x\\0\\3"},{1:"nu",2:"\\h\\u\\f\\g\\8\\0\\3v\\n\\nv\\r"},{1:"nw",2:"\\o\\i\\9\\4\\8\\0\\C\\4q\\0\\3"},{1:"nx",2:"\\o\\d\\8\\0\\4V\\5r\\0\\3"},{1:"nz",2:"\\o\\d\\A\\0\\3"},{1:"nA",2:"\\h\\u\\l\\9\\8\\0\\3v\\n\\0\\3"},{1:"nB",2:"\\1t\\J\\8\\0\\3v\\n\\0\\3"},{1:"nC",2:"\\o\\d\\9\\A\\0\\3"},{1:"nD",2:"\\o\\i\\a\\6\\0\\3"},{1:"nE",2:"\\o\\i\\l\\9\\0\\3"},{1:"nF",2:"\\i\\5L\\l\\9\\0\\3\\D\\15\\0\\3"},{1:"nG",2:"\\w\\j\\8\\0\\11\\C\\0\\3"},{1:"nH",2:"\\o\\i\\3k\\3H\\f\\g\\0\\3"},{1:"nI",2:"\\b\\1P\\a\\6\\8\\0"},{1:"nJ",2:"\\nK\\4H\\w\\14\\8\\0\\4a\\5o\\5p\\4c"},{1:"nL",2:"\\3S\\1J\\5\\4\\6\\7\\0\\3"},{1:"nM",2:"\\o\\i\\2w\\9\\5\\4\\6\\7\\0\\3"},{1:"nN",2:"\\o\\i\\17\\T\\5\\4\\6\\7\\0\\3"},{1:"nO",2:"\\o\\i\\1y\\22\\z\\1H\\5\\4\\6\\7\\0\\3"},{1:"nP",2:"\\nQ\\5m\\5\\4\\6\\7\\0\\3"},{1:"nR",2:"\\5o\\5p\\5\\4\\6\\7\\0\\3"},{1:"nS",2:"\\Z\\3m\\b\\C\\9\\A\\0\\3"},{1:"nT",2:"\\nU\\4q\\C\\3D\\0\\3"},{1:"nV",2:"\\o\\d\\Z\\1g\\5\\4\\6\\7\\0\\3"},{1:"nW",2:"\\o\\d\\nX\\nY\\5\\4\\6\\7\\0\\3"},{1:"nZ",2:"\\o\\i\\4V\\j\\5\\4\\6\\7\\0\\3"},{1:"o0",2:"\\o\\i\\L\\9\\A\\5\\4\\6\\7\\0\\3"},{1:"o1",2:"\\o\\i\\11\\m\\5\\4\\6\\7\\0\\3"},{1:"o2",2:"\\7p\\j\\5\\4\\6\\7\\0\\3"},{1:"o3",2:"\\o\\i\\a\\0\\6\\7\\5\\4\\0\\3"},{1:"o4",2:"\\o\\i\\3C\\3b\\Y\\3b\\5\\4\\0\\3"},{1:"o5",2:"\\o\\d\\38\\8\\5\\4\\6\\7\\0\\3"},{1:"o6",2:"\\3v\\n\\S\\7\\5\\4\\0\\3"},{1:"o7",2:"\\o\\i\\3g\\1S\\5\\4\\0\\3"},{1:"o8",2:"\\o\\i\\1r\\1f\\5\\4\\6\\7\\0\\3"},{1:"o9",2:"\\o\\i\\5\\4\\6\\7\\0\\3"},{1:"oa",2:"\\o\\i\\G\\24\\5\\4\\6\\7\\0\\3"},{1:"ob",2:"\\o\\i\\48\\1q\\5\\4\\6\\7\\0\\3"},{1:"oc",2:"\\o\\i\\1R\\z\\5\\4\\6\\7\\0\\3"},{1:"od",2:"\\o\\i\\4p\\b\\5\\4\\6\\7\\0\\3"},{1:"oe",2:"\\5m\\of\\5\\4\\6\\7\\0\\3"},{1:"og",2:"\\oh\\3Y\\5\\4\\6\\7\\0\\3"},{1:"oi",2:"\\E\\e\\5\\4\\6\\7\\0\\3"},{1:"oj",2:"\\x\\27\\5\\4\\6\\7\\0\\3"},{1:"ok",2:"\\o\\i\\2v\\z\\5\\4\\6\\7\\0\\3"},{1:"ol",2:"\\5m\\3y\\5\\4\\6\\7\\0\\3"},{1:"om",2:"\\on\\E\\5\\4\\6\\7\\0\\3"},{1:"oo",2:"\\5o\\5p\\I\\P\\5\\4\\6\\7\\0\\3"},{1:"op",2:"\\6o\\43\\5\\4\\6\\7\\0\\3"},{1:"oq",2:"\\o\\i\\9\\V\\5\\4\\6\\7\\0\\3"},{1:"or",2:"\\o\\i\\2K\\1F\\16\\1w\\5\\4\\0\\3"},{1:"os",2:"\\o\\i\\3n\\3a\\5K\\S\\5\\4\\0\\3"},{1:"ou",2:"\\o\\i\\5d\\U\\1o\\S\\7\\5\\4\\0\\3"},{1:"ov",2:"\\o\\i\\14\\S\\5\\4\\0\\3"},{1:"ow",2:"\\o\\d\\1r\\1f\\5\\4\\6\\7\\0\\3"},{1:"ox",2:"\\o\\d\\9\\v\\6\\7\\5\\4\\0\\3"},{1:"oy",2:"\\w\\j\\oz\\oA\\5\\4\\6\\7\\0\\3"},{1:"oB",2:"\\e\\2f\\5\\4\\6\\7\\0\\3"},{1:"oD",2:"\\oE\\oF\\5\\4\\6\\7\\0\\3"},{1:"oG",2:"\\3v\\n\\D\\15\\5\\4\\6\\7\\0\\3"},{1:"oH",2:"\\o\\d\\3o\\U\\y\\19\\5\\4\\6\\7\\0\\3"},{1:"oJ",2:"\\o\\d\\b\\2m\\l\\9\\5\\4\\0\\3"},{1:"oK",2:"\\o\\d\\a\\6\\5\\4\\6\\7\\0\\3"},{1:"oL",2:"\\5n\\d\\y\\19\\5\\4\\6\\7\\0\\3"},{1:"oM",2:"\\o\\i\\9\\A\\5\\4\\0\\3"},{1:"oN",2:"\\7k\\K\\k\\0\\q\\t\\s\\a\\0\\r"},{1:"oO",2:"\\o\\d\\26\\23\\I\\P\\9\\v\\5\\4\\6\\7\\0\\3"},{1:"oP",2:"\\o\\i\\l\\9\\5\\4\\0\\3"},{1:"oQ",2:"\\o\\d\\C\\b\\A\\V\\5\\4\\0\\3"},{1:"oR",2:"\\o\\d\\C\\4q\\a\\6\\5\\4\\0\\3"},{1:"oS",2:"\\o\\d\\D\\15\\5\\4\\0\\3"},{1:"oT",2:"\\o\\i\\9\\v\\5\\4\\6\\7\\0\\3"},{1:"oU",2:"\\o\\d\\1U\\3l\\5\\4\\6\\7\\0\\3"},{1:"oV",2:"\\o\\i\\a\\V\\5\\4\\0\\3"},{1:"oW",2:"\\o\\d\\a\\6\\V\\47\\5\\4\\0\\3"},{1:"oX",2:"\\w\\j\\5\\4\\6\\7\\0\\3"},{1:"oY",2:"\\o\\d\\3v\\e\\5\\4\\6\\7\\0\\3"},{1:"oZ",2:"\\o\\d\\4V\\5r\\5\\4\\0\\3"},{1:"p0",2:"\\o\\i\\14\\l\\5\\4\\0\\3"},{1:"p1",2:"\\o\\d\\D\\G\\5\\4\\0\\3"},{1:"p2",2:"\\i\\5L\\5\\4\\6\\7\\0\\3"},{1:"p3",2:"\\o\\i\\b\\1P\\5\\4\\0\\3"},{1:"p4",2:"\\o\\d\\C\\A\\5\\4\\0\\3"},{1:"p5",2:"\\o\\d\\C\\2b\\5\\4\\0\\3"},{1:"p7",2:"\\o\\i\\3d\\3j\\2Y\\2B\\9\\v\\5\\4\\0\\3"},{1:"p8",2:"\\o\\i\\1e\\30\\5\\4\\0\\3"},{1:"p9",2:"\\o\\d\\i\\C\\5\\4\\0\\3"},{1:"pa",2:"\\o\\i\\3W\\11\\a\\6\\5\\4\\0\\3"},{1:"pb",2:"\\o\\i\\7c\\7F\\3B\\5s\\5\\4\\0\\3"},{1:"pc",2:"\\5n\\d\\2a\\1m\\5\\4\\6\\7\\0\\3"},{1:"pd",2:"\\o\\i\\I\\P\\9\\v\\5\\4\\0\\3"},{1:"pe",2:"\\o\\i\\1m\\4w\\9\\v\\5\\4\\0\\3"},{1:"pf",2:"\\5n\\d\\D\\15\\5\\4\\0\\3"},{1:"pg",2:"\\o\\i\\ph\\32\\5c\\5\\4\\0\\3"},{1:"pj",2:"\\34\\m\\6E\\3p\\67\\62\\q\\t\\s\\a\\0\\r"},{1:"pk",2:"\\o\\c\\8\\0"},{1:"pl",2:"\\o\\c\\a\\6\\8\\0"},{1:"pm",2:"\\32\\J\\z\\1q\\a\\6\\8\\0"},{1:"pn",2:"\\32\\J\\l\\9\\8\\0"},{1:"po",2:"\\o\\c\\k\\a\\8\\0"},{1:"pp",2:"\\pq\\e\\Z\\1i\\k\\0\\3"},{1:"pr",2:"\\o\\c\\w\\k\\Y\\8\\0"},{1:"ps",2:"\\32\\J\\k\\0\\3"},{1:"pt",2:"\\o\\c\\f\\g\\8\\0"},{1:"pu",2:"\\o\\c\\f\\g\\0\\3"},{1:"pv",2:"\\o\\c\\Z\\1i\\f\\g\\0\\3"},{1:"pw",2:"\\x\\5x\\0\\3"},{1:"py",2:"\\5I\\J\\f\\g\\0\\3"},{1:"pA",2:"\\o\\c\\S\\7\\0\\3"},{1:"pB",2:"\\o\\c\\Z\\1i\\8\\0"},{1:"pC",2:"\\7D\\5l\\0\\3"},{1:"pE",2:"\\7E\\d\\0\\3"},{1:"pG",2:"\\o\\c\\a\\6\\f\\g\\0\\3"},{1:"pH",2:"\\o\\c\\O\\y\\0\\3"},{1:"pI",2:"\\b\\B\\0\\3"},{1:"pJ",2:"\\pK\\d\\0\\3"},{1:"pL",2:"\\32\\J\\1g\\F\\9\\4\\0\\3"},{1:"pM",2:"\\32\\J\\1L\\1Q\\0\\3"},{1:"pN",2:"\\pO\\d\\0\\3"},{1:"pP",2:"\\o\\c\\16\\1W\\0\\3"},{1:"pQ",2:"\\h\\n\\S\\7\\24\\2Z\\0\\3"},{1:"pR",2:"\\o\\c\\8\\0\\3g\\3K\\14\\l\\0\\3"},{1:"pS",2:"\\o\\c\\a\\6\\8\\0\\pT\\j\\0\\3"},{1:"pU",2:"\\o\\c\\Z\\1i\\8\\0\\pV\\55\\p\\0\\3"},{1:"pW",2:"\\o\\c\\f\\g\\8\\0\\pX\\e\\0\\3"},{1:"pY",2:"\\o\\c\\f\\g\\0\\3\\f\\5c\\0\\3"},{1:"pZ",2:"\\o\\c\\w\\k\\Y\\8\\0\\q0\\5z\\3E\\11\\k\\Y\\0\\3"},{1:"q2",2:"\\32\\J\\z\\1q\\a\\6\\8\\0\\I\\P\\a\\6\\0\\3"},{1:"q3",2:"\\32\\J\\l\\9\\8\\0\\37\\14\\1T\\l\\0\\3"},{1:"q4",2:"\\o\\c\\U\\H\\1o\\0\\3"},{1:"q5",2:"\\h\\u\\1g\\28\\1g\\F\\8\\0\\h\\n\\0\\3"},{1:"q6",2:"\\o\\c\\1R\\z\\5\\4\\6\\7\\0\\3"},{1:"q7",2:"\\o\\c\\1r\\1f\\q\\t\\s\\a\\0\\r"},{1:"q8",2:"\\b\\B\\5\\4\\6\\7\\0\\3"},{1:"q9",2:"\\o\\c\\1y\\22\\z\\1H\\5\\4\\6\\7\\0\\3"},{1:"qa",2:"\\32\\J\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"qb",2:"\\o\\c\\5\\4\\6\\7\\0\\3"},{1:"qc",2:"\\5G\\d\\5\\4\\6\\7\\0\\3"},{1:"qe",2:"\\o\\c\\1m\\4w\\9\\v\\5\\4\\6\\7\\0\\3"},{1:"qf",2:"\\o\\c\\17\\T\\5\\4\\6\\7\\0\\3"},{1:"qg",2:"\\o\\c\\9\\4\\5\\4\\6\\7\\0\\3"},{1:"qh",2:"\\o\\c\\H\\3f\\A\\1Y\\5\\4\\6\\7\\0\\3"},{1:"qi",2:"\\o\\c\\L\\4\\5\\4\\6\\7\\0\\3"},{1:"qj",2:"\\5G\\d\\1U\\2A\\5\\4\\6\\7\\0\\3"},{1:"qk",2:"\\o\\c\\G\\24\\5\\4\\6\\7\\0\\3"},{1:"ql",2:"\\o\\c\\26\\23\\5\\4\\6\\7\\0\\3"},{1:"qm",2:"\\h\\n\\5\\4\\0\\3"},{1:"qn",2:"\\32\\J\\j\\1y\\5\\4\\0\\3"},{1:"qo",2:"\\o\\c\\y\\V\\5\\4\\6\\7\\0\\3"},{1:"qp",2:"\\o\\c\\9\\A\\5\\4\\6\\7\\0\\3"},{1:"qq",2:"\\o\\c\\5K\\S\\5\\4\\0\\3"},{1:"qr",2:"\\o\\c\\z\\1H\\5\\4\\6\\7\\0\\3"},{1:"qs",2:"\\o\\c\\D\\15\\5\\4\\0\\3"},{1:"qt",2:"\\o\\c\\5B\\C\\H\\3f\\5\\4\\0\\3"},{1:"qv",2:"\\5G\\d\\D\\15\\5\\4\\0\\3"},{1:"qw",2:"\\7D\\5l\\5\\4\\0\\3"},{1:"qx",2:"\\o\\c\\9\\v\\5\\4\\0\\3"},{1:"qy",2:"\\o\\c\\l\\9\\5\\4\\6\\7\\0\\3"},{1:"qz",2:"\\7E\\d\\5\\4\\0\\3"},{1:"qA",2:"\\o\\c\\y\\19\\5\\4\\0\\3"},{1:"qB",2:"\\o\\c\\1C\\1z\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"qC",2:"\\o\\c\\a\\6\\5\\4\\0\\3"},{1:"qD",2:"\\o\\c\\2a\\1m\\5\\4\\6\\7\\0\\3"},{1:"qE",2:"\\o\\c\\3Q\\6t\\H\\3f\\5\\4\\0\\3"},{1:"qF",2:"\\o\\c\\1B\\2Q\\5\\4\\6\\7\\0\\3"},{1:"qG",2:"\\o\\c\\w\\43\\5\\4\\0\\3"},{1:"qH",2:"\\n\\b\\8\\0"},{1:"qI",2:"\\7M\\d\\0\\3"},{1:"qK",2:"\\n\\b\\f\\g\\8\\0"},{1:"qL",2:"\\n\\b\\k\\0\\3"},{1:"qM",2:"\\n\\2T\\y\\19\\0\\3"},{1:"qN",2:"\\20\\3n\\0\\3"},{1:"qO",2:"\\n\\b\\5\\4\\6\\7\\0\\3"},{1:"qP",2:"\\20\\3n\\D\\15\\5\\4\\0\\3"},{1:"qQ",2:"\\n\\b\\2p\\2P\\5\\4\\6\\7\\0\\3"},{1:"qR",2:"\\n\\b\\1S\\1F\\5\\4\\0\\3"},{1:"qS",2:"\\n\\b\\U\\H\\1o\\5\\4\\0\\3"},{1:"qT",2:"\\7M\\2x\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"qU",2:"\\n\\b\\y\\V\\5\\4\\6\\7\\0\\3"},{1:"qV",2:"\\n\\b\\9\\A\\5\\4\\0\\3"},{1:"qW",2:"\\20\\3n\\1g\\28\\1L\\1Q\\5\\4\\0\\3"},{1:"qX",2:"\\n\\b\\a\\6\\5\\4\\0\\3"},{1:"qY",2:"\\20\\3n\\l\\9\\5\\4\\0\\3"},{1:"qZ",2:"\\X\\K\\8\\0"},{1:"r1",2:"\\X\\K\\2v\\z\\8\\0"},{1:"r2",2:"\\X\\K\\17\\T\\8\\0"},{1:"r3",2:"\\X\\K\\k\\a\\8\\0"},{1:"r4",2:"\\c\\b\\8\\0"},{1:"r5",2:"\\X\\K\\f\\g\\8\\0"},{1:"r6",2:"\\X\\K\\14\\l\\0\\3"},{1:"r7",2:"\\X\\K\\20\\35\\0\\3"},{1:"r9",2:"\\13\\e\\f\\g\\0\\3"},{1:"ra",2:"\\R\\M\\U\\H\\1o\\8\\0"},{1:"rc",2:"\\c\\b\\1S\\1F\\8\\0"},{1:"rd",2:"\\R\\M\\21\\7\\0\\3"},{1:"re",2:"\\X\\K\\a\\6\\0\\3"},{1:"rf",2:"\\X\\K\\l\\9\\8\\0"},{1:"rg",2:"\\X\\K\\9\\A\\8\\0"},{1:"rh",2:"\\X\\K\\9\\v\\0\\3"},{1:"ri",2:"\\X\\K\\8\\0\\D\\15\\a\\6\\0\\3"},{1:"rj",2:"\\X\\K\\16\\1W\\0\\3"},{1:"rk",2:"\\X\\K\\2C\\14\\a\\6\\0\\3"},{1:"rl",2:"\\R\\M\\U\\H\\1o\\8\\0\\X\\K\\b\\1P\\59\\57\\0\\3"},{1:"rm",2:"\\X\\K\\f\\g\\8\\0\\3o\\U\\A\\V\\0\\3"},{1:"rn",2:"\\X\\K\\9\\A\\8\\0\\2Q\\ro\\0\\3"},{1:"rp",2:"\\X\\K\\9\\A\\8\\0\\rq\\3E\\0\\3"},{1:"rr",2:"\\X\\K\\2v\\z\\8\\0\\rs\\T\\0\\3"},{1:"rt",2:"\\X\\K\\3k\\3H\\f\\g\\0\\3"},{1:"ru",2:"\\X\\K\\1g\\F\\5\\4\\6\\7\\0\\3"},{1:"rv",2:"\\X\\K\\z\\1H\\q\\t\\s\\a\\0\\r"},{1:"rw",2:"\\X\\K\\9\\4\\5\\4\\6\\7\\0\\3"},{1:"rx",2:"\\X\\K\\20\\35\\5\\4\\0\\3"},{1:"ry",2:"\\X\\K\\9\\V\\5\\4\\6\\7\\0\\3"},{1:"rz",2:"\\X\\K\\1R\\z\\5\\4\\6\\7\\0\\3"},{1:"rA",2:"\\X\\K\\z\\1q\\9\\v\\5\\4\\0\\3"},{1:"rB",2:"\\X\\K\\n\\3D\\5\\4\\6\\7\\0\\3"},{1:"rC",2:"\\X\\K\\I\\P\\6\\7\\5\\4\\0\\3"},{1:"rD",2:"\\X\\K\\2j\\2l\\5\\4\\0\\3"},{1:"rE",2:"\\X\\K\\D\\15\\1T\\l\\5\\4\\0\\3"},{1:"rF",2:"\\X\\K\\9\\v\\5\\4\\6\\7\\0\\3"},{1:"rG",2:"\\X\\K\\7m\\4n\\5C\\5\\4\\0\\3"},{1:"rI",2:"\\X\\K\\D\\15\\5\\4\\0\\3"},{1:"rJ",2:"\\X\\K\\1y\\22\\z\\1H\\5\\4\\6\\7\\0\\3"},{1:"rK",2:"\\X\\K\\9\\A\\5\\4\\0\\3"},{1:"rL",2:"\\X\\K\\29\\2d\\6\\7\\5\\4\\0\\3"},{1:"rM",2:"\\X\\K\\20\\35\\k\\Y\\q\\t\\s\\a\\0\\r"},{1:"rN",2:"\\X\\K\\k\\Y\\q\\t\\s\\a\\0\\r"},{1:"rO",2:"\\X\\K\\1e\\30\\5\\4\\6\\7\\0\\3"},{1:"rP",2:"\\X\\K\\O\\y\\5\\4\\0\\3"},{1:"rQ",2:"\\X\\K\\a\\3W\\5\\4\\0\\3"},{1:"rR",2:"\\X\\K\\G\\25\\9\\v\\5\\4\\0\\3"},{1:"rS",2:"\\X\\K\\z\\rT\\5\\4\\0\\3"},{1:"rU",2:"\\X\\K\\2U\\27\\5\\4\\0\\3"},{1:"rV",2:"\\X\\K\\A\\1Y\\5\\4\\0\\3"},{1:"rW",2:"\\X\\K\\17\\T\\5\\4\\0\\3"},{1:"rX",2:"\\X\\K\\1n\\9\\5\\4\\0\\3"},{1:"rY",2:"\\X\\K\\1L\\1Q\\5\\4\\0\\3"},{1:"rZ",2:"\\X\\K\\m\\5j\\6\\7\\5\\4\\0\\3"},{1:"s0",2:"\\X\\K\\34\\5A\\2i\\5X\\5\\4\\0\\3"},{1:"s2",2:"\\X\\K\\S\\7\\9\\v\\5\\4\\0\\3"},{1:"s3",2:"\\X\\K\\2w\\9\\5\\4\\0\\3"},{1:"s4",2:"\\X\\K\\z\\I\\5\\4\\0\\3"},{1:"s5",2:"\\X\\K\\y\\V\\5\\4\\0\\3"},{1:"s6",2:"\\X\\K\\1C\\1z\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"s7",2:"\\X\\K\\14\\1n\\S\\7\\5\\4\\0\\3"},{1:"s8",2:"\\X\\K\\2X\\2R\\9\\v\\5\\4\\0\\3"},{1:"s9",2:"\\R\\M\\8\\0"},{1:"sa",2:"\\c\\b\\17\\T\\8\\0"},{1:"sb",2:"\\z\\1q\\a\\6\\8\\0"},{1:"sc",2:"\\c\\b\\1c\\2k\\8\\0"},{1:"sd",2:"\\1X\\1A\\l\\9\\8\\0"},{1:"se",2:"\\c\\b\\a\\6\\8\\0"},{1:"sf",2:"\\1X\\1A\\I\\P\\9\\v\\8\\0"},{1:"sg",2:"\\R\\M\\l\\9\\0\\3"},{1:"sh",2:"\\c\\C\\8\\0"},{1:"si",2:"\\w\\H\\Z\\2d\\1g\\28\\7U\\3g\\0\\3"},{1:"sk",2:"\\R\\M\\L\\4\\8\\0"},{1:"sl",2:"\\c\\1M\\0\\3"},{1:"sn",2:"\\R\\M\\k\\a\\8\\0"},{1:"so",2:"\\1X\\1A\\w\\k\\Y\\8\\0"},{1:"sp",2:"\\M\\h\\k\\0\\3"},{1:"sq",2:"\\R\\M\\f\\g\\8\\0"},{1:"sr",2:"\\c\\C\\f\\g\\8\\0"},{1:"ss",2:"\\7X\\E\\f\\g\\0\\3"},{1:"su",2:"\\1x\\e\\f\\g\\0\\3"},{1:"sv",2:"\\4S\\80\\0\\3"},{1:"sy",2:"\\R\\M\\14\\l\\0\\3"},{1:"sz",2:"\\6h\\sA\\f\\g\\0\\3"},{1:"sB",2:"\\2O\\j\\f\\g\\0\\3"},{1:"sC",2:"\\c\\b\\O\\y\\8\\0"},{1:"sD",2:"\\1X\\1A\\1r\\1f\\0\\3"},{1:"sE",2:"\\R\\M\\33\\2O\\0\\3"},{1:"sF",2:"\\c\\b\\Z\\1i\\8\\0"},{1:"sG",2:"\\1X\\1A\\0\\3"},{1:"sH",2:"\\1X\\1A\\9\\4\\0\\3"},{1:"sI",2:"\\sJ\\sK\\sL\\0\\3"},{1:"sM",2:"\\R\\M\\1L\\1Q\\0\\3"},{1:"sN",2:"\\R\\M\\Z\\1i\\0\\3"},{1:"sO",2:"\\R\\M\\16\\1W\\0\\3"},{1:"sP",2:"\\1X\\1A\\i\\2p\\0\\3"},{1:"sQ",2:"\\z\\1q\\a\\6\\8\\0\\1X\\1A\\0\\3"},{1:"sR",2:"\\1X\\1A\\l\\9\\8\\0\\9\\v\\6\\7\\0\\3"},{1:"sS",2:"\\R\\M\\2j\\2l\\0\\3"},{1:"sT",2:"\\1X\\1A\\I\\P\\9\\v\\8\\0\\5t\\4t\\4R\\4e\\1T\\l\\0\\3"},{1:"sV",2:"\\1X\\1A\\14\\l\\0\\3"},{1:"sW",2:"\\R\\M\\9\\A\\0\\3"},{1:"sX",2:"\\R\\M\\U\\H\\1o\\8\\0\\1X\\1A\\0\\3"},{1:"sY",2:"\\1X\\1A\\k\\0\\3"},{1:"sZ",2:"\\R\\M\\9\\4\\a\\6\\0\\3"},{1:"t0",2:"\\R\\M\\8\\0\\51\\D\\0\\3"},{1:"t1",2:"\\c\\b\\O\\y\\8\\0\\F\\t2\\0\\3"},{1:"t3",2:"\\R\\M\\8\\0\\51\\e\\0\\3"},{1:"t4",2:"\\R\\M\\14\\1n\\S\\7\\0\\3"},{1:"t5",2:"\\c\\b\\a\\6\\8\\0\\D\\15\\0\\3"},{1:"t6",2:"\\c\\b\\17\\T\\8\\0\\82\\83\\0\\3"},{1:"t9",2:"\\1X\\1A\\f\\g\\0\\3"},{1:"ta",2:"\\R\\M\\z\\3c\\z\\3a\\0\\3"},{1:"tb",2:"\\1X\\1A\\3X\\3U\\q\\t\\s\\a\\0\\r"},{1:"tc",2:"\\Z\\3m\\R\\M\\F\\4Y\\0\\3"},{1:"td",2:"\\1X\\1A\\1g\\28\\5\\4\\6\\7\\0\\3"},{1:"te",2:"\\R\\M\\z\\1H\\5\\4\\6\\7\\0\\3"},{1:"tf",2:"\\1X\\1A\\5\\4\\6\\7\\0\\3"},{1:"tg",2:"\\R\\M\\1n\\9\\5\\4\\6\\7\\0\\3"},{1:"th",2:"\\R\\M\\1y\\22\\5\\4\\6\\7\\0\\3"},{1:"ti",2:"\\b\\tj\\5\\4\\6\\7\\0\\3"},{1:"tk",2:"\\1x\\e\\5\\4\\6\\7\\0\\3"},{1:"tl",2:"\\R\\M\\1g\\F\\5\\4\\6\\7\\0\\3"},{1:"tm",2:"\\R\\M\\2v\\z\\5\\4\\6\\7\\0\\3"},{1:"tn",2:"\\R\\M\\1R\\z\\5\\4\\6\\7\\0\\3"},{1:"to",2:"\\7X\\E\\5\\4\\6\\7\\0\\3"},{1:"tp",2:"\\R\\M\\17\\T\\5\\4\\6\\7\\0\\3"},{1:"tq",2:"\\R\\M\\9\\A\\5\\4\\6\\7\\0\\3"},{1:"tr",2:"\\R\\M\\9\\v\\5\\4\\6\\7\\0\\3"},{1:"ts",2:"\\R\\M\\G\\25\\5\\4\\6\\7\\0\\3"},{1:"tt",2:"\\2D\\d\\5\\4\\6\\7\\0\\3"},{1:"tu",2:"\\R\\M\\81\\5Z\\I\\P\\6\\7\\5\\4\\0\\3"},{1:"tv",2:"\\R\\M\\H\\3f\\tw\\tx\\5\\4\\0\\3"},{1:"ty",2:"\\1X\\1A\\L\\4\\a\\6\\5\\4\\0\\3"},{1:"tz",2:"\\4S\\80\\5\\4\\6\\7\\0\\3"},{1:"tA",2:"\\tB\\d\\5\\4\\6\\7\\0\\3"},{1:"tC",2:"\\tD\\j\\5\\4\\6\\7\\0\\3"},{1:"tE",2:"\\1X\\1A\\S\\7\\5\\4\\0\\3"},{1:"tF",2:"\\R\\M\\5\\4\\6\\7\\0\\3"},{1:"tG",2:"\\2O\\j\\5\\4\\6\\7\\0\\3"},{1:"tH",2:"\\tI\\m\\5\\4\\6\\7\\0\\3"},{1:"tJ",2:"\\R\\M\\A\\1Y\\5\\4\\0\\3"},{1:"tK",2:"\\R\\M\\2K\\1F\\16\\1w\\5\\4\\0\\3"},{1:"tL",2:"\\o\\m\\5\\4\\6\\7\\0\\3"},{1:"tM",2:"\\R\\M\\I\\P\\5\\4\\6\\7\\0\\3"},{1:"tN",2:"\\R\\M\\14\\1n\\2j\\2l\\5\\4\\0\\3"},{1:"tO",2:"\\R\\M\\C\\11\\26\\23\\5\\4\\0\\3"},{1:"tP",2:"\\R\\M\\1T\\l\\5\\4\\0\\3"},{1:"tQ",2:"\\R\\M\\S\\7\\5\\4\\0\\3"},{1:"tR",2:"\\R\\M\\w\\k\\Y\\q\\t\\s\\a\\0\\r"},{1:"tS",2:"\\R\\M\\a\\6\\5\\4\\0\\3"},{1:"tT",2:"\\R\\M\\14\\1n\\5C\\4\\5\\4\\0\\3"},{1:"tU",2:"\\R\\M\\O\\y\\5\\4\\0\\3"},{1:"tV",2:"\\R\\M\\D\\15\\5\\4\\0\\3"},{1:"tW",2:"\\R\\M\\26\\23\\5\\4\\0\\3"},{1:"tX",2:"\\R\\M\\1C\\1z\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"tY",2:"\\R\\M\\13\\e\\5\\4\\0\\3"},{1:"tZ",2:"\\R\\M\\20\\x\\5\\4\\0\\3"},{1:"u0",2:"\\M\\h\\1C\\1z\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"u1",2:"\\R\\M\\2a\\1m\\38\\7Q\\5\\4\\0\\3"},{1:"u2",2:"\\R\\M\\2z\\2y\\5\\4\\6\\7\\0\\3"},{1:"u3",2:"\\84\\w\\5\\4\\6\\7\\0\\3"},{1:"u5",2:"\\R\\M\\82\\83\\2z\\2y\\5\\4\\0\\3"},{1:"u6",2:"\\R\\M\\z\\1q\\1R\\u7\\5\\4\\6\\7\\0\\3"},{1:"u8",2:"\\R\\M\\14\\u9\\5\\4\\0\\3"},{1:"ua",2:"\\M\\b\\1C\\1z\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"ub",2:"\\R\\M\\2B\\l\\5\\4\\0\\3"},{1:"uc",2:"\\1X\\1A\\9\\4\\5\\4\\6\\7\\0\\3"},{1:"ud",2:"\\R\\M\\c\\b\\1g\\28\\5\\4\\0\\3"},{1:"ue",2:"\\1X\\1A\\9\\V\\5\\4\\6\\7\\0\\3"},{1:"uf",2:"\\R\\M\\29\\2d\\6\\7\\5\\4\\0\\3"},{1:"ug",2:"\\1s\\d\\8\\0"},{1:"ui",2:"\\1s\\d\\k\\a\\8\\0"},{1:"uj",2:"\\4O\\3i\\k\\0\\3"},{1:"um",2:"\\1s\\E\\w\\k\\0\\3"},{1:"un",2:"\\1s\\d\\f\\g\\8\\0"},{1:"uo",2:"\\4O\\3i\\f\\g\\0\\3"},{1:"up",2:"\\3O\\3L\\0\\3"},{1:"ur",2:"\\2o\\3i\\Z\\1i\\f\\g\\0\\3"},{1:"us",2:"\\m\\3S\\0\\3"},{1:"ut",2:"\\1s\\d\\9\\v\\29\\2d\\6\\7\\0\\3"},{1:"uu",2:"\\6L\\4E\\0\\3"},{1:"uw",2:"\\4b\\b\\Z\\1i\\f\\g\\0\\3"},{1:"uy",2:"\\1s\\d\\O\\y\\8\\0"},{1:"uz",2:"\\1s\\d\\Z\\1i\\8\\0"},{1:"uA",2:"\\1s\\E\\0\\3"},{1:"uB",2:"\\5P\\6u\\1y\\f\\g\\0\\3"},{1:"uD",2:"\\1s\\d\\A\\0\\3"},{1:"uE",2:"\\1s\\E\\w\\k\\0\\3\\uF\\uG\\0\\3"},{1:"uH",2:"\\1s\\d\\O\\y\\8\\0\\A\\1Y\\0\\3"},{1:"uI",2:"\\1s\\d\\8\\0\\a\\6\\0\\3"},{1:"uJ",2:"\\1s\\d\\8\\0\\2h\\1J\\0\\3"},{1:"uK",2:"\\1s\\d\\Z\\1i\\8\\0\\2C\\14\\a\\6\\0\\3"},{1:"uL",2:"\\1s\\d\\f\\g\\8\\0\\5Q\\uN\\0\\3"},{1:"uO",2:"\\4O\\3i\\k\\0\\3\\k\\0\\3J\\a\\6\\0\\3"},{1:"uP",2:"\\1s\\d\\k\\a\\8\\0\\uQ\\uR\\Z\\1i\\k\\Y\\0\\3"},{1:"uS",2:"\\1s\\d\\f\\g\\0\\3"},{1:"uT",2:"\\1s\\d\\l\\9\\0\\3"},{1:"uU",2:"\\4b\\b\\Z\\1i\\k\\0\\q\\t\\s\\a\\0\\r"},{1:"uV",2:"\\1s\\d\\16\\1w\\5\\4\\0\\3"},{1:"uW",2:"\\1s\\d\\17\\T\\5\\4\\6\\7\\0\\3"},{1:"uX",2:"\\1s\\d\\1g\\F\\5\\4\\6\\7\\0\\3"},{1:"uY",2:"\\1s\\d\\z\\1q\\I\\P\\5\\4\\6\\7\\0\\3"},{1:"uZ",2:"\\m\\3S\\5\\4\\6\\7\\0\\3"},{1:"v0",2:"\\4b\\i\\b\\Z\\1i\\5\\4\\6\\7\\0\\3"},{1:"v1",2:"\\4b\\b\\Z\\1i\\5\\4\\6\\7\\0\\3"},{1:"v2",2:"\\4O\\3i\\5\\4\\6\\7\\0\\3"},{1:"v3",2:"\\1s\\d\\D\\15\\5\\4\\0\\3"},{1:"v4",2:"\\1s\\d\\9\\4\\5\\4\\6\\7\\0\\3"},{1:"v5",2:"\\1s\\d\\z\\1H\\5\\4\\6\\7\\0\\3"},{1:"v6",2:"\\5P\\6u\\1y\\5\\4\\6\\7\\0\\3"},{1:"v7",2:"\\3O\\3L\\5\\4\\6\\7\\0\\3"},{1:"v8",2:"\\4b\\c\\b\\Z\\1i\\5\\4\\6\\7\\0\\3"},{1:"v9",2:"\\1s\\d\\2w\\9\\5\\4\\6\\7\\0\\3"},{1:"va",2:"\\4O\\3i\\k\\Y\\q\\t\\s\\a\\0\\r"},{1:"vb",2:"\\1s\\E\\2B\\l\\5\\4\\0\\3"},{1:"vc",2:"\\1s\\E\\5\\4\\6\\7\\0\\3"},{1:"vd",2:"\\5R\\5S\\5\\4\\6\\7\\0\\3"},{1:"vg",2:"\\1s\\d\\5\\4\\6\\7\\0\\3"},{1:"vh",2:"\\1s\\d\\vi\\C\\5\\4\\0\\3"},{1:"vj",2:"\\1s\\d\\9\\A\\5\\4\\0\\3"},{1:"vk",2:"\\1s\\E\\1C\\1z\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"vl",2:"\\3O\\3L\\1C\\1z\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"vm",2:"\\4b\\b\\Z\\1i\\1C\\1z\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"vn",2:"\\5R\\5S\\k\\0\\q\\t\\s\\a\\0\\r"},{1:"vo",2:"\\1s\\d\\G\\24\\5\\4\\6\\7\\0\\3"},{1:"vp",2:"\\5R\\5S\\1C\\1z\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"vq",2:"\\1s\\d\\L\\4\\5\\4\\0\\3"},{1:"vr",2:"\\1d\\b\\8\\0"},{1:"vs",2:"\\2G\\2h\\l\\9\\8\\0"},{1:"vt",2:"\\1d\\b\\L\\4\\8\\0"},{1:"vu",2:"\\c\\b\\J\\4\\8\\0"},{1:"vv",2:"\\2G\\2h\\k\\a\\8\\0"},{1:"vw",2:"\\8\\l\\8\\0"},{1:"vx",2:"\\1d\\b\\w\\k\\0\\3"},{1:"vy",2:"\\1d\\b\\f\\g\\8\\0"},{1:"vz",2:"\\vA\\T\\0\\3"},{1:"vB",2:"\\41\\6R\\f\\g\\0\\3"},{1:"vD",2:"\\5Z\\vE\\0\\3"},{1:"vF",2:"\\2Y\\j\\0\\3"},{1:"vG",2:"\\4W\\x\\0\\3"},{1:"vH",2:"\\1d\\b\\O\\y\\8\\0"},{1:"vI",2:"\\1d\\b\\S\\7\\0\\3"},{1:"vJ",2:"\\1d\\b\\Z\\1i\\8\\0"},{1:"vK",2:"\\5I\\6S\\f\\g\\0\\3"},{1:"vM",2:"\\5T\\5U\\f\\g\\0\\3"},{1:"vO",2:"\\1d\\b\\16\\1w\\0\\3"},{1:"vP",2:"\\2G\\2h\\0\\3"},{1:"vQ",2:"\\14\\j\\0\\3"},{1:"vR",2:"\\1d\\b\\y\\19\\1T\\l\\0\\3"},{1:"vS",2:"\\1d\\b\\8\\0\\6U\\5x\\0\\3"},{1:"vU",2:"\\1d\\b\\8\\0\\1L\\1Q\\14\\1n\\0\\3"},{1:"vV",2:"\\2G\\2h\\l\\9\\8\\0\\10\\4Q\\0\\3"},{1:"vW",2:"\\1d\\b\\f\\g\\8\\0\\A\\0\\3"},{1:"vX",2:"\\1d\\b\\f\\g\\8\\0\\14\\l\\0\\3"},{1:"vY",2:"\\2G\\2h\\k\\a\\8\\0\\n\\27\\0\\3"},{1:"vZ",2:"\\1d\\b\\S\\7\\0\\3\\14\\C\\0\\3"},{1:"w0",2:"\\1d\\b\\9\\A\\0\\3"},{1:"w1",2:"\\6U\\c\\a\\6\\f\\g\\0\\3"},{1:"w2",2:"\\2G\\2h\\3h\\1B\\q\\t\\s\\a\\0\\r"},{1:"w3",2:"\\1d\\b\\H\\6B\\3N\\27\\5\\4\\0\\3"},{1:"w4",2:"\\1d\\b\\17\\T\\5\\4\\6\\7\\0\\3"},{1:"w5",2:"\\2G\\2h\\9\\4\\5\\4\\6\\7\\0\\3"},{1:"w6",2:"\\1d\\b\\L\\4\\5\\4\\6\\7\\0\\3"},{1:"w7",2:"\\1d\\b\\2K\\1F\\16\\1w\\5\\4\\0\\3"},{1:"w8",2:"\\1d\\b\\14\\1n\\S\\7\\5\\4\\0\\3"},{1:"w9",2:"\\1d\\b\\1r\\1f\\2i\\2S\\5\\4\\6\\7\\0\\3"},{1:"wa",2:"\\1d\\b\\a\\6\\I\\P\\5\\4\\0\\3"},{1:"wb",2:"\\c\\wc\\6s\\wd\\5\\4\\6\\7\\0\\3"},{1:"we",2:"\\2G\\2h\\S\\7\\5\\4\\0\\3"},{1:"wf",2:"\\5I\\6S\\L\\4\\5\\4\\6\\7\\0\\3"},{1:"wg",2:"\\1d\\b\\2U\\27\\5\\4\\6\\7\\0\\3"},{1:"wh",2:"\\1d\\b\\wi\\wj\\5f\\2H\\5\\4\\0\\3"},{1:"wl",2:"\\1d\\b\\H\\3p\\9\\4\\5\\4\\6\\7\\0\\3"},{1:"wm",2:"\\1d\\b\\1R\\z\\5\\4\\6\\7\\0\\3"},{1:"wn",2:"\\1d\\b\\J\\4\\5\\4\\6\\7\\0\\3"},{1:"wo",2:"\\1d\\b\\D\\15\\G\\24\\5\\4\\0\\3"},{1:"wp",2:"\\1d\\b\\9\\v\\5\\4\\0\\3"},{1:"wq",2:"\\41\\6R\\k\\0\\q\\t\\s\\a\\0\\r"},{1:"wr",2:"\\5T\\5U\\k\\Y\\q\\t\\s\\a\\0\\r"},{1:"ws",2:"\\2Y\\j\\w\\k\\Y\\q\\t\\s\\a\\0\\r"},{1:"wt",2:"\\5V\\e\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"wv",2:"\\1J\\6X\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"wx",2:"\\1d\\b\\11\\2o\\5\\4\\0\\3"},{1:"wy",2:"\\1d\\b\\39\\4\\5\\4\\6\\7\\0\\3"},{1:"wz",2:"\\1d\\b\\y\\V\\U\\2E\\5\\4\\0\\3"},{1:"wA",2:"\\1d\\b\\20\\wB\\5\\4\\6\\7\\0\\3"},{1:"wC",2:"\\1J\\6X\\5\\4\\0\\3"},{1:"wD",2:"\\1d\\b\\A\\1Y\\5\\4\\0\\3"},{1:"wE",2:"\\2G\\2h\\2a\\1m\\5\\4\\0\\3"},{1:"wF",2:"\\1d\\b\\26\\23\\5\\4\\6\\7\\0\\3"},{1:"wG",2:"\\1d\\b\\1L\\1Q\\5\\4\\0\\3"},{1:"wH",2:"\\4W\\x\\2a\\1m\\5\\4\\0\\3"},{1:"wI",2:"\\1d\\b\\U\\2E\\U\\1o\\5\\4\\0\\3"},{1:"wJ",2:"\\8\\l\\L\\J\\5\\4\\6\\7\\0\\3"},{1:"wK",2:"\\34\\m\\wL\\3p\\67\\62\\q\\t\\s\\a\\0\\r"},{1:"wM",2:"\\1d\\b\\O\\y\\5\\4\\0\\3"},{1:"wN",2:"\\c\\46\\8\\0"},{1:"wP",2:"\\c\\46\\Z\\1i\\8\\0"},{1:"wQ",2:"\\c\\46\\46\\k\\0\\3"},{1:"wR",2:"\\c\\46\\16\\1w\\q\\t\\s\\a\\0\\r"},{1:"wS",2:"\\6i\\wT\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"wU",2:"\\c\\46\\5\\4\\6\\7\\0\\3"},{1:"wV",2:"\\c\\h\\8\\0"},{1:"wW",2:"\\c\\m\\17\\T\\8\\0"},{1:"wX",2:"\\c\\h\\9\\4\\8\\0"},{1:"wY",2:"\\c\\m\\l\\9\\8\\0"},{1:"wZ",2:"\\c\\m\\z\\1q\\a\\6\\8\\0"},{1:"x0",2:"\\c\\m\\9\\4\\8\\0"},{1:"x1",2:"\\c\\m\\G\\25\\a\\6\\8\\0"},{1:"x2",2:"\\c\\m\\a\\6\\8\\0"},{1:"x3",2:"\\c\\m\\1c\\2k\\8\\0"},{1:"x4",2:"\\1N\\c\\a\\6\\8\\0"},{1:"x6",2:"\\c\\m\\9\\v\\8\\0"},{1:"x7",2:"\\13\\m\\8\\0"},{1:"x8",2:"\\c\\h\\L\\J\\a\\6\\8\\0"},{1:"x9",2:"\\1N\\c\\w\\k\\Y\\8\\0"},{1:"xa",2:"\\1N\\c\\f\\g\\8\\0"},{1:"xb",2:"\\4i\\m\\8\\0"},{1:"xc",2:"\\1N\\c\\l\\9\\0\\3"},{1:"xd",2:"\\69\\70\\14\\l\\0\\3"},{1:"xf",2:"\\5Y\\E\\f\\g\\0\\3"},{1:"xh",2:"\\72\\b\\f\\g\\0\\3"},{1:"xj",2:"\\c\\m\\U\\H\\1o\\8\\0"},{1:"xk",2:"\\c\\h\\1S\\1F\\8\\0"},{1:"xl",2:"\\c\\m\\1r\\1f\\0\\3"},{1:"xm",2:"\\c\\m\\33\\2O\\0\\3"},{1:"xn",2:"\\c\\m\\21\\7\\0\\3"},{1:"xo",2:"\\c\\m\\14\\l\\0\\3"},{1:"xp",2:"\\73\\J\\0\\3"},{1:"xr",2:"\\A\\3R\\0\\3"},{1:"xt",2:"\\m\\38\\0\\3"},{1:"xu",2:"\\c\\m\\3Q\\C\\0\\3"},{1:"xv",2:"\\c\\m\\O\\y\\0\\3"},{1:"xw",2:"\\c\\m\\2v\\z\\8\\0"},{1:"xx",2:"\\c\\m\\1g\\28\\0\\3"},{1:"xy",2:"\\c\\m\\k\\0\\3"},{1:"xz",2:"\\c\\m\\6T\\3n\\0\\3"},{1:"xA",2:"\\c\\m\\U\\2E\\0\\3"},{1:"xB",2:"\\c\\m\\59\\57\\0\\3"},{1:"xC",2:"\\c\\u\\0\\3"},{1:"xD",2:"\\c\\m\\55\\27\\0\\3"},{1:"xE",2:"\\1N\\c\\H\\3f\\A\\V\\0\\3"},{1:"xF",2:"\\1N\\c\\2X\\2R\\9\\v\\0\\3"},{1:"xG",2:"\\c\\m\\17\\T\\9\\v\\0\\3"},{1:"xH",2:"\\c\\m\\17\\T\\8\\0\\D\\15\\0\\3"},{1:"xI",2:"\\c\\h\\8\\0\\26\\23\\0\\3"},{1:"xJ",2:"\\c\\m\\G\\25\\a\\6\\8\\0\\C\\6o\\0\\3"},{1:"xK",2:"\\c\\m\\O\\y\\0\\3\\3g\\56\\0\\3"},{1:"xM",2:"\\1N\\c\\a\\6\\8\\0\\xN\\u\\0\\3"},{1:"xO",2:"\\c\\m\\9\\4\\8\\0\\h\\1P\\I\\P\\9\\v\\0\\3"},{1:"xP",2:"\\4i\\m\\8\\0\\c\\m\\3W\\11\\0\\3"},{1:"xQ",2:"\\c\\m\\z\\1q\\a\\6\\8\\0\\13\\m\\0\\3"},{1:"xR",2:"\\c\\h\\9\\4\\8\\0\\2h\\1J\\0\\3"},{1:"xS",2:"\\13\\m\\8\\0\\2o\\C\\0\\3"},{1:"xT",2:"\\c\\m\\l\\9\\8\\0\\q\\a\\0\\3"},{1:"xU",2:"\\c\\m\\a\\6\\8\\0\\q\\11\\0\\3"},{1:"xV",2:"\\1N\\c\\0\\xW\\f\\g\\0\\3"},{1:"xX",2:"\\1N\\c\\9\\4\\5\\4\\6\\7\\0\\3"},{1:"xY",2:"\\xZ\\y0\\5\\4\\6\\7\\0\\3"},{1:"y1",2:"\\c\\m\\z\\1H\\q\\t\\s\\a\\0\\r"},{1:"y2",2:"\\1N\\c\\2U\\27\\5\\4\\6\\7\\0\\3"},{1:"y3",2:"\\1N\\c\\H\\3p\\9\\4\\5\\4\\6\\7\\0\\3"},{1:"y4",2:"\\c\\m\\1g\\28\\5\\4\\6\\7\\0\\3"},{1:"y5",2:"\\1N\\c\\O\\y\\5\\4\\6\\7\\0\\3"},{1:"y6",2:"\\1N\\c\\17\\T\\5\\4\\6\\7\\0\\3"},{1:"y7",2:"\\1N\\c\\5\\4\\6\\7\\0\\3"},{1:"y8",2:"\\c\\m\\q\\11\\a\\6\\5\\4\\0\\3"},{1:"y9",2:"\\c\\m\\D\\15\\G\\24\\5\\4\\0\\3"},{1:"ya",2:"\\1N\\c\\1U\\3l\\9\\v\\5\\4\\6\\7\\0\\3"},{1:"yb",2:"\\69\\70\\5\\4\\6\\7\\0\\3"},{1:"yc",2:"\\1N\\c\\1g\\28\\5\\4\\6\\7\\0\\3"},{1:"yd",2:"\\1N\\c\\z\\1q\\I\\P\\5\\4\\6\\7\\0\\3"},{1:"ye",2:"\\1N\\c\\2v\\z\\5\\4\\6\\7\\0\\3"},{1:"yf",2:"\\c\\m\\n\\yg\\5\\4\\0\\3"},{1:"yh",2:"\\c\\m\\2z\\2y\\a\\6\\5\\4\\0\\3"},{1:"yi",2:"\\c\\m\\i\\1P\\3n\\2N\\5\\4\\6\\7\\0\\3"},{1:"yj",2:"\\1N\\c\\16\\1w\\5\\4\\0\\3"},{1:"yk",2:"\\1N\\c\\y\\19\\1T\\l\\5\\4\\6\\7\\0\\3"},{1:"yl",2:"\\c\\m\\1U\\3l\\5\\4\\6\\7\\0\\3"},{1:"ym",2:"\\5Y\\E\\5\\4\\6\\7\\0\\3"},{1:"yn",2:"\\c\\m\\5\\4\\6\\7\\0\\3"},{1:"yo",2:"\\A\\3R\\5\\4\\6\\7\\0\\3"},{1:"yp",2:"\\1a\\w\\5\\4\\6\\7\\0\\3"},{1:"yr",2:"\\4i\\m\\5\\4\\6\\7\\0\\3"},{1:"ys",2:"\\72\\b\\5\\4\\6\\7\\0\\3"},{1:"yt",2:"\\m\\38\\5\\4\\6\\7\\0\\3"},{1:"yu",2:"\\3O\\M\\5\\4\\6\\7\\0\\3"},{1:"yv",2:"\\1N\\c\\1e\\30\\5\\4\\0\\3"},{1:"yw",2:"\\1N\\c\\9\\A\\5\\4\\0\\3"},{1:"yx",2:"\\1N\\c\\z\\1q\\a\\6\\5\\4\\0\\3"},{1:"yy",2:"\\1N\\c\\1L\\1Q\\77\\78\\5\\4\\0\\3"},{1:"yB",2:"\\c\\m\\k\\0\\q\\t\\s\\a\\0\\r"},{1:"yC",2:"\\73\\J\\5\\4\\6\\7\\0\\3"},{1:"yD",2:"\\1N\\c\\S\\7\\5\\4\\0\\3"},{1:"yE",2:"\\2e\\d\\8\\0"},{1:"yF",2:"\\2e\\d\\l\\9\\8\\0"},{1:"yG",2:"\\2e\\d\\17\\T\\8\\0"},{1:"yH",2:"\\2r\\2n\\L\\4\\8\\0"},{1:"yK",2:"\\2r\\2n\\w\\k\\Y\\8\\0"},{1:"yL",2:"\\c\\h\\f\\g\\8\\0"},{1:"yM",2:"\\2e\\d\\D\\15\\0\\3"},{1:"yN",2:"\\64\\i\\0\\3"},{1:"yP",2:"\\F\\1y\\f\\g\\0\\3"},{1:"yQ",2:"\\x\\c\\0\\3"},{1:"yR",2:"\\2e\\d\\O\\y\\8\\0"},{1:"yS",2:"\\c\\h\\Z\\1i\\8\\0"},{1:"yT",2:"\\2r\\2n\\1S\\1F\\0\\3"},{1:"yU",2:"\\2r\\2n\\Z\\1i\\f\\g\\0\\3"},{1:"yV",2:"\\2e\\d\\14\\l\\0\\3"},{1:"yW",2:"\\2r\\2n\\k\\0\\3"},{1:"yX",2:"\\2e\\d\\9\\4\\0\\3"},{1:"yY",2:"\\c\\h\\f\\g\\8\\0\\56\\3g\\0\\3"},{1:"yZ",2:"\\2e\\d\\O\\y\\8\\0\\64\\4Q\\0\\3"},{1:"z0",2:"\\2e\\d\\O\\y\\8\\0\\13\\1e\\0\\3"},{1:"z1",2:"\\2e\\d\\17\\T\\8\\0\\37\\14\\0\\3"},{1:"z2",2:"\\2e\\d\\l\\9\\8\\0\\6\\7\\9\\v\\0\\3"},{1:"z3",2:"\\2e\\d\\1c\\1n\\5\\4\\6\\7\\0\\3"},{1:"z4",2:"\\64\\b\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"z5",2:"\\3Y\\c\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"z6",2:"\\2r\\2n\\G\\25\\5\\4\\6\\7\\0\\3"},{1:"z7",2:"\\4R\\2s\\5\\4\\6\\7\\0\\3"},{1:"z8",2:"\\2e\\d\\U\\1o\\5\\4\\0\\3"},{1:"z9",2:"\\2e\\d\\5\\4\\6\\7\\0\\3"},{1:"za",2:"\\2r\\2n\\16\\1W\\5\\4\\0\\3"},{1:"zb",2:"\\2r\\2n\\J\\4\\5\\4\\6\\7\\0\\3"},{1:"zc",2:"\\2r\\2n\\9\\4\\5\\4\\6\\7\\0\\3"},{1:"zd",2:"\\18\\65\\5\\4\\0\\3"},{1:"zf",2:"\\2r\\2n\\17\\T\\5\\4\\6\\7\\0\\3"},{1:"zg",2:"\\2e\\d\\3N\\27\\3d\\3j\\5\\4\\6\\7\\0\\3"},{1:"zh",2:"\\2r\\2n\\L\\4\\5\\4\\6\\7\\0\\3"},{1:"zi",2:"\\2r\\2n\\66\\4K\\9\\v\\5\\4\\6\\7\\0\\3"},{1:"zk",2:"\\2r\\2n\\61\\1U\\5\\4\\6\\7\\0\\3"},{1:"zl",2:"\\2r\\2n\\1R\\z\\5\\4\\6\\7\\0\\3"},{1:"zm",2:"\\2r\\2n\\7b\\5l\\3h\\1B\\5\\4\\6\\7\\0\\3"},{1:"zn",2:"\\4d\\5t\\49\\3h\\5\\4\\6\\7\\0\\3"},{1:"zo",2:"\\2r\\2n\\2a\\1m\\5\\4\\0\\3"},{1:"zp",2:"\\2e\\d\\a\\6\\5\\4\\0\\3"},{1:"zq",2:"\\K\\E\\5\\4\\6\\7\\0\\3"},{1:"zr",2:"\\3s\\2b\\26\\23\\5\\4\\0\\3"},{1:"zs",2:"\\1e\\n\\8\\0"},{1:"zt",2:"\\1e\\n\\f\\g\\8\\0"},{1:"zu",2:"\\1e\\n\\Z\\1i\\8\\0"},{1:"zv",2:"\\1e\\n\\8\\0\\2G\\7z\\0\\3"},{1:"zw",2:"\\1e\\n\\2a\\1m\\5\\4\\6\\7\\0\\3"},{1:"zx",2:"\\1e\\n\\16\\1w\\5\\4\\0\\3"},{1:"zy",2:"\\1e\\n\\66\\4K\\7e\\k\\5\\4\\6\\7\\0\\3"},{1:"zA",2:"\\1e\\n\\17\\T\\5\\4\\6\\7\\0\\3"},{1:"zB",2:"\\1e\\n\\G\\25\\5\\4\\6\\7\\0\\3"},{1:"zC",2:"\\c\\B\\D\\15\\5\\4\\6\\7\\0\\3"},{1:"zD",2:"\\1e\\n\\q\\t\\5\\4\\6\\7\\0\\3"},{1:"zE",2:"\\1e\\n\\zF\\2D\\4j\\5\\4\\6\\7\\0\\3"},{1:"zG",2:"\\B\\2b\\8\\0"},{1:"zH",2:"\\B\\2b\\k\\a\\8\\0"},{1:"zI",2:"\\B\\2b\\f\\g\\0\\3"},{1:"zJ",2:"\\h\\1P\\Z\\1i\\8\\0"},{1:"zK",2:"\\B\\2b\\l\\9\\0\\3"},{1:"zL",2:"\\B\\2b\\8\\0\\11\\C\\0\\3"},{1:"zM",2:"\\5t\\M\\2U\\27\\0\\3"},{1:"zN",2:"\\w\\H\\49\\4\\8\\0\\5t\\M\\0\\3"},{1:"zO",2:"\\B\\2b\\Z\\1i\\5\\4\\6\\7\\0\\3"},{1:"zP",2:"\\B\\2b\\9\\4\\5\\4\\0\\3"},{1:"zQ",2:"\\B\\2b\\5\\4\\6\\7\\0\\3"},{1:"zR",2:"\\B\\2b\\9\\A\\5\\4\\6\\7\\0\\3"},{1:"zS",2:"\\B\\2b\\O\\y\\5\\4\\6\\7\\0\\3"},{1:"zT",2:"\\B\\2b\\2K\\1F\\16\\1w\\5\\4\\0\\3"},{1:"zU",2:"\\B\\2b\\G\\24\\5\\4\\6\\7\\0\\3"},{1:"zV",2:"\\B\\2b\\3p\\2q\\3A\\2q\\5\\4\\6\\7\\0\\3"},{1:"zW",2:"\\B\\2b\\1C\\1z\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"zX",2:"\\B\\2b\\S\\7\\5\\4\\0\\3"},{1:"zY",2:"\\11\\1u\\8\\0"},{1:"A0",2:"\\A1\\4E\\4j\\8\\0"},{1:"A2",2:"\\11\\1u\\L\\4\\8\\0"},{1:"A3",2:"\\1c\\x\\1q\\8\\0"},{1:"A4",2:"\\11\\1u\\k\\a\\8\\0"},{1:"A5",2:"\\11\\1u\\f\\g\\8\\0"},{1:"A6",2:"\\A7\\A8\\8\\0"},{1:"A9",2:"\\6v\\7g\\f\\g\\0\\3"},{1:"Ab",2:"\\11\\1u\\O\\y\\8\\0"},{1:"Ac",2:"\\11\\1u\\S\\7\\0\\3"},{1:"Ad",2:"\\11\\1u\\9\\v\\0\\3"},{1:"Ae",2:"\\1M\\1t\\0\\3"},{1:"Af",2:"\\11\\1u\\16\\1W\\0\\3"},{1:"Ag",2:"\\11\\1u\\8\\0\\a\\0\\6\\7\\0\\3"},{1:"Ah",2:"\\11\\1u\\L\\4\\8\\0\\a\\0\\6\\7\\0\\3"},{1:"Ai",2:"\\11\\1u\\k\\a\\8\\0\\Aj\\37\\0\\3"},{1:"Ak",2:"\\11\\1u\\O\\y\\8\\0\\A\\1Y\\0\\3"},{1:"Al",2:"\\1c\\x\\1q\\8\\0\\a\\6\\0\\3"},{1:"Am",2:"\\3q\\5r\\f\\g\\s\\a\\0\\r"},{1:"An",2:"\\11\\1u\\L\\4\\5\\4\\6\\7\\0\\3"},{1:"Ao",2:"\\4f\\3F\\4j\\2u\\5\\4\\8\\0"},{1:"Ap",2:"\\11\\1u\\Aq\\Ar\\1h\\k\\0\\s\\a\\0\\r"},{1:"As",2:"\\68\\6i\\At\\Au\\5\\4\\6\\7\\0\\3"},{1:"Av",2:"\\11\\1u\\1R\\z\\5\\4\\6\\7\\0\\3"},{1:"Aw",2:"\\11\\1u\\2w\\9\\5\\4\\6\\7\\0\\3"},{1:"Ax",2:"\\11\\1u\\2U\\27\\5\\4\\6\\7\\0\\3"},{1:"Ay",2:"\\1M\\1t\\5\\4\\6\\7\\0\\3"},{1:"Az",2:"\\6v\\7g\\5\\4\\6\\7\\0\\3"},{1:"AA",2:"\\6h\\68\\1b\\5\\4\\6\\7\\0\\3"},{1:"AB",2:"\\84\\33\\7n\\AC\\5\\4\\6\\7\\0\\3"},{1:"AD",2:"\\11\\1u\\G\\24\\5\\4\\6\\7\\0\\3"},{1:"AE",2:"\\11\\1u\\7h\\7i\\16\\1w\\q\\t\\s\\a\\0\\r"},{1:"AH",2:"\\11\\1u\\26\\23\\5\\4\\6\\7\\0\\3"},{1:"AI",2:"\\11\\1u\\F\\j\\5\\4\\6\\7\\0\\3"},{1:"AJ",2:"\\11\\1u\\17\\T\\5\\4\\6\\7\\0\\3"},{1:"AK",2:"\\11\\1u\\1c\\x\\1q\\5\\4\\6\\7\\0\\3"},{1:"AL",2:"\\11\\1u\\5\\4\\8\\0"},{1:"AM",2:"\\11\\1u\\1r\\1f\\5\\4\\6\\7\\0\\3"},{1:"AN",2:"\\11\\1u\\29\\2d\\5\\4\\6\\7\\0\\3"},{1:"AO",2:"\\11\\1u\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"AP",2:"\\11\\1u\\1U\\2A\\5\\4\\6\\7\\0\\3"},{1:"AQ",2:"\\11\\1u\\1m\\5C\\G\\24\\7h\\7i\\2o\\11\\5\\4\\6\\7\\0\\3"},{1:"AR",2:"\\1v\\AS\\5\\4\\6\\7\\0\\3"},{1:"AT",2:"\\11\\1u\\a\\6\\5\\4\\6\\7\\0\\3"},{1:"AU",2:"\\1b\\d\\q\\37\\2p\\2P\\6\\7\\5\\4\\0\\3"},{1:"AV",2:"\\b\\u\\1L\\1Q\\5\\4\\0\\3"},{1:"AW",2:"\\e\\1b\\G\\38\\5\\4\\0\\3"},{1:"AX",2:"\\1b\\d\\I\\P\\5\\4\\6\\7\\0\\3"},{1:"AY",2:"\\4B\\6a\\4D\\2D\\5\\4\\6\\7\\0\\3"},{1:"B0",2:"\\1b\\d\\9\\4\\5c\\4o\\2X\\1Y\\U\\4G\\5\\4\\0\\3"},{1:"B1",2:"\\3P\\d\\1C\\1z\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"B2",2:"\\3P\\d\\1m\\2H\\9\\v\\5\\4\\6\\7\\0\\3"},{1:"B3",2:"\\e\\1b\\A\\V\\5\\4\\0\\3"},{1:"B4",2:"\\b\\T\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"B5",2:"\\3x\\d\\w\\7O\\4R\\4e\\5\\4\\0\\3"},{1:"B6",2:"\\e\\1b\\2B\\l\\5\\4\\0\\3"},{1:"B7",2:"\\e\\1b\\O\\5g\\5\\4\\0\\3"},{1:"B8",2:"\\e\\1b\\D\\44\\G\\24\\5\\4\\0\\3"},{1:"Ba",2:"\\W\\e\\8\\0"},{1:"Bc",2:"\\3t\\d\\z\\1q\\a\\6\\8\\0"},{1:"Be",2:"\\W\\e\\9\\4\\8\\0"},{1:"Bf",2:"\\W\\e\\l\\9\\8\\0"},{1:"Bg",2:"\\W\\e\\n\\2m\\0\\3"},{1:"Bh",2:"\\W\\e\\L\\J\\8\\0"},{1:"Bi",2:"\\3u\\d\\k\\a\\8\\0"},{1:"Bk",2:"\\W\\e\\w\\k\\Y\\8\\0"},{1:"Bl",2:"\\W\\e\\f\\g\\8\\0"},{1:"Bm",2:"\\3t\\d\\f\\g\\8\\0"},{1:"Bn",2:"\\p\\d\\f\\g\\0\\3"},{1:"Bo",2:"\\6d\\2o\\14\\l\\0\\3"},{1:"Bq",2:"\\2x\\d\\0\\3"},{1:"Br",2:"\\3u\\d\\8\\0"},{1:"Bs",2:"\\5V\\1y\\0\\3"},{1:"Bt",2:"\\W\\e\\9\\A\\8\\0"},{1:"Bu",2:"\\3w\\2o\\0\\3"},{1:"Bv",2:"\\w\\H\\21\\7\\0\\3"},{1:"Bw",2:"\\w\\H\\2Z\\6e\\0\\3"},{1:"By",2:"\\34\\m\\n\\16\\0\\3"},{1:"Bz",2:"\\W\\e\\4X\\4E\\0\\3"},{1:"BB",2:"\\W\\e\\a\\6\\0\\3"},{1:"BC",2:"\\B\\36\\9\\v\\0\\3"},{1:"BE",2:"\\W\\e\\1y\\22\\1y\\z\\0\\3"},{1:"BF",2:"\\W\\e\\O\\y\\8\\0"},{1:"BG",2:"\\W\\e\\16\\1W\\0\\3"},{1:"BH",2:"\\7s\\d\\0\\3"},{1:"BJ",2:"\\B\\36\\8\\0"},{1:"BK",2:"\\W\\e\\2j\\2l\\0\\3"},{1:"BL",2:"\\W\\e\\7t\\2C\\0\\3"},{1:"BN",2:"\\W\\e\\BO\\BP\\U\\H\\1o\\0\\3"},{1:"BQ",2:"\\B\\36\\8\\4W\\7u\\0\\3"},{1:"BS",2:"\\W\\e\\8\\0\\D\\15\\0\\3"},{1:"BT",2:"\\W\\e\\8\\0\\B\\36\\l\\9\\0\\3"},{1:"BU",2:"\\W\\e\\9\\4\\8\\0\\BV\\e\\0\\3"},{1:"BW",2:"\\W\\e\\f\\g\\8\\0\\3g\\56\\0\\3"},{1:"BX",2:"\\B\\36\\8\\0\\a\\0\\6\\7\\0\\3"},{1:"BY",2:"\\3t\\d\\z\\1q\\a\\6\\8\\0\\I\\P\\9\\v\\0\\3"},{1:"BZ",2:"\\W\\e\\l\\9\\8\\0\\a\\6\\3J\\S\\7\\0\\3"},{1:"C0",2:"\\W\\e\\n\\2m\\0\\3\\i\\n\\a\\0\\6\\7\\0\\3"},{1:"C1",2:"\\W\\e\\L\\J\\8\\0\\7d\\E\\0\\3"},{1:"C2",2:"\\3u\\d\\k\\a\\8\\0\\3L\\19\\0\\3"},{1:"C3",2:"\\W\\e\\w\\k\\Y\\8\\0\\1p\\e\\0\\3"},{1:"C4",2:"\\3t\\d\\f\\g\\8\\0\\C5\\e\\0\\3"},{1:"C6",2:"\\p\\d\\f\\g\\0\\3\\5Q\\C7\\0\\3"},{1:"C8",2:"\\6d\\2o\\14\\l\\0\\3\\C9\\3Q\\0\\3"},{1:"Ca",2:"\\3u\\d\\8\\0\\Cb\\e\\0\\3"},{1:"Cc",2:"\\W\\e\\9\\A\\8\\0\\3t\\d\\A\\0\\3"},{1:"Cd",2:"\\3w\\2o\\0\\3\\b\\p\\0\\3"},{1:"Ce",2:"\\w\\H\\2Z\\6e\\0\\3\\26\\23\\a\\6\\0\\3"},{1:"Cf",2:"\\W\\e\\O\\y\\8\\0\\i\\1P\\0\\3"},{1:"Cg",2:"\\3u\\d\\8\\0\\D\\15\\0\\3"},{1:"Ch",2:"\\3Z\\19\\8\\0\\W\\e\\0\\3"},{1:"Ci",2:"\\N\\n\\O\\y\\8\\0\\W\\e\\0\\3"},{1:"Cj",2:"\\W\\e\\U\\H\\1o\\0\\3"},{1:"Ck",2:"\\B\\36\\Cl\\Cm\\1a\\8\\0"},{1:"Cn",2:"\\3u\\d\\6P\\5z\\8\\0"},{1:"Co",2:"\\B\\36\\5\\4\\6\\7\\0\\3"},{1:"Cp",2:"\\3u\\d\\5\\4\\6\\7\\0\\3"},{1:"Cq",2:"\\W\\e\\17\\T\\5\\4\\6\\7\\0\\3"},{1:"Cr",2:"\\1B\\C\\5\\4\\6\\7\\0\\3"},{1:"Cs",2:"\\B\\36\\D\\15\\5\\4\\6\\7\\0\\3"},{1:"Ct",2:"\\W\\e\\z\\1H\\5\\4\\6\\7\\0\\3"},{1:"Cu",2:"\\W\\e\\3Z\\19\\a\\6\\5\\4\\0\\3"},{1:"Cv",2:"\\W\\e\\9\\A\\5\\4\\6\\7\\0\\3"},{1:"Cw",2:"\\2x\\d\\5\\4\\6\\7\\0\\3"},{1:"Cx",2:"\\W\\e\\9\\V\\5\\4\\6\\7\\0\\3"},{1:"Cy",2:"\\W\\e\\k\\Y\\q\\t\\s\\a\\0\\r"},{1:"Cz",2:"\\W\\e\\1R\\z\\5\\4\\6\\7\\0\\3"},{1:"CA",2:"\\W\\e\\G\\24\\5\\4\\6\\7\\0\\3"},{1:"CB",2:"\\W\\e\\S\\7\\5\\4\\0\\3"},{1:"CC",2:"\\W\\e\\y\\V\\5\\4\\6\\7\\0\\3"},{1:"CD",2:"\\W\\e\\A\\4\\5\\4\\6\\7\\0\\3"},{1:"CE",2:"\\W\\e\\y\\19\\5\\4\\6\\7\\0\\3"},{1:"CF",2:"\\W\\e\\1L\\1Q\\5\\4\\0\\3"},{1:"CG",2:"\\W\\e\\1f\\5B\\5\\4\\6\\7\\0\\3"},{1:"CH",2:"\\W\\e\\16\\1w\\5\\4\\0\\3"},{1:"CI",2:"\\W\\e\\1B\\2Q\\5\\4\\0\\3"},{1:"CJ",2:"\\W\\e\\9\\4\\5\\4\\6\\7\\0\\3"},{1:"CK",2:"\\3t\\d\\5\\4\\6\\7\\0\\3"},{1:"CL",2:"\\3w\\2o\\5\\4\\6\\7\\0\\3"},{1:"CM",2:"\\p\\d\\5\\4\\6\\7\\0\\3"},{1:"CN",2:"\\6d\\2o\\5\\4\\6\\7\\0\\3"},{1:"CO",2:"\\7s\\d\\5\\4\\6\\7\\0\\3"},{1:"CP",2:"\\5V\\1y\\5\\4\\6\\7\\0\\3"},{1:"CQ",2:"\\W\\e\\i\\1P\\5\\4\\6\\7\\0\\3"},{1:"CR",2:"\\3i\\4f\\9\\A\\5\\4\\6\\7\\0\\3"},{1:"CS",2:"\\W\\e\\k\\0\\q\\t\\s\\a\\0\\r"},{1:"CT",2:"\\W\\e\\3X\\3U\\2X\\2R\\5\\4\\6\\7\\0\\3"},{1:"CU",2:"\\3t\\d\\a\\6\\5\\4\\6\\7\\0\\3"},{1:"CV",2:"\\W\\e\\13\\CW\\5\\4\\6\\7\\0\\3"},{1:"CX",2:"\\3w\\2o\\b\\2m\\5\\4\\6\\7\\0\\3"},{1:"CY",2:"\\W\\e\\o\\2g\\G\\24\\5\\4\\6\\7\\0\\3"},{1:"CZ",2:"\\3t\\d\\4X\\D0\\5\\4\\6\\7\\0\\3"},{1:"D1",2:"\\W\\e\\2v\\z\\5\\4\\6\\7\\0\\3"},{1:"D2",2:"\\B\\36\\2a\\1m\\5\\4\\6\\7\\0\\3"},{1:"D3",2:"\\2x\\d\\a\\6\\5\\4\\0\\3"},{1:"D4",2:"\\W\\e\\H\\3f\\n\\2i\\5\\4\\6\\7\\0\\3"},{1:"D5",2:"\\W\\e\\1r\\1f\\5\\4\\6\\7\\0\\3"},{1:"D6",2:"\\3u\\d\\a\\6\\5\\4\\0\\3"},{1:"D7",2:"\\W\\e\\2z\\2y\\5\\4\\6\\7\\0\\3"},{1:"D8",2:"\\W\\e\\D9\\4e\\3c\\3a\\5\\4\\0\\3"},{1:"Da",2:"\\W\\e\\L\\4\\A\\V\\5\\4\\0\\3"},{1:"Db",2:"\\W\\e\\3V\\4T\\5b\\1f\\5\\4\\0\\3"},{1:"Dc",2:"\\W\\e\\Dd\\j\\De\\2c\\11\\4o\\1L\\1Q\\3J\\3K\\38\\5\\4\\0\\3"},{1:"Df",2:"\\m\\Q\\8\\0"},{1:"Dh",2:"\\w\\H\\a\\0\\6\\7\\8\\0"},{1:"Di",2:"\\2t\\2I\\9\\4\\8\\0"},{1:"Dk",2:"\\m\\Q\\9\\4\\8\\0"},{1:"Dl",2:"\\m\\Q\\l\\9\\8\\0"},{1:"Dm",2:"\\m\\Q\\9\\v\\8\\0"},{1:"Dn",2:"\\m\\Q\\L\\4\\8\\0"},{1:"Do",2:"\\m\\Q\\k\\a\\8\\0"},{1:"Dp",2:"\\6j\\6k\\k\\0\\3"},{1:"Ds",2:"\\4x\\b\\k\\0\\3"},{1:"Du",2:"\\m\\Q\\w\\k\\Y\\8\\0"},{1:"Dv",2:"\\m\\Q\\f\\g\\8\\0"},{1:"Dw",2:"\\3r\\E\\f\\g\\0\\3"},{1:"Dx",2:"\\m\\K\\f\\g\\0\\3"},{1:"Dy",2:"\\2L\\h\\f\\g\\8\\0"},{1:"Dz",2:"\\2F\\j\\0\\3"},{1:"DA",2:"\\4x\\c\\0\\3"},{1:"DB",2:"\\6m\\d\\0\\3"},{1:"DD",2:"\\m\\Q\\O\\y\\8\\0"},{1:"DE",2:"\\4B\\d\\0\\3"},{1:"DF",2:"\\DG\\p\\0\\3"},{1:"DH",2:"\\2L\\b\\f\\g\\0\\3"},{1:"DI",2:"\\3O\\4A\\0\\3"},{1:"DJ",2:"\\m\\Q\\G\\25\\8\\0"},{1:"DK",2:"\\m\\Q\\a\\6\\0\\3"},{1:"DL",2:"\\m\\Q\\20\\3D\\0\\3"},{1:"DM",2:"\\2t\\2I\\0\\3"},{1:"DN",2:"\\6j\\6k\\0\\3"},{1:"DO",2:"\\5x\\d\\0\\3"},{1:"DP",2:"\\m\\Q\\11\\C\\0\\3"},{1:"DQ",2:"\\m\\Q\\14\\2D\\I\\P\\9\\v\\0\\3"},{1:"DR",2:"\\m\\Q\\U\\H\\1o\\0\\3"},{1:"DS",2:"\\m\\Q\\O\\y\\8\\0\\A\\0\\3"},{1:"DT",2:"\\m\\Q\\8\\0\\e\\2L\\0\\3"},{1:"DU",2:"\\m\\Q\\9\\v\\8\\0\\1R\\z\\0\\3"},{1:"DV",2:"\\m\\Q\\9\\4\\8\\0\\9\\A\\0\\3"},{1:"DW",2:"\\m\\Q\\G\\25\\8\\0\\D\\15\\G\\24\\0\\3"},{1:"DX",2:"\\m\\Q\\L\\4\\8\\0\\y\\19\\6\\7\\0\\3"},{1:"DY",2:"\\m\\Q\\f\\g\\8\\0\\4x\\e\\0\\3"},{1:"DZ",2:"\\m\\Q\\k\\a\\8\\0\\3s\\4U\\k\\0\\3"},{1:"E0",2:"\\3r\\E\\f\\g\\0\\3\\I\\P\\9\\v\\0\\3"},{1:"E1",2:"\\2L\\h\\f\\g\\8\\0\\I\\P\\0\\3"},{1:"E2",2:"\\2t\\2I\\f\\g\\0\\3"},{1:"E3",2:"\\x\\n\\8\\0\\14\\F\\0\\3"},{1:"E4",2:"\\m\\Q\\5\\4\\6\\7\\0\\3"},{1:"E5",2:"\\2L\\h\\5\\4\\6\\7\\0\\3"},{1:"E6",2:"\\7B\\p\\5\\4\\6\\7\\0\\3"},{1:"E8",2:"\\2L\\b\\3D\\2t\\8\\0"},{1:"E9",2:"\\m\\Q\\A\\V\\5\\4\\6\\7\\0\\3"},{1:"Ea",2:"\\m\\Q\\1y\\22\\1y\\z\\5\\4\\6\\7\\0\\3"},{1:"Eb",2:"\\3r\\E\\5\\4\\6\\7\\0\\3"},{1:"Ec",2:"\\3O\\4A\\5\\4\\6\\7\\0\\3"},{1:"Ed",2:"\\Z\\3m\\4X\\37\\a\\6\\5\\4\\0\\3"},{1:"Ee",2:"\\m\\Q\\16\\1w\\5\\4\\0\\3"},{1:"Ef",2:"\\2L\\b\\5\\4\\6\\7\\0\\3"},{1:"Eg",2:"\\m\\Q\\9\\4\\y\\19\\5\\4\\6\\7\\0\\3"},{1:"Eh",2:"\\2t\\2I\\T\\2d\\5\\4\\6\\7\\0\\3"},{1:"Ei",2:"\\m\\Q\\9\\V\\5\\4\\6\\7\\0\\3"},{1:"Ej",2:"\\4B\\d\\5\\4\\6\\7\\0\\3"},{1:"Ek",2:"\\5P\\m\\5\\4\\6\\7\\0\\3"},{1:"El",2:"\\m\\Q\\z\\1q\\I\\P\\5\\4\\6\\7\\0\\3"},{1:"Em",2:"\\Z\\3m\\2t\\2I\\y\\19\\6\\7\\5\\4\\0\\3"},{1:"En",2:"\\m\\Q\\17\\T\\5\\4\\6\\7\\0\\3"},{1:"Eo",2:"\\m\\Q\\1r\\1f\\2i\\2S\\5\\4\\6\\7\\0\\3"},{1:"Ep",2:"\\m\\Q\\w\\k\\Y\\q\\t\\s\\a\\0\\r"},{1:"Eq",2:"\\m\\Q\\k\\0\\q\\t\\s\\a\\0\\r"},{1:"Er",2:"\\7C\\d\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"Et",2:"\\2t\\2I\\5\\4\\6\\7\\0\\3"},{1:"Eu",2:"\\6m\\d\\5\\4\\6\\7\\0\\3"},{1:"Ev",2:"\\5x\\d\\5\\4\\6\\7\\0\\3"},{1:"Ew",2:"\\7f\\D\\5\\4\\6\\7\\0\\3"},{1:"Ex",2:"\\m\\Q\\o\\52\\3c\\3a\\5\\4\\6\\7\\0\\3"},{1:"Ey",2:"\\Z\\3m\\2t\\2I\\1p\\p\\5\\4\\6\\7\\0\\3"},{1:"Ez",2:"\\m\\Q\\z\\5M\\9\\v\\5\\4\\6\\7\\0\\3"},{1:"EA",2:"\\m\\Q\\3h\\1B\\a\\6\\5\\4\\0\\3"},{1:"EB",2:"\\m\\Q\\D\\15\\1T\\l\\5\\4\\0\\3"},{1:"EC",2:"\\m\\Q\\1R\\z\\5\\4\\6\\7\\0\\3"},{1:"ED",2:"\\m\\Q\\9\\A\\5\\4\\0\\3"},{1:"EE",2:"\\m\\Q\\w\\7q\\a\\6\\5\\4\\0\\3"},{1:"EF",2:"\\3r\\E\\a\\6\\5\\4\\0\\3"},{1:"EG",2:"\\7C\\d\\5\\4\\6\\7\\0\\3"},{1:"EH",2:"\\m\\Q\\H\\3p\\a\\6\\5\\4\\0\\3"},{1:"EI",2:"\\m\\K\\5\\4\\6\\7\\0\\3"},{1:"EJ",2:"\\m\\Q\\S\\7\\5\\4\\0\\3"},{1:"EK",2:"\\6n\\6q\\j\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"EM",2:"\\m\\Q\\O\\V\\5\\4\\0\\3"},{1:"EN",2:"\\m\\Q\\H\\3f\\A\\1Y\\5\\4\\0\\3"},{1:"EO",2:"\\m\\Q\\34\\m\\5\\4\\0\\3"},{1:"EP",2:"\\m\\Q\\J\\4\\5\\4\\6\\7\\0\\3"},{1:"EQ",2:"\\m\\Q\\5u\\2Z\\5\\4\\0\\3"},{1:"ER",2:"\\m\\Q\\11\\ES\\6M\\6s\\5\\4\\6\\7\\0\\3"},{1:"ET",2:"\\m\\Q\\2v\\z\\5\\4\\6\\7\\0\\3"},{1:"EU",2:"\\m\\Q\\9\\4\\5\\4\\6\\7\\0\\3"},{1:"EV",2:"\\Z\\3m\\2t\\2I\\O\\y\\5\\4\\0\\3"},{1:"EW",2:"\\m\\K\\k\\Y\\q\\t\\s\\a\\0\\r"},{1:"EX",2:"\\m\\Q\\3o\\U\\y\\19\\5\\4\\0\\3"},{1:"EY",2:"\\m\\Q\\EZ\\n\\A\\1Y\\5\\4\\0\\3"},{1:"F0",2:"\\2t\\2I\\5A\\2D\\5\\4\\6\\7\\0\\3"},{1:"F1",2:"\\6j\\6k\\y\\19\\6\\7\\5\\4\\0\\3"},{1:"F2",2:"\\Z\\3m\\m\\Q\\1L\\1Q\\5\\4\\0\\3"},{1:"F3",2:"\\Q\\A\\5\\4\\0\\3"},{1:"F4",2:"\\6n\\6q\\j\\5\\4\\6\\7\\0\\3"},{1:"F5",2:"\\m\\Q\\26\\23\\I\\P\\9\\v\\5\\4\\0\\3"},{1:"F6",2:"\\m\\Q\\49\\4\\5\\4\\6\\7\\0\\3"},{1:"F7",2:"\\2t\\2I\\I\\P\\6\\7\\5\\4\\0\\3"},{1:"F8",2:"\\F9\\D\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"Fa",2:"\\2F\\j\\5\\4\\6\\7\\0\\3"},{1:"Fb",2:"\\6m\\d\\D\\15\\5\\4\\0\\3"},{1:"Fc",2:"\\m\\Q\\2z\\2y\\5\\4\\6\\7\\0\\3"},{1:"Fd",2:"\\4x\\c\\2a\\1m\\5\\4\\0\\3"},{1:"Fe",2:"\\2t\\2I\\1C\\1z\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"Ff",2:"\\m\\Q\\13\\e\\5\\4\\0\\3"},{1:"Fg",2:"\\m\\Q\\3x\\1q\\5\\4\\6\\7\\0\\3"},{1:"Fh",2:"\\m\\Q\\2F\\Fi\\3B\\S\\7\\5\\4\\0\\3"},{1:"Fj",2:"\\m\\Q\\7N\\3C\\9\\v\\5\\4\\0\\3"},{1:"Fk",2:"\\m\\Q\\2C\\2T\\5\\4\\0\\3"},{1:"Fl",2:"\\2t\\2I\\a\\6\\5\\4\\0\\3"},{1:"Fm",2:"\\4x\\h\\2a\\1m\\5\\4\\0\\3"},{1:"Fn",2:"\\3r\\E\\1C\\1z\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"Fo",2:"\\2g\\2f\\8\\0"},{1:"Fp",2:"\\C\\6Y\\8\\0"},{1:"Fq",2:"\\1l\\d\\8\\0"},{1:"Fs",2:"\\1l\\G\\9\\v\\0\\3"},{1:"Ft",2:"\\1l\\G\\L\\J\\8\\0"},{1:"Fu",2:"\\5W\\21\\8\\0"},{1:"Fv",2:"\\1l\\G\\k\\a\\8\\0"},{1:"Fw",2:"\\1l\\G\\w\\k\\Y\\8\\0"},{1:"Fx",2:"\\1l\\G\\f\\g\\8\\0"},{1:"Fy",2:"\\3G\\e\\0\\3"},{1:"FA",2:"\\18\\7G\\0\\3"},{1:"FC",2:"\\B\\1J\\f\\g\\0\\3"},{1:"FD",2:"\\2s\\d\\f\\g\\0\\3"},{1:"FE",2:"\\3G\\b\\f\\g\\8\\0"},{1:"FF",2:"\\2g\\2f\\l\\9\\0\\3"},{1:"FG",2:"\\20\\2h\\0\\3"},{1:"FH",2:"\\1k\\FI\\0\\3"},{1:"FJ",2:"\\1l\\G\\16\\1W\\0\\3"},{1:"FK",2:"\\FL\\5r\\0\\3"},{1:"FM",2:"\\FN\\5z\\8\\0"},{1:"FO",2:"\\2g\\2f\\C\\2g\\0\\3"},{1:"FP",2:"\\3G\\b\\l\\9\\0\\3"},{1:"FQ",2:"\\1l\\G\\f\\g\\8\\0\\3G\\b\\a\\6\\0\\3"},{1:"FR",2:"\\1l\\G\\L\\J\\8\\0\\i\\1P\\0\\3"},{1:"FS",2:"\\2g\\2f\\9\\0\\3"},{1:"FT",2:"\\E\\4g\\0\\3"},{1:"FU",2:"\\2g\\2f\\8\\0\\3w\\FV\\0\\3"},{1:"FW",2:"\\1l\\d\\8\\0\\FX\\7H\\0\\3"},{1:"FZ",2:"\\5W\\21\\8\\0\\7H\\G0\\0\\3"},{1:"G1",2:"\\1l\\G\\f\\g\\8\\0\\63\\3q\\0\\3"},{1:"G2",2:"\\1l\\d\\U\\1o\\U\\V\\0\\3"},{1:"G3",2:"\\1l\\G\\e\\2b\\0\\3"},{1:"G4",2:"\\2s\\d\\I\\P\\9\\v\\0\\3"},{1:"G5",2:"\\1l\\d\\l\\9\\0\\3"},{1:"G6",2:"\\1l\\G\\L\\J\\8\\0\\1B\\j\\0\\3"},{1:"G7",2:"\\1l\\G\\4l\\1S\\17\\T\\5\\4\\0\\3"},{1:"G8",2:"\\1l\\G\\A\\4\\q\\t\\s\\a\\0\\r"},{1:"G9",2:"\\4u\\d\\5\\4\\6\\7\\0\\3"},{1:"Gb",2:"\\3G\\c\\5\\4\\6\\7\\0\\3"},{1:"Gc",2:"\\5N\\2h\\5\\4\\8\\0"},{1:"Gd",2:"\\1l\\G\\C\\b\\48\\1q\\5\\4\\0\\3"},{1:"Ge",2:"\\1l\\d\\5\\4\\6\\7\\0\\3"},{1:"Gf",2:"\\1l\\G\\J\\4\\5\\4\\6\\7\\0\\3"},{1:"Gg",2:"\\1l\\G\\I\\P\\5\\4\\6\\7\\0\\3"},{1:"Gh",2:"\\1l\\G\\1y\\22\\z\\1H\\5\\4\\6\\7\\0\\3"},{1:"Gi",2:"\\1l\\G\\z\\1H\\5\\4\\6\\7\\0\\3"},{1:"Gj",2:"\\2g\\2f\\n\\2m\\5\\4\\6\\7\\0\\3"},{1:"Gk",2:"\\1l\\G\\L\\4\\5\\4\\6\\7\\0\\3"},{1:"Gl",2:"\\2g\\2f\\k\\0\\q\\t\\s\\a\\0\\r"},{1:"Gm",2:"\\1l\\G\\2a\\1m\\5\\4\\6\\7\\0\\3"},{1:"Gn",2:"\\2s\\d\\k\\0\\q\\t\\s\\a\\0\\r"},{1:"Go",2:"\\1l\\d\\5B\\C\\5\\4\\0\\3"},{1:"Gp",2:"\\2s\\d\\3X\\3U\\2X\\2R\\5\\4\\0\\3"},{1:"Gq",2:"\\2s\\d\\C\\4g\\5\\4\\0\\3"},{1:"Gr",2:"\\2s\\d\\l\\9\\5\\4\\0\\3"},{1:"Gs",2:"\\1l\\G\\16\\1w\\5\\4\\0\\3"},{1:"Gt",2:"\\3G\\h\\5\\4\\6\\7\\0\\3"},{1:"Gu",2:"\\1l\\d\\5N\\2h\\5\\4\\6\\7\\0\\3"},{1:"Gv",2:"\\2g\\2f\\5K\\S\\5\\4\\0\\3"},{1:"Gw",2:"\\2g\\2f\\C\\F\\3o\\U\\5\\4\\6\\7\\0\\3"},{1:"Gx",2:"\\1l\\d\\a\\6\\5\\4\\6\\7\\0\\3"},{1:"Gy",2:"\\2s\\d\\y\\V\\5\\4\\6\\7\\0\\3"},{1:"Gz",2:"\\1l\\G\\5D\\U\\y\\19\\V\\47\\5\\4\\6\\7\\0\\3"},{1:"GA",2:"\\GB\\5y\\7J\\5\\4\\6\\7\\0\\3"},{1:"GD",2:"\\1l\\G\\1m\\2H\\9\\v\\5\\4\\6\\7\\0\\3"},{1:"GE",2:"\\1l\\G\\S\\7\\5\\4\\0\\3"},{1:"GF",2:"\\1l\\G\\1C\\1z\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"GG",2:"\\2g\\2f\\D\\15\\5\\4\\0\\3"},{1:"GH",2:"\\2s\\d\\9\\S\\21\\7\\5\\4\\0\\3"},{1:"GI",2:"\\20\\2h\\5\\4\\6\\7\\0\\3"},{1:"GJ",2:"\\B\\1J\\5\\4\\6\\7\\0\\3"},{1:"GK",2:"\\1l\\d\\2p\\2P\\5\\4\\6\\7\\0\\3"},{1:"GL",2:"\\2g\\2f\\2o\\7K\\5\\4\\6\\7\\0\\3"},{1:"GN",2:"\\2g\\2f\\2p\\2P\\5\\4\\6\\7\\0\\3"},{1:"GO",2:"\\1l\\G\\1r\\1f\\5\\4\\6\\7\\0\\3"},{1:"GP",2:"\\4u\\d\\D\\15\\5\\4\\0\\3"},{1:"GQ",2:"\\2g\\2f\\b\\2m\\5\\4\\0\\3"},{1:"GR",2:"\\2g\\2f\\i\\n\\5\\4\\6\\7\\0\\3"},{1:"GS",2:"\\4u\\d\\a\\6\\5\\4\\0\\3"},{1:"GT",2:"\\4u\\d\\l\\9\\5\\4\\0\\3"},{1:"GU",2:"\\18\\7G\\j\\5\\4\\0\\3"},{1:"GV",2:"\\4u\\d\\2a\\1m\\5\\4\\0\\3"},{1:"GW",2:"\\2s\\d\\n\\2m\\5\\4\\0\\3"},{1:"GX",2:"\\2s\\d\\2w\\9\\5\\4\\0\\3"},{1:"GY",2:"\\2g\\2f\\m\\3p\\a\\6\\5\\4\\0\\3"},{1:"GZ",2:"\\2s\\d\\1C\\1z\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"H0",2:"\\3G\\e\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"H1",2:"\\2s\\d\\9\\v\\5\\4\\6\\7\\0\\3"},{1:"H2",2:"\\b\\1M\\8\\0"},{1:"H3",2:"\\C\\i\\17\\T\\8\\0"},{1:"H4",2:"\\i\\C\\l\\9\\8\\0"},{1:"H5",2:"\\b\\1M\\1g\\28\\8\\0"},{1:"H6",2:"\\e\\c\\l\\9\\8\\0"},{1:"H7",2:"\\54\\1J\\4I\\4s\\4r\\0\\3"},{1:"Hb",2:"\\e\\c\\L\\4\\8\\0"},{1:"Hc",2:"\\e\\c\\w\\k\\Y\\8\\0"},{1:"Hd",2:"\\4m\\b\\k\\0\\3"},{1:"Hf",2:"\\e\\c\\f\\g\\8\\0"},{1:"Hg",2:"\\N\\7P\\f\\g\\0\\3"},{1:"Hi",2:"\\4S\\1V\\0\\3"},{1:"Hj",2:"\\4m\\b\\f\\g\\0\\3"},{1:"Hk",2:"\\Hl\\5k\\j\\8\\0"},{1:"Hn",2:"\\e\\c\\O\\y\\8\\0"},{1:"Ho",2:"\\e\\c\\a\\6\\0\\3"},{1:"Hp",2:"\\54\\1J\\4I\\0\\3"},{1:"Hq",2:"\\Hr\\44\\0\\3"},{1:"Hs",2:"\\e\\c\\a\\6\\f\\g\\8\\0"},{1:"Ht",2:"\\b\\1M\\9\\v\\0\\3"},{1:"Hu",2:"\\e\\c\\16\\1W\\0\\3"},{1:"Hv",2:"\\11\\Hw\\0\\3"},{1:"Hx",2:"\\4J\\e\\0\\3"},{1:"Hy",2:"\\e\\c\\9\\v\\0\\3"},{1:"Hz",2:"\\b\\1M\\l\\9\\0\\3"},{1:"HA",2:"\\e\\c\\29\\2d\\a\\6\\0\\3"},{1:"HB",2:"\\e\\c\\2X\\2R\\0\\3"},{1:"HC",2:"\\b\\1M\\9\\0\\3"},{1:"HD",2:"\\b\\1M\\8\\0\\a\\0\\6\\7\\0\\3"},{1:"HE",2:"\\b\\1M\\8\\0\\5A\\1e\\0\\3"},{1:"HF",2:"\\C\\i\\17\\T\\8\\0\\l\\9\\0\\3"},{1:"HG",2:"\\i\\C\\l\\9\\8\\0\\13\\e\\0\\3"},{1:"HH",2:"\\b\\1M\\1g\\28\\8\\0\\a\\6\\0\\3"},{1:"HI",2:"\\e\\c\\l\\9\\8\\0\\29\\2d\\a\\0\\0\\3"},{1:"HJ",2:"\\54\\1J\\4I\\4s\\4r\\0\\3\\a\\6\\S\\7\\0\\3"},{1:"HK",2:"\\e\\c\\L\\4\\8\\0\\b\\1M\\A\\0\\3"},{1:"HL",2:"\\e\\c\\w\\k\\Y\\8\\0\\a\\6\\0\\3"},{1:"HM",2:"\\e\\c\\f\\g\\8\\0\\a\\0\\6\\7\\0\\3"},{1:"HN",2:"\\4m\\b\\f\\g\\0\\3\\a\\6\\0\\3"},{1:"HO",2:"\\e\\c\\a\\6\\f\\g\\8\\0\\l\\9\\0\\3"},{1:"HP",2:"\\e\\c\\O\\y\\8\\0\\26\\23\\y\\19\\1T\\l\\0\\3"},{1:"HQ",2:"\\b\\1M\\f\\g\\0\\3"},{1:"HR",2:"\\e\\c\\9\\4\\5\\4\\6\\7\\0\\3"},{1:"HS",2:"\\e\\c\\k\\0\\q\\t\\s\\a\\0\\r"},{1:"HT",2:"\\4J\\e\\5\\4\\8\\0"},{1:"HU",2:"\\4J\\e\\5\\4\\6\\7\\0\\3"},{1:"HV",2:"\\e\\c\\2K\\1F\\16\\1w\\5\\4\\0\\3"},{1:"HW",2:"\\e\\c\\4s\\4r\\9\\S\\21\\7\\5\\4\\6\\7\\0\\3"},{1:"HX",2:"\\e\\c\\1L\\1Q\\A\\V\\5\\4\\0\\3"},{1:"HY",2:"\\e\\c\\z\\1H\\5\\4\\6\\7\\0\\3"},{1:"HZ",2:"\\e\\c\\3d\\3j\\9\\v\\5\\4\\0\\3"},{1:"I0",2:"\\e\\c\\S\\7\\5\\4\\0\\3"},{1:"I1",2:"\\7u\\5i\\5\\4\\6\\7\\0\\3"},{1:"I2",2:"\\e\\c\\I\\P\\29\\2d\\5\\4\\6\\7\\0\\3"},{1:"I3",2:"\\e\\c\\17\\T\\5\\4\\6\\7\\0\\3"},{1:"I4",2:"\\e\\c\\O\\y\\5\\4\\0\\3"},{1:"I5",2:"\\e\\c\\29\\2d\\6\\7\\5\\4\\0\\3"},{1:"I6",2:"\\e\\c\\26\\23\\5\\4\\6\\7\\0\\3"},{1:"I7",2:"\\e\\c\\9\\4\\9\\v\\5\\4\\6\\7\\0\\3"},{1:"I8",2:"\\e\\c\\1R\\z\\5\\4\\6\\7\\0\\3"},{1:"I9",2:"\\e\\c\\a\\6\\5\\4\\0\\3"},{1:"Ia",2:"\\b\\1M\\5\\4\\0\\3"},{1:"Ib",2:"\\e\\c\\U\\1o\\U\\V\\5\\4\\0\\3"},{1:"Ic",2:"\\e\\c\\9\\4\\V\\47\\5\\4\\6\\7\\0\\3"},{1:"Id",2:"\\4S\\1V\\5\\4\\6\\7\\0\\3"},{1:"Ie",2:"\\e\\c\\29\\2d\\9\\v\\5\\4\\0\\3"},{1:"If",2:"\\e\\c\\1m\\2H\\a\\6\\5\\4\\0\\3"},{1:"Ig",2:"\\e\\c\\G\\24\\5\\4\\6\\7\\0\\3"},{1:"Ih",2:"\\6r\\d\\5\\4\\6\\7\\0\\3"},{1:"Ii",2:"\\b\\1M\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"Ij",2:"\\e\\c\\w\\k\\Y\\q\\t\\s\\a\\0\\r"},{1:"Ik",2:"\\e\\c\\Il\\Im\\2p\\2P\\5\\4\\6\\7\\0\\3"},{1:"In",2:"\\e\\c\\y\\19\\1T\\l\\5\\4\\0\\3"},{1:"Io",2:"\\e\\c\\6w\\6y\\5\\4\\6\\7\\0\\3"},{1:"Ip",2:"\\e\\c\\9\\v\\5\\4\\0\\3"},{1:"Iq",2:"\\e\\c\\1e\\30\\5\\4\\0\\3"},{1:"Ir",2:"\\N\\7P\\5\\4\\6\\7\\0\\3"},{1:"Is",2:"\\e\\c\\1g\\28\\5\\4\\6\\7\\0\\3"},{1:"It",2:"\\e\\c\\L\\4\\9\\v\\5\\4\\0\\3"},{1:"Iu",2:"\\4m\\c\\a\\6\\5\\4\\0\\3"},{1:"Iv",2:"\\e\\c\\2a\\1m\\5\\4\\0\\3"},{1:"Iw",2:"\\e\\c\\11\\2U\\27\\a\\6\\5\\4\\0\\3"},{1:"Ix",2:"\\e\\c\\7v\\J\\3o\\U\\y\\V\\5\\4\\0\\3"},{1:"Iy",2:"\\e\\c\\2W\\Iz\\2S\\IA\\5\\4\\0\\3"},{1:"IB",2:"\\e\\c\\3h\\1B\\5\\4\\6\\7\\0\\3"},{1:"IC",2:"\\e\\c\\1T\\l\\5\\4\\0\\3"},{1:"ID",2:"\\e\\c\\2j\\2l\\5\\4\\0\\3"},{1:"IE",2:"\\e\\c\\9\\A\\5\\4\\6\\7\\0\\3"},{1:"IF",2:"\\54\\1J\\4I\\4s\\4r\\5\\4\\6\\7\\0\\3"},{1:"IG",2:"\\5A\\1e\\a\\6\\5\\4\\0\\3"},{1:"IH",2:"\\4m\\d\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"II",2:"\\e\\c\\1y\\22\\5\\4\\0\\3"},{1:"IJ",2:"\\4S\\1V\\1C\\1z\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"IK",2:"\\1t\\m\\5\\4\\6\\7\\0\\3"},{1:"IL",2:"\\e\\c\\IM\\d\\5\\4\\0\\3"},{1:"IN",2:"\\e\\c\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"IO",2:"\\b\\1M\\3c\\3a\\2j\\52\\5\\4\\0\\3"},{1:"IP",2:"\\j\\i\\8\\0"},{1:"IQ",2:"\\w\\H\\n\\2m\\8\\0"},{1:"IR",2:"\\w\\H\\1c\\2k\\8\\0\\4a\\C\\i\\4c"},{1:"IS",2:"\\j\\i\\a\\6\\8\\0"},{1:"IT",2:"\\1e\\2c\\a\\6\\8\\0"},{1:"IU",2:"\\19\\b\\8\\0"},{1:"IV",2:"\\3T\\2x\\8\\0\\14\\y\\0\\3"},{1:"IX",2:"\\6x\\D\\8\\0\\i\\1M\\0\\3"},{1:"IZ",2:"\\1e\\2c\\l\\9\\8\\0\\J0\\2c\\0\\3"},{1:"J1",2:"\\j\\i\\f\\g\\8\\0\\J2\\j\\0\\3"},{1:"J3",2:"\\j\\i\\O\\y\\8\\0\\4C\\j\\0\\3"},{1:"J4",2:"\\w\\H\\1c\\2k\\8\\0\\7T\\22\\0\\3"},{1:"J6",2:"\\j\\i\\a\\6\\8\\0\\2W\\j\\a\\6\\0\\3"},{1:"J7",2:"\\1e\\2c\\L\\4\\8\\0\\n\\1A\\0\\3"},{1:"J8",2:"\\j\\i\\O\\y\\8\\0\\i\\1P\\0\\3"},{1:"J9",2:"\\19\\b\\8\\0\\2s\\D\\0\\3"},{1:"Ja",2:"\\h\\u\\z\\3c\\0\\3\\26\\23\\3W\\6N\\2l\\1r\\0\\3"},{1:"Jb",2:"\\1e\\2c\\l\\9\\8\\0"},{1:"Jc",2:"\\j\\i\\G\\25\\8\\0"},{1:"Jd",2:"\\2u\\3F\\9\\4\\8\\0"},{1:"Je",2:"\\j\\i\\l\\9\\8\\0"},{1:"Jf",2:"\\j\\i\\L\\4\\8\\0"},{1:"Jg",2:"\\1e\\2c\\L\\4\\8\\0"},{1:"Jh",2:"\\3I\\2J\\k\\0\\3"},{1:"Jj",2:"\\2W\\j\\k\\0\\3"},{1:"Jk",2:"\\1p\\d\\k\\0\\3"},{1:"Jl",2:"\\j\\i\\w\\k\\Y\\8\\0"},{1:"Jm",2:"\\19\\B\\k\\0\\3"},{1:"Jn",2:"\\j\\i\\f\\g\\8\\0"},{1:"Jo",2:"\\41\\3r\\f\\g\\8\\0"},{1:"Jp",2:"\\6x\\D\\8\\0"},{1:"Jq",2:"\\1J\\d\\0\\3"},{1:"Jr",2:"\\1p\\d\\0\\3"},{1:"Js",2:"\\3F\\i\\8\\0"},{1:"Jt",2:"\\3s\\7V\\8\\0"},{1:"Jv",2:"\\2W\\j\\0\\3"},{1:"Jw",2:"\\19\\B\\0\\3"},{1:"Jx",2:"\\50\\4D\\0\\3"},{1:"Jz",2:"\\j\\i\\O\\y\\8\\0"},{1:"JA",2:"\\j\\i\\1r\\1f\\0\\3"},{1:"JB",2:"\\j\\i\\S\\7\\0\\3"},{1:"JC",2:"\\2u\\3F\\k\\Y\\0\\3"},{1:"JD",2:"\\1e\\2c\\1p\\n\\0\\3"},{1:"JE",2:"\\6A\\1O\\0\\3"},{1:"JG",2:"\\j\\i\\9\\S\\21\\7\\0\\3"},{1:"JH",2:"\\1e\\2c\\8\\0"},{1:"JI",2:"\\3T\\2x\\8\\0"},{1:"JJ",2:"\\3I\\2J\\0\\3"},{1:"JK",2:"\\j\\i\\16\\1W\\0\\3"},{1:"JL",2:"\\j\\i\\17\\T\\0\\3"},{1:"JM",2:"\\j\\i\\9\\A\\0\\3"},{1:"JN",2:"\\j\\i\\48\\1q\\0\\3"},{1:"JO",2:"\\3T\\2x\\b\\j\\0\\3"},{1:"JP",2:"\\3I\\2J\\a\\6\\0\\3"},{1:"JQ",2:"\\j\\i\\5B\\7K\\0\\3"},{1:"JR",2:"\\1e\\2c\\JS\\7l\\a\\6\\0\\3"},{1:"JT",2:"\\1e\\2c\\2F\\n\\0\\3"},{1:"JU",2:"\\j\\i\\26\\23\\0\\3"},{1:"JV",2:"\\j\\i\\63\\3q\\0\\3"},{1:"JW",2:"\\j\\i\\C\\JX\\9\\0\\3"},{1:"JY",2:"\\1e\\2c\\9\\0\\3"},{1:"JZ",2:"\\2u\\3F\\l\\9\\0\\3"},{1:"K0",2:"\\j\\i\\1S\\1F\\0\\3"},{1:"K1",2:"\\2u\\3F\\f\\g\\0\\3"},{1:"K2",2:"\\j\\i\\1e\\30\\1S\\3A\\0\\3"},{1:"K3",2:"\\j\\i\\1T\\l\\0\\3"},{1:"K4",2:"\\j\\i\\L\\4\\9\\v\\0\\3"},{1:"K5",2:"\\j\\i\\k\\0\\q\\t\\s\\a\\0\\r"},{1:"K6",2:"\\50\\4D\\k\\0\\s\\a\\0\\r"},{1:"K7",2:"\\j\\i\\A\\4\\5\\4\\6\\7\\0\\3"},{1:"K8",2:"\\j\\i\\z\\1H\\q\\t\\s\\a\\0\\r"},{1:"K9",2:"\\Ka\\Kb\\5\\4\\6\\7\\0\\3"},{1:"Kc",2:"\\41\\3r\\43\\i\\5\\4\\6\\7\\0\\3"},{1:"Kd",2:"\\1e\\2c\\5\\4\\6\\7\\0\\3"},{1:"Ke",2:"\\65\\n\\5\\4\\0\\3"},{1:"Kf",2:"\\j\\i\\5\\4\\0\\3"},{1:"Kg",2:"\\j\\i\\4M\\2S\\5\\4\\6\\7\\0\\3"},{1:"Kh",2:"\\Ki\\7B\\5\\4\\6\\7\\0\\3"},{1:"Kj",2:"\\19\\B\\5\\4\\6\\7\\0\\3"},{1:"Kk",2:"\\3I\\2J\\5\\4\\0\\3"},{1:"Kl",2:"\\3T\\2x\\5\\4\\0\\3"},{1:"Km",2:"\\i\\4N\\5\\4\\0\\3"},{1:"Kn",2:"\\6x\\D\\5\\4\\6\\7\\0\\3"},{1:"Ko",2:"\\1p\\d\\5\\4\\0\\3"},{1:"Kp",2:"\\j\\i\\a\\6\\5\\4\\0\\3"},{1:"Kq",2:"\\j\\i\\2X\\2R\\5\\4\\0\\3"},{1:"Kr",2:"\\1J\\d\\a\\6\\5\\4\\0\\3"},{1:"Ks",2:"\\j\\i\\1H\\2h\\a\\6\\5\\4\\0\\3"},{1:"Kt",2:"\\j\\i\\Ku\\7y\\O\\V\\5\\4\\0\\3"},{1:"Kv",2:"\\j\\i\\1y\\22\\5\\4\\0\\3"},{1:"Kw",2:"\\j\\i\\66\\4K\\7e\\k\\5\\4\\0\\3"},{1:"Kx",2:"\\1e\\2c\\7U\\2m\\5\\4\\6\\7\\0\\3"},{1:"Ky",2:"\\i\\4N\\a\\6\\5\\4\\0\\3"},{1:"Kz",2:"\\j\\i\\17\\T\\5\\4\\0\\3"},{1:"KA",2:"\\7Y\\37\\5\\4\\0\\3"},{1:"KC",2:"\\j\\i\\U\\V\\5\\4\\0\\3"},{1:"KD",2:"\\1e\\2c\\4R\\4e\\1T\\l\\5\\4\\6\\7\\0\\3"},{1:"KE",2:"\\j\\i\\I\\P\\5\\4\\6\\7\\0\\3"},{1:"KF",2:"\\1e\\2c\\4H\\7J\\5\\4\\6\\7\\0\\3"},{1:"KG",2:"\\j\\i\\7T\\22\\5\\4\\0\\3"},{1:"KH",2:"\\j\\i\\y\\V\\5\\4\\0\\3"},{1:"KI",2:"\\j\\i\\9\\4\\5\\4\\0\\3"},{1:"KJ",2:"\\j\\i\\1n\\9\\5\\4\\0\\3"},{1:"KK",2:"\\1e\\2c\\5Q\\KL\\5\\4\\6\\7\\0\\3"},{1:"KM",2:"\\19\\b\\5\\4\\0\\3"},{1:"KN",2:"\\3T\\2x\\9\\v\\5\\4\\6\\7\\0\\3"},{1:"KO",2:"\\j\\i\\6L\\14\\a\\6\\5\\4\\0\\3"},{1:"KP",2:"\\j\\i\\U\\H\\1o\\5\\4\\0\\3"},{1:"KQ",2:"\\3I\\2J\\9\\A\\5\\4\\0\\3"},{1:"KR",2:"\\1J\\d\\5\\4\\6\\7\\0\\3"},{1:"KS",2:"\\6A\\1O\\a\\6\\5\\4\\0\\3"},{1:"KT",2:"\\7Y\\37\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"KU",2:"\\j\\i\\w\\k\\Y\\q\\t\\s\\a\\0\\r"},{1:"KV",2:"\\19\\b\\9\\v\\5\\4\\6\\7\\0\\3"},{1:"KW",2:"\\j\\i\\z\\1q\\5\\4\\6\\7\\0\\3"},{1:"KX",2:"\\j\\i\\1L\\1Q\\5\\4\\0\\3"},{1:"KY",2:"\\j\\i\\KZ\\4\\5\\4\\0\\3"},{1:"L0",2:"\\j\\i\\4t\\J\\a\\6\\5\\4\\0\\3"},{1:"L1",2:"\\2W\\j\\5\\4\\6\\7\\0\\3"},{1:"L2",2:"\\j\\i\\U\\2E\\59\\57\\5\\4\\0\\3"},{1:"L3",2:"\\j\\i\\Y\\3b\\3C\\3b\\5\\4\\0\\3"},{1:"L4",2:"\\j\\i\\A\\1Y\\5\\4\\0\\3"},{1:"L5",2:"\\j\\i\\2w\\9\\5\\4\\0\\3"},{1:"L6",2:"\\j\\i\\D\\15\\G\\24\\5\\4\\0\\3"},{1:"L7",2:"\\3T\\2x\\2z\\2y\\9\\v\\5\\4\\0\\3"},{1:"L8",2:"\\j\\i\\2K\\1F\\16\\1w\\5\\4\\0\\3"},{1:"L9",2:"\\50\\4D\\1G\\1S\\5\\4\\0\\3"},{1:"La",2:"\\j\\i\\2j\\2l\\5\\4\\0\\3"},{1:"Lb",2:"\\3s\\7V\\5\\4\\0\\3"},{1:"Lc",2:"\\6A\\1O\\5\\4\\0\\3"},{1:"Ld",2:"\\j\\i\\l\\9\\5\\4\\0\\3"},{1:"Le",2:"\\j\\i\\14\\1n\\5C\\4\\5\\4\\0\\3"},{1:"Lf",2:"\\1e\\2c\\43\\2m\\4l\\Lg\\5\\4\\0\\3"},{1:"Lh",2:"\\19\\b\\1C\\1z\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"Li",2:"\\19\\b\\2B\\l\\5\\4\\0\\3"},{1:"Lj",2:"\\2W\\j\\2B\\l\\5\\4\\0\\3"},{1:"Lk",2:"\\j\\i\\n\\2E\\5\\4\\0\\3"},{1:"Ll",2:"\\3I\\2J\\2B\\l\\5\\4\\0\\3"},{1:"Lm",2:"\\3I\\2J\\9\\v\\5\\4\\0\\3"},{1:"Ln",2:"\\50\\4D\\5\\4\\0\\3"},{1:"Lo",2:"\\j\\i\\S\\7\\24\\2Z\\5\\4\\0\\3"},{1:"Lp",2:"\\65\\n\\n\\2m\\5\\4\\0\\3"},{1:"Lq",2:"\\j\\i\\3V\\4T\\5b\\1f\\5\\4\\0\\3"},{1:"Lr",2:"\\C\\h\\1y\\22\\1y\\z\\8\\0"},{1:"Ls",2:"\\1K\\d\\8\\0"},{1:"Lu",2:"\\x\\b\\l\\9\\8\\0"},{1:"Lv",2:"\\1K\\d\\2w\\9\\4\\0\\3"},{1:"Lw",2:"\\x\\b\\9\\4\\8\\0"},{1:"Lx",2:"\\x\\b\\a\\6\\8\\0"},{1:"Ly",2:"\\w\\2M\\9\\0\\3"},{1:"Lz",2:"\\x\\b\\L\\4\\8\\0"},{1:"LA",2:"\\x\\b\\a\\6\\0\\3"},{1:"LB",2:"\\x\\b\\4K\\4\\y\\19\\0\\3"},{1:"LC",2:"\\x\\b\\w\\k\\0\\3"},{1:"LD",2:"\\11\\44\\k\\0\\3"},{1:"LE",2:"\\x\\b\\8\\0"},{1:"LF",2:"\\x\\b\\f\\g\\8\\0"},{1:"LG",2:"\\I\\E\\f\\g\\0\\3"},{1:"LH",2:"\\6C\\2T\\f\\g\\0\\3"},{1:"LJ",2:"\\m\\E\\f\\g\\0\\3"},{1:"LK",2:"\\4Z\\1M\\0\\3"},{1:"LM",2:"\\b\\E\\f\\g\\0\\3"},{1:"LN",2:"\\3R\\E\\f\\g\\0\\3"},{1:"LO",2:"\\A\\4z\\f\\g\\0\\3"},{1:"LQ",2:"\\x\\b\\O\\y\\1S\\1F\\8\\0"},{1:"LR",2:"\\1K\\d\\1g\\28\\9\\4\\1T\\l\\0\\3"},{1:"LS",2:"\\2F\\2L\\0\\3"},{1:"LT",2:"\\4h\\6F\\j\\0\\3"},{1:"LV",2:"\\3R\\E\\l\\9\\0\\3"},{1:"LW",2:"\\11\\44\\0\\3"},{1:"LX",2:"\\I\\E\\L\\J\\0\\3"},{1:"LY",2:"\\m\\E\\9\\0\\3"},{1:"LZ",2:"\\x\\b\\9\\v\\0\\3"},{1:"M0",2:"\\b\\E\\l\\9\\0\\3"},{1:"M1",2:"\\x\\b\\D\\G\\0\\3"},{1:"M2",2:"\\x\\b\\16\\1W\\0\\3"},{1:"M3",2:"\\2F\\x\\a\\6\\0\\3"},{1:"M4",2:"\\1U\\2A\\16\\1W\\0\\3"},{1:"M5",2:"\\1K\\d\\a\\6\\0\\3"},{1:"M6",2:"\\1K\\d\\9\\4\\29\\2d\\6\\7\\0\\3"},{1:"M7",2:"\\1K\\d\\f\\g\\0\\3"},{1:"M8",2:"\\1K\\d\\O\\y\\0\\3"},{1:"M9",2:"\\2F\\x\\17\\T\\0\\3"},{1:"Ma",2:"\\A\\4z\\9\\0\\3"},{1:"Mb",2:"\\x\\b\\8\\0\\Z\\1m\\0\\3"},{1:"Mc",2:"\\x\\b\\f\\g\\8\\0\\11\\3D\\0\\3"},{1:"Md",2:"\\I\\E\\f\\g\\0\\3\\C\\Me\\0\\3"},{1:"Mf",2:"\\m\\E\\f\\g\\0\\3\\2C\\14\\1T\\l\\0\\3"},{1:"Mg",2:"\\11\\44\\k\\0\\3\\20\\5j\\0\\3"},{1:"Mh",2:"\\x\\b\\a\\6\\0\\3\\11\\a\\0\\3"},{1:"Mi",2:"\\x\\b\\l\\9\\8\\0\\4X\\1P\\a\\6\\0\\3"},{1:"Mj",2:"\\w\\2M\\9\\0\\3\\I\\P\\A\\1Y\\0\\3"},{1:"Mk",2:"\\A\\4z\\0\\3"},{1:"Ml",2:"\\1K\\d\\1X\\Mm\\O\\y\\0\\3"},{1:"Mn",2:"\\1K\\d\\Mo\\2D\\y\\V\\1T\\l\\0\\3"},{1:"Mp",2:"\\x\\b\\5\\4\\6\\7\\0\\3"},{1:"Mq",2:"\\6G\\x\\5\\4\\6\\7\\0\\3"},{1:"Ms",2:"\\20\\2f\\35\\5\\4\\6\\7\\0\\3"},{1:"Mt",2:"\\1K\\d\\1U\\3l\\5\\4\\6\\7\\0\\3"},{1:"Mu",2:"\\w\\d\\8\\0"},{1:"Mv",2:"\\3M\\85\\8\\0"},{1:"Mx",2:"\\x\\b\\1R\\z\\q\\t\\s\\a\\0\\r"},{1:"My",2:"\\6H\\5f\\8\\0"},{1:"MA",2:"\\x\\b\\O\\1S\\45\\1Y\\q\\t\\s\\a\\0\\r"},{1:"MB",2:"\\MC\\E\\5\\4\\6\\7\\0\\3"},{1:"MD",2:"\\1K\\d\\z\\1H\\q\\t\\s\\a\\0\\r"},{1:"ME",2:"\\2F\\x\\1y\\22\\5\\4\\6\\7\\0\\3"},{1:"MF",2:"\\4Z\\1M\\5\\4\\6\\7\\0\\3"},{1:"MG",2:"\\x\\b\\9\\4\\3q\\I\\P\\1n\\5\\4\\0\\3"},{1:"MH",2:"\\x\\b\\1y\\22\\3J\\3d\\3j\\5\\4\\0\\3"},{1:"MI",2:"\\A\\4z\\5\\4\\6\\7\\0\\3"},{1:"MJ",2:"\\4h\\6F\\j\\9\\4\\5\\4\\6\\7\\0\\3"},{1:"MK",2:"\\6C\\2T\\5\\4\\6\\7\\0\\3"},{1:"ML",2:"\\19\\27\\5\\4\\6\\7\\0\\3"},{1:"MM",2:"\\x\\b\\2K\\1F\\16\\1w\\5\\4\\0\\3"},{1:"MN",2:"\\5q\\6I\\5\\4\\6\\7\\0\\3"},{1:"MP",2:"\\x\\b\\9\\4\\5\\4\\6\\7\\0\\3"},{1:"MQ",2:"\\1K\\d\\MR\\1e\\k\\0\\q\\t\\s\\a\\0\\r"},{1:"MS",2:"\\6H\\5f\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"MT",2:"\\x\\b\\MU\\1W\\5\\4\\0\\3"},{1:"MV",2:"\\x\\b\\4v\\6e\\9\\v\\5\\4\\0\\3"},{1:"MW",2:"\\1K\\d\\I\\P\\a\\6\\5\\4\\0\\3"},{1:"MX",2:"\\6G\\x\\k\\0\\q\\t\\s\\a\\0\\r"},{1:"MY",2:"\\b\\E\\k\\0\\q\\t\\s\\a\\0\\r"},{1:"MZ",2:"\\A\\4z\\k\\0\\q\\t\\s\\a\\0\\r"},{1:"N0",2:"\\1K\\d\\z\\1q\\I\\P\\5\\4\\6\\7\\0\\3"},{1:"N1",2:"\\I\\E\\5\\4\\6\\7\\0\\3"},{1:"N2",2:"\\N3\\j\\N4\\J\\18\\7\\5\\4\\0\\3"},{1:"N5",2:"\\1K\\d\\9\\4\\m\\5j\\5\\4\\0\\3"},{1:"N6",2:"\\6V\\D\\5\\4\\0\\3"},{1:"N7",2:"\\x\\b\\y\\V\\5\\4\\0\\3"},{1:"N8",2:"\\x\\b\\17\\T\\5\\4\\6\\7\\0\\3"},{1:"N9",2:"\\x\\b\\L\\4\\5\\4\\0\\3"},{1:"Na",2:"\\1K\\d\\1L\\1Q\\5\\4\\0\\3"},{1:"Nb",2:"\\1K\\d\\5\\4\\6\\7\\0\\3"},{1:"Nc",2:"\\x\\b\\I\\P\\Nd\\2Z\\5\\4\\0\\3"},{1:"Ne",2:"\\x\\b\\J\\4\\5\\4\\0\\3"},{1:"Nf",2:"\\x\\b\\9\\4\\V\\47\\5\\4\\0\\3"},{1:"Ng",2:"\\1K\\d\\z\\1H\\5\\4\\6\\7\\0\\3"},{1:"Nh",2:"\\6C\\2T\\a\\6\\5\\4\\0\\3"},{1:"Ni",2:"\\x\\b\\G\\25\\5\\4\\6\\7\\0\\3"},{1:"Nj",2:"\\6G\\x\\3C\\3b\\5\\4\\0\\3"},{1:"Nk",2:"\\1K\\d\\D\\15\\5\\4\\0\\3"},{1:"Nl",2:"\\m\\E\\5\\4\\6\\7\\0\\3"},{1:"Nm",2:"\\11\\44\\5\\4\\6\\7\\0\\3"},{1:"Nn",2:"\\No\\6n\\4e\\5\\4\\6\\7\\0\\3"},{1:"Np",2:"\\6H\\5f\\9\\V\\5\\4\\0\\3"},{1:"Nq",2:"\\4Z\\1M\\4s\\4r\\5\\4\\0\\3"},{1:"Nr",2:"\\1K\\d\\l\\9\\5\\4\\0\\3"},{1:"Ns",2:"\\1K\\d\\I\\P\\9\\v\\5\\4\\0\\3"},{1:"Nt",2:"\\13\\Nu\\77\\78\\5\\4\\6\\7\\0\\3"},{1:"Nv",2:"\\3M\\85\\14\\1n\\S\\7\\5\\4\\0\\3"},{1:"Nw",2:"\\x\\b\\1n\\9\\5\\4\\0\\3"},{1:"Nx",2:"\\x\\b\\S\\7\\5\\4\\0\\3"},{1:"Ny",2:"\\x\\b\\1R\\z\\5\\4\\0\\3"},{1:"Nz",2:"\\x\\b\\2B\\l\\5\\4\\0\\3"},{1:"NA",2:"\\4Z\\1M\\z\\5M\\5\\4\\0\\3"},{1:"NB",2:"\\I\\E\\3o\\U\\5\\4\\6\\7\\0\\3"},{1:"NC",2:"\\5q\\6I\\2z\\2y\\9\\v\\5\\4\\0\\3"},{1:"ND",2:"\\b\\E\\5\\4\\0\\3"},{1:"NE",2:"\\1K\\d\\A\\V\\1L\\1Q\\5\\4\\0\\3"},{1:"NF",2:"\\x\\b\\NG\\NH\\5\\4\\0\\3"},{1:"NI",2:"\\3R\\E\\5\\4\\6\\7\\0\\3"},{1:"NJ",2:"\\1K\\d\\1C\\1z\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"NK",2:"\\m\\E\\1C\\1z\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"NL",2:"\\1K\\d\\2F\\x\\2B\\l\\5\\4\\0\\3"},{1:"NM",2:"\\x\\b\\k\\0\\q\\t\\s\\a\\0\\r"},{1:"NN",2:"\\1K\\d\\O\\45\\1B\\2Q\\5\\4\\0\\3"},{1:"NO",2:"\\b\\E\\L\\4\\5\\4\\0\\3"},{1:"NP",2:"\\3R\\E\\a\\6\\5\\4\\0\\3"},{1:"NQ",2:"\\5q\\6I\\2U\\27\\1n\\9\\5\\4\\0\\3"},{1:"NR",2:"\\4h\\6F\\j\\14\\1n\\S\\7\\5\\4\\0\\3"},{1:"NS",2:"\\18\\1a\\8\\0"},{1:"NT",2:"\\C\\w\\a\\6\\8\\0"},{1:"NU",2:"\\18\\1a\\a\\6\\8\\0"},{1:"NV",2:"\\13\\e\\8\\0"},{1:"NW",2:"\\18\\1a\\9\\v\\8\\0"},{1:"NX",2:"\\w\\H\\4n\\4v\\8\\0\\4a\\18\\1a\\4c"},{1:"NY",2:"\\18\\1a\\3X\\3U\\8\\0"},{1:"NZ",2:"\\18\\1a\\2w\\9\\8\\0"},{1:"O0",2:"\\18\\1a\\l\\9\\8\\0"},{1:"O1",2:"\\p\\h\\9\\4\\8\\0"},{1:"O2",2:"\\C\\w\\L\\4\\8\\0"},{1:"O3",2:"\\p\\h\\w\\k\\Y\\8\\0"},{1:"O4",2:"\\C\\w\\f\\g\\8\\0"},{1:"O5",2:"\\p\\h\\8\\0"},{1:"O6",2:"\\p\\h\\f\\g\\0\\3"},{1:"O7",2:"\\2F\\5k\\f\\g\\0\\3"},{1:"O8",2:"\\p\\h\\Z\\1i\\0\\3"},{1:"O9",2:"\\p\\h\\14\\l\\0\\3"},{1:"Oa",2:"\\w\\b\\O\\y\\1S\\1F\\8\\0"},{1:"Ob",2:"\\18\\1a\\1r\\1f\\0\\3"},{1:"Oc",2:"\\p\\h\\21\\7\\0\\3"},{1:"Od",2:"\\w\\b\\Z\\1i\\8\\0"},{1:"Oe",2:"\\p\\h\\2z\\2y\\9\\4\\0\\3"},{1:"Of",2:"\\p\\h\\9\\v\\0\\3"},{1:"Og",2:"\\p\\h\\l\\9\\0\\3"},{1:"Oh",2:"\\p\\h\\a\\6\\0\\3"},{1:"Oi",2:"\\p\\h\\k\\Y\\0\\3"},{1:"Oj",2:"\\e\\1a\\8\\0"},{1:"Ok",2:"\\20\\35\\8\\0"},{1:"Ol",2:"\\p\\h\\16\\1w\\0\\3"},{1:"Om",2:"\\6J\\5T\\l\\9\\0\\3"},{1:"Oo",2:"\\18\\1a\\33\\2O\\0\\3"},{1:"Op",2:"\\p\\h\\y\\19\\0\\3"},{1:"Oq",2:"\\18\\1a\\A\\0\\3"},{1:"Or",2:"\\18\\1a\\i\\p\\0\\3"},{1:"Os",2:"\\1a\\2T\\0\\3"},{1:"Ot",2:"\\18\\1M\\3e\\3i\\0\\3"},{1:"Ou",2:"\\18\\1M\\l\\9\\0\\3"},{1:"Ov",2:"\\18\\1a\\1m\\2H\\9\\v\\0\\3"},{1:"Ow",2:"\\18\\1a\\8\\0\\Ox\\Oy\\0\\3"},{1:"Oz",2:"\\p\\h\\8\\0\\56\\3g\\0\\3"},{1:"OA",2:"\\18\\1a\\a\\6\\8\\0\\D\\15\\0\\3"},{1:"OB",2:"\\20\\35\\8\\0\\a\\6\\0\\3"},{1:"OC",2:"\\e\\1a\\8\\0\\14\\l\\0\\3"},{1:"OD",2:"\\p\\h\\9\\4\\8\\0\\9\\v\\6\\7\\0\\3"},{1:"OE",2:"\\18\\1a\\9\\v\\8\\0\\2v\\z\\3J\\I\\P\\9\\v\\0\\3"},{1:"OF",2:"\\18\\1a\\3X\\3U\\8\\0\\U\\y\\V\\0\\3"},{1:"OG",2:"\\18\\1M\\9\\0\\3"},{1:"OH",2:"\\18\\1a\\9\\A\\0\\3"},{1:"OI",2:"\\13\\e\\8\\0\\9\\v\\6\\7\\0\\3"},{1:"OJ",2:"\\13\\e\\8\\0\\14\\l\\0\\3"},{1:"OK",2:"\\p\\h\\A\\V\\0\\3"},{1:"OL",2:"\\p\\h\\2z\\2y\\9\\4\\0\\3\\a\\6\\0\\3"},{1:"OM",2:"\\p\\h\\k\\Y\\0\\3\\Y\\2B\\0\\3"},{1:"ON",2:"\\p\\h\\Z\\1i\\0\\3\\a\\6\\0\\3"},{1:"OO",2:"\\p\\h\\y\\19\\0\\3\\1F\\A\\0\\3"},{1:"OP",2:"\\18\\1a\\1r\\1f\\0\\3\\1r\\1f\\a\\6\\0\\3"},{1:"OQ",2:"\\p\\h\\f\\g\\0\\3\\14\\l\\0\\3"},{1:"OR",2:"\\p\\h\\14\\l\\0\\3\\l\\9\\0\\3"},{1:"OS",2:"\\p\\h\\9\\v\\0\\3\\11\\6\\7\\0\\3"},{1:"OT",2:"\\14\\C\\0\\3"},{1:"OU",2:"\\18\\1a\\0\\3"},{1:"OV",2:"\\18\\1a\\9\\v\\a\\6\\0\\3"},{1:"OW",2:"\\18\\1a\\l\\9\\8\\0\\C\\2b\\0\\3"},{1:"OX",2:"\\C\\w\\f\\g\\8\\0\\18\\1a\\2j\\2l\\0\\3"},{1:"OY",2:"\\18\\1a\\24\\2Z\\9\\v\\0\\3"},{1:"OZ",2:"\\p\\h\\3k\\3H\\f\\g\\0\\3"},{1:"P0",2:"\\P1\\E\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"P2",2:"\\18\\1a\\5\\4\\6\\7\\0\\3"},{1:"P3",2:"\\2F\\5k\\5\\4\\6\\7\\0\\3"},{1:"P4",2:"\\13\\e\\5\\4\\0\\3"},{1:"P5",2:"\\6J\\d\\l\\9\\5\\4\\0\\3"},{1:"P6",2:"\\p\\h\\9\\4\\5\\4\\6\\7\\0\\3"},{1:"P7",2:"\\4L\\d\\5\\4\\8\\0"},{1:"P8",2:"\\18\\1a\\D\\15\\5\\4\\0\\3"},{1:"P9",2:"\\p\\h\\5\\4\\6\\7\\0\\3"},{1:"Pa",2:"\\18\\1a\\4l\\7o\\5\\4\\6\\7\\0\\3"},{1:"Pb",2:"\\5z\\Pc\\5\\4\\6\\7\\0\\3"},{1:"Pd",2:"\\7L\\E\\5\\4\\6\\7\\0\\3"},{1:"Pf",2:"\\18\\1a\\9\\V\\5\\4\\0\\3"},{1:"Pg",2:"\\6J\\d\\5\\4\\6\\7\\0\\3"},{1:"Ph",2:"\\18\\1a\\9\\v\\5\\4\\6\\7\\0\\3"},{1:"Pi",2:"\\Pj\\Pk\\5\\4\\0\\3"},{1:"Pl",2:"\\p\\h\\2w\\9\\5\\4\\6\\7\\0\\3"},{1:"Pm",2:"\\p\\h\\17\\T\\5\\4\\6\\7\\0\\3"},{1:"Pn",2:"\\p\\h\\w\\k\\Y\\q\\t\\s\\a\\0\\r"},{1:"Po",2:"\\18\\1a\\1g\\n\\5\\4\\6\\7\\0\\3"},{1:"Pp",2:"\\18\\1a\\1U\\3l\\5\\4\\6\\7\\0\\3"},{1:"Pq",2:"\\18\\1a\\2p\\2P\\9\\v\\5\\4\\0\\3"},{1:"Pr",2:"\\p\\h\\20\\35\\5\\4\\6\\7\\0\\3"},{1:"Ps",2:"\\Pt\\d\\5\\4\\6\\7\\0\\3"},{1:"Pu",2:"\\18\\1a\\z\\1H\\5\\4\\6\\7\\0\\3"},{1:"Pv",2:"\\p\\h\\1y\\22\\1y\\z\\5\\4\\6\\7\\0\\3"},{1:"Pw",2:"\\p\\h\\D\\15\\G\\24\\5\\4\\6\\7\\0\\3"},{1:"Px",2:"\\18\\1a\\16\\1w\\5\\4\\0\\3"},{1:"Py",2:"\\p\\h\\1m\\2H\\a\\6\\5\\4\\0\\3"},{1:"Pz",2:"\\p\\h\\3M\\PA\\5\\4\\0\\3"},{1:"PB",2:"\\18\\1a\\a\\6\\5\\4\\0\\3"},{1:"PC",2:"\\18\\1a\\U\\1o\\U\\2E\\5\\4\\0\\3"},{1:"PD",2:"\\18\\1a\\I\\P\\2j\\52\\5\\4\\6\\7\\0\\3"},{1:"PE",2:"\\18\\1M\\5\\4\\0\\3"},{1:"PF",2:"\\18\\1a\\A\\V\\5\\4\\0\\3"},{1:"PG",2:"\\p\\h\\S\\7\\5\\4\\0\\3"},{1:"PH",2:"\\18\\1a\\17\\T\\5\\4\\0\\3"},{1:"PI",2:"\\5Y\\B\\5\\4\\6\\7\\0\\3"},{1:"PJ",2:"\\13\\e\\9\\v\\5\\4\\6\\7\\0\\3"},{1:"PK",2:"\\e\\1a\\S\\7\\5\\4\\0\\3"},{1:"PL",2:"\\18\\1a\\9\\4\\5\\4\\6\\7\\0\\3"},{1:"PM",2:"\\18\\1a\\Z\\1S\\5\\4\\0\\3"},{1:"PN",2:"\\4L\\i\\5\\4\\6\\7\\0\\3"},{1:"PO",2:"\\p\\h\\O\\45\\5\\4\\0\\3"},{1:"PP",2:"\\2F\\5k\\a\\6\\5\\4\\0\\3"},{1:"PQ",2:"\\p\\h\\H\\6B\\3N\\27\\5\\4\\0\\3"},{1:"PR",2:"\\p\\h\\1m\\4w\\9\\v\\5\\4\\6\\7\\0\\3"},{1:"PS",2:"\\20\\35\\z\\1H\\5\\4\\0\\3"},{1:"PT",2:"\\p\\h\\a\\6\\5\\4\\0\\3"},{1:"PU",2:"\\p\\h\\1e\\30\\5\\4\\0\\3"},{1:"PV",2:"\\p\\h\\9\\v\\5\\4\\0\\3"},{1:"PW",2:"\\20\\35\\1L\\1Q\\5\\4\\6\\7\\0\\3"},{1:"PX",2:"\\F\\2f\\5\\4\\0\\3"},{1:"PY",2:"\\p\\h\\1r\\1f\\5\\4\\0\\3"},{1:"PZ",2:"\\7L\\E\\2z\\2y\\5\\4\\6\\7\\0\\3"},{1:"Q0",2:"\\p\\h\\1C\\1z\\f\\g\\q\\t\\s\\a\\0\\r"},{1:"Q1",2:"\\31\\5i\\8\\0"},{1:"Q2",2:"\\1t\\3e\\8\\0"},{1:"Q3",2:"\\p\\b\\8\\0"},{1:"Q4",2:"\\w\\b\\8\\0"},{1:"Q5",2:"\\p\\b\\a\\6\\8\\0"},{1:"Q6",2:"\\13\\2q\\l\\9\\8\\0"},{1:"Q7",2:"\\p\\b\\L\\4\\8\\0"},{1:"Q8",2:"\\w\\b\\J\\4\\a\\6\\8\\0"},{1:"Q9",2:"\\p\\b\\w\\k\\Y\\8\\0"},{1:"Qa",2:"\\p\\b\\f\\g\\8\\0"},{1:"Qb",2:"\\p\\b\\l\\9\\0\\3"},{1:"Qc",2:"\\31\\b\\0\\3"},{1:"Qd",2:"\\5v\\E\\f\\g\\0\\3"},{1:"Qe",2:"\\5O\\E\\0\\3"},{1:"Qf",2:"\\6f\\1n\\0\\3"},{1:"Qg",2:"\\p\\b\\14\\l\\0\\3"},{1:"Qh",2:"\\p\\b\\a\\6\\0\\3"},{1:"Qi",2:"\\p\\b\\2C\\14\\a\\6\\0\\3"},{1:"Qj",2:"\\p\\b\\A\\0\\3"},{1:"Qk",2:"\\b\\C\\8\\0"},{1:"Ql",2:"\\13\\2q\\k\\0\\3"},{1:"Qm",2:"\\13\\2q\\0\\3"},{1:"Qn",2:"\\p\\b\\9\\v\\0\\3"},{1:"Qo",2:"\\p\\b\\D\\15\\0\\3"},{1:"Qp",2:"\\p\\b\\9\\0\\3"},{1:"Qq",2:"\\p\\b\\O\\1S\\y\\19\\0\\3"},{1:"Qr",2:"\\p\\b\\16\\1W\\0\\3"},{1:"Qs",2:"\\p\\b\\9\\4\\8\\0"},{1:"Qt",2:"\\p\\b\\48\\1q\\0\\3"},{1:"Qu",2:"\\p\\b\\3k\\4Y\\f\\g\\0\\3"},{1:"Qv",2:"\\p\\b\\k\\Y\\0\\3"},{1:"Qw",2:"\\p\\b\\3o\\U\\y\\19\\0\\3"},{1:"Qx",2:"\\31\\5i\\8\\0\\2o\\31\\0\\3"},{1:"Qy",2:"\\p\\b\\9\\4\\8\\0\\a\\6\\0\\3"},{1:"Qz",2:"\\p\\b\\a\\6\\8\\0\\75\\31\\0\\3"},{1:"QA",2:"\\b\\C\\8\\0\\4l\\j\\0\\3"},{1:"QB",2:"\\p\\b\\A\\0\\3\\h\\10\\0\\3"},{1:"QC",2:"\\p\\b\\f\\g\\8\\0\\7t\\2D\\0\\3"},{1:"QD",2:"\\p\\b\\L\\4\\8\\0\\i\\1P\\a\\6\\0\\3"},{1:"QE",2:"\\w\\b\\J\\4\\a\\6\\8\\0\\3o\\U\\0\\3"},{1:"QF",2:"\\p\\b\\14\\l\\0\\3\\QG\\QH\\0\\3"},{1:"QI",2:"\\p\\b\\l\\9\\0\\3\\b\\p\\0\\3"},{1:"QJ",2:"\\5v\\E\\f\\g\\0\\3\\b\\74\\0\\3"},{1:"QK",2:"\\p\\b\\9\\v\\0\\3\\29\\2d\\6\\7\\0\\3"},{1:"QL",2:"\\p\\b\\w\\k\\Y\\8\\0\\31\\4t\\0\\3"},{1:"QM",2:"\\1t\\3e\\8\\0\\58\\1G\\71\\0\\3"},{1:"QN",2:"\\13\\2q\\l\\9\\8\\0\\D\\b\\0\\3"},{1:"QO",2:"\\13\\2q\\f\\g\\0\\3"}];', 62, 3275, 'u5b66|code|name|u9662|u4e1a|u804c|u6280|u672f|u5927|u5de5|u79d1|u5357|u897f|u5dde|u6c5f|u5e08|u8303|u5317|u4e1c|u5c71|u533b|u7406|u5b89|u6d77|u5e7f|u6e56|u9ad8|u6821|u4e13|u7b49|u4eac|u7a0b|u4e2d|u6cb3|u7ecf|u7535|u5546|u5b81|u534e|u57ce|u9633|u5929|u5efa|u56fd|u4fe1|u6797|u5e86|u519c|u5ddd|u4e0a|u8d22|u606f|u5fbd|u56db|u827a|u901a|u5916|u8d38|u6d59|u91cd|u836f|u6c11|u6d25|u65b0|u8fbd|u957f|u6587|u5e02|u8b66|u4ea4|u6b66|u6d4e|u6c49|u82cf|u77f3|u4e91|u9752|u80b2|u822a|u5c14|u65cf|u9ed1|u9f99|u798f|u751f|u5316|u8bed|u6ee8|u5b50|u4f53|u8d35|u5409|u7586|u54c8|u5b98|u5185|u6c34|u513f|u90fd|u91d1|u5e7c|u8499|u53e4|u6cd5|u5bb6|u529b|u8fde|u5fb7|u90d1|u65c5|u660c|u9655|u5e84|u65b9|u6e38|u673a|u653f|u7ba1|u94c1|u6625|u5bdf|u6210|u52a1|u6c88|u4e09|u7f8e|u5229|u4ee3|u8bbe|u7b51|u73b0|u6e90|u7a7a|u5e94|u536b|u590f|u5c9b|u7528|u5170|u95e8|u53a6|u660e|u8fd0|u4f20|u6cb9|u5a92|u6d0b|u8083|u5174|u8f6f|u6c99|u7518|u6cc9|u5408|u9f50|u90ae|u8f7b|u53f0|u8f66|u6c7d|u9053|u62a4|u4eba|u8fbe|u4e8b|u9ec4|u6606|u7269|u80a5|u574a|u53f8|u6dee|u539f|u592a|u4e50|u4ef6|u878d|u88c5|u52a8|u53e3|u80fd|u5e38|u6cf0|u670d|u4fdd|u8ba1|u5e74|u6e58|u6842|u97f3|u516c|u5ce1|u6ce2|u535a|u5eb7|u9521|u89c6|u54c1|u5f71|u73af|u9996|u9645|u884c|u51b6|u4e49|u5883|u7b2c|u8def|u529e|u4e9a|u6d89|u9632|u548c|u961c|u4e34|u676d|u6e29|u73e0|u5609|u626c|u5934|u65e0|u6cbb|u620f|u98df|u8054|u65af|u9c81|u95fd|u4e8c|u6f4d|u4e0e|u5065|u4ec1|u5f00|u8d44|u94dc|u5f90|u57f9|u6d1b|u987a|u70df|u7ec7|u7279|u521b|u7eba|u5b9a|u540c|u5510|u66f2|u592e|u8fdc|u4e61|u7a0e|u85cf|u6613|u5973|u77ff|uff08|u9ed4|uff09|u767d|u5e97|u4e4c|u5149|u5e73|u5ef6|u6728|u6e24|u8239|u8d63|u5730|u533a|u5cad|u7acb|u74f7|u9676|u674f|u6f33|u8d28|u6001|u7696|u5eca|u4e18|u9675|u5bbf|u71d5|u6cfd|u91cc|u5173|u5305|u6e2f|u9547|u4e5d|u7267|u9102|u52b3|u8425|u9075|u664b|u6865|u9152|u5b9c|u6b8a|u5e8a|u677e|u7ea2|u4e07|u4e00|u8bb8|u83cf|u9526|u64ad|u4e39|u666f|u601d|u77e5|u8bd1|u5f20|u7ffb|u76d0|u6559|u56ed|u7701|u547c|u4f5c|u4f1a|u5cf0|u6f6d|u5168|u5188|u8272|u6c55|u60e0|u6df1|u5733|u9e64|u7530|u5267|u94f6|u5ba1|u8861|u6b63|u6c60|u6d32|u6069|u5171|u82f1|u4ea7|u5bf9|u90a2|u7164|u67f3|u4f73|u7389|u8d64|u6f14|u839e|u6c14|u9ece|u90b5|u516d|u6c42|u6bd5|u8282|u695a|u96c4|u4e3d|u96c6|u8f93|u54b8|u666e|u591a|u94a2|u961f|u534f|u9647|u5a01|u755c|u90e8|u514b|u5b9d|u8fc1|u6ca7|u70ad|u7ecd|u91cf|u6000|u7cfb|u963f|u62c9|u868c|u57e0|u9634|u6ec1|u9a6c|u6e05|u627f|u978d|u629a|u7248|u8d24|u76d8|u4f0a|u5236|u804a|u9020|u7261|u67a3|u571f|u5468|u57a6|u8fb9|u9876|u6f2f|u7126|u58c1|u8346|u5ffb|u51ef|u51fa|u610f|u5415|u80af|u6d69|u9756|u6eaa|u6b27|u6ec7|u6c38|u6881|u5b8f|u4fa8|u90af|u9e21|u754c|u6e2d|u6986|u5cb3|u6f47|u76ca|u70f9|u996a|u90f8|u5237|u6709|u821e|u66a8|u517d|u5ba3|u7281|u5175|u56e2|u5180|u8087|u661f|u623f|u90ed|u8236|u4f5b|u6fb3|u6714|u8862|u6811|u9e70|u67ab|u5907|u6d66|u7ff0|u4ed1|u5e03|u829c|u4eb3|u767e|u68a7|u8e48|u5937|u8bda|u8f68|u6e7e|u624d|u8944|u743c|u7cae|u745e|u9976|u590d|u65e6|u5370|u80dc|u98de|u6c82|u6c47|u7ef5|u6dc4|u4f26|u5bbe|u6258|u5e0c|u671b|u5df4|u5c01|u8d1d|0000001519630000|u89c9|UniversityList|0000000455820000|0000000723440000|0000011227580000|0000001079180000|0000000420490000|u969c|0000000646760000|0000001439940000|0000010003210000|0000010426420000|u793e|0000011224580000|0000011224610000|0000000647390000|0000011229430000|0000011224630000|0000011224650000|0000011224660000|0000011224680000|0000011224690000|0000011224700000|0000011224710000|u7960|0000011224730000|0000011223490000|0000002470460000|0000011227630000|0000011224760000|0000001289630000|0000008262050000|0000000397340000|0000011228680000|0000000715540000|0000000418710000|0000007581410000|0000011228740000|0000001434850000|0000011228780000|0000010427990000|0000007482010000|0000000914930000|0000000014530000|0000002438010000|0000000900270000|0000000157620000|0000009931720000|0000011228900000|0000000043390000|0000011229460000|0000001219240000|u6c7e|0000001321230000|0000011228950000|0000000135540000|0000000505380000|0000011228970000|0000000168160000|0000000472260000|0000001179370000|0000011229010000|u6f5e|0000002308350000|0000000364560000|0000005406180000|0000002307560000|0000003632270000|0000011229090000|0000001550240000|0000009408460000|0000001142870000|0000011229130000|0000011229160000|0000011229190000|u8001|0000009949730000|0000011229230000|0000000309190000|0000011229260000|0000011179690000|0000003074680000|0000010246960000|0000007480970000|0000011229330000|0000011229360000|0000007012590000|0000011229410000|0000000977960000|0000003622240000|0000000698860000|0000000609890000|0000001534120000|0000001258120000|0000001518560000|0000001134160000|0000001801260000|0000002332550000|0000000216680000|0000001881770000|0000001197520000|0000011223500000|0000002289950000|0000001318220000|0000000844780000|0000000864430000|0000000803830000|0000011227740000|0000000003260000|0000000300640000|0000011223510000|u5957|0000010063240000|0000001304560000|0000003579250000|0000011225610000|0000007479110000|0000011225640000|u9e3f|0000011225660000|0000011225680000|0000001327550000|0000010012710000|0000001138760000|0000000691680000|0000010585900000|u4e30|0000010014900000|0000000450380000|0000006998060000|0000000055160000|0000000247760000|0000011229720000|0000006689610000|0000002280460000|0000002118640000|0000002562350000|0000011229810000|0000002303100000|0000011223470000|u52d2|0000000201300000|0000000523690000|0000011229850000|0000010790120000|u72ee|0000011225770000|0000001390840000|u8a00|0000011229910000|u6c81|0000001360900000|0000001289030000|0000011229960000|0000011229990000|0000011230010000|0000011230040000|0000011230060000|0000009930210000|0000007544070000|0000010754250000|0000011230100000|0000007884430000|0000011230150000|u6ee1|0000002568040000|u4fc4|0000011230160000|0000011230190000|0000011230200000|0000001881290000|0000011225780000|u5584|0000011230250000|0000011230280000|0000011230310000|0000000869880000|0000000595730000|0000011230340000|u624e|u5c6f|0000001386940000|0000000340050000|0000011225790000|0000000600420000|0000003041700000|0000002299130000|0000002090660000|0000001139050000|0000000167910000|0000000012410000|0000001475650000|0000010099630000|0000000341900000|0000000485680000|0000004468550000|0000001532900000|0000002105250000|0000001165990000|0000007478130000|0000011225810000|0000002143740000|0000001587230000|0000002111480000|0000000167960000|0000001619480000|0000000043770000|0000000908900000|0000003073190000|0000000451010000|0000001613380000|0000003481760000|0000009481680000|0000000665500000|0000001969130000|u5211|0000000110220000|0000001171690000|0000000963090000|0000000155430000|u8fc5|0000007480570000|0000001882610000|0000000325530000|0000001814320000|0000011226250000|0000004652110000|0000002961680000|0000011223520000|0000011226330000|0000011226360000|0000011226400000|0000001114260000|0000011226500000|0000007479990000|0000011226590000|0000011226630000|0000011226680000|0000011226720000|u7597|0000011226770000|0000011226830000|0000011226870000|0000011226930000|0000011226980000|0000011227020000|0000011225830000|0000010949520000|0000011227110000|0000011225850000|0000007578160000|u4f55|u6c0f|0000011227190000|u6cbd|0000007571320000|0000011223530000|0000011227260000|0000011227930000|0000011231080000|u671d|0000002150460000|0000011225860000|0000000970980000|0000000068010000|0000001467970000|0000011231150000|0000011225870000|0000001115430000|0000004838580000|0000000062390000|0000011231200000|0000011223190000|0000011225880000|0000000611120000|0000003479400000|0000001219090000|0000001872740000|u7231|0000000668770000|0000011231340000|0000007479950000|0000000015220000|0000001967050000|0000007578170000|0000002431930000|0000000934340000|0000011231470000|0000001437530000|u544a|0000000916170000|0000001780150000|0000001185040000|0000000254850000|0000011225890000|0000000047570000|0000005807310000|0000001783470000|0000000417290000|0000011229940000|0000000177780000|0000007578180000|0000000429250000|u53f6|0000006313720000|0000004528980000|0000001038440000|0000001714780000|0000001917640000|0000005822520000|0000008716130000|0000011231580000|0000007480900000|0000008794320000|0000006751510000|0000000601540000|0000000971000000|0000008465630000|0000008185180000|0000007480580000|0000007479580000|0000011231710000|0000007482430000|0000011231770000|0000011231780000|0000011231790000|0000011231820000|0000011231830000|0000011227990000|0000001627760000|0000000443850000|0000010006000000|0000001036610000|0000000303040000|0000000104850000|0000001786830000|0000000838970000|0000001482560000|0000000392160000|0000011223540000|0000002956250000|0000001600450000|0000000168800000|0000001489290000|0000001389950000|0000011227560000|0000001534910000|0000001284820000|0000011223550000|0000000779430000|0000002579240000|0000006595700000|0000000981400000|0000002871300000|0000003117220000|0000003903810000|0000004872490000|0000003627320000|0000002731610000|0000007479600000|0000000103120000|0000011224590000|0000000950550000|0000011224620000|0000011224640000|0000011227620000|0000011224670000|0000007480510000|0000011223560000|0000008059120000|u753b|0000011224720000|0000007276290000|0000007479980000|0000011224740000|0000011224750000|0000000328020000|0000002959510000|0000011228650000|0000000400830000|0000000109680000|0000000388700000|0000001259360000|0000000671580000|0000000396220000|0000011228720000|0000004649700000|0000001273690000|0000011228790000|0000000469840000|0000001573510000|0000010292550000|0000011228850000|0000004311620000|0000009184020000|0000011228890000|0000009398810000|0000007480000000|0000007877580000|0000011228960000|0000001074500000|0000001359660000|0000001277290000|0000002962620000|0000000012460000|0000000555250000|0000000743970000|0000001933920000|0000007478620000|0000001765200000|u5c4b|0000011227650000|u516b|0000001436420000|0000001520600000|0000004711070000|0000000553920000|0000001533210000|0000003067220000|0000001023310000|0000002113360000|0000003067300000|0000000030100000|0000001763380000|0000001032980000|0000000374960000|0000001010910000|0000001334430000|0000002481720000|u7ee5|0000000144610000|0000004714480000|0000007572920000|0000001538560000|0000011227700000|0000011227710000|0000011225310000|0000003637670000|0000007480250000|0000007480330000|0000000008920000|0000011227720000|0000011225450000|0000011227730000|0000000376920000|0000007576390000|u5251|0000011225540000|u53d1|0000001533870000|0000011223570000|0000007245500000|0000007481460000|0000001576570000|0000011229530000|0000001410550000|0000000622190000|0000002134180000|0000011229560000|0000002005400000|0000001401510000|0000001982850000|0000002478510000|0000011229610000|0000001072090000|0000004610940000|0000002072850000|0000011229680000|0000004643520000|u5c97|0000000974000000|0000000893650000|0000002876670000|0000003044700000|0000005052800000|0000011229820000|0000000631280000|0000001017500000|0000011229860000|0000011229890000|0000011229920000|0000007479450000|0000001116270000|0000011220410000|0000001237270000|0000001820100000|0000004712360000|0000011230050000|u4e03|0000001251520000|0000002447550000|0000009600480000|0000011129990000|0000011230110000|0000004979830000|0000006254530000|0000011224830000|0000002101470000|0000010155840000|0000005754060000|0000011230210000|0000011230260000|0000011230290000|0000011230320000|u51b0|u96ea|0000001342890000|0000002309490000|0000001296770000|0000000450580000|0000001406260000|0000000976060000|0000000082510000|0000000028160000|0000000918460000|0000002056890000|0000001454200000|0000000657290000|0000011225970000|0000001071250000|0000009476640000|0000010013830000|0000000814050000|0000003643690000|0000000079630000|0000000101300000|0000000647570000|0000011223580000|0000004248880000|0000000658790000|0000000718050000|0000001236710000|0000001785440000|0000000923000000|0000000686240000|0000001069380000|0000010955270000|0000011224850000|0000000302090000|0000001453540000|0000000180870000|u6749|0000003251520000|0000000763330000|0000000151630000|0000000937240000|0000011226200000|u4f1f|0000010046850000|0000011230410000|0000011226220000|0000011230430000|0000011226230000|0000000123430000|0000011227850000|u7ebd|u7ea6|0000002013840000|0000011223590000|0000011230900000|0000011230920000|0000002071350000|0000011230450000|0000001411100000|0000002143410000|0000011224870000|0000000696720000|0000000003320000|0000002439530000|0000002428100000|0000010163100000|u9707|0000007960140000|0000008637010000|var|0000002139440000|0000000245710000|0000010238160000|0000002911160000|0000005339510000|0000011230980000|0000001941320000|0000001977820000|0000000168040000|u90a6|0000000962390000|0000011224890000|0000001228370000|0000011231020000|0000000657310000|0000000658170000|0000008138290000|0000007480160000|0000011223600000|0000000222080000|0000001988890000|0000000609250000|0000003193190000|0000000391190000|0000001606310000|0000000421910000|0000000039890000|0000000167240000|0000001492320000|0000001323420000|0000010955790000|0000001242760000|0000000044200000|0000000887620000|0000000126840000|0000002129990000|0000000602630000|u4e16|0000000951050000|0000001986250000|0000000921810000|u7eaa|0000000804560000|0000002033180000|0000003197380000|0000004797620000|0000002107070000|0000003573050000|0000011223480000|0000001982270000|0000002568100000|0000009017390000|0000002282900000|0000000294450000|0000010153110000|0000001561730000|u719f|0000001586250000|0000001144390000|0000001095090000|0000002140010000|0000003071590000|0000000271020000|0000000140100000|0000003115590000|0000000792180000|u6653|0000011223610000|0000001109260000|0000000518940000|0000011227460000|0000011227470000|0000007479430000|u68ee|0000011227510000|0000001809070000|0000011231800000|0000001441860000|0000007480130000|0000001876210000|0000000527590000|0000000717590000|0000011224920000|0000011224110000|0000011224130000|0000011224150000|u7d2b|0000011224170000|0000011224190000|0000011224200000|0000011224220000|0000011224240000|0000000093310000|0000011224260000|0000011224270000|0000011224290000|0000010332160000|0000011224310000|0000011224330000|0000011224350000|0000011224370000|0000011224390000|0000011224400000|0000011224420000|0000011224440000|0000011224460000|u803f|0000011224480000|0000011224500000|0000006524190000|0000011224520000|0000011224540000|0000000179420000|0000000943370000|0000003453640000|0000011227600000|0000007789590000|0000011227610000|u675c|0000000169050000|0000011228510000|0000001202150000|0000002124300000|0000001739350000|0000007479530000|0000008721710000|0000011228560000|0000000793500000|0000011228600000|0000001600680000|0000002133340000|0000011223280000|0000011224950000|0000001123100000|0000011228620000|0000011228640000|0000000431570000|0000000013410000|0000011228660000|0000000791460000|0000000486610000|0000000048800000|0000003645530000|u7845|0000011228700000|0000011228730000|0000011228750000|0000000161210000|0000001201490000|0000001623540000|0000011228820000|0000002874070000|0000011228860000|0000001476380000|0000010916910000|0000011228910000|0000003115260000|0000001332600000|0000004355060000|0000011228940000|0000001388230000|0000000205480000|0000000999860000|0000011223620000|0000000246210000|0000002422210000|0000000574000000|0000000604650000|0000001556770000|0000001615100000|0000011223460000|0000006697330000|0000011227810000|0000011229020000|u708e|0000000488680000|0000011229040000|0000002449650000|0000000387940000|u949f|0000001308610000|0000000355170000|0000011229100000|0000002210100000|0000010925490000|0000011229140000|0000001129280000|0000011229200000|0000007478650000|0000003142300000|0000000498490000|0000011229270000|0000002121020000|0000000292810000|0000000505730000|0000011229310000|0000000653580000|0000011229370000|0000011229390000|0000000221020000|0000005929290000|0000011229440000|0000001307590000|0000011229450000|0000000561840000|0000003476230000|0000008271910000|0000009728020000|u767b|0000011229490000|0000002304510000|0000008746980000|0000000307900000|0000011227050000|0000011227090000|0000011227140000|0000001588260000|0000002450340000|0000002281430000|u682a|0000001789130000|0000000121600000|0000000101560000|0000001717290000|0000000865730000|u4f17|0000011231120000|0000000305470000|0000002160730000|0000001934310000|0000001402450000|0000007740750000|0000001580010000|0000001228830000|0000000559400000|0000011231230000|u9669|0000007480560000|0000000839560000|u7f51|u7edc|0000002442910000|0000002057890000|0000002203030000|0000001946400000|0000001137500000|0000011231380000|0000000250420000|0000001013880000|0000000617500000|0000011231420000|u90f4|0000011231450000|u5a04|u5e95|0000002988900000|0000011227840000|0000000322530000|0000000453800000|0000002012780000|0000000662470000|0000011231510000|0000001429110000|0000000685980000|0000011231530000|0000000867090000|0000009494560000|0000011229070000|0000001245570000|0000000408740000|0000001436230000|0000001575370000|0000001967080000|0000000532390000|0000000936590000|0000011231610000|0000000311590000|0000005971490000|0000001441420000|0000011178460000|u5db7|0000001779010000|0000007574510000|0000011231760000|0000001158840000|0000001569640000|0000000979710000|u6d41|0000007480640000|u901f|0000007746370000|0000002309330000|0000001561740000|0000011228040000|0000003283410000|0000010615140000|0000011228070000|0000007545630000|0000011228110000|0000008722720000|0000007480390000|u592b|0000008070000000|0000011228190000|0000007480320000|0000007579920000|0000011228220000|0000003343040000|0000000482810000|u5c5e|0000011228240000|0000011228280000|0000011228300000|0000011228330000|0000000300290000|0000002329450000|0000002307790000|0000001741220000|0000001436080000|0000001050090000|0000003199250000|0000000862330000|0000011227590000|0000002053080000|0000001521310000|0000000823810000|0000000963410000|0000001497290000|u97f6|0000001142120000|u707e|0000001946570000|u97e9|0000011223800000|0000000608120000|0000001276550000|0000000227470000|0000000619000000|0000000094490000|0000000958080000|0000000209670000|0000001456130000|0000002797980000|0000002482730000|0000005802080000|0000011223810000|0000001223530000|0000002156680000|0000011224770000|0000000246350000|0000010159500000|u4ef2|u607a|0000000715440000|u4e94|u9091|0000000065190000|0000011224780000|0000007478640000|0000001327830000|0000005778010000|0000000296250000|0000003479660000|0000001518380000|0000007480040000|0000000281680000|0000000540170000|0000011223820000|0000011224800000|0000011224810000|0000011224820000|0000011224840000|0000011224860000|0000011224880000|u5bf8|0000011224910000|0000011224930000|0000011224940000|u5206|0000011224980000|0000011225000000|0000001212460000|0000011225030000|0000011225050000|0000011225080000|0000011225100000|0000007479550000|0000011223830000|0000011225140000|0000011225150000|0000007479680000|0000006546720000|0000000926060000|u9999|0000001033160000|0000007094440000|0000000616510000|0000005450860000|0000011229320000|u6f6e|0000003640970000|0000011229380000|0000011229400000|u79c1|0000001230690000|0000010007640000|u756a|u79ba|0000000272000000|0000002959790000|0000003121960000|0000001186780000|0000000988390000|0000010105770000|0000007252840000|0000009061890000|0000000036940000|0000000528550000|0000003084560000|0000001088220000|0000001985560000|0000003894160000|0000011229580000|0000000132350000|u5c3e|0000000868270000|u7f57|0000000209320000|0000000477740000|0000001194280000|0000011229710000|0000011229740000|u63ed|0000000526900000|0000000251360000|0000000992330000|0000002804210000|0000004150580000|0000011227880000|0000011229830000|0000002343980000|0000007480620000|0000001257020000|0000005906090000|u706b|u70ac|0000003075330000|0000011226440000|0000001791450000|u8302|u540d|0000000114610000|0000000445320000|0000011226490000|0000011230030000|0000010431390000|0000002310070000|0000011223840000|0000011230090000|0000010601300000|0000002454040000|0000003853780000|0000002308630000|0000001924680000|0000000168120000|0000000833060000|0000011230270000|0000002449960000|0000005709380000|0000011230360000|0000009935070000|0000007887440000|0000000698150000|0000010613650000|0000011230440000|0000007481040000|0000007480930000|0000011226540000|0000007480910000|0000007480540000|0000011230540000|0000011223850000|0000011230590000|0000011230620000|0000011223860000|0000011230660000|0000011230690000|0000011230720000|u78a7|0000011226580000|0000011230740000|0000001868600000|0000011223870000|0000000168140000|0000010731470000|0000000860920000|0000002108980000|u53f3|0000002479060000|0000001089370000|0000001037030000|0000003645570000|0000003482840000|0000001235120000|0000011226620000|0000001197610000|0000011226670000|0000001096470000|0000002094910000|0000000168690000|0000011226710000|0000002457310000|0000011226760000|0000011226240000|0000000225760000|0000011226260000|0000000169020000|u94a6|0000007885100000|0000011226310000|0000000168220000|u8d3a|0000011226380000|0000011226420000|0000011226470000|0000011226520000|u9e7f|0000011226560000|u76f8|0000011226610000|u6f13|0000011226660000|0000011226700000|u8d5b|0000011226820000|0000011226750000|0000011226810000|0000007480020000|0000011226910000|0000000724230000|0000002317940000|0000002478600000|0000001143750000|0000011231000000|0000001766010000|0000011231030000|0000011226860000|0000001112310000|0000000297430000|0000000245930000|0000003048690000|0000003050490000|0000008014670000|0000000743770000|0000008864930000|0000000793620000|0000009300050000|0000001136400000|0000004061420000|0000009891510000|0000000350180000|0000003331380000|0000010688090000|0000000383030000|0000011231280000|0000011231320000|0000011047230000|0000007482970000|0000007582440000|0000008261410000|0000008114200000|0000011231430000|0000007479690000|0000011231480000|0000011231490000|0000011231500000|0000001051960000|0000000169080000|0000011226920000|0000001732730000|0000003456610000|0000010306680000|0000007884440000|0000002097500000|0000010275870000|0000001437690000|0000001933280000|0000000762580000|0000011231590000|0000003047180000|0000007480030000|0000009685520000|0000011088560000|0000007578380000|0000000961520000|0000011229180000|0000000259390000|0000001255280000|0000001249070000|0000000344790000|0000001459110000|0000002007680000|0000000823340000|0000011226970000|0000000659110000|0000009087660000|0000011227010000|0000001519600000|0000001766440000|0000002982650000|0000000175410000|0000001043420000|0000011224230000|0000011224250000|0000011223880000|0000011224280000|0000011224300000|0000011224320000|0000011224340000|u667a|0000011224360000|u6d3e|0000011224380000|u79fb|0000011223890000|0000009061480000|0000001550270000|0000000629240000|0000000827090000|0000008032470000|0000007986070000|0000007730890000|0000000183300000|0000001702040000|0000007482670000|0000000655310000|0000000023140000|0000010872740000|0000011227060000|0000008585160000|0000000539560000|0000000107480000|0000011228580000|0000003488640000|0000004610390000|0000010386250000|0000008924390000|0000007245450000|0000010011930000|0000011223900000|u8baf|0000006156350000|0000008432320000|0000007480940000|0000007480200000|0000007480950000|0000011223910000|0000007576700000|0000000829830000|0000011228770000|0000011228800000|0000011228810000|0000011228830000|0000011228840000|0000011228870000|0000011228880000|0000001405970000|0000003064940000|0000000670440000|0000002427680000|0000001237860000|0000000620530000|0000011223920000|0000000701560000|0000001941580000|0000001938800000|0000011227100000|0000000892720000|0000002565560000|0000001827090000|0000011223930000|0000000580520000|0000003453320000|0000000239100000|0000000690870000|0000002110300000|0000011227150000|0000003578190000|0000001205080000|0000011227180000|0000001934870000|0000000168680000|0000011223940000|u575d|0000003474510000|0000000778550000|0000000331830000|0000000097870000|0000002152750000|0000011227690000|0000007885070000|0000000303970000|u6500|u679d|u82b1|0000011223950000|0000002266320000|0000004829850000|0000007480520000|0000011225400000|0000011225430000|0000011225470000|0000011225500000|0000011227220000|0000011225520000|0000011225550000|0000011225560000|0000002989660000|0000011229520000|0000011225570000|0000011225580000|u5e9c|0000011225590000|0000011225600000|0000011225630000|0000011225650000|0000001609030000|0000011227920000|0000007885910000|0000011225700000|0000002290920000|0000011229640000|0000000397710000|0000002156720000|0000000463030000|0000000843340000|0000001446710000|0000011229780000|u5145|0000000317760000|0000010154420000|0000002315860000|0000000461140000|0000001980060000|0000011229880000|0000002012210000|0000001488620000|0000000432910000|0000000319580000|0000001032760000|0000000651580000|u6807|u699c|0000001456890000|0000000082980000|0000000852700000|u6cf8|0000000673430000|u7709|0000009536650000|0000000094750000|0000000709110000|0000002134770000|u96c5|0000000934560000|0000000439240000|0000001201320000|0000004831020000|0000007482480000|0000007309200000|0000003340080000|0000008852260000|0000001174960000|0000011230380000|0000002298910000|0000005061770000|0000008635320000|0000010963380000|0000007960700000|0000007571730000|0000011230520000|0000011230550000|0000011230570000|0000011230600000|0000011230630000|0000001918640000|0000011230640000|0000011230670000|u68b0|0000011230700000|u8f69|0000011230730000|0000011230750000|0000011223960000|0000011230770000|0000011230780000|0000011230790000|0000001333460000|0000002028560000|0000011227800000|0000004646000000|0000001919510000|0000001243840000|0000000637800000|0000000103610000|0000000708230000|0000000168490000|0000011231100000|0000011189440000|0000000168190000|0000011226210000|0000000849740000|0000001739630000|0000000397960000|0000000503010000|0000007478630000|0000007881050000|0000000612100000|0000011227860000|0000011231140000|0000011223970000|0000011226290000|u65f6|u73cd|0000011226320000|0000011226350000|0000011226390000|0000011226430000|0000011226480000|0000001743160000|u662f|0000011226530000|0000011226570000|u795e|u5947|0000011223980000|0000011227910000|0000000167000000|0000003043660000|0000003635620000|0000000272290000|0000000141360000|0000011230990000|0000000895280000|0000000277650000|0000000840960000|0000011231010000|0000010986570000|0000000743580000|0000011231040000|0000001006540000|0000003074050000|0000000589240000|0000011231090000|0000002864690000|0000009409460000|0000011231130000|0000000922010000|0000000940200000|0000008577280000|0000011231160000|u76db|0000011231180000|0000011231190000|0000011231210000|0000011231220000|0000011231240000|0000011231260000|0000011231290000|0000011231330000|0000000118180000|0000002243870000|0000001983770000|0000007478150000|0000005503620000|0000011223990000|0000003573070000|0000001015150000|0000011224000000|u662d|0000001950860000|0000000860400000|0000011224010000|u6d31|0000008945820000|0000000633690000|0000000040470000|0000000601980000|0000001203230000|0000002980430000|u6750|0000001117370000|0000000005270000|0000000459100000|0000001485440000|0000011183680000|0000011227380000|0000011227390000|0000000811850000|0000011227400000|0000011227410000|0000011227420000|0000011227430000|0000011227440000|0000011227450000|0000007480140000|0000011227490000|0000000873420000|0000000943880000|0000001018390000|0000000208460000|0000002206240000|0000010069050000|0000001136950000|0000001090950000|0000008213800000|0000001587510000|u53cc|u7eb3|0000010630940000|0000001286990000|0000001056400000|0000001826830000|u70ed|u5e26|0000001483230000|0000006007110000|0000001486150000|0000001276840000|0000011224020000|0000011228250000|0000011228290000|0000011228310000|0000011228340000|0000011228350000|0000002121660000|0000011228370000|0000000066360000|0000011228390000|0000008946450000|0000011228420000|0000007685010000|u946b|0000006268530000|0000011228470000|0000011228490000|0000011228500000|0000007574140000|0000011228530000|0000011228540000|0000007579700000|0000011228550000|u6d88|0000011228590000|0000000868990000|0000002802020000|0000011224030000|0000007353460000|0000001458340000|0000011228630000|u8428|0000003160990000|0000001611710000|0000000404520000|0000000637930000|0000001522280000|0000001018590000|0000000685430000|0000001717140000|0000000856870000|0000001463370000|0000003367560000|0000000309530000|0000000168770000|0000001406340000|0000001868080000|0000011224040000|0000001205870000|0000001187640000|0000001789300000|0000000273600000|0000002282560000|0000002117940000|u79e6|0000001344800000|u7687|0000000168950000|0000003622360000|0000001744060000|0000000026370000|0000004831330000|0000000406630000|0000000139790000|0000000666640000|0000003443400000|0000011231350000|0000011227660000|0000000623080000|0000000516230000|0000007885120000|0000011224050000|0000000307580000|0000001582600000|0000001271730000|0000003449560000|0000001600830000|0000004709080000|0000004344790000|0000007480100000|0000011225240000|0000011225260000|0000011225280000|0000011225300000|0000011225330000|0000011231360000|0000011225350000|u9550|0000011225360000|0000011225380000|0000011225410000|0000011225440000|0000011225480000|0000011225510000|0000011225530000|0000007884520000|u524d|0000003125260000|0000001987310000|u6768|u51cc|0000001018890000|0000001241210000|0000004705650000|0000003196640000|0000001539310000|0000000009190000|0000005798950000|0000010383780000|0000011229600000|0000001169260000|0000001469660000|0000000446870000|0000006875880000|0000000663110000|0000009553770000|u68e0|0000011229760000|0000011229790000|0000000281070000|0000011229800000|0000002872240000|0000000654590000|0000000508510000|0000000168820000|0000000428410000|0000011231370000|0000002304550000|0000000962020000|0000011229950000|0000011229980000|0000011230000000|0000007482350000|0000010000090000|0000005060680000|0000011231390000|0000000026510000|0000000456600000|0000010254100000|0000007479960000|0000001064400000|0000000951250000|0000001581360000|0000000805780000|0000011231440000|0000011231460000|0000011227750000|0000000045450000|0000000169040000|0000001418310000|0000011223450000|0000000284480000|0000001594610000|0000011224060000|0000001714240000|0000000198150000|0000008425770000|0000011225910000|0000011225930000|0000011224070000|0000011225960000|0000011225990000|0000011226010000|0000011226030000|0000011226050000|0000000263280000|0000011230650000|0000011230680000|0000001607620000|0000001234390000|0000000787910000|0000001486120000|0000001960970000|0000000240720000|0000002010890000|0000001219520000|0000000661970000|0000001485840000|0000000296880000|0000011230820000|0000002243430000|0000000950350000|0000009572640000|0000010063830000|0000007883980000|0000007577270000|0000011230860000|0000011230870000|0000011230880000|0000011230890000|0000002794560000|0000000301880000|0000006265820000|0000011226280000|0000001001060000|0000003644240000|0000003199740000|0000001160810000|0000000127520000|0000001759530000|0000011230940000|0000011230950000|0000011230960000|u67f4|0000003631120000|0000010150740000|0000003343310000|0000003341180000|0000007735720000|0000011226850000|0000011224080000|0000011226960000|0000004241660000|0000000537820000|0000005058590000|0000003269420000|0000002029850000|0000000292970000|0000000036540000|0000011231050000|0000011231060000|0000011231070000|0000002134470000|0000007480450000|0000001066070000|u5854|0000000143000000|0000002426300000|0000001091120000|0000001717040000|0000011224090000|u5580|u4ec0|0000001340260000|0000001574120000|0000005372090000|0000001057980000|0000011227950000|0000000479730000|0000011231300000|0000011227330000|0000011227340000|0000011227350000|u539a|0000011227360000|0000011227370000|0000011231400000|0000003451300000|0000010952380000|0000000966850000|u7ef4|u543e|0000003042730000|u739b|u4f9d|0000002322030000|0000001786550000|0000006161400000|0000000535640000|0000000253460000|0000011231540000|0000011231550000|u695e|0000001259280000|0000001276410000|0000000810900000|0000000252560000|0000008063300000|0000011064090000|0000002130670000|0000004348590000|0000000795400000|0000011231600000|0000011231620000|0000011231640000|0000011231660000|0000011231670000|0000011231690000|u5bc6|0000011231730000|0000010151090000|0000010245710000|0000008993330000|0000008019250000|0000007483600000|0000010156930000|0000007571410000|0000011229620000|0000006670430000|0000011229650000|0000011229660000|0000011229690000|0000011229730000|0000011229750000|0000011229770000|0000000578240000|0000001259940000|0000002423140000|0000000650370000|0000000667290000|0000000253360000|0000000773690000|0000002093310000|0000010248970000|0000011223630000|0000000057450000|0000000168320000|0000001252370000|0000000483690000|0000002799170000|0000000140690000|0000000391010000|0000000616840000|0000000483120000|0000000276000000|0000002013630000|0000002032720000|0000000985590000|0000002742020000|0000002103800000|0000007479910000|0000003539670000|0000011231570000|0000002453860000|0000002960650000|0000010160950000|0000011230220000|0000000221470000|0000011227760000|0000000266440000|0000010013620000|0000001524470000|0000001520690000|0000001118900000|0000006746080000|0000009484440000|u8d8a|u79c0|0000010150680000|0000002287400000|0000011225900000|0000011225920000|0000011225940000|u4e4b|0000011225950000|0000011225980000|0000011226000000|0000011226020000|0000011226040000|0000011226060000|0000011226070000|0000011226080000|0000011226090000|u94b1|0000011226100000|u771f|0000011226110000|u5143|0000011226120000|u74ef|0000011226130000|0000011226140000|0000011226150000|0000011226160000|0000011226170000|0000011226180000|0000011226190000|0000007479510000|0000011227820000|u8bfa|u4e01|0000011227830000|0000003459940000|0000011230850000|0000001793260000|0000003582160000|0000000207440000|0000002310790000|0000002301040000|0000001936630000|0000001367210000|0000000258310000|0000011230910000|0000002345470000|0000000127870000|0000000685640000|0000000096170000|0000011230930000|0000000973940000|0000011223640000|0000011230970000|0000000701510000|0000002986720000|0000000513350000|0000001544730000|0000000441030000|0000001606480000|0000007479570000|0000001255390000|0000002558990000|0000001633390000|0000001793420000|0000003767010000|0000000808050000|0000001030920000|0000002211270000|u5f81|0000004049090000|0000000116180000|0000000054400000|u5411|0000011231110000|0000004457930000|0000002907800000|0000000278680000|0000000645950000|0000003340100000|0000003842320000|0000005019590000|u6a2a|0000008472540000|0000007576900000|0000011231250000|u821f|u7fa4|0000000973000000|0000000853960000|0000000279670000|0000000141840000|0000001363540000|0000003116790000|0000000948600000|0000008477520000|0000000013740000|0000000913600000|0000003475400000|0000003455810000|0000011231680000|0000001743770000|0000011231700000|0000011223650000|0000000318670000|0000002796820000|0000011227960000|0000008320560000|0000004709590000|0000000792780000|0000000900850000|0000011231740000|0000001483460000|0000003050640000|0000000799330000|u5de2|0000000647810000|0000001712020000|0000011223660000|0000001421440000|0000010388260000|0000000202310000|0000001871830000|0000011227980000|0000000562960000|0000007571400000|0000007480610000|0000011227480000|0000011227500000|0000011227520000|0000011227530000|0000011227540000|0000011227550000|0000011224120000|0000011224140000|0000011224160000|0000011224180000|0000000732030000|0000011224210000|0000003628420000|0000000325330000|0000000781380000|0000007080550000|0000000087770000|0000003198250000|0000001289280000|0000000810750000|0000003643490000|0000011228260000|0000004347340000|0000011228320000|0000001362040000|0000000395080000|0000002965820000|0000000594080000|0000011228400000|0000000010290000|0000011228440000|0000002331680000|0000003573660000|0000001004170000|0000000962500000|0000011228520000|0000008494220000|0000001487200000|0000001417840000|0000002089430000|0000011228570000|0000000850320000|0000011228610000|0000001315730000|0000002311140000|0000001873090000|0000002980280000|0000003621870000|0000000566830000|0000011228670000|0000011228690000|0000002056370000|0000011228710000|0000002114900000|0000011228760000|0000007884500000|0000000943340000|0000002458320000|0000002153920000|0000001593300000|0000000937670000|0000002798250000|u95fb|0000001933060000|0000000066770000|0000011228920000|0000011228930000|0000009062880000|0000010366390000|u7eff|0000010564110000|0000008008870000|0000011228980000|0000011228990000|0000010245940000|0000011229000000|0000006172240000|0000011229030000|0000011229050000|u6850|0000011229060000|0000007480370000|0000011229080000|0000007572910000|0000011229110000|0000011229120000|0000011229150000|0000011229170000|u6885|0000011229210000|0000011229220000|0000011229240000|0000011229280000|0000011229290000|0000001461950000|0000001455710000|0000000357460000|0000011231810000|0000001090460000|0000001104010000|0000000808470000|0000000634010000|0000010480330000|0000000339650000|0000000608590000|0000010723790000|0000000302200000|u6cca|0000006598860000|0000000073980000|0000011223670000|0000001518420000|0000001030340000|0000003191050000|u5ca9|0000005820140000|0000001612580000|u8386|0000000569750000|u4ef0|0000011225620000|0000010014710000|0000011225670000|0000011225690000|0000011225710000|0000011225720000|0000011225730000|u5e9a|0000011225740000|u81f3|0000003894510000|0000011225750000|u6bc5|0000011225760000|0000007479840000|0000008715050000|0000011225800000|0000011225820000|0000011225840000|0000007479640000|0000011229840000|0000002064020000|0000010385110000|0000011229900000|0000002097160000|0000000567260000|0000001883030000|0000002315920000|0000000114200000|0000011230020000|0000002581700000|0000000974360000|0000001449330000|0000011230070000|0000002106580000|0000011230120000|0000002457930000|0000006280370000|0000011230170000|0000010542330000|0000000290640000|0000000995410000|0000007023450000|0000004706800000|0000011230350000|0000006752790000|0000011230370000|0000000532820000|0000011230420000|u6e44|0000001936920000|0000000655940000|0000005045350000|0000011230480000|0000001795210000|0000011230500000|0000000941910000|0000002453380000|0000006279160000|0000011230610000|0000011228020000|0000005685620000|0000007758540000|0000010085820000|0000011230710000|0000006286230000|0000011230760000|0000011223680000|0000010263470000|0000003818500000|0000011230800000|0000011230810000|0000008320570000|0000007481630000|0000011230830000|0000011230840000|0000002730930000|0000001806660000|0000001733400000|0000010012730000|0000003048310000|0000000293700000|0000007480490000|0000000677310000|0000007481740000|0000000766780000|0000007483250000|0000000549900000|0000001742220000|0000002105920000|0000000863110000|0000001920840000|0000001201190000|0000000544350000|0000001436920000|u4e95|0000007481670000|0000000399100000|0000004037080000|0000011226450000|0000011227890000|u840d|0000007884420000|0000001455380000|0000007474750000|0000006597750000|u4f59|0000002963750000|0000011226780000|0000001573290000|0000011226880000|0000007480270000|0000007482320000|0000011227030000|0000011227070000|0000011227120000|0000011227160000|0000011227200000|0000011227230000|0000011227240000|0000011227270000|0000011227280000|0000011227290000|0000011227300000|0000011227310000|0000011227320000|0000011227940000|0000000839490000|0000011231170000|0000000058280000|0000000689560000|0000002730670000|0000000205340000|0000000304420000|0000004036730000|0000001479670000|0000000783810000|0000001153830000|0000001332520000|0000004370780000|0000000162180000|0000000921190000|0000000166920000|0000000155580000|0000004705250000|0000010201510000|0000007483020000|0000000012950000|0000000050830000|0000001424800000|0000007578350000|0000000722470000|0000001212620000|0000000138530000|0000000778780000|0000000296340000|0000001422020000|u5148|u950b|0000010949370000|0000000175190000|0000007518300000|0000001157150000|0000000728600000|0000001201230000|0000003903350000|0000005513200000|0000011231720000|0000011231750000|0000011016030000|0000008299950000|u8c6a|u6f2b|0000007581400000|0000007578340000|0000011231840000|0000011228000000|0000011228030000|0000011228050000|0000011228060000|0000011228080000|0000011228100000|0000011228130000|0000011228150000|u6d2a|0000011228170000|0000011228180000|0000001029480000|0000000711320000|0000003374030000|0000000841080000|0000001435410000|0000001088060000|0000011224410000|0000011228090000|0000011224430000|0000001720080000|0000011224450000|u7434|0000011224470000|u5386|0000011224490000|0000011224510000|0000011228120000|0000011224530000|0000011224550000|0000011224560000|0000011224570000|0000011224600000|0000001078330000|0000000003220000|0000011223690000|0000000154990000|0000000859200000|0000007644120000|0000000499000000|0000011228140000|0000001389880000|0000009751370000|0000000665970000|0000001172180000|0000000183910000|0000000162090000|0000001525360000|0000003893360000|0000000968340000|0000001482870000|0000000099080000|0000011228160000|0000000991720000|0000001781000000|0000003901210000|0000000470730000|0000002110330000|0000001076320000|0000000695490000|0000011224790000|0000003196260000|0000000574070000|0000000016230000|0000000178970000|0000000684130000|0000000438680000|0000002571930000|0000004713630000|0000001516240000|0000001967510000|0000007480090000|0000000168090000|0000010386690000|0000010154720000|0000011224960000|u6052|0000007482330000|0000011225010000|0000007479850000|0000011225060000|u5b87|0000000762790000|0000011225110000|0000000709310000|0000007479710000|0000006596630000|0000011227670000|0000011227680000|0000001340450000|0000011229250000|0000003196480000|0000000211790000|0000011229300000|u65e5|u7167|0000002284540000|0000001203740000|0000011229340000|0000007480010000|0000000156840000|0000011229420000|u83b1|0000003632260000|0000000202650000|0000001424090000|0000004716930000|0000011229470000|0000000302110000|0000000656000000|0000001732750000|0000011229510000|0000011229540000|0000002751100000|u5723|0000002475190000|0000001400060000|0000000152190000|0000011229570000|0000002732360000|0000011229590000|0000002204650000|0000000416790000|0000003193720000|0000000136700000|0000011229670000|0000000879730000|0000001386510000|0000000023900000|0000004706450000|0000004076560000|u5b9e|0000002800780000|0000003562450000|0000008226910000|0000006565300000|0000011229870000|0000002872950000|0000011229930000|0000000491000000|0000001460870000|0000001025900000|0000000332820000|0000000348520000|0000002310140000|u94dd|0000010244260000|0000002566200000|0000011230080000|0000000168080000|0000000660710000|0000011230130000|0000000653990000|0000011230180000|0000005836350000|0000011230230000|0000011114600000|0000011230300000|0000011230330000|0000011194240000|0000007480960000|0000007480080000|u5458|0000011230390000|0000007578150000|0000007576400000|0000011230460000|0000007576350000|0000007479470000|0000011230510000|0000011230530000|0000011230560000|0000011230580000|0000011227770000|0000000695400000|0000000493440000|0000000565810000|0000001357670000|0000001416820000|0000003895120000|0000000903540000|0000000676650000|0000000494300000|0000011227780000|0000001038860000|0000000385730000|0000000928350000|0000000959460000|0000003896130000|0000000416550000|0000011229350000|0000000830070000|0000001319300000|0000000022770000|0000000359010000|0000002962950000|0000001116350000|0000001602480000|0000011224100000|0000000211090000|0000001070440000|0000002730400000|0000001532940000|0000004866900000|0000004125420000|0000011227870000|0000001027760000|0000004044780000|0000000696740000|0000003366550000|0000007479540000|0000002072790000|0000011227900000|0000007479940000|0000011226640000|0000004099170000|0000011226730000|0000011226790000|0000007572990000|0000011226890000|0000011226940000|0000011226990000|u9510|0000011227040000|0000011227080000|0000011227130000|0000011227170000|0000011227210000|0000001787980000|0000011227250000|u529f|0000007480150000|u5347|0000000496680000|0000001271020000|0000000700780000|0000000815180000|0000000617410000|0000000398960000|0000000013670000|0000000651610000|0000001886650000|0000003368480000|0000002445260000|0000001041460000|0000000825440000|u6fee|0000002576300000|0000000486580000|0000001382820000|0000011231270000|0000011231310000|0000001976990000|0000000061080000|0000001395170000|0000000853700000|0000002577010000|0000011231410000|0000004649490000|0000001454900000|0000007071830000|u6f8d|0000000927280000|0000000354370000|u68c0|0000001977260000|0000009491630000|0000002285080000|0000011231520000|0000001119120000|0000001197170000|0000000011100000|0000011227970000|u5d69|u5c11|0000002202040000|0000011231560000|0000009403140000|0000000790580000|0000000741820000|0000000507640000|0000000954330000|0000011231630000|u7edf|0000011231650000|0000001329400000|0000004342540000|0000008199260000|0000010237390000|0000010020220000|0000007480190000|0000010721500000|0000010699190000|0000011231850000|u9a7b|0000011228010000|0000011223700000|0000007480980000|0000011223710000|0000007883990000|u57a3|0000007479860000|0000007479520000|0000000654680000|0000007479650000|0000007480070000|0000011228200000|0000007883770000|0000011228210000|0000011228230000|0000011228270000|0000007571480000|u63a8|u62ff|0000007480300000|0000007883550000|0000011228360000|0000011228380000|0000007053370000|0000011228410000|0000011228430000|0000011228450000|0000011228460000|0000011228480000|0000001419160000|0000001956800000|0000010960490000|0000001282120000|0000002982210000|0000000086290000|0000000797300000|0000011223720000|0000001182900000|0000001778990000|0000000120760000|0000010240770000|0000000462080000|0000000408100000|0000001547230000|0000001552800000|0000000196200000|0000007478660000|0000000425720000|0000001798060000|0000001470300000|0000002574890000|0000002238360000|0000000839500000|0000008215490000|0000007582210000|0000007479120000|0000002732190000|0000002346160000|0000001185430000|0000011227640000|0000011227570000|0000001933550000|0000001395820000|0000011223730000|0000011223740000|0000007480220000|0000011224900000|0000007480600000|0000001171080000|0000011224970000|u73de|u73c8|0000011224990000|0000011225020000|0000011225040000|0000011225070000|0000011225090000|0000011225120000|0000011225130000|0000011223750000|0000011223760000|0000011225160000|0000011225170000|0000011225180000|0000011225210000|0000011225220000|0000011225230000|0000011225250000|0000011225270000|0000011225290000|0000011225320000|0000011225340000|0000011223770000|0000011225370000|0000011225390000|0000011225420000|0000011225460000|0000011225490000|0000000167940000|0000011229480000|u90e7|0000003064800000|0000011229500000|0000001931090000|0000007480290000|0000011229550000|0000000141370000|0000007012990000|0000000915540000|0000001890500000|0000000712280000|u65bd|0000011223780000|0000003343110000|0000009834170000|0000011229630000|0000002964690000|0000002206270000|u4ed9|u6843|0000011229700000|0000001591930000|0000001872160000|0000001825780000|0000001963700000|0000000651420000|0000001801780000|0000001545300000|u968f|0000000742120000|0000002804080000|0000000269610000|0000000724850000|0000002986850000|0000000085190000|u653e|0000011229970000|0000006599940000|0000000483950000|0000011223790000|0000000882860000|0000002056270000|0000000858060000|0000000631850000|0000001357530000|0000011230140000|0000000086170000|0000001185460000|0000003894340000|0000011230240000|0000000405050000|0000002115260000|0000000246270000|0000003339120000|0000004922560000|0000011208770000|0000011230400000|0000010685920000|0000007571330000|0000011230470000|0000007481680000|0000011230490000|0000000905380000|0000001529930000|0000000532370000|0000001795120000|0000000210840000|0000000268940000|0000000484180000|0000000199470000|0000000772100000|0000000054040000|0000001732950000|0000002344530000|0000000362240000|0000000762160000|0000011227790000|0000000923420000|0000000596330000|0000001714110000|0000001191090000|0000006489020000|0000000507560000|0000002140250000|0000001091580000|0000003067260000|0000001351030000|0000003338270000|0000009813300000|0000003899520000|0000009964690000|0000010952690000|0000011226270000|0000000173570000|0000011226300000|0000011226340000|0000011226370000|0000011226410000|0000011226460000|0000011226510000|0000011226550000|0000011226600000|0000011226650000|u8299|u84c9|0000011226690000|0000011226740000|0000011226800000|0000011226840000|0000011226900000|0000011226950000|0000011227000000'.split('|'), 0, {}))

var MajorList={"bachelor":[{"code":"010101","name":"哲学"},{"code":"010102","name":"逻辑学"},{"code":"010103","name":"宗教学"},{"code":"020101","name":"经济学"},{"code":"020102","name":"经济统计学"},{"code":"020201","name":"财政学"},{"code":"020202","name":"税收学"},{"code":"020301","name":"金融学"},{"code":"020302","name":"金融工程"},{"code":"020303","name":"保险学"},{"code":"020304","name":"投资学"},{"code":"020401","name":"国际经济与贸易"},{"code":"020402","name":"贸易经济"},{"code":"030101","name":"法学"},{"code":"030201","name":"政治学与行政学"},{"code":"030202","name":"国际政治"},{"code":"030203","name":"外交学"},{"code":"030301","name":"社会学"},{"code":"030302","name":"社会工作"},{"code":"030401","name":"民族学"},{"code":"030501","name":"科学社会主义"},{"code":"030502","name":"中国共产党历史"},{"code":"030503","name":"思想政治教育"},{"code":"030601","name":"治安学"},{"code":"030602","name":"侦查学"},{"code":"030603","name":"边防管理"},{"code":"040101","name":"教育学"},{"code":"040102","name":"科学教育"},{"code":"040103","name":"人文教育"},{"code":"040104","name":"教育技术学"},{"code":"040105","name":"艺术教育"},{"code":"040106","name":"学前教育"},{"code":"040107","name":"小学教育"},{"code":"040108","name":"特殊教育"},{"code":"040201","name":"体育教育"},{"code":"040202","name":"运动训练"},{"code":"040203","name":"社会体育指导与管理"},{"code":"040204","name":"武术与民族传统体育"},{"code":"040205","name":"运动人体科学"},{"code":"050101","name":"汉语言文学"},{"code":"050102","name":"汉语言"},{"code":"050103","name":"汉语国际教育"},{"code":"050104","name":"中国少数民族语言文学"},{"code":"050105","name":"古典文献学"},{"code":"050201","name":"英语"},{"code":"050202","name":"俄语"},{"code":"050203","name":"德语"},{"code":"050204","name":"法语"},{"code":"050205","name":"西班牙语"},{"code":"050206","name":"阿拉伯语"},{"code":"050207","name":"日语"},{"code":"050208","name":"波斯语"},{"code":"050209","name":"朝鲜语"},{"code":"050210","name":"菲律宾语"},{"code":"050211","name":"梵语巴利语"},{"code":"050212","name":"印度尼西亚语"},{"code":"050213","name":"印地语"},{"code":"050214","name":"柬埔寨语"},{"code":"050215","name":"老挝语"},{"code":"050216","name":"缅甸语"},{"code":"050217","name":"马来语"},{"code":"050218","name":"蒙古语"},{"code":"050219","name":"僧伽罗语"},{"code":"050220","name":"泰语"},{"code":"050221","name":"乌尔都语"},{"code":"050222","name":"希伯来语"},{"code":"050223","name":"越南语"},{"code":"050224","name":"豪萨语"},{"code":"050225","name":"斯瓦希里语"},{"code":"050226","name":"阿尔巴尼亚语"},{"code":"050227","name":"保加利亚语"},{"code":"050228","name":"波兰语"},{"code":"050229","name":"捷克语"},{"code":"050230","name":"斯洛伐克语"},{"code":"050231","name":"罗马尼亚语"},{"code":"050232","name":"葡萄牙语"},{"code":"050233","name":"瑞典语"},{"code":"050234","name":"塞尔维亚语"},{"code":"050235","name":"土耳其语"},{"code":"050236","name":"希腊语"},{"code":"050237","name":"匈牙利语"},{"code":"050238","name":"意大利语"},{"code":"050239","name":"泰米尔语"},{"code":"050240","name":"普什图语"},{"code":"050241","name":"世界语"},{"code":"050242","name":"孟加拉语"},{"code":"050243","name":"尼泊尔语"},{"code":"050244","name":"克罗地亚语"},{"code":"050245","name":"荷兰语"},{"code":"050246","name":"芬兰语"},{"code":"050247","name":"乌克兰语"},{"code":"050248","name":"挪威语"},{"code":"050249","name":"丹麦语"},{"code":"050250","name":"冰岛语"},{"code":"050251","name":"爱尔兰语"},{"code":"050252","name":"拉脱维亚语"},{"code":"050253","name":"立陶宛语"},{"code":"050254","name":"斯洛文尼亚语"},{"code":"050255","name":"爱沙尼亚语"},{"code":"050256","name":"马耳他语"},{"code":"050257","name":"哈萨克语"},{"code":"050258","name":"乌兹别克语"},{"code":"050259","name":"祖鲁语"},{"code":"050260","name":"拉丁语"},{"code":"050261","name":"翻译"},{"code":"050262","name":"商务英语"},{"code":"050301","name":"新闻学"},{"code":"050302","name":"广播电视学"},{"code":"050303","name":"广告学"},{"code":"050304","name":"传播学"},{"code":"050305","name":"编辑出版学"},{"code":"060101","name":"历史学"},{"code":"060102","name":"世界史"},{"code":"060103","name":"考古学"},{"code":"060104","name":"文物与博物馆学"},{"code":"070101","name":"数学与应用数学"},{"code":"070102","name":"信息与计算科学"},{"code":"070201","name":"物理学"},{"code":"070202","name":"应用物理学"},{"code":"070203","name":"核物理"},{"code":"070301","name":"化学"},{"code":"070302","name":"应用化学"},{"code":"070401","name":"天文学"},{"code":"070501","name":"地理科学"},{"code":"070502","name":"自然地理与资源环境"},{"code":"070503","name":"人文地理与城乡规划"},{"code":"070504","name":"地理信息科学"},{"code":"070601","name":"大气科学"},{"code":"070602","name":"应用气象学"},{"code":"070701","name":"海洋科学"},{"code":"070702","name":"海洋技术"},{"code":"070801","name":"地球物理学"},{"code":"070802","name":"空间科学与技术"},{"code":"070901","name":"地质学"},{"code":"070902","name":"地球化学"},{"code":"071001","name":"生物科学"},{"code":"071002","name":"生物技术"},{"code":"071003","name":"生物信息学"},{"code":"071004","name":"生态学"},{"code":"071101","name":"心理学"},{"code":"071102","name":"应用心理学"},{"code":"071201","name":"统计学"},{"code":"071202","name":"应用统计学"},{"code":"080101","name":"理论与应用力学"},{"code":"080102","name":"工程力学"},{"code":"080201","name":"机械工程"},{"code":"080202","name":"机械设计制造及其自动化"},{"code":"080203","name":"材料成型及控制工程"},{"code":"080204","name":"机械电子工程"},{"code":"080205","name":"工业设计"},{"code":"080206","name":"过程装备与控制工程"},{"code":"080207","name":"车辆工程"},{"code":"080208","name":"汽车服务工程"},{"code":"080301","name":"测控技术与仪器"},{"code":"080401","name":"材料科学与工程"},{"code":"080402","name":"材料物理"},{"code":"080403","name":"材料化学"},{"code":"080404","name":"冶金工程"},{"code":"080405","name":"金属材料工程"},{"code":"080406","name":"无机非金属材料工程"},{"code":"080407","name":"高分子材料与工程"},{"code":"080408","name":"复合材料与工程"},{"code":"080501","name":"能源与动力工程"},{"code":"080601","name":"电气工程及其自动化"},{"code":"080701","name":"电子信息工程"},{"code":"080702","name":"电子科学与技术"},{"code":"080703","name":"通信工程"},{"code":"080704","name":"微电子科学与工程"},{"code":"080705","name":"光电信息科学与工程"},{"code":"080706","name":"信息工程"},{"code":"080801","name":"自动化"},{"code":"080901","name":"计算机科学与技术"},{"code":"080902","name":"软件工程"},{"code":"080903","name":"网络工程"},{"code":"080904","name":"信息安全"},{"code":"080905","name":"物联网工程"},{"code":"080906","name":"数字媒体技术"},{"code":"081001","name":"土木工程"},{"code":"081002","name":"建筑环境与能源应用工程"},{"code":"081003","name":"给排水科学与工程"},{"code":"081004","name":"建筑电气与智能化"},{"code":"081101","name":"水利水电工程"},{"code":"081102","name":"水文与水资源工程"},{"code":"081103","name":"港口航道与海岸工程"},{"code":"081201","name":"测绘工程"},{"code":"081202","name":"遥感科学与技术"},{"code":"081301","name":"化学工程与工艺"},{"code":"081302","name":"制药工程"},{"code":"081401","name":"地质工程"},{"code":"081402","name":"勘查技术与工程"},{"code":"081403","name":"资源勘查工程"},{"code":"081501","name":"采矿工程"},{"code":"081502","name":"石油工程"},{"code":"081503","name":"矿物加工工程"},{"code":"081504","name":"油气储运工程"},{"code":"081601","name":"纺织工程"},{"code":"081602","name":"服装设计与工程"},{"code":"081701","name":"轻化工程"},{"code":"081702","name":"包装工程"},{"code":"081703","name":"印刷工程"},{"code":"081801","name":"交通运输"},{"code":"081802","name":"交通工程"},{"code":"081803","name":"航海技术"},{"code":"081804","name":"轮机工程"},{"code":"081805","name":"飞行技术"},{"code":"081901","name":"船舶与海洋工程"},{"code":"082001","name":"航空航天工程"},{"code":"082002","name":"飞行器设计与工程"},{"code":"082003","name":"飞行器制造工程"},{"code":"082004","name":"飞行器动力工程"},{"code":"082005","name":"飞行器环境与生命保障工程"},{"code":"082101","name":"武器系统与工程"},{"code":"082102","name":"武器发射工程"},{"code":"082103","name":"探测制导与控制技术"},{"code":"082104","name":"弹药工程与爆炸技术"},{"code":"082105","name":"特种能源技术与工程"},{"code":"082106","name":"装甲车辆工程"},{"code":"082107","name":"信息对抗技术"},{"code":"082201","name":"核工程与核技术"},{"code":"082202","name":"辐射防护与核安全"},{"code":"082203","name":"工程物理"},{"code":"082204","name":"核化工与核燃料工程"},{"code":"082301","name":"农业工程"},{"code":"082302","name":"农业机械化及其自动化"},{"code":"082303","name":"农业电气化"},{"code":"082304","name":"农业建筑环境与能源工程"},{"code":"082305","name":"农业水利工程"},{"code":"082401","name":"森林工程"},{"code":"082402","name":"木材科学与工程"},{"code":"082403","name":"林产化工"},{"code":"082501","name":"环境科学与工程"},{"code":"082502","name":"环境工程"},{"code":"082503","name":"环境科学"},{"code":"082504","name":"环境生态工程"},{"code":"082601","name":"生物医学工程"},{"code":"082701","name":"食品科学与工程"},{"code":"082702","name":"食品质量与安全"},{"code":"082703","name":"粮食工程"},{"code":"082704","name":"乳品工程"},{"code":"082705","name":"酿酒工程"},{"code":"082801","name":"建筑学"},{"code":"082802","name":"城乡规划"},{"code":"082803","name":"风景园林"},{"code":"082901","name":"安全工程"},{"code":"083001","name":"生物工程"},{"code":"083101","name":"刑事科学技术"},{"code":"083102","name":"消防工程"},{"code":"090101","name":"农学"},{"code":"090102","name":"园艺"},{"code":"090103","name":"植物保护"},{"code":"090104","name":"植物科学与技术"},{"code":"090105","name":"种子科学与工程"},{"code":"090106","name":"设施农业科学与工程"},{"code":"090201","name":"农业资源与环境"},{"code":"090202","name":"野生动物与自然保护区管理"},{"code":"090203","name":"水土保持与荒漠化防治"},{"code":"090301","name":"动物科学"},{"code":"090401","name":"动物医学"},{"code":"090402","name":"动物药学"},{"code":"090501","name":"林学"},{"code":"090502","name":"园林"},{"code":"090503","name":"森林保护"},{"code":"090601","name":"水产养殖学"},{"code":"090602","name":"海洋渔业科学与技术"},{"code":"090701","name":"草业科学"},{"code":"100101","name":"基础医学"},{"code":"100201","name":"临床医学"},{"code":"100301","name":"口腔医学"},{"code":"100401","name":"预防医学"},{"code":"100402","name":"食品卫生与营养学"},{"code":"100501","name":"中医学"},{"code":"100502","name":"针灸推拿学"},{"code":"100503","name":"藏医学"},{"code":"100504","name":"蒙医学"},{"code":"100505","name":"维医学"},{"code":"100506","name":"壮医学"},{"code":"100507","name":"哈医学"},{"code":"100601","name":"中西医临床医学"},{"code":"100701","name":"药学"},{"code":"100702","name":"药物制剂"},{"code":"100801","name":"中药学"},{"code":"100802","name":"中药资源与开发"},{"code":"100901","name":"法医学"},{"code":"101001","name":"医学检验技术"},{"code":"101002","name":"医学实验技术"},{"code":"101003","name":"医学影像技术"},{"code":"101004","name":"眼视光学"},{"code":"101005","name":"康复治疗学"},{"code":"101006","name":"口腔医学技术"},{"code":"101007","name":"卫生检验与检疫"},{"code":"101101","name":"护理学"},{"code":"120101","name":"管理科学"},{"code":"120102","name":"信息管理与信息系统"},{"code":"120103","name":"工程管理"},{"code":"120104","name":"房地产开发与管理"},{"code":"120105","name":"工程造价"},{"code":"120201","name":"工商管理"},{"code":"120202","name":"市场营销"},{"code":"120203","name":"会计学"},{"code":"120204","name":"财务管理"},{"code":"120205","name":"国际商务"},{"code":"120206","name":"人力资源管理"},{"code":"120207","name":"审计学"},{"code":"120208","name":"资产评估"},{"code":"120209","name":"物业管理"},{"code":"120210","name":"文化产业管理"},{"code":"120301","name":"农林经济管理"},{"code":"120302","name":"农村区域发展"},{"code":"120401","name":"公共事业管理"},{"code":"120402","name":"行政管理"},{"code":"120403","name":"劳动与社会保障"},{"code":"120404","name":"土地资源管理"},{"code":"120405","name":"城市管理"},{"code":"120501","name":"图书馆学"},{"code":"120502","name":"档案学"},{"code":"120503","name":"信息资源管理"},{"code":"120601","name":"物流管理"},{"code":"120602","name":"物流工程"},{"code":"120701","name":"工业工程"},{"code":"120801","name":"电子商务"},{"code":"120901","name":"旅游管理"},{"code":"120902","name":"酒店管理"},{"code":"120903","name":"会展经济与管理"},{"code":"130101","name":"艺术史论"},{"code":"130201","name":"音乐表演"},{"code":"130202","name":"音乐学"},{"code":"130203","name":"作曲与作曲技术理论"},{"code":"130204","name":"舞蹈表演"},{"code":"130205","name":"舞蹈学"},{"code":"130206","name":"舞蹈编导"},{"code":"130301","name":"表演"},{"code":"130302","name":"戏剧学"},{"code":"130303","name":"电影学"},{"code":"130304","name":"戏剧影视文学"},{"code":"130305","name":"广播电视编导"},{"code":"130306","name":"戏剧影视导演"},{"code":"130307","name":"戏剧影视美术设计"},{"code":"130308","name":"录音艺术"},{"code":"130309","name":"播音与主持艺术"},{"code":"130310","name":"动画"},{"code":"130401","name":"美术学"},{"code":"130402","name":"绘画"},{"code":"130403","name":"雕塑"},{"code":"130404","name":"摄影"},{"code":"130501","name":"艺术设计学"},{"code":"130502","name":"视觉传达设计"},{"code":"130503","name":"环境设计"},{"code":"130504","name":"产品设计"},{"code":"130505","name":"服装与服饰设计"},{"code":"130506","name":"公共艺术"},{"code":"130507","name":"工艺美术"},{"code":"130508","name":"数字媒体艺术"}],"master":[{"code":"010101","name":"马克思主义哲学"},{"code":"010102","name":"中国哲学"},{"code":"010103","name":"外国哲学"},{"code":"010104","name":"逻辑学"},{"code":"010105","name":"伦理学"},{"code":"010106","name":"美学"},{"code":"010107","name":"宗教学"},{"code":"010108","name":"科学技术哲学"},{"code":"020101","name":"政治经济学"},{"code":"020102","name":"经济思想史"},{"code":"020103","name":"经济史"},{"code":"020104","name":"西方经济学"},{"code":"020105","name":"世界经济"},{"code":"020106","name":"人口、资源与环境经济学"},{"code":"020201","name":"国民经济学"},{"code":"020202","name":"区域经济学"},{"code":"020203","name":"财政学"},{"code":"020204","name":"金融学"},{"code":"020205","name":"产业经济学"},{"code":"020206","name":"国际贸易学"},{"code":"020207","name":"劳动经济学"},{"code":"020208","name":"统计学"},{"code":"020209","name":"数量经济学"},{"code":"020210","name":"国防经济"},{"code":"030101","name":"法学理论"},{"code":"030102","name":"法律史"},{"code":"030103","name":"宪法学与行政法学"},{"code":"030104","name":"刑法学"},{"code":"030105","name":"民商法学"},{"code":"030106","name":"诉讼法学"},{"code":"030107","name":"经济法学"},{"code":"030108","name":"环境与资源保护法学"},{"code":"030109","name":"国际法学"},{"code":"030110","name":"军事法学"},{"code":"030201","name":"政治学理论"},{"code":"030202","name":"中外政治制度"},{"code":"030203","name":"科学社会主义与国际共产主义运动"},{"code":"030204","name":"中共党史"},{"code":"030206","name":"国际政治"},{"code":"030207","name":"国际关系"},{"code":"030208","name":"外交学"},{"code":"030301","name":"社会学"},{"code":"030302","name":"人口学"},{"code":"030303","name":"人类学"},{"code":"030304","name":"民俗学"},{"code":"030401","name":"民族学"},{"code":"030402","name":"马克思主义民族理论与政策"},{"code":"030403","name":"中国少数民族经济"},{"code":"030404","name":"中国少数民族史"},{"code":"030405","name":"中国少数民族艺术"},{"code":"030501","name":"马克思主义基本原理"},{"code":"030502","name":"马克思主义发展史"},{"code":"030503","name":"马克思主义中国化研究"},{"code":"030504","name":"国外马克思主义研究"},{"code":"030505","name":"思想政治教育"},{"code":"040101","name":"教育学原理"},{"code":"040102","name":"课程与教学论"},{"code":"040103","name":"教育史"},{"code":"040104","name":"比较教育学"},{"code":"040105","name":"学前教育学"},{"code":"040106","name":"高等教育学"},{"code":"040107","name":"成人教育学"},{"code":"040108","name":"职业技术教育学"},{"code":"040109","name":"特殊教育学"},{"code":"040110","name":"教育技术学"},{"code":"040201","name":"基础心理学"},{"code":"040202","name":"发展与教育心理学"},{"code":"040203","name":"应用心理学"},{"code":"040301","name":"体育人文社会学"},{"code":"040302","name":"运动人体科学"},{"code":"040303","name":"体育教育训练学"},{"code":"040304","name":"民族传统体育学"},{"code":"050101","name":"文艺学"},{"code":"050102","name":"语言学及应用语言学"},{"code":"050103","name":"汉语言文字学"},{"code":"050104","name":"中国古典文献学"},{"code":"050105","name":"中国古代文学"},{"code":"050106","name":"中国现当代文学"},{"code":"050107","name":"中国少数民族语言文学"},{"code":"050108","name":"比较文学与世界文学"},{"code":"050201","name":"英语语言文学"},{"code":"050202","name":"俄语语言文学"},{"code":"050203","name":"法语语言文学"},{"code":"050204","name":"德语语言文学"},{"code":"050205","name":"日语语言文学"},{"code":"050206","name":"印度语言文学"},{"code":"050207","name":"西班牙语语言文学"},{"code":"050208","name":"阿拉伯语语言文学"},{"code":"050209","name":"欧洲语言文学"},{"code":"050210","name":"亚非语言文学"},{"code":"050211","name":"外国语言学及应用语言学"},{"code":"050301","name":"新闻学"},{"code":"050302","name":"传播学"},{"code":"050401","name":"艺术学"},{"code":"050402","name":"音乐学"},{"code":"050403","name":"美术学"},{"code":"050404","name":"设计艺术学"},{"code":"050405","name":"戏剧戏曲学"},{"code":"050406","name":"电影学"},{"code":"050407","name":"广播电视艺术学"},{"code":"050408","name":"舞蹈学"},{"code":"060101","name":"史学理论及史学史"},{"code":"060102","name":"考古学及博物馆学"},{"code":"060103","name":"历史地理学"},{"code":"060104","name":"历史文献学"},{"code":"060105","name":"专门史"},{"code":"060106","name":"中国古代史"},{"code":"060107","name":"中国近现代史"},{"code":"060108","name":"世界史"},{"code":"070101","name":"基础数学"},{"code":"070102","name":"计算数学"},{"code":"070103","name":"概率论与数理统计"},{"code":"070104","name":"应用数学"},{"code":"070105","name":"运筹学与控制论"},{"code":"070201","name":"理论物理"},{"code":"070202","name":"粒子物理与原子核物理"},{"code":"070203","name":"原子与分子物理"},{"code":"070204","name":"等离子体物理"},{"code":"070205","name":"凝聚态物理"},{"code":"070206","name":"声学"},{"code":"070207","name":"光学"},{"code":"070208","name":"无线电物理"},{"code":"070301","name":"无机化学"},{"code":"070302","name":"分析化学"},{"code":"070303","name":"有机化学"},{"code":"070304","name":"物理化学"},{"code":"070305","name":"高分子化学与物理"},{"code":"070401","name":"天体物理"},{"code":"070402","name":"天体测量与天体力学"},{"code":"070501","name":"自然地理学"},{"code":"070502","name":"人文地理学"},{"code":"070503","name":"地图学与地理信息系统"},{"code":"070601","name":"气象学"},{"code":"070602","name":"大气物理学与大气环境"},{"code":"070701","name":"物理海洋学"},{"code":"070702","name":"海洋化学"},{"code":"070703","name":"海洋生物学"},{"code":"070704","name":"海洋地质"},{"code":"070801","name":"固体地球物理学"},{"code":"070802","name":"空间物理学"},{"code":"070901","name":"矿物学、岩石学、矿床学"},{"code":"070902","name":"地球化学"},{"code":"070903","name":"古生物学与地层学"},{"code":"070904","name":"构造地质学"},{"code":"070905","name":"第四纪地质学"},{"code":"071001","name":"植物学"},{"code":"071002","name":"动物学"},{"code":"071003","name":"生理学"},{"code":"071004","name":"水生生物学"},{"code":"071005","name":"微生物学"},{"code":"071006","name":"神经生物学"},{"code":"071007","name":"遗传学"},{"code":"071008","name":"发育生物学"},{"code":"071009","name":"细胞生物学"},{"code":"071010","name":"生物化学与分子生物学"},{"code":"071011","name":"生物物理学"},{"code":"071012","name":"生态学"},{"code":"071101","name":"系统理论"},{"code":"071102","name":"系统分析与集成"},{"code":"080101","name":"一般力学与力学基础"},{"code":"080102","name":"固体力学"},{"code":"080103","name":"流体力学"},{"code":"080104","name":"工程力学"},{"code":"080201","name":"机械制造及其自动化"},{"code":"080202","name":"机械电子工程"},{"code":"080203","name":"机械设计及理论"},{"code":"080204","name":"车辆工程"},{"code":"080401","name":"精密仪器及机械"},{"code":"080402","name":"测试计量技术及仪器"},{"code":"080501","name":"材料物理与化学"},{"code":"080502","name":"材料学"},{"code":"080503","name":"材料加工工程"},{"code":"080601","name":"冶金物理化学"},{"code":"080602","name":"钢铁冶金"},{"code":"080603","name":"有色金属冶金"},{"code":"080701","name":"工程热物理"},{"code":"080702","name":"热能工程"},{"code":"080703","name":"动力机械及工程"},{"code":"080704","name":"流体机械及工程"},{"code":"080705","name":"制冷及低温工程"},{"code":"080706","name":"化工过程机械"},{"code":"080801","name":"电机与电器"},{"code":"080802","name":"电力系统及其自动化"},{"code":"080803","name":"高电压与绝缘技术"},{"code":"080804","name":"电力电子与电力传动"},{"code":"080805","name":"电工理论与新技术"},{"code":"080901","name":"物理电子学"},{"code":"080902","name":"电路与系统"},{"code":"080903","name":"微电子学与固体电子学"},{"code":"080904","name":"电磁场与微波技术"},{"code":"081001","name":"通信与信息系统"},{"code":"081002","name":"信号与信息处理"},{"code":"081101","name":"控制理论与控制工程"},{"code":"081102","name":"检测技术与自动化装置"},{"code":"081103","name":"系统工程"},{"code":"081104","name":"模式识别与智能系统"},{"code":"081105","name":"导航、制导与控制"},{"code":"081201","name":"计算机系统结构"},{"code":"081202","name":"计算机软件与理论"},{"code":"081203","name":"计算机应用技术"},{"code":"081301","name":"建筑历史与理论"},{"code":"081302","name":"建筑设计及其理论"},{"code":"081303","name":"城市规划与设计"},{"code":"081304","name":"建筑技术科学"},{"code":"081401","name":"岩土工程"},{"code":"081402","name":"结构工程"},{"code":"081403","name":"市政工程"},{"code":"081404","name":"供热、供燃气、通风及空调工程"},{"code":"081405","name":"防灾减灾工程及防护工程"},{"code":"081406","name":"桥梁与隧道工程"},{"code":"081501","name":"水文学及水资源"},{"code":"081502","name":"水力学及河流动力学"},{"code":"081503","name":"水工结构工程"},{"code":"081504","name":"水利水电工程"},{"code":"081505","name":"港口、海岸及近海工程"},{"code":"081601","name":"大地测量学与测量工程"},{"code":"081602","name":"摄影测量与遥感"},{"code":"081603","name":"地图制图学与地理信息工程"},{"code":"081701","name":"化学工程"},{"code":"081702","name":"化学工艺"},{"code":"081703","name":"生物化工"},{"code":"081704","name":"应用化学"},{"code":"081705","name":"工业催化"},{"code":"081801","name":"矿产普查与勘探"},{"code":"081802","name":"地球探测与信息技术"},{"code":"081803","name":"地质工程"},{"code":"081901","name":"采矿工程"},{"code":"081902","name":"矿物加工工程"},{"code":"081903","name":"安全技术及工程"},{"code":"082001","name":"油气井工程"},{"code":"082002","name":"油气田开发工程"},{"code":"082003","name":"油气储运工程"},{"code":"082101","name":"纺织工程"},{"code":"082102","name":"纺织材料与纺织品设计"},{"code":"082103","name":"纺织化学与染整工程"},{"code":"082104","name":"服装设计与工程"},{"code":"082201","name":"制浆造纸工程"},{"code":"082202","name":"制糖工程"},{"code":"082203","name":"发酵工程"},{"code":"082204","name":"皮革化学与工程"},{"code":"082301","name":"道路与铁道工程"},{"code":"082302","name":"交通信息工程及控制"},{"code":"082303","name":"交通运输规划与管理"},{"code":"082304","name":"载运工具运用工程"},{"code":"082401","name":"船舶与海洋结构物设计制造"},{"code":"082402","name":"轮机工程"},{"code":"082403","name":"水声工程"},{"code":"082501","name":"飞行器设计"},{"code":"082502","name":"航空宇航推进理论与工程"},{"code":"082503","name":"航空宇航制造工程"},{"code":"082504","name":"人机与环境工程"},{"code":"082601","name":"武器系统与运用工程"},{"code":"082602","name":"兵器发射理论与技术"},{"code":"082603","name":"火炮、自动武器与弹药工程"},{"code":"082604","name":"军事化学与烟火技术"},{"code":"082701","name":"核能科学与工程"},{"code":"082702","name":"核燃料循环与材料"},{"code":"082703","name":"核技术及应用"},{"code":"082704","name":"辐射防护及环境保护"},{"code":"082801","name":"农业机械化工程"},{"code":"082802","name":"农业水土工程"},{"code":"082803","name":"农业生物环境与能源工程"},{"code":"082804","name":"农业电气化与自动化"},{"code":"082901","name":"森林工程"},{"code":"082902","name":"木材科学与技术"},{"code":"082903","name":"林产化学加工工程"},{"code":"083001","name":"环境科学"},{"code":"083002","name":"环境工程"},{"code":"083201","name":"食品科学"},{"code":"083202","name":"粮食、油脂及植物蛋白工程"},{"code":"083203","name":"农产品加工及贮藏工程"},{"code":"083204","name":"水产品加工及贮藏工程"},{"code":"090101","name":"作物栽培学与耕作学"},{"code":"090102","name":"作物遗传育种"},{"code":"090201","name":"果树学"},{"code":"090202","name":"蔬菜学"},{"code":"090203","name":"茶学"},{"code":"090301","name":"土壤学"},{"code":"090302","name":"植物营养学"},{"code":"090401","name":"植物病理学"},{"code":"090402","name":"农业昆虫与害虫防治"},{"code":"090403","name":"农药学"},{"code":"090501","name":"动物遗传育种与繁殖"},{"code":"090502","name":"动物营养与饲料科学"},{"code":"090503","name":"草业科学"},{"code":"090504","name":"特种经济动物饲养"},{"code":"090601","name":"基础兽医学"},{"code":"090602","name":"预防兽医学"},{"code":"090603","name":"临床兽医学"},{"code":"090701","name":"林木遗传育种"},{"code":"090702","name":"森林培育"},{"code":"090703","name":"森林保护学"},{"code":"090704","name":"森林经理学"},{"code":"090705","name":"野生动植物保护与利用"},{"code":"090706","name":"园林植物与观赏园艺"},{"code":"090707","name":"水土保持与荒漠化防治"},{"code":"090801","name":"水产养殖"},{"code":"090802","name":"捕捞学"},{"code":"090803","name":"渔业资源"},{"code":"100101","name":"人体解剖与组织胚胎学"},{"code":"100102","name":"免疫学"},{"code":"100103","name":"病原生物学"},{"code":"100104","name":"病理学与病理生理学"},{"code":"100105","name":"法医学"},{"code":"100106","name":"放射医学"},{"code":"100107","name":"航空、航天与航海医学"},{"code":"100201","name":"内科学"},{"code":"100202","name":"儿科学"},{"code":"100203","name":"老年医学"},{"code":"100204","name":"神经病学"},{"code":"100205","name":"精神病与精神卫生学"},{"code":"100206","name":"皮肤病与性病学"},{"code":"100207","name":"影像医学与核医学"},{"code":"100208","name":"临床检验诊断学"},{"code":"100209","name":"护理学"},{"code":"100210","name":"外科学"},{"code":"100211","name":"妇产科学"},{"code":"100212","name":"眼科学"},{"code":"100213","name":"耳鼻咽喉科学"},{"code":"100214","name":"肿瘤学"},{"code":"100215","name":"康复医学与理疗学"},{"code":"100216","name":"运动医学"},{"code":"100217","name":"麻醉学"},{"code":"100218","name":"急诊医学"},{"code":"100301","name":"口腔基础医学"},{"code":"100302","name":"口腔临床医学"},{"code":"100401","name":"流行病与卫生统计学"},{"code":"100402","name":"劳动卫生与环境卫生学"},{"code":"100403","name":"营养与食品卫生学"},{"code":"100404","name":"儿少卫生与妇幼保健学"},{"code":"100405","name":"卫生毒理学"},{"code":"100406","name":"军事预防医学"},{"code":"100501","name":"中医基础理论"},{"code":"100502","name":"中医临床基础"},{"code":"100503","name":"中医医史文献"},{"code":"100504","name":"方剂学"},{"code":"100505","name":"中医诊断学"},{"code":"100506","name":"中医内科学"},{"code":"100507","name":"中医外科学"},{"code":"100508","name":"中医骨伤科学"},{"code":"100509","name":"中医妇科学"},{"code":"100510","name":"中医儿科学"},{"code":"100511","name":"中医五官科学"},{"code":"100512","name":"针灸推拿学"},{"code":"100513","name":"民族医学"},{"code":"100601","name":"中西医结合基础"},{"code":"100602","name":"中西医结合临床"},{"code":"100701","name":"药物化学"},{"code":"100702","name":"药剂学"},{"code":"100703","name":"生药学"},{"code":"100704","name":"药物分析学"},{"code":"100705","name":"微生物与生化药学"},{"code":"100706","name":"药理学"},{"code":"110101","name":"军事思想"},{"code":"110102","name":"军事历史"},{"code":"110201","name":"军事战略学"},{"code":"110202","name":"战争动员学"},{"code":"110301","name":"联合战役学"},{"code":"110302","name":"军种战役学"},{"code":"110401","name":"合同战术学"},{"code":"110402","name":"兵种战术学"},{"code":"110501","name":"作战指挥学"},{"code":"110502","name":"军事运筹学"},{"code":"110503","name":"军事通信学"},{"code":"110504","name":"军事情报学"},{"code":"110505","name":"密码学"},{"code":"110506","name":"军事教育训练学"},{"code":"110601","name":"军事组织编制学"},{"code":"110602","name":"军队管理学"},{"code":"110801","name":"军事后勤学"},{"code":"110802","name":"后方专业勤务"},{"code":"110803","name":"军事装备学"},{"code":"120201","name":"会计学"},{"code":"120202","name":"企业管理"},{"code":"120203","name":"旅游管理"},{"code":"120204","name":"技术经济及管理"},{"code":"120301","name":"农业经济管理"},{"code":"120302","name":"林业经济管理"},{"code":"120401","name":"行政管理"},{"code":"120402","name":"社会医学与卫生事业管理"},{"code":"120403","name":"教育经济与管理"},{"code":"120404","name":"社会保障"},{"code":"120405","name":"土地资源管理"},{"code":"120501","name":"图书馆学"},{"code":"120502","name":"情报学"},{"code":"120503","name":"档案学"}],"doctor":[{"code":"010101","name":"马克思主义哲学"},{"code":"010102","name":"中国哲学"},{"code":"010103","name":"外国哲学"},{"code":"010104","name":"逻辑学"},{"code":"010105","name":"伦理学"},{"code":"010106","name":"美学"},{"code":"010107","name":"宗教学"},{"code":"010108","name":"科学技术哲学"},{"code":"020101","name":"政治经济学"},{"code":"020102","name":"经济思想史"},{"code":"020103","name":"经济史"},{"code":"020104","name":"西方经济学"},{"code":"020105","name":"世界经济"},{"code":"020106","name":"人口、资源与环境经济学"},{"code":"020201","name":"国民经济学"},{"code":"020202","name":"区域经济学"},{"code":"020203","name":"财政学"},{"code":"020204","name":"金融学"},{"code":"020205","name":"产业经济学"},{"code":"020206","name":"国际贸易学"},{"code":"020207","name":"劳动经济学"},{"code":"020208","name":"统计学"},{"code":"020209","name":"数量经济学"},{"code":"020210","name":"国防经济"},{"code":"030101","name":"法学理论"},{"code":"030102","name":"法律史"},{"code":"030103","name":"宪法学与行政法学"},{"code":"030104","name":"刑法学"},{"code":"030105","name":"民商法学"},{"code":"030106","name":"诉讼法学"},{"code":"030107","name":"经济法学"},{"code":"030108","name":"环境与资源保护法学"},{"code":"030109","name":"国际法学"},{"code":"030110","name":"军事法学"},{"code":"030201","name":"政治学理论"},{"code":"030202","name":"中外政治制度"},{"code":"030203","name":"科学社会主义与国际共产主义运动"},{"code":"030204","name":"中共党史"},{"code":"030206","name":"国际政治"},{"code":"030207","name":"国际关系"},{"code":"030208","name":"外交学"},{"code":"030301","name":"社会学"},{"code":"030302","name":"人口学"},{"code":"030303","name":"人类学"},{"code":"030304","name":"民俗学"},{"code":"030401","name":"民族学"},{"code":"030402","name":"马克思主义民族理论与政策"},{"code":"030403","name":"中国少数民族经济"},{"code":"030404","name":"中国少数民族史"},{"code":"030405","name":"中国少数民族艺术"},{"code":"030501","name":"马克思主义基本原理"},{"code":"030502","name":"马克思主义发展史"},{"code":"030503","name":"马克思主义中国化研究"},{"code":"030504","name":"国外马克思主义研究"},{"code":"030505","name":"思想政治教育"},{"code":"040101","name":"教育学原理"},{"code":"040102","name":"课程与教学论"},{"code":"040103","name":"教育史"},{"code":"040104","name":"比较教育学"},{"code":"040105","name":"学前教育学"},{"code":"040106","name":"高等教育学"},{"code":"040107","name":"成人教育学"},{"code":"040108","name":"职业技术教育学"},{"code":"040109","name":"特殊教育学"},{"code":"040110","name":"教育技术学"},{"code":"040201","name":"基础心理学"},{"code":"040202","name":"发展与教育心理学"},{"code":"040203","name":"应用心理学"},{"code":"040301","name":"体育人文社会学"},{"code":"040302","name":"运动人体科学"},{"code":"040303","name":"体育教育训练学"},{"code":"040304","name":"民族传统体育学"},{"code":"050101","name":"文艺学"},{"code":"050102","name":"语言学及应用语言学"},{"code":"050103","name":"汉语言文字学"},{"code":"050104","name":"中国古典文献学"},{"code":"050105","name":"中国古代文学"},{"code":"050106","name":"中国现当代文学"},{"code":"050107","name":"中国少数民族语言文学"},{"code":"050108","name":"比较文学与世界文学"},{"code":"050201","name":"英语语言文学"},{"code":"050202","name":"俄语语言文学"},{"code":"050203","name":"法语语言文学"},{"code":"050204","name":"德语语言文学"},{"code":"050205","name":"日语语言文学"},{"code":"050206","name":"印度语言文学"},{"code":"050207","name":"西班牙语语言文学"},{"code":"050208","name":"阿拉伯语语言文学"},{"code":"050209","name":"欧洲语言文学"},{"code":"050210","name":"亚非语言文学"},{"code":"050211","name":"外国语言学及应用语言学"},{"code":"050301","name":"新闻学"},{"code":"050302","name":"传播学"},{"code":"050401","name":"艺术学"},{"code":"050402","name":"音乐学"},{"code":"050403","name":"美术学"},{"code":"050404","name":"设计艺术学"},{"code":"050405","name":"戏剧戏曲学"},{"code":"050406","name":"电影学"},{"code":"050407","name":"广播电视艺术学"},{"code":"050408","name":"舞蹈学"},{"code":"060101","name":"史学理论及史学史"},{"code":"060102","name":"考古学及博物馆学"},{"code":"060103","name":"历史地理学"},{"code":"060104","name":"历史文献学"},{"code":"060105","name":"专门史"},{"code":"060106","name":"中国古代史"},{"code":"060107","name":"中国近现代史"},{"code":"060108","name":"世界史"},{"code":"070101","name":"基础数学"},{"code":"070102","name":"计算数学"},{"code":"070103","name":"概率论与数理统计"},{"code":"070104","name":"应用数学"},{"code":"070105","name":"运筹学与控制论"},{"code":"070201","name":"理论物理"},{"code":"070202","name":"粒子物理与原子核物理"},{"code":"070203","name":"原子与分子物理"},{"code":"070204","name":"等离子体物理"},{"code":"070205","name":"凝聚态物理"},{"code":"070206","name":"声学"},{"code":"070207","name":"光学"},{"code":"070208","name":"无线电物理"},{"code":"070301","name":"无机化学"},{"code":"070302","name":"分析化学"},{"code":"070303","name":"有机化学"},{"code":"070304","name":"物理化学"},{"code":"070305","name":"高分子化学与物理"},{"code":"070401","name":"天体物理"},{"code":"070402","name":"天体测量与天体力学"},{"code":"070501","name":"自然地理学"},{"code":"070502","name":"人文地理学"},{"code":"070503","name":"地图学与地理信息系统"},{"code":"070601","name":"气象学"},{"code":"070602","name":"大气物理学与大气环境"},{"code":"070701","name":"物理海洋学"},{"code":"070702","name":"海洋化学"},{"code":"070703","name":"海洋生物学"},{"code":"070704","name":"海洋地质"},{"code":"070801","name":"固体地球物理学"},{"code":"070802","name":"空间物理学"},{"code":"070901","name":"矿物学、岩石学、矿床学"},{"code":"070902","name":"地球化学"},{"code":"070903","name":"古生物学与地层学"},{"code":"070904","name":"构造地质学"},{"code":"070905","name":"第四纪地质学"},{"code":"071001","name":"植物学"},{"code":"071002","name":"动物学"},{"code":"071003","name":"生理学"},{"code":"071004","name":"水生生物学"},{"code":"071005","name":"微生物学"},{"code":"071006","name":"神经生物学"},{"code":"071007","name":"遗传学"},{"code":"071008","name":"发育生物学"},{"code":"071009","name":"细胞生物学"},{"code":"071010","name":"生物化学与分子生物学"},{"code":"071011","name":"生物物理学"},{"code":"071012","name":"生态学"},{"code":"071101","name":"系统理论"},{"code":"071102","name":"系统分析与集成"},{"code":"080101","name":"一般力学与力学基础"},{"code":"080102","name":"固体力学"},{"code":"080103","name":"流体力学"},{"code":"080104","name":"工程力学"},{"code":"080201","name":"机械制造及其自动化"},{"code":"080202","name":"机械电子工程"},{"code":"080203","name":"机械设计及理论"},{"code":"080204","name":"车辆工程"},{"code":"080401","name":"精密仪器及机械"},{"code":"080402","name":"测试计量技术及仪器"},{"code":"080501","name":"材料物理与化学"},{"code":"080502","name":"材料学"},{"code":"080503","name":"材料加工工程"},{"code":"080601","name":"冶金物理化学"},{"code":"080602","name":"钢铁冶金"},{"code":"080603","name":"有色金属冶金"},{"code":"080701","name":"工程热物理"},{"code":"080702","name":"热能工程"},{"code":"080703","name":"动力机械及工程"},{"code":"080704","name":"流体机械及工程"},{"code":"080705","name":"制冷及低温工程"},{"code":"080706","name":"化工过程机械"},{"code":"080801","name":"电机与电器"},{"code":"080802","name":"电力系统及其自动化"},{"code":"080803","name":"高电压与绝缘技术"},{"code":"080804","name":"电力电子与电力传动"},{"code":"080805","name":"电工理论与新技术"},{"code":"080901","name":"物理电子学"},{"code":"080902","name":"电路与系统"},{"code":"080903","name":"微电子学与固体电子学"},{"code":"080904","name":"电磁场与微波技术"},{"code":"081001","name":"通信与信息系统"},{"code":"081002","name":"信号与信息处理"},{"code":"081101","name":"控制理论与控制工程"},{"code":"081102","name":"检测技术与自动化装置"},{"code":"081103","name":"系统工程"},{"code":"081104","name":"模式识别与智能系统"},{"code":"081105","name":"导航、制导与控制"},{"code":"081201","name":"计算机系统结构"},{"code":"081202","name":"计算机软件与理论"},{"code":"081203","name":"计算机应用技术"},{"code":"081301","name":"建筑历史与理论"},{"code":"081302","name":"建筑设计及其理论"},{"code":"081303","name":"城市规划与设计"},{"code":"081304","name":"建筑技术科学"},{"code":"081401","name":"岩土工程"},{"code":"081402","name":"结构工程"},{"code":"081403","name":"市政工程"},{"code":"081404","name":"供热、供燃气、通风及空调工程"},{"code":"081405","name":"防灾减灾工程及防护工程"},{"code":"081406","name":"桥梁与隧道工程"},{"code":"081501","name":"水文学及水资源"},{"code":"081502","name":"水力学及河流动力学"},{"code":"081503","name":"水工结构工程"},{"code":"081504","name":"水利水电工程"},{"code":"081505","name":"港口、海岸及近海工程"},{"code":"081601","name":"大地测量学与测量工程"},{"code":"081602","name":"摄影测量与遥感"},{"code":"081603","name":"地图制图学与地理信息工程"},{"code":"081701","name":"化学工程"},{"code":"081702","name":"化学工艺"},{"code":"081703","name":"生物化工"},{"code":"081704","name":"应用化学"},{"code":"081705","name":"工业催化"},{"code":"081801","name":"矿产普查与勘探"},{"code":"081802","name":"地球探测与信息技术"},{"code":"081803","name":"地质工程"},{"code":"081901","name":"采矿工程"},{"code":"081902","name":"矿物加工工程"},{"code":"081903","name":"安全技术及工程"},{"code":"082001","name":"油气井工程"},{"code":"082002","name":"油气田开发工程"},{"code":"082003","name":"油气储运工程"},{"code":"082101","name":"纺织工程"},{"code":"082102","name":"纺织材料与纺织品设计"},{"code":"082103","name":"纺织化学与染整工程"},{"code":"082104","name":"服装设计与工程"},{"code":"082201","name":"制浆造纸工程"},{"code":"082202","name":"制糖工程"},{"code":"082203","name":"发酵工程"},{"code":"082204","name":"皮革化学与工程"},{"code":"082301","name":"道路与铁道工程"},{"code":"082302","name":"交通信息工程及控制"},{"code":"082303","name":"交通运输规划与管理"},{"code":"082304","name":"载运工具运用工程"},{"code":"082401","name":"船舶与海洋结构物设计制造"},{"code":"082402","name":"轮机工程"},{"code":"082403","name":"水声工程"},{"code":"082501","name":"飞行器设计"},{"code":"082502","name":"航空宇航推进理论与工程"},{"code":"082503","name":"航空宇航制造工程"},{"code":"082504","name":"人机与环境工程"},{"code":"082601","name":"武器系统与运用工程"},{"code":"082602","name":"兵器发射理论与技术"},{"code":"082603","name":"火炮、自动武器与弹药工程"},{"code":"082604","name":"军事化学与烟火技术"},{"code":"082701","name":"核能科学与工程"},{"code":"082702","name":"核燃料循环与材料"},{"code":"082703","name":"核技术及应用"},{"code":"082704","name":"辐射防护及环境保护"},{"code":"082801","name":"农业机械化工程"},{"code":"082802","name":"农业水土工程"},{"code":"082803","name":"农业生物环境与能源工程"},{"code":"082804","name":"农业电气化与自动化"},{"code":"082901","name":"森林工程"},{"code":"082902","name":"木材科学与技术"},{"code":"082903","name":"林产化学加工工程"},{"code":"083001","name":"环境科学"},{"code":"083002","name":"环境工程"},{"code":"083201","name":"食品科学"},{"code":"083202","name":"粮食、油脂及植物蛋白工程"},{"code":"083203","name":"农产品加工及贮藏工程"},{"code":"083204","name":"水产品加工及贮藏工程"},{"code":"090101","name":"作物栽培学与耕作学"},{"code":"090102","name":"作物遗传育种"},{"code":"090201","name":"果树学"},{"code":"090202","name":"蔬菜学"},{"code":"090203","name":"茶学"},{"code":"090301","name":"土壤学"},{"code":"090302","name":"植物营养学"},{"code":"090401","name":"植物病理学"},{"code":"090402","name":"农业昆虫与害虫防治"},{"code":"090403","name":"农药学"},{"code":"090501","name":"动物遗传育种与繁殖"},{"code":"090502","name":"动物营养与饲料科学"},{"code":"090503","name":"草业科学"},{"code":"090504","name":"特种经济动物饲养"},{"code":"090601","name":"基础兽医学"},{"code":"090602","name":"预防兽医学"},{"code":"090603","name":"临床兽医学"},{"code":"090701","name":"林木遗传育种"},{"code":"090702","name":"森林培育"},{"code":"090703","name":"森林保护学"},{"code":"090704","name":"森林经理学"},{"code":"090705","name":"野生动植物保护与利用"},{"code":"090706","name":"园林植物与观赏园艺"},{"code":"090707","name":"水土保持与荒漠化防治"},{"code":"090801","name":"水产养殖"},{"code":"090802","name":"捕捞学"},{"code":"090803","name":"渔业资源"},{"code":"100101","name":"人体解剖与组织胚胎学"},{"code":"100102","name":"免疫学"},{"code":"100103","name":"病原生物学"},{"code":"100104","name":"病理学与病理生理学"},{"code":"100105","name":"法医学"},{"code":"100106","name":"放射医学"},{"code":"100107","name":"航空、航天与航海医学"},{"code":"100201","name":"内科学"},{"code":"100202","name":"儿科学"},{"code":"100203","name":"老年医学"},{"code":"100204","name":"神经病学"},{"code":"100205","name":"精神病与精神卫生学"},{"code":"100206","name":"皮肤病与性病学"},{"code":"100207","name":"影像医学与核医学"},{"code":"100208","name":"临床检验诊断学"},{"code":"100209","name":"护理学"},{"code":"100210","name":"外科学"},{"code":"100211","name":"妇产科学"},{"code":"100212","name":"眼科学"},{"code":"100213","name":"耳鼻咽喉科学"},{"code":"100214","name":"肿瘤学"},{"code":"100215","name":"康复医学与理疗学"},{"code":"100216","name":"运动医学"},{"code":"100217","name":"麻醉学"},{"code":"100218","name":"急诊医学"},{"code":"100301","name":"口腔基础医学"},{"code":"100302","name":"口腔临床医学"},{"code":"100401","name":"流行病与卫生统计学"},{"code":"100402","name":"劳动卫生与环境卫生学"},{"code":"100403","name":"营养与食品卫生学"},{"code":"100404","name":"儿少卫生与妇幼保健学"},{"code":"100405","name":"卫生毒理学"},{"code":"100406","name":"军事预防医学"},{"code":"100501","name":"中医基础理论"},{"code":"100502","name":"中医临床基础"},{"code":"100503","name":"中医医史文献"},{"code":"100504","name":"方剂学"},{"code":"100505","name":"中医诊断学"},{"code":"100506","name":"中医内科学"},{"code":"100507","name":"中医外科学"},{"code":"100508","name":"中医骨伤科学"},{"code":"100509","name":"中医妇科学"},{"code":"100510","name":"中医儿科学"},{"code":"100511","name":"中医五官科学"},{"code":"100512","name":"针灸推拿学"},{"code":"100513","name":"民族医学"},{"code":"100601","name":"中西医结合基础"},{"code":"100602","name":"中西医结合临床"},{"code":"100701","name":"药物化学"},{"code":"100702","name":"药剂学"},{"code":"100703","name":"生药学"},{"code":"100704","name":"药物分析学"},{"code":"100705","name":"微生物与生化药学"},{"code":"100706","name":"药理学"},{"code":"110101","name":"军事思想"},{"code":"110102","name":"军事历史"},{"code":"110201","name":"军事战略学"},{"code":"110202","name":"战争动员学"},{"code":"110301","name":"联合战役学"},{"code":"110302","name":"军种战役学"},{"code":"110401","name":"合同战术学"},{"code":"110402","name":"兵种战术学"},{"code":"110501","name":"作战指挥学"},{"code":"110502","name":"军事运筹学"},{"code":"110503","name":"军事通信学"},{"code":"110504","name":"军事情报学"},{"code":"110505","name":"密码学"},{"code":"110506","name":"军事教育训练学"},{"code":"110601","name":"军事组织编制学"},{"code":"110602","name":"军队管理学"},{"code":"110801","name":"军事后勤学"},{"code":"110802","name":"后方专业勤务"},{"code":"110803","name":"军事装备学"},{"code":"120201","name":"会计学"},{"code":"120202","name":"企业管理"},{"code":"120203","name":"旅游管理"},{"code":"120204","name":"技术经济及管理"},{"code":"120301","name":"农业经济管理"},{"code":"120302","name":"林业经济管理"},{"code":"120401","name":"行政管理"},{"code":"120402","name":"社会医学与卫生事业管理"},{"code":"120403","name":"教育经济与管理"},{"code":"120404","name":"社会保障"},{"code":"120405","name":"土地资源管理"},{"code":"120501","name":"图书馆学"},{"code":"120502","name":"情报学"},{"code":"120503","name":"档案学"}]};

var DegreeList = [
 { "code": "bachelor", "name": "本科毕业论文" },
 { "code": "master", "name": "硕士毕业论文" },
 { "code": "doctor", "name": "博士毕业论文" },
 { "code": "postdoctor", "name": "博士后毕业论文" }
 ];

var LanguageList = [
 { "code": "zh_CN", "name": "中文" },
 { "code": "en", "name": "英文" }
 ];

function getLanguage(code) {
    var result = code;
    for (var i in LanguageList) {
        if (LanguageList[i].code == code) {
            result = LanguageList[i].name;
            break;
        }
    }
    return result;
}
var ProviderList = [
 { "code": "CNKI", "name": "中国知网" },
 { "code": "WanFang", "name": "万方" },
 { "code": "VIP", "name": "重庆维普" }
 ];

function getProvider(code) {
    var result = code;
    for (var i in ProviderList) {
        if (ProviderList[i].code == code) {
            result = ProviderList[i].name;
            break;
        }
    }
    return result;
}

var ChineseNumbers = ["一", "二", "三", "四", "五", "六", "七", "八", "九"
, "十", "十一", "十二", "十三", "十四", "十五", "十六", "十七", "十八", "十九"
, "二十", "二十一", "二十二", "二十三", "二十四", "二十五", "二十六", "二十七", "二十八", "二十九"
, "三十", "三十一", "三十二", "三十三", "三十四", "三十五", "三十六", "三十七", "三十八", "三十九"
, "四十", "四十一", "四十二", "四十三", "四十四", "四十五", "四十六", "四十七", "四十八", "四十九"
, "五十", "五十一", "五十二", "五十三", "五十四", "五十五", "五十六", "五十七", "五十八", "五十九"
, "六十", "六十一", "六十二", "六十三", "六十四", "六十五", "六十六", "六十七", "六十八", "六十九"
, "七十", "七十一", "七十二", "七十三", "七十四", "七十五", "七十六", "七十七", "七十八", "七十九"
, "八十", "八十一", "八十二", "八十三", "八十四", "八十五", "八十六", "八十七", "八十八", "八十九"
, "九十", "九十一", "九十二", "九十三", "九十四", "九十五", "九十六", "九十七", "九十八", "九十九"
];

function Guid(g) {
	var arr = new Array();
	if (typeof(g) == "string") {
		InitByString(arr, g)
	} else {
		InitByOther(arr)
	}
	this.Equals = function(o) {
		if (o && o.IsGuid) {
			return this.ToString() == o.ToString()
		} else {
			return false
		}
	}
	this.IsGuid = function() {}
	this.ToString = function(format) {
		if (typeof(format) == "string") {
			if (format == "N" || format == "D" || format == "B" || format == "P") {
				return ToStringWithFormat(arr, format)
			} else {
				return ToStringWithFormat(arr, "D")
			}
		} else {
			return ToStringWithFormat(arr, "D")
		}
	}
	function InitByString(arr, g) {
		g = g.replace(/\{|\(|\)|\}|-/g, "");
		g = g.toLowerCase();
		if (g.length != 32 || g.search(/[^0-9,a-f]/i) != -1) {
			InitByOther(arr)
		} else {
			for (var i = 0; i < g.length; i++) {
				arr.push(g[i])
			}
		}
	}
	function InitByOther(arr) {
		var i = 32;
		while (i--) {
			arr.push("0")
		}
	}
	function ToStringWithFormat(arr, format) {
		switch (format) {
		case "N":
			return arr.toString().replace(/,/g, "");
		case "D":
			var str = arr.slice(0, 8) + "-" + arr.slice(8, 12) + "-" + arr.slice(12, 16) + "-" + arr.slice(16, 20) + "-" + arr.slice(20, 32);
			str = str.replace(/,/g, "");
			return str;
		case "B":
			var str = ToStringWithFormat(arr, "D");
			str = "{" + str + "}";
			return str;
		case "P":
			var str = ToStringWithFormat(arr, "D");
			str = "(" + str + ")";
			return str;
		default:
			return new Guid()
		}
	}
}
Guid.Empty = new Guid();
Guid.NewGuid = function() {
	var g = "";
	var i = 32;
	while (i--) {
		g += Math.floor(Math.random() * 16.0).toString(16)
	}
	return new Guid(g)
}




//整除  
function $Div(exp1, exp2) {
    var n1 = Math.round(exp1); //四舍五入  
    var n2 = Math.round(exp2); //四舍五入  

    var rslt = n1 / n2; //除  

    if (rslt >= 0) {
        rslt = Math.floor(rslt); //返回小于等于原rslt的最大整数。
    }
    else {
        rslt = Math.ceil(rslt); //返回大于等于原rslt的最小整数。  
    }
    if (n1 % n2 > 0) rslt += 1; //进一制
    return rslt;
}
