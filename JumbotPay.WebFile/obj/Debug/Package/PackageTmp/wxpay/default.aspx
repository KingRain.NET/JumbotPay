﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="JumbotPay.WebFile.WxPay._default" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>微信扫描支付_<%=site.Name %></title>
<link href="/_statics/css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/_statics/js/jquery.1.7.2.js"></script>
<script type="text/javascript" src="/_statics/js/jquery.timer.js"></script>
</head>
<body>
<div class="payHead">
  <div class="payWrap">
    <div class="payTitle">
      <div class="logo"><a href="<%=APP_Url%>" target="_blank"><img src="<%=APP_Logo%>" alt="<%=APP_Name%>" title="<%=APP_Name%>" class="pngfix" ></a></div>
      <span>微信扫描支付</span>
     </div>
  </div>
</div>
<div class="payWrap">
  <div class="order">
    <div class="orderDetail"> <span>请扫描下方二维码</span><span class="orderTip1">[?]</span>
      <div class="orderTip2">
        <div class="orderTip_content"> <strong>付款后资金直接进入对方账户</strong>
          <p>若发生退款需联系收款方协商，如付款给陌生人，请谨慎操作。</p>
        </div>
        <div class="orderTip_arrow"></div>
      </div>
    </div>
    <div class="commodity_message"><strong><%=Product_Name%></strong></div>
    <div class="payAmount_area"><strong><%=Pay_Amount%></strong>元</div>
  </div>
  <div class="order_center">
    <div class="occode_area">
      <div class="occode_top">
        <p style="font-size:12px;">扫一扫付款（元）</p>
        <strong><%=Pay_Amount%></strong> </div>
      <div class="occode_mid">
        <div class="occode_img"><asp:Image ID="Image1" runat="server" style="width:168px;height:168px;"/></div>
        <div class="occode_explain"> <img src="/_statics/images/payment_bg2.png" />
          <p>打开微信客户端<br/>
            扫一扫继续付款</p>
        </div>
      </div>
      <div class="occode_foot"></div>
    </div>
    <div class="ordcen_example"><img src="/_statics/images/payment_bg3.png"></div>
  </div>
</div>
<script type="text/javascript">
    $(function () {
        $(".orderTip2").css("display", "none");
        $(".orderTip1").hover(function () {
            $(".orderTip2").toggle();
        });
    });
</script>
<div class="clear"></div>
<div class="payFooter"></div>
<script>
    var ajaxloaded = false;
    function ajaxCheckOrderState() {
        if (ajaxloaded) return;
        ajaxloaded = true;
        $.ajax({
            type: "get",
            dataType: "json",
            data: "order_id=<%=Order_ID %>&clienttime=" + Math.random(),
            url: 'order_query.aspx',
            error: function (XmlHttpRequest, textStatus, errorThrown) { ajaxloaded = false; if (XmlHttpRequest.responseText != "") { alert(XmlHttpRequest.responseText); } },
            success: function (d) {
                ajaxloaded = false;
                switch (d.result) {
                    case '0':
                        break;
                    case '1':
                        /*try {
                        opener.paymentSuccess();
                        }
                        catch (e) {
                        }
                        window.open('', '_self').close();
                        */
                        location.href = 'return_url.aspx?order_id=<%=Order_ID %>';
                        break;
                }
            }
        });
    }
    var lastseconds = 2;
    function CheckOrderState() {
        $('body').everyTime('1s', function () {
            lastseconds--;
            if (lastseconds < 0) {
                lastseconds = 2;
                ajaxCheckOrderState();
                return;
            }
        }, 0, true);
    }
    CheckOrderState();
</script>
</body>
</html>
