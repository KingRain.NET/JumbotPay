﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="order_list.aspx.cs" Inherits="JumbotPay.WebFile.List.report_list" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html   xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta   http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta   name="robots" content="noindex, nofollow" />
<meta   http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<title>order list</title>
<script type="text/javascript" src="../_libs/jquery.tools.pack.js"></script>
<script type="text/javascript" src="../_data/global.js?v20150402"></script>
<link type="text/css" rel="stylesheet" href="../_data/global.css" />
<link type="text/css" rel="stylesheet" href="../statics/admin/css/common.css" />
<script type="text/javascript" src="../statics/admin/js/common.js"></script>
<script type="text/javascript" src="/_libs/my97datepicker4.8/WdatePicker.js"></script>
<script type="text/javascript">
    var pagesize = 20;
    var page = thispage();
    var pay_order_id = "";
    var begindate = '<%=BeginDate %>';
    var enddate = '<%=EndDate %>';
    $(document).ready(function () {
        ajaxList(page);
        $("#txtpay_order_id").unbind('keydown').bind('keydown', function (event) {
        if (event.keyCode == 13)//enter
        {
            ajaxSearch();
        }
    });
    });
    function ajaxList(currentpage) {
        if (currentpage != null) page = currentpage;
        $.ajax({
            type: "get",
            dataType: "json",
            data: "pay_order_id=" + encodeURIComponent(pay_order_id) + "&begindate=" + begindate + "&enddate=" + enddate + "&page=" + currentpage + "&pagesize=" + pagesize + "&clienttime=" + Math.random(),
            url: "order_ajax.aspx?oper=ajaxGetList",
            error: function (XmlHttpRequest, textStatus, errorThrown) { alert(XmlHttpRequest.responseText); },
            success: function (d) {
                switch (d.result) {
                    case '0':
                        JumbotPay.Alert(d.returnval, "0");
                        break;
                    case '1':
                        $("#ajaxList").setTemplateElement("tplList", null, { filter_data: true });
                        $("#ajaxList").processTemplate(d);
                        $("#ajaxPageBar").html(d.pagebar);
                        break;
                }
            }
        });
    }
 function ajaxSearch() {
     page = 1;
     begindate = $("#txtBeginDate").val();
     enddate = $("#txtEndDate").val();
     pay_order_id = $.trim($("#txtpay_order_id").val());
    ajaxList(page);
}
</script>
</head>
<body>

<textarea class="template" id="tplList" style="display:none">
<table class="cooltable">
<thead>
	<tr>
		<th align="center" scope="col" style="width:40px;"><input onclick="checkAllLine()" id="checkedAll" name="checkedAll" type="checkbox" title="全部选择/全部不选" /></th>
		<th style="width:100px;">订单号</th>
   		<th width="*">来源</th>
		<th style="width:80px;">支付方式</th>
		<th style="width:80px;">金额</th>
		<th style="width:60px;">支付状态</th>
        <th scope="col" style="width:150px;">订单时间</th>
		<th scope="col" style="width:200px;">out_trade_no</th>
		<th scope="col" style="width:200px;">pay_order_id</th>
        <th scope="col" style="width:150px;">支付时间</th>
		<th scope="col" style="width:60px;">通知</th>
	</tr>
</thead>
<tbody>
	{#foreach $T.table as record}
	<tr>
		<td align="center"><input class="checkbox" name="selectID" type="checkbox" value='{$T.record.id}' /></td>
		<td align="center">{$T.record.order_id}</td>
		<td align="center">{$T.record.product_name}</td>
		<td align="center">{$T.record.pay_method}</td>
        <td align="center">{$T.record.pay_amount}</td>
        <td align="center">{$T.record.pay_status}</td>
		<td align="center">{formatDate($T.record.order_time,"yyyy-MM-dd HH:mm")}</td>
        <td align="center">{$T.record.out_trade_no}</td>
        <td align="center">{$T.record.pay_order_id}</td>
		<td align="center">{formatDate($T.record.pay_time,"yyyy-MM-dd HH:mm")}</td>
        <td align="center">{$T.record.notified}</td>
	</tr>
	{#/for}
</tbody>
</table>
</textarea>
<div class="mrg10A"><input name="txtBeginDate" type="text" value="<%=BeginDate %>" maxlength="14" id="txtBeginDate" class="Wdate ipt pad5A" onfocus="WdatePicker({isShowClear:false,readOnly:false,skin:&#39;blue&#39;,startDate:&#39;<%=BeginDate %>&#39;})" style="width:110px" />
                                      至
                                      <input name="txtEndDate" type="text" value="<%=EndDate %>" maxlength="14" id="txtEndDate" class="Wdate ipt pad5A" onfocus="WdatePicker({isShowClear:false,readOnly:false,skin:&#39;blue&#39;,startDate:&#39;<%=EndDate %>&#39;})" style="width:110px" />

                              第三方支付单号：<input type="text" id="txtpay_order_id" style="width:280px;" />
                              <input type="button" class="btnsubmit" value="查询" onclick="ajaxSearch()" />
                              </div>
    <div id="ajaxList" style="width:100%;margin:0;padding:0" class="mrg10T"></div>
    <div id="ajaxPageBar" class="pages"></div>
</body>
</html>
