﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default2.aspx.cs" Inherits="JumbotPay.WebFile.WxPay._default2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>微信扫描支付</title>
<script type="text/javascript" src="../_statics/js/jquery.1.7.2.js"></script>
<script type="text/javascript" src="../_statics/js/jquery.timer.js"></script>
</head>
<body style="margin:0;padding:0;">
<asp:Image ID="Image1" runat="server" style="width:168px;height:168px;"/>
<script>
    var ajaxloaded = false;
    function ajaxCheckOrderState() {
        if (ajaxloaded) return;
        ajaxloaded = true;
        $.ajax({
            type: "get",
            dataType: "json",
            data: "order_id=<%=Order_ID %>&clienttime=" + Math.random(),
            url: 'order_query.aspx',
            error: function (XmlHttpRequest, textStatus, errorThrown) { ajaxloaded = false; if (XmlHttpRequest.responseText != "") { alert(XmlHttpRequest.responseText); } },
            success: function (d) {
                ajaxloaded = false;
                switch (d.result) {
                    case '0':
                        break;
                    case '1':
                        try {
                            parent.PaymentSuccess();
                        }
                        catch (e) {
                        }
                        break;
                }
            }
        });
    }
    var lastseconds = 2;
    function CheckOrderState() {
        $('body').everyTime('1s', function () {
            lastseconds--;
            if (lastseconds < 0) {
                lastseconds = 2;
                ajaxCheckOrderState();
                return;
            }
        }, 0, true);
    }
    CheckOrderState();
</script>
</body>
</html>
