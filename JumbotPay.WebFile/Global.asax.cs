﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Timers;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
namespace JumbotPay.WebFile
{
    public class Global : System.Web.HttpApplication
    {
        public static HttpContext myContext = HttpContext.Current;
        private static string SiteUrl = System.Configuration.ConfigurationManager.AppSettings["JumbotPay:SiteUrl"];
        protected void Application_Start(object sender, EventArgs e)
        {
            JumbotPay.DAL.AutoTaskDAL.Thread_CheckPayState = new Thread(new ThreadStart(JumbotPay.DAL.AutoTaskDAL.CheckPayState));
            JumbotPay.DAL.AutoTaskDAL.Thread_CheckPayState.Start();

            JumbotPay.DAL.AutoTaskDAL.Thread_SendPayNotice = new Thread(new ThreadStart(JumbotPay.DAL.AutoTaskDAL.SendPayNotice));
            JumbotPay.DAL.AutoTaskDAL.Thread_SendPayNotice.Start();
        }
        protected void Application_End(object sender, EventArgs e)
        {
            //System.Threading.Thread.Sleep(1000);
            //这里设置你的web地址，可以随便指向你的任意一个aspx页面甚至不存在的页面，目的是要激发Application_Start  
            try
            {
                string url = SiteUrl + "/loading.html";
                HttpWebRequest myHttpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();
                Stream receiveStream = myHttpWebResponse.GetResponseStream();//得到回写的字节流  
            }
            catch
            {
            }
        }
        protected void Application_Error(object sender, EventArgs e)
        {
            CYQ.Data.Log.WriteLogToTxt("\r\n客户机IP:" + Request.UserHostAddress + "\r\n错误地址:" + Request.Url + "\r\n异常信息:" + Server.GetLastError().Message);
        }
    }
}