﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using JumbotPay.API.Bdpay;

namespace JumbotPay.WebFile.Bdpay
{
    public partial class _default2 : JumbotPay.UI.BasicPage
    {
        public string GenerateOutTradeNo()
        {
            var ran = new Random();
            return string.Format("{0}{1}", DateTime.Now.ToString("yyyyMMddHHmmss"), ran.Next(999));
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            string appid = q("appid");//appid
            string order_id = q("order_id");//订单号
            string sign = q("sign");
            string key = new JumbotPay.DAL.AppDAL().GetKey(appid);
            if (key == "")
            {
                Response.Write("无效的appid");
                Response.End();
            }
            string[] array0 ={
			    "order_id="+order_id,
                 "appid="+appid
				};
            if (!JumbotPay.Utils.Strings.CheckSign(array0, key, sign))
            {
                Response.Write("验签失败");
                Response.End();
            }
            object[] _value = new JumbotPay.DAL.OrderDAL().GetOrderFields("bdpay", order_id, "", "product_name,product_desc,pay_amount");
            string subject = _value[0].ToString();                            //商品名称
            string body = _value[1].ToString();                                 //商品描述
            string pay_amount = _value[2].ToString();
            string out_trade_no = order_id;
            //string out_trade_no = GenerateOutTradeNo();
            //new JumbotPay.DAL.OrderDAL().UpdateOutOrderNO(order_id, out_trade_no);


            LogWriter logwriter = new LogWriter();


            string strXmlFile = HttpContext.Current.Server.MapPath("~/_data/config/payment_bdpay.config");
            JumbotPay.DBUtility.XmlControl XmlTool = new JumbotPay.DBUtility.XmlControl(strXmlFile);
            string sp_no = "sp_no=" + XmlTool.GetText("Root/sp_no");
            XmlTool.Dispose();

            //商品分类号BFB_INTERFACE_CURRENTCY
            string service_code ="service_code=1";
            //商户号
            //交易的超时时间,当前时间加2天
            DateTime dt1 = DateTime.Now;
            DateTime dt2 = dt1.AddDays(2);
            string expire_time ="expire_time=" + string.Format("{0:yyyyMMddHHmmss}", dt2);

            //订单创建时间
            string order_create_time1 = string.Format("{0:yyyyMMddHHmmss}", dt1);
            string order_create_time = "order_create_time=" + order_create_time1;
            //订单号
            Random Random1 = new Random();
            string order_no = "order_no=" + out_trade_no;

            //币种
            string currency = "currency=1";
            //编码
            string input_charset = "input_charset=1";
            //版本
            string version = "version=2";
            //加密方式md5或者sha1
            string sign_method = "sign_method=1";
            //提交地址
            string BFB_PAY_DIRECT_LOGIN_URL = "https://www.baifubao.com/api/0/pay/0/direct/0";

            /**
             * 2、获取页面表单提交的值
             */
            //商品种类
            String goods_category = "goods_category=1";

            //商品名称
            String goods_name = "goods_name=" + subject;
            String goods_name1 = "goods_name=" + System.Web.HttpUtility.UrlEncode(subject, Encoding.GetEncoding("GBK"));
            //商品描述
            String goods_desc = "goods_desc=" + body;
            String goods_desc1 = "goods_desc=" + System.Web.HttpUtility.UrlEncode(body, Encoding.GetEncoding("GBK"));
            //商品在商户网站上的URL
            String goods_url = "goods_url=" + site.Url;
            String goods_url1 = "goods_url=" + System.Web.HttpUtility.UrlEncode(site.Url, Encoding.GetEncoding("GBK"));

            //单价
            String unit_amount = "unit_amount=" + (Str2Double(pay_amount)*100);
            //数量
            String unit_count = "unit_count=1";
            //运费
            String transport_amount = "transport_amount=0";
            //总金额
            String total_amount = "total_amount=" + (Str2Double(pay_amount) * 100);
            //买家在商户网站的用户名
            String buyer_sp_username = "buyer_sp_username=" + "";
            String buyer_sp_username1 = "buyer_sp_username=" + System.Web.HttpUtility.UrlEncode("", Encoding.GetEncoding("GBK"));
            //后台通知地址
            String return_url = "return_url="+site.Url+"/bdpay/notify_url.aspx";
            String return_url1 = "return_url=" + System.Web.HttpUtility.UrlEncode(site.Url + "/bdpay/notify_url.aspx", Encoding.GetEncoding("GBK"));
            //前台通知地址
            String page_url = "page_url=" + site.Url + "/bdpay/return_url.aspx";
            String page_url1 = "page_url=" + System.Web.HttpUtility.UrlEncode(site.Url + "/bdpay/return_url.aspx", Encoding.GetEncoding("GBK"));
            //支付方式
            String pay_type = "pay_type=2";
            //默认银行的编码
            String bank_no = "bank_no=201";
            //用户在商户端的用户ID
            String sp_uno = "sp_uno=" + appid;
            //商户自定义数据
            String extra = "extra=" + appid;
            String extra1 = "extra=" + System.Web.HttpUtility.UrlEncode(appid, Encoding.GetEncoding("GBK"));

            //分润



            //签名串拼接数组
            string[] array ={
			    service_code,
				sp_no,
				order_create_time,
				order_no,
				goods_category,
				goods_name,
				goods_desc,
				goods_url,
				unit_amount,
				unit_count,
				transport_amount,
				total_amount,
				currency,
				buyer_sp_username ,
				return_url,
				page_url,
				pay_type,
				bank_no,
				expire_time,
				input_charset,
				version,
				sign_method
				,extra
               // ,profit_solution
				};
            //浏览器参数拼接数组
            string[] array1 ={
			    service_code,
				sp_no,
				order_create_time,
				order_no,
				goods_category,
				goods_name1,
				goods_desc1,
				goods_url1,
				unit_amount,
				unit_count,
				transport_amount,
				total_amount,
				currency,
				buyer_sp_username1,
				return_url1,
				page_url1,
				pay_type,
				bank_no,
				expire_time,
				input_charset,
				version,
				sign_method
				,extra1
               // ,profit_solution
				};
            /**
		     * 3、调用common.cs里create_baifubao_url方法生成百度钱包PC端即时到账支付接口URL(需要登录)
		     *    array是待签名串
		     *    array1地址栏拼接串
		     */
            string getURL = new JumbotPay.API.Bdpay.Common().create_baifubao_url(array, array1, BFB_PAY_DIRECT_LOGIN_URL);
            //Response.Write(new Common().make_sign(array));
            logwriter.appendToLog("提交串：" + getURL, Server.MapPath("~/Logs/NetLogBFB.log"));
            //CYQ.Data.Log.WriteLogToTxt("\r\n" + Request.UrlReferrer + "\r\n" + Request.Url.ToString() + "\r\n");
            Response.Redirect(getURL);
        }
    }
}
