﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Specialized;
using System.Collections.Generic;
using JumbotPay.API.Bdpay;

namespace JumbotPay.WebFile.Bdpay
{

    /// <summary>
    /// 创建该页面文件时，请留心该页面文件中无任何HTML代码及空格。
    /// 该页面称作“通知页”，是异步被支付宝服务器所调用。
    /// 当支付宝的订单状态改变时，支付宝服务器则会自动调用此页面，因此请做好自身网站订单信息与支付宝上的订单的同步工作
    /// </summary>
    public partial class _notify_url : JumbotPay.UI.BasicPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            LogWriter logwriter = new LogWriter();
            string getStrPre = Request.QueryString.ToString();
            logwriter.appendToLog("notifyUrl支付返回地址栏数据：" + getStrPre, Server.MapPath("~/Logs/NetLogBFB.log"));
            string getStr = System.Web.HttpUtility.UrlDecode(getStrPre, System.Text.Encoding.GetEncoding("GBK"));
            string[] resultStrTemp = getStr.Split('&');
            string[] resultStr = new String[resultStrTemp.Length - 1];
            string signtemp = resultStrTemp[resultStrTemp.LongLength - 1];
            int stateInd = signtemp.IndexOf("=") + 1;
            string sign = signtemp.Substring(stateInd);
            logwriter.appendToLog("百度钱包返回的签名串：" + sign, Server.MapPath("~/Logs/NetLogBFB.log"));

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            for (int i = 0; i < resultStrTemp.Length - 1; i++)
            {
                resultStr[i] = resultStrTemp[i];
                sb.AppendLine(resultStr[i]);
            }
            logwriter.appendToLog("百度钱包返回参数", Server.MapPath("~/Logs/NetLogBFB.log"));
            logwriter.appendToLog(sb.ToString(), Server.MapPath("~/Logs/NetLogBFB.log"));
            String Localsign = new JumbotPay.API.Bdpay.Common().make_sign(resultStr);
            logwriter.appendToLog("本地签名串：" + Localsign, Server.MapPath("~/Logs/NetLogBFB.log"));
            logwriter.appendToLog("百度钱包返回签名串：" + sign, Server.MapPath("~/Logs/NetLogBFB.log"));

            //比对签名
            if (Request["pay_result"].Equals("1"))
            {
                Response.Write("返回签名册" + sign.Trim());
                Response.Write("本地验证签名串" + Localsign.Trim());
                if (sign.Trim().Equals(Localsign.Trim()))
                {
                    string pay_order_id = Request.QueryString["bfb_order_no"];         //百付宝交易号
                    string out_trade_no = Request.QueryString["order_no"];	        //获取订单号
                    string total_amount = Request.QueryString["total_amount"];            //获取总金额
                    string appid = Request.QueryString["extra"];
                    if (new JumbotPay.DAL.OrderDAL().UpdateOrder("bdpay", out_trade_no, pay_order_id))
                    {
                    }
                    Response.Write("success");
                }
                else if (!sign.Trim().Equals(Localsign.Trim()))
                {
                    Response.Write("支付成功，验签失败");
                    logwriter.appendToLog("支付成功，验签失败", Server.MapPath("~/Logs/NetLogBFB.log"));
                }
            }
            else
            {
                Response.Write("支付失败");
                logwriter.appendToLog("支付失败", Server.MapPath("~/Logs/NetLogBFB.log"));
            }
        }
    }
}