﻿/*
 * 程序名称: JumbotPay
 * 
 * 程序版本: 1.x
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using JumbotPay.Utils;
using JumbotPay.Common;
namespace JumbotPay.WebFile
{
    public partial class _direct2 : JumbotPay.UI.BasicPage
    {
        private string _operType = string.Empty;
        private string _response = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            string pay_method = q("pay_method");
            string pay_amount = q("pay_amount");
            string product_name = q("product_name");
            string product_desc = q("product_desc");
            string order_id = q("order_id");
            string appid = q("appid");
            string sign = q("sign");
            string key = new JumbotPay.DAL.AppDAL().GetKey(appid);
            if (key == "")
            {
                Response.Write("无效的appid");
                Response.End();
            }
            string[] array0 ={
                                 "order_id="+order_id,
                                 "appid="+appid,
                                 "product_name="+Server.UrlEncode(product_name),
                                 "product_desc="+Server.UrlEncode(product_desc),
                                 "pay_method="+pay_method,
                                 "pay_amount="+pay_amount
                             };
            if (!JumbotPay.Utils.Strings.CheckSign(array0, key, sign))
            {
                Response.Write("验签失败");
                Response.End();
            }
            order_id = new JumbotPay.DAL.OrderDAL().NewOrder(appid, order_id, pay_method, Str2Double(pay_amount), product_name, product_desc);//订单号
            //为防止乱码，参数只传appid,order_id
            string[] array1 ={
                                 "appid="+appid,
                                 "order_id="+order_id
                             };
            string sign1 = JumbotPay.Utils.Strings.GetSign(array1,key);
            string _url =  pay_method + "/default2.aspx?" + JumbotPay.Utils.Strings.GetParaStr(array1, sign1);
            //CYQ.Data.Log.WriteLogToTxt("\r\n" + Request.UrlReferrer + "\r\n" + Request.Url.ToString() + "\r\n");
            Response.Redirect(_url);

            //Response.Write("<script>location.href='" + _url+ "';</script>");
        }
    }
}