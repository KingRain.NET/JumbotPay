﻿using System;
using System.Collections.Specialized;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using JumbotPay.Utils;
using JumbotPay.Common;





namespace JumbotPay.WebFile.List
{
    public partial class report_ajax : JumbotPay.UI.BasicPage
    {
        private string _operType = string.Empty;
        private string _response = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {

            ajaxGetList();
            Response.Write(this._response);
        }

        private void ajaxGetList()
        {
            int page = Int_ThisPage();
            int PSize = Str2Int(q("pagesize"), 20);
            int totalCount = 0;
            string sqlStr = "";
            string _pay_order_id = JumbotPay.Utils.Strings.DelSymbol(q("pay_order_id"));
            string begindate = q("begindate");
            string enddate = q("enddate");
            string whereStr = "1=1";//分页条件(不带A.)
            if (_pay_order_id.Length > 0)
            {
                whereStr += " and [pay_order_id]='" + _pay_order_id + "'";
            }
            if (JumbotPay.Utils.Validator.IsStringDate(begindate))
            {
                whereStr += " AND [order_time]>='" + begindate + " 0:0:0'";
            }
            if (JumbotPay.Utils.Validator.IsStringDate(enddate))
            {
                whereStr += " AND [order_time]<='" + enddate + " 23:59:59'";
            }
            NameValueCollection orders = new NameValueCollection();
            orders.Add("Id", "desc");

            string FieldList = "*";

            doh.Reset();
            doh.ConditionExpress = whereStr;
            totalCount = doh.Count("jpay_order");
            sqlStr = JumbotPay.Utils.SqlHelp.GetSql1(FieldList, "jpay_order", totalCount, PSize, page, orders, whereStr);


            doh.Reset();
            doh.SqlCmd = sqlStr;
            DataTable dt = doh.GetDataTable();
            this._response = "{\"result\" :\"1\"," +
                "\"returnval\" :\"操作成功\"," +
                "\"pagebar\" :\"" + JumbotPay.Utils.PageBar.GetPageBar(3, "js", 2, totalCount, PSize, page, "javascript:ajaxList($#page#$);") + "\"," +
                JumbotPay.Utils.dtHelper.DT2JSON(dt) +
                "}";
            dt.Clear();
            dt.Dispose();
        }
    }
}