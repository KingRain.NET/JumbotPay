﻿/*
 * 程序名称: JumbotPay
 * 
 * 程序版本: 1.x
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using JumbotPay.Utils;
using JumbotPay.DBUtility;
using JumbotPay.Common;

namespace JumbotPay.DAL
{
    /// <summary>
    /// 网站参数
    /// </summary>
    public class SiteDAL
    {
        public SiteDAL()
        { }
        /// <summary>
        /// 获得网站参数
        /// </summary>
        /// <returns></returns>
        public JumbotPay.Entity.Site GetEntity()
        {
            JumbotPay.Entity.Site eSite = new JumbotPay.Entity.Site();
            eSite.Name = JumbotPay.Utils.XmlCOM.ReadConfig("~/_data/config/site", "Name");
            eSite.Url = JumbotPay.Utils.XmlCOM.ReadConfig("~/_data/config/site", "Url");
            if (eSite.Url == "")
                eSite.Url = JumbotPay.Utils.App.Url;
            else
                eSite.Url = eSite.Url.TrimEnd('/');
            eSite.Dir = JumbotPay.Utils.App.Path;
            return eSite;
        }
    }
}
