﻿/*
 * 程序名称: JumbotPay
 * 
 * 程序版本: 1.x
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using JumbotPay.Utils;
using JumbotPay.DBUtility;

namespace JumbotPay.DAL
{
    /// <summary>
    /// 
    /// </summary>
    public class AppDAL : Common
    {
        public AppDAL()
        {
            base.SetupSystemDate();
        }
        /// <summary>
        /// 或得签名用的key
        /// </summary>
        /// <param name="_ordernum"></param>
        /// <param name="_out_trade_no"></param>
        /// <returns></returns>
        public string GetKey(string _appid)
        {
            using (DbOperHandler _doh = new Common().Doh())
            {
                _doh.Reset();
                _doh.ConditionExpress = "appid=@appid and state=1";
                _doh.AddConditionParameter("@appid", _appid);
                return _doh.GetField("jpay_app","appkey").ToString();
            }
        }
    }
}
