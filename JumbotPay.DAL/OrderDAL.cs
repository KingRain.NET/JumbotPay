﻿/*
 * 程序名称: JumbotPay
 * 
 * 程序版本: 1.x
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using JumbotPay.Utils;
using JumbotPay.DBUtility;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
namespace JumbotPay.DAL
{
    public class OrderDAL : Common
    {
        public OrderDAL()
        {
            base.SetupSystemDate();
        }
        /// <summary>
        /// 新增订单
        /// </summary>
        /// <param name="_appid"></param>
        /// <param name="_order_id"></param>
        /// <param name="_pay_method">如：alipay、tenpay、wxpay、bdpay等</param>
        /// <param name="_pay_amount"></param>
        /// <param name="_product_name"></param>
        /// <param name="_product_desc"></param>
        /// <returns></returns>
        public string NewOrder(string _appid, string _order_id, string _pay_method, double _pay_amount, string _product_name, string _product_desc)
        {
            using (DbOperHandler _doh = new Common().Doh())
            {
                if (_order_id == "")
                    _order_id = GetProductOrderNum();//订单号

                _doh.Reset();
                _doh.ConditionExpress = "appid=@appid and order_id=@order_id and pay_method=@pay_method";
                _doh.AddConditionParameter("@appid", _appid);
                _doh.AddConditionParameter("@order_id", _order_id);
                _doh.AddConditionParameter("@pay_method", _pay_method);
                object[] value = _doh.GetFields("jpay_order", "pay_status");
                if (value != null)//订单已经存在
                {
                    if (value[0].ToString() == "1")//已经支付
                        return "";
                    else
                        return _order_id;
                }
                _doh.Reset();
                _doh.AddFieldItem("appid", _appid);
                _doh.AddFieldItem("order_id", _order_id);
                _doh.AddFieldItem("pay_status", 0);
                _doh.AddFieldItem("pay_method", _pay_method);
                _doh.AddFieldItem("pay_amount", _pay_amount);
                _doh.AddFieldItem("product_name", _product_name);
                _doh.AddFieldItem("product_desc", _product_desc);
                _doh.AddFieldItem("order_time", DateTime.Now.ToString());
                _doh.AddFieldItem("out_trade_no", _order_id);
                _doh.Insert("jpay_order");
                return _order_id;
            }
        }
        /// <summary>
        /// 在线支付成功，通知app的notify地址
        /// </summary>
        /// <param name="_appid"></param>
        /// <param name="_out_trade_no"></param>
        /// <returns></returns>
        public bool UpdateOrder(string _pay_method, string _out_trade_no, string _pay_order_id)
        {
            if (_pay_order_id == "")
                _pay_order_id = _out_trade_no;
            using (DbOperHandler _doh = new Common(true).Doh())
            {
                object[] value0 = new JumbotPay.DAL.OrderDAL().GetOrderFields(_pay_method, "", _out_trade_no, "appid,order_id,out_trade_no,pay_status");
                if (value0 == null)
                    return false;
                string _appid = value0[0].ToString();
                string _order_id = value0[1].ToString();
                int pay_status = Str2Int(value0[3].ToString());
                _doh.Reset();
                _doh.ConditionExpress = "appid=@appid";
                _doh.AddConditionParameter("@appid", _appid);
                object[] value = _doh.GetFields("jpay_app", "notify_url");
                if (value != null)
                {
                    string _notify_url = value[0].ToString();
                    string key = new JumbotPay.DAL.AppDAL().GetKey(_appid);

                    string[] array1 = { "order_id=" + _order_id, "pay_status=0", "pay_order_id=" + _pay_order_id };
                    string sign1 = JumbotPay.Utils.Strings.GetSign(array1, key);
                    string _posturl = _notify_url;
                    string _postData = JumbotPay.Utils.Strings.GetParaStr(array1, sign1);
                    string _html = JumbotPay.Utils.HttpHelper.Post_Http(_posturl, _postData, System.Text.Encoding.GetEncoding("utf-8"));
                    //if (_appid == "10004")
                    {
                        CYQ.Data.Log.WriteLogToTxt(_html);
                    }
                    if (_html == "success")
                    {
                        _doh.Reset();
                        _doh.ConditionExpress = "appid=@appid and out_trade_no=@out_trade_no and pay_status=0 and pay_method=@pay_method";
                        _doh.AddConditionParameter("@appid", _appid);
                        _doh.AddConditionParameter("@out_trade_no", _out_trade_no);
                        _doh.AddConditionParameter("@pay_method", _pay_method);
                        _doh.AddFieldItem("pay_status", 1);
                        _doh.AddFieldItem("pay_time", System.DateTime.Now.ToString());
                        _doh.AddFieldItem("notified", 1);
                        _doh.AddFieldItem("NoticeTimes", 1);
                        _doh.AddFieldItem("pay_order_id", _pay_order_id);

                        _doh.Update("jpay_order");
                        return true;
                    }
                    else
                    {
                        _doh.Reset();
                        _doh.ConditionExpress = "appid=@appid and out_trade_no=@out_trade_no and pay_status=0 and pay_method=@pay_method";
                        _doh.AddConditionParameter("@appid", _appid);
                        _doh.AddConditionParameter("@out_trade_no", _out_trade_no);
                        _doh.AddConditionParameter("@pay_method", _pay_method);
                        _doh.AddFieldItem("pay_status", 1);
                        _doh.AddFieldItem("pay_time", System.DateTime.Now.ToString());
                        _doh.AddFieldItem("notified", 0);
                        _doh.AddFieldItem("pay_order_id", _pay_order_id);
                        _doh.Update("jpay_order");
                        CYQ.Data.Log.WriteLogToTxt("请求：" + _posturl + "\r\n参数：" + _postData + "\r\n返回：" + _html);
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }
        /// <summary>
        /// 获得订单信息
        /// </summary>
        /// <param name="_order_id"></param>
        /// <param name="_fieldname"></param>
        /// <returns></returns>
        public object[] GetOrderFields(string _pay_method, string _order_id, string _out_trade_no, string _fieldname)
        {
            using (DbOperHandler _doh = new Common().Doh())
            {
                _doh.Reset();
                if (_out_trade_no == "")
                {
                    _doh.ConditionExpress = "order_id=@order_id and pay_method=@pay_method";
                    _doh.AddConditionParameter("@order_id", _order_id);
                    _doh.AddConditionParameter("@pay_method", _pay_method);
                }
                else
                {
                    _doh.ConditionExpress = "out_trade_no=@out_trade_no and pay_method=@pay_method";
                    _doh.AddConditionParameter("@out_trade_no", _out_trade_no);
                    _doh.AddConditionParameter("@pay_method", _pay_method);
                }
                return _doh.GetFields("View_Order", _fieldname);
            }
        }
        /// <summary>
        /// 获得流水号
        /// </summary>
        /// <param name="_order_id"></param>
        /// <param name="_out_trade_no"></param>
        /// <returns></returns>
        public string GetOutOrderNO(string _order_id)
        {
            using (DbOperHandler _doh = new Common().Doh())
            {
                _doh.Reset();
                _doh.ConditionExpress = "order_id=@order_id";
                _doh.AddConditionParameter("@order_id", _order_id);
                return _doh.GetField("jpay_order", "out_trade_no").ToString();
            }
        }
        /// <summary>
        /// 更新订单流水号，防止第三方支付平台有重复(目前只需要支持微信扫码)
        /// </summary>
        /// <param name="_order_id"></param>
        /// <param name="_out_trade_no"></param>
        /// <returns></returns>
        public string UpdateOutOrderNO(string _order_id, string _out_trade_no)
        {
            using (DbOperHandler _doh = new Common().Doh())
            {
                _doh.Reset();
                _doh.ConditionExpress = "order_id=@order_id and order_id=out_trade_no and pay_status=0 and pay_method='wxpay'";
                _doh.AddConditionParameter("@order_id", _order_id);
                _doh.AddFieldItem("out_trade_no", _out_trade_no);
                if (_doh.Update("jpay_order") > 0)
                {
                    return _out_trade_no;
                }
                else
                {
                    _doh.Reset();
                    _doh.ConditionExpress = "order_id=@order_id";
                    _doh.AddConditionParameter("@order_id", _order_id);
                    return _doh.GetField("jpay_order", "out_trade_no").ToString();
                }
            }
        }
    }
}
