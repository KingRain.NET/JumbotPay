﻿/*
 * 程序名称: JumbotPay
 * 
 * 程序版本: 1.x
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using JumbotPay.Common;
using JumbotPay.DBUtility;

namespace JumbotPay.DAL
{
    public class Common
    {
        public string StaticKey = "!@#$%^&*";
        public static string connectionString = Const.ConnectionString;
        public string ORDER_BY_RND()
        {
            return "newid()";
        }
        public string vbCrlf = "\r\n";//换行符
        protected JumbotPay.Entity.Site site;
        public Common()
        {
            if (System.Web.HttpContext.Current.Application["ixueshuV1_site"] == null)
            {
                System.Web.HttpContext.Current.Application.Lock();
                System.Web.HttpContext.Current.Application["ixueshuV1_site"] = new JumbotPay.DAL.SiteDAL().GetEntity();
               System.Web.HttpContext.Current.Application.UnLock();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_formatsystem">是否初始化site</param>
        public Common(bool _formatsystem)
        {
            if (System.Web.HttpContext.Current.Application["ixueshuV1_site"] == null)
            {
                System.Web.HttpContext.Current.Application.Lock();
                System.Web.HttpContext.Current.Application["ixueshuV1_site"] = new JumbotPay.DAL.SiteDAL().GetEntity();
                System.Web.HttpContext.Current.Application.UnLock();
            }
            if (_formatsystem)
            {
                site = (JumbotPay.Entity.Site)System.Web.HttpContext.Current.Application["ixueshuV1_site"];
           }
        }
        /// <summary>
        /// 初始化系统信息
        /// </summary>
        protected void SetupSystemDate()
        {

            site = (JumbotPay.Entity.Site)System.Web.HttpContext.Current.Application["ixueshuV1_site"];
        }
        public DbOperHandler Doh()
        {
            return new SqlDbOperHandler(new SqlConnection(connectionString));
        }
        public string JoinEndHTML(string _pagestr)
        {
           // string _tag = "</" + "b" + "o" + "d" + "y>";
            //return _pagestr.Replace(_tag, _tag + "\r\n<!--" + JumbotPay.Utils.Strings.DecryptStr("tndupcnvk!zc!fubfsd") + site.Version + "-->");
            return _pagestr;
        }
        /// <summary>
        /// 生成随机数字字符串
        /// </summary>
        /// <param name="int_NumberLength">数字长度</param>
        /// <returns></returns>
        public string GetRandomNumberString(int int_NumberLength)
        {
            return GetRandomNumberString(int_NumberLength, false);
        }
        /// <summary>
        /// 生成随机数字字符串
        /// </summary>
        /// <param name="int_NumberLength">数字长度</param>
        /// <returns></returns>
        public string GetRandomNumberString(int int_NumberLength, bool onlyNumber)
        {
            Random random = new Random();
            return GetRandomNumberString(int_NumberLength, onlyNumber, random);
        }
        /// <summary>
        /// 生成随机数字字符串
        /// </summary>
        /// <param name="int_NumberLength">数字长度</param>
        /// <returns></returns>
        public string GetRandomNumberString(int int_NumberLength, bool onlyNumber, Random random)
        {
            string strings = "123456789";
            if (!onlyNumber) strings += "abcdefghjkmnpqrstuvwxyz";
            char[] chars = strings.ToCharArray();
            string returnCode = string.Empty;
            for (int i = 0; i < int_NumberLength; i++)
                returnCode += chars[random.Next(0, chars.Length)].ToString();
            return returnCode;
        }
        /// <summary>
        /// 生成产品订单号，全站统一格式
        /// </summary>
        /// <returns></returns>
        public string GetProductOrderNum()
        {
            return DateTime.Now.ToString("yyyyMMddHHmmss") + GetRandomNumberString(4, true);
        }
        /// <summary>
        /// 产生随机数字字符串
        /// </summary>
        /// <returns></returns>
        public string RandomStr(int Num)
        {
            int number;
            char code;
            string returnCode = String.Empty;

            Random random = new Random();

            for (int i = 0; i < Num; i++)
            {
                number = random.Next();
                code = (char)('0' + (char)(number % 10));
                returnCode += code.ToString();
            }
            return returnCode;
        }
        /// <summary>
        /// 执行Sql脚本文件
        /// </summary>
        /// <param name="pathToScriptFile">物理路径</param>
        /// <returns></returns>
        public bool ExecuteSqlInFile(string pathToScriptFile)
        {
            return JumbotPay.Utils.ExecuteSqlBlock.Go("1", connectionString, pathToScriptFile);

        }
        /// <summary>
        /// 获得翻页Bar，适合js和html
        /// </summary>
        /// <param name="mode">支持1=simple,2=normal,3=full</param>
        /// <param name="stype"></param>
        /// <param name="totalCount"></param>
        /// <param name="PSize"></param>
        /// <param name="currentPage"></param>
        /// <param name="Http"></param>
        /// <returns></returns>
        public string getPageBar(int mode, string stype, int stepNum, int totalCount, int PSize, int currentPage, string HttpN)
        {
            return JumbotPay.Utils.PageBar.GetPageBar(mode, stype, stepNum, totalCount, PSize, currentPage, HttpN);
        }
        /// <summary>
        /// 获得翻页Bar，适合js和html
        /// </summary>
        /// <param name="mode">支持1=simple,2=normal,3=full</param>
        /// <param name="stype"></param>
        /// <param name="stepNum"></param>
        /// <param name="totalCount"></param>
        /// <param name="PSize"></param>
        /// <param name="currentPage"></param>
        /// <param name="Http1"></param>
        /// <param name="HttpM"></param>
        /// <param name="HttpN"></param>
        /// <param name="limitPage"></param>
        /// <returns></returns>
        public string getPageBar(int mode, string stype, int stepNum, int totalCount, int PSize, int currentPage, string Http1, string HttpM, string HttpN, int limitPage)
        {
            return JumbotPay.Utils.PageBar.GetPageBar(mode, stype, stepNum, totalCount, PSize, currentPage, Http1, HttpM, HttpN, limitPage);
        }

        /// <summary>
        /// 返回整数，默认为t
        /// </summary>
        /// <param name="s">参数值</param>
        /// <returns>返回值</returns>
        public int Str2Int(string s, int t)
        {
            return JumbotPay.Utils.Validator.StrToInt(s, t);
        }

        /// <summary>
        /// 返回整数，默认为0
        /// </summary>
        /// <param name="s">参数值</param>
        /// <returns>返回值</returns>
        public int Str2Int(string s)
        {
            return Str2Int(s, 0);
        }
        /// <summary>
        /// 返回整数，默认为t
        /// </summary>
        /// <param name="s">参数值</param>
        /// <returns>返回值</returns>
        public long Str2Long(string s, long t)
        {
            return JumbotPay.Utils.Validator.StrToLong(s, t);
        }
        public long Str2Long(string s)
        {
            return Str2Long(s, 0);
        }
        /// <summary>
        /// 返回整数，默认为0
        /// </summary>
        /// <param name="s">参数值</param>
        /// <returns>返回值</returns>
        public Double Str2Double(string s)
        {
            return Str2Double(s, 0);
        }
        public Double Str2Double(string s, Double t)
        {
            return JumbotPay.Utils.Validator.StrToDouble(s, t);
        }

        /// <summary>
        /// 返回整数，默认为0
        /// </summary>
        /// <param name="s">参数值</param>
        /// <returns>返回值</returns>

        /// <summary>
        /// 返回非空字符串，默认为"0"
        /// </summary>
        /// <param name="s">参数值</param>
        /// <returns>返回值</returns>
        public string Str2Str(string s)
        {
            return JumbotPay.Utils.Validator.StrToInt(s, 0).ToString();
        }
        /// <summary>
        /// 字符串长度
        /// </summary>
        //protected int GetStringLen(string str)
        //{
        //    byte[] bs = System.Text.Encoding.UTF8.GetBytes(str);
        //    return bs.Length;
        //}
        /// <summary>
        /// 字符串截断
        /// </summary>
        /// <param name="str"></param>
        /// <param name="Length">以汉字计算，比如Length为100表示取200个字符，100个汉字</param>
        /// <returns></returns>
        protected string GetCutString(string str, int Length)
        {
            Length *= 2;
            byte[] bs = System.Text.Encoding.Default.GetBytes(str);//请勿随意改编码，否则计算有误
            if (bs.Length <= Length)
            {
                return str;
            }
            else
            {
                return System.Text.Encoding.Default.GetString(bs, 0, Length);//请勿随意改编码，否则计算有误
            }
        }
    }
}
