﻿/*
 * 程序名称: JumbotPay
 * 
 * 程序版本: 1.x
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Collections.Specialized;
using System.Data;
using System.Web;
using System.Web.UI;
using System.IO;
using System.Data.SqlClient;
using System.Text;
using JumbotPay.Common;
using JumbotPay.Utils;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
namespace JumbotPay.UI
{

    /// <summary>
    /// BasicPage 的摘要说明
    /// </summary>
    public class BasicPage : JumbotPay.DBUtility.UI.PageUI
    {
        public static string WebRoot = System.Configuration.ConfigurationManager.AppSettings["JumbotPay:WebRoot"];
        public string Version = "1.0.0903";
        public string vbCrlf = "\r\n";//换行符
        public bool NeedLicense = false;//是否需要许可证(对IP访问不限制)
        protected JumbotPay.Entity.Site site = new JumbotPay.Entity.Site();

        /// <summary>
        /// api静态秘钥
        /// </summary>
        public string StaticKey = "!@#$%^&*";
        override protected void OnInit(EventArgs e)
        {
            Server.ScriptTimeout = 90;//默认脚本过期时间
            LoadJumbotPay();
            base.OnInit(e);
            //if (!IsPostBack)
            //{
            //    if (!ActionValidator.IsValid(ActionValidator.ActionTypeEnum.Normal))
            //    {
            //        Response.Write("您太频繁访问了，端个椅子坐一会吧");
            //        Response.End();
            //    }
            //}
            //else
            //{
            //    if (!ActionValidator.IsValid(ActionValidator.ActionTypeEnum.Postback))
            //    {
            //        Response.Write("您太频繁访问了，端个椅子坐一会吧");
            //        Response.End();
            //    }
            //}

        }
        public void LoadJumbotPay()
        {
            this.ConnectDb();
            if (System.Web.HttpContext.Current.Application["ixueshuV1_site"] == null)
            {
                SetupSystemDate();
            }
            site = (JumbotPay.Entity.Site)System.Web.HttpContext.Current.Application["ixueshuV1_site"];
        }
        public string ORDER_BY_RND()
        {
            return "newid()";
        }
        /// <summary>
        /// 连接数据库
        /// </summary>
        public override void ConnectDb()
        {
            if (doh == null)
            {
                try
                {

                    if (System.Web.HttpContext.Current.Application["ixueshuV1_dbConnStr"] == null)
                    {
                        string dbServerIP = JumbotPay.Utils.XmlCOM.ReadConfig("~/_data/config/conn", "dbServerIP");
                        string dbLoginName = JumbotPay.Utils.XmlCOM.ReadConfig("~/_data/config/conn", "dbLoginName");
                        string dbLoginPass = JumbotPay.Utils.XmlCOM.ReadConfig("~/_data/config/conn", "dbLoginPass");
                        string dbName = JumbotPay.Utils.XmlCOM.ReadConfig("~/_data/config/conn", "dbName");
                        string dbConnStr = "Data Source=" + dbServerIP + ";Initial Catalog=" + dbName + ";User ID=" + dbLoginName + ";Password=" + dbLoginPass + ";Pooling=true;max pool size=1024;";
                        System.Web.HttpContext.Current.Application.Lock();
                        System.Web.HttpContext.Current.Application["ixueshuV1_dbConnStr"] = dbConnStr;
                        System.Web.HttpContext.Current.Application.UnLock();
                    }
                    System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(System.Web.HttpContext.Current.Application["ixueshuV1_dbConnStr"].ToString());
                    if (conn.State != System.Data.ConnectionState.Open) conn.Open();
                    doh = new JumbotPay.DBUtility.SqlDbOperHandler(conn);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        /// <summary>
        /// 关闭数据库连接
        /// </summary>
        public void CloseDB()
        {
            if (doh != null) doh.Dispose();
        }
        /// <summary>
        /// 生成随机数字字符串
        /// </summary>
        /// <param name="int_NumberLength">数字长度</param>
        /// <returns></returns>
        public string GetRandomNumberString(int int_NumberLength)
        {
            return GetRandomNumberString(int_NumberLength, false);
        }
        /// <summary>
        /// 生成随机数字字符串
        /// </summary>
        /// <param name="int_NumberLength">数字长度</param>
        /// <returns></returns>
        public string GetRandomNumberString(int int_NumberLength, bool onlyNumber)
        {
            Random random = new Random();
            return GetRandomNumberString(int_NumberLength, onlyNumber, random);
        }
        /// <summary>
        /// 生成随机数字字符串
        /// </summary>
        /// <param name="int_NumberLength">数字长度</param>
        /// <returns></returns>
        public string GetRandomNumberString(int int_NumberLength, bool onlyNumber, Random random)
        {
            string strings = "123456789";
            if (!onlyNumber) strings += "abcdefghjkmnpqrstuvwxyz";
            char[] chars = strings.ToCharArray();
            string returnCode = string.Empty;
            for (int i = 0; i < int_NumberLength; i++)
                returnCode += chars[random.Next(0, chars.Length)].ToString();
            return returnCode;
        }
        /// <summary>
        /// 生成产品订单号，全站统一格式
        /// </summary>
        /// <returns></returns>
        public string GetProductOrderNum()
        {
            return DateTime.Now.ToString("yyyyMMddHHmmss") + GetRandomNumberString(4, true);
        }
        public void DownloadFile(string _filePath)
        {
            DownloadFile(_filePath, "");
        }
        public void DownloadFile(string _filePath, string _fileName)
        {
            //Response.Redirect(_filePath);
            //return;
            //暂时不用如下方式，服务器会吃不消
            Response.Clear();
            bool success = true;
            if (_filePath.StartsWith("http://") || _filePath.StartsWith("https://") || _filePath.StartsWith("ftp://"))
                Response.Redirect(_filePath);
            else if (!JumbotPay.Utils.DirFile.FileExists(_filePath))
            {
                System.IO.StreamWriter sw = new System.IO.StreamWriter(WebRoot + "\\Logs\\nofile_" + DateTime.Now.ToString("yyyyMMdd") + ".log", true, System.Text.Encoding.UTF8);
                sw.WriteLine(System.DateTime.Now.ToString());
                sw.WriteLine("\tIP 地 址：" + Const.GetUserIp);
                sw.WriteLine("\t浏 览 器：" + HttpContext.Current.Request.Browser.Browser + " " + HttpContext.Current.Request.Browser.Version);
                sw.WriteLine("\t下载页面：" + ServerUrl() + Const.GetCurrentUrl);
                sw.WriteLine("\t无效文件：" + _filePath);
                sw.WriteLine("---------------------------------------------------------------------------------------------------");
                sw.Close();
                Response.Write("指定的文件不存在,请通知管理员");
            }
            else
            {
                success = JumbotPay.Utils.DirFile.DownloadFile(Request, Response, Server.MapPath(_filePath), _fileName, 1024000);
                if (!success)
                {
                    System.IO.StreamWriter sw = new System.IO.StreamWriter(WebRoot + "\\Logs\\downerror_" + DateTime.Now.ToString("yyyyMMdd") + ".log", true, System.Text.Encoding.UTF8);
                    sw.WriteLine(System.DateTime.Now.ToString());
                    sw.WriteLine("\tIP 地 址：" + Const.GetUserIp);
                    sw.WriteLine("\t浏 览 器：" + HttpContext.Current.Request.Browser.Browser + " " + HttpContext.Current.Request.Browser.Version);
                    sw.WriteLine("\t下载页面：" + ServerUrl() + Const.GetCurrentUrl);
                    sw.WriteLine("\t失败文件：" + _filePath);
                    sw.WriteLine("---------------------------------------------------------------------------------------------------");
                    sw.Close();
                    Response.Redirect(_filePath);
                }
            }
            Response.End();
        }
        /// <summary>
        /// 简单的防止站外提交表单
        /// 仿一般黑客，防不住高手
        /// 如果checkHost是false，则不允许直接访问，否则还不允许外站嵌入
        /// </summary>
        /// <param name="checkHost">如果checkHost是false，则不允许直接访问，否则还不允许外站嵌入</param>
        /// <returns></returns>
        public bool CheckFormUrl(bool checkHost)
        {
            if (HttpContext.Current.Request.UrlReferrer == null)
            {
                //SaveVisitLog(2, 0);
                return false;
            }
            if (checkHost)
            {
                if ((HttpContext.Current.Request.UrlReferrer.Host) != (HttpContext.Current.Request.Url.Host))
                {
                    SaveVisitLog(2, 0);
                    return false;
                }
            }
            return true;
        }
        public bool CheckFormUrl()
        {
            return CheckFormUrl(true);
        }
        #region 处理过程完成
        /// <summary>
        /// 处理过程完成
        /// </summary>
        /// <param name="pageMsg">页面提示信息</param>
        /// <param name="go2Url">如果倒退步数为0，就转到该地址</param>
        /// <param name="BackStep">倒退步数</param>
        protected void FinalMessage(string pageMsg, string go2Url, int BackStep)
        {
            FinalMessage(pageMsg, go2Url, BackStep, 2);
        }
        /// <summary>
        /// 处理过程完成
        /// </summary>
        /// <param name="pageMsg">页面提示信息</param>
        /// <param name="go2Url">如果倒退步数为0，就转到该地址</param>
        /// <param name="BackStep">倒退步数</param>
        /// <param name="BackStep">自动转向的秒数</param>
        protected void FinalMessage(string pageMsg, string go2Url, int BackStep, int Seconds)
        {
            FinalMessage(":)", pageMsg, go2Url, BackStep, Seconds);
        }
        protected void FinalMessage(string face,string pageMsg, string go2Url, int BackStep, int Seconds)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>\r\n");
            sb.Append("<html xmlns='http://www.w3.org/1999/xhtml'>\r\n");
            sb.Append("<head>\r\n");
            sb.Append("<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />\r\n");
            sb.Append("<title>系统提示</title>\r\n");
            sb.Append("<style type=\"text/css\">\r\n");
            sb.Append("*{ padding: 0; margin: 0; }\r\n");
            sb.Append("body{ background: #fff; font-family: '微软雅黑'; color: #333; font-size: 16px; }\r\n");
            sb.Append(".system-message{ padding: 24px 48px; }\r\n");
            sb.Append(".system-message h1{ font-size: 100px; font-weight: normal; line-height: 120px; margin-bottom: 12px; }\r\n");
            sb.Append(".system-message .jump{ padding-top: 10px}\r\n");
            sb.Append(".system-message .jump a{ color: #333;}\r\n");
            sb.Append(".system-message .success,.system-message .error{ line-height: 1.8em; font-size: 36px }\r\n");
            sb.Append(".system-message .detail{ font-size: 12px; line-height: 20px; margin-top: 12px; display:none}\r\n");
            sb.Append("</style>\r\n");
            sb.Append("<script language=\"javascript\">\r\n");
            sb.Append("var seconds=" + Seconds + ";\r\n");
            sb.Append("for(i=1;i<=seconds;i++)\r\n");
            sb.Append("{window.setTimeout(\"update(\" + i + \")\", i * 1000);}\r\n");
            sb.Append("function update(num)\r\n");
            sb.Append("{\r\n");
            sb.Append("if(num == seconds)\r\n");
            if (BackStep > 0)
                sb.Append("{ history.go(" + (0 - BackStep) + "); }\r\n");
            else
            {
                if (go2Url != "")
                    sb.Append("{ self.location.href='" + go2Url + "'; }\r\n");
                else
                    sb.Append("{window.open('','_self').close();}\r\n");
            }
            sb.Append("else\r\n");
            sb.Append("{ }\r\n");
            sb.Append("}\r\n");
            sb.Append("</script>\r\n");
            sb.Append("<base target='_self' />\r\n");
            sb.Append("</head>\r\n");
            sb.Append("<body>\r\n");
            sb.Append("<div class='system-message'>\r\n");
            sb.Append("<h1>"+face+"</h1>\r\n");
            sb.Append("<p class='success'>" + pageMsg + "</p>\r\n");
            sb.Append("<p class='detail'></p>\r\n");
            sb.Append("<p class='jump'>\r\n");
            if (BackStep > 0)
                sb.Append("页面自动 <a href=\"javascript:history.go(" + (0 - BackStep) + ")\">跳转</a> 等待时间： <b id='wait'>" + Seconds + "</b>\r\n");
            else
                sb.Append("页面自动 <a href=\"" + go2Url + "\">跳转</a> 等待时间： <b id='wait'>" + Seconds + "</b>\r\n");
            sb.Append("</p>\r\n");
            sb.Append("</div>\r\n");
            sb.Append("</body>\r\n");
            sb.Append("</html>\r\n");
            HttpContext.Current.Response.Write(sb.ToString());
            //以下这行千万别手痒痒删掉
            HttpContext.Current.Response.End();

        }
        #endregion
        /// <summary>
        /// 当前页码
        /// </summary>
        public int Int_ThisPage()
        {
            int _page = Str2Int(q("page"), 0) < 1 ? 1 : Str2Int(q("page"), 0);
            return _page;
        }

        /// <summary>
        /// 执行Sql脚本文件
        /// </summary>
        /// <param name="pathToScriptFile">物理路径</param>
        /// <returns></returns>
        public bool ExecuteSqlInFile(string pathToScriptFile)
        {
            return JumbotPay.Utils.ExecuteSqlBlock.Go("1", System.Web.HttpContext.Current.Application["ixueshuV1_dbConnStr"].ToString(), pathToScriptFile);
        }
        /// <summary>
        /// 附加被选择的字段
        /// </summary>
        /// <param name="_fields">格式为[字段1],[字段2]</param>
        /// <returns></returns>
        public static string JoinFields(string _fields)
        {
            if (_fields.Trim().Length == 0)
                return "";
            else
                return "," + _fields;
        }

        /// <summary>
        /// 页面访问超时后记录日志
        /// </summary>
        /// <param name="_second">超时秒数</param>
        public void SavePageLog(int _second)
        {
            SaveVisitLog(1, _second);
        }
        /// <summary>
        /// 保存访问日志
        /// </summary>
        /// <param name="_type">1代表访问者,2代表非法</param>
        /// <param name="_second">脚本秒数</param>
        public void SaveVisitLog(int _type, int _second)
        {
            SaveVisitLog(_type, _second, "");
        }
        /// <summary>
        /// 保存访问日志
        /// </summary>
        /// <param name="_type">1代表访问者,2代表非法</param>
        /// <param name="_second">脚本秒数</param>
        /// <param name="_logfilename">自定义log保存路径</param>
        public void SaveVisitLog(int _type, int _second, string _logfilename)
        {
            if (_type == 1)
            {
                string _savefile = _logfilename == "" ? "~/Logs/vister_" + DateTime.Now.ToString("yyyyMMdd") + ".log" : _logfilename;
                Single s = (Single)DateTime.Now.Subtract(HttpContext.Current.Timestamp).TotalSeconds;
                if (s > _second)
                {
                    System.IO.StreamWriter sw = new System.IO.StreamWriter(HttpContext.Current.Server.MapPath(_savefile), true, System.Text.Encoding.UTF8);
                    sw.WriteLine(System.DateTime.Now.ToString());
                    sw.WriteLine("\tIP 地 址：" + Const.GetUserIp);
                    sw.WriteLine("\t浏 览 器：" + HttpContext.Current.Request.Browser.Browser + " " + HttpContext.Current.Request.Browser.Version);
                    sw.WriteLine("\t耗    时：" + ((Single)DateTime.Now.Subtract(HttpContext.Current.Timestamp).TotalSeconds).ToString("0.000") + "秒");
                    sw.WriteLine("\t地    址：" + ServerUrl() + Const.GetCurrentUrl);
                    sw.WriteLine("---------------------------------------------------------------------------------------------------");
                    sw.Close();
                    sw.Dispose();
                }
            }
            else
            {
                string _savefile = _logfilename == "" ? "~/Logs/hacker_" + DateTime.Now.ToString("yyyyMMdd") + ".log" : _logfilename;
                System.IO.StreamWriter sw = new System.IO.StreamWriter(HttpContext.Current.Server.MapPath(_savefile), true, System.Text.Encoding.UTF8);
                sw.WriteLine(System.DateTime.Now.ToString());
                sw.WriteLine("\tIP 地 址：" + Const.GetUserIp);

                sw.WriteLine("\t浏 览 器：" + HttpContext.Current.Request.Browser.Browser + " " + HttpContext.Current.Request.Browser.Version);
                sw.WriteLine("\t来    源：" + Const.GetRefererUrl);
                sw.WriteLine("\t地    址：" + ServerUrl() + Const.GetCurrentUrl);
                sw.WriteLine("---------------------------------------------------------------------------------------------------");
                sw.Close();
                sw.Dispose();
            }
        }
        /// <summary>
        /// 服务器地址
        /// </summary>
        /// <returns></returns>
        protected string ServerUrl()
        {
             return "http://" + HttpContext.Current.Request.ServerVariables["HTTP_HOST"].ToString();
        }
        /// <summary>
        /// 产生随机数字字符串
        /// </summary>
        /// <returns></returns>
        public string RandomStr(int Num)
        {
            int number;
            char code;
            string returnCode = String.Empty;

            Random random = new Random();

            for (int i = 0; i < Num; i++)
            {
                number = random.Next();
                code = (char)('0' + (char)(number % 10));
                returnCode += code.ToString();
            }
            return returnCode;
        }
        /// <summary>
        /// 初始化系统信息
        /// </summary>
        protected void SetupSystemDate()
        {
            site = new JumbotPay.DAL.SiteDAL().GetEntity();
            System.Web.HttpContext.Current.Application.Lock();
            System.Web.HttpContext.Current.Application["ixueshuV1_site"] = site;
            System.Web.HttpContext.Current.Application.UnLock();
        }
        /// <summary>
        /// 输出json结果
        /// </summary>
        /// <param name="success">是否操作成功,0表示失败;1表示成功</param>
        /// <param name="str">输出字符串</param>
        /// <returns></returns>
        protected string JsonResult(int success, string str)
        {
            JObject _jo = new JObject();
            _jo.Add("result", success.ToString());
            _jo.Add("returnval", str);
            return _jo.ToString();
            //return "{\"result\" :\"" + success.ToString() + "\",\"returnval\" :\"" + str + "\"}";

        }
   
        /// <summary>
        /// 当前地址前缀
        /// </summary>
        public string GetUrlPrefix()
        {
            HttpRequest Request = HttpContext.Current.Request;
            string strUrl;
            strUrl = HttpContext.Current.Request.ServerVariables["Url"];
            if (HttpContext.Current.Request.QueryString.Count == 0) //如果无参数
                return strUrl + "?page=";
            else
            {
                //if (JumbotPay.Utils.Strings.Left(HttpContext.Current.Request.ServerVariables["Query_String"], 5) == "page=")//只有页参数
                if (HttpContext.Current.Request.ServerVariables["Query_String"].StartsWith("page=", StringComparison.OrdinalIgnoreCase))//只有页参数
                    return strUrl + "?page=";
                else
                {
                    string[] strUrl_left;
                    strUrl_left = HttpContext.Current.Request.ServerVariables["Query_String"].Split(new string[] { "page=" }, StringSplitOptions.None);
                    if (strUrl_left.Length == 1)//没有页参数
                        return strUrl + "?" + strUrl_left[0] + "&page=";
                    else
                        return strUrl + "?" + strUrl_left[0] + "page=";
                }

            }

        }
        
        /// <summary>
        /// 获取querystring
        /// </summary>
        /// <param name="s">参数名</param>
        /// <returns>返回值</returns>
        public string q(string s)
        {
            if (HttpContext.Current.Request.QueryString[s] != null && HttpContext.Current.Request.QueryString[s] != "")
            {
                return JumbotPay.Utils.Strings.SafetyQueryS(HttpContext.Current.Request.QueryString[s].ToString());
            }
            return string.Empty;
        }

        /// <summary>
        /// 获取post得到的参数
        /// </summary>
        /// <param name="s">参数名</param>
        /// <returns>返回值</returns>
        public string f(string s)
        {
            if (HttpContext.Current.Request.Form[s] != null && HttpContext.Current.Request.Form[s] != "")
            {
                return HttpContext.Current.Request.Form[s].ToString();
            }
            return string.Empty;
        }

        /// <summary>
        /// 返回整数，默认为t
        /// </summary>
        /// <param name="s">参数值</param>
        /// <returns>返回值</returns>
        public int Str2Int(string s, int t)
        {
            return JumbotPay.Utils.Validator.StrToInt(s, t);
        }

        /// <summary>
        /// 返回整数，默认为0
        /// </summary>
        /// <param name="s">参数值</param>
        /// <returns>返回值</returns>
        public int Str2Int(string s)
        {
            return Str2Int(s, 0);
        }
        /// <summary>
        /// 返回整数，默认为t
        /// </summary>
        /// <param name="s">参数值</param>
        /// <returns>返回值</returns>
        public long Str2Long(string s, long t)
        {
            return JumbotPay.Utils.Validator.StrToLong(s, t);
        }
        public long Str2Long(string s)
        {
            return Str2Long(s, 0);
        }
        /// <summary>
        /// 返回双精度浮点数，默认为0
        /// </summary>
        /// <param name="s">参数值</param>
        /// <returns>返回值</returns>
        public Double Str2Double(string s)
        {
            return Str2Double(s, 0);
        }
        public Double Str2Double(string s, Double t)
        {
            return JumbotPay.Utils.Validator.StrToDouble(s, t);
        }

        /// <summary>
        /// 返回整数，默认为0
        /// </summary>
        /// <param name="s">参数值</param>
        /// <returns>返回值</returns>

        /// <summary>
        /// 返回非空字符串，默认为"0"
        /// </summary>
        /// <param name="s">参数值</param>
        /// <returns>返回值</returns>
        public string Str2Str(string s)
        {
            return JumbotPay.Utils.Validator.StrToInt(s, 0).ToString();
        }
        /// <summary>
        /// 字符串长度
        /// </summary>
        //protected int GetStringLen(string str)
        //{
        //    byte[] bs = System.Text.Encoding.UTF8.GetBytes(str);
        //    return bs.Length;
        //}
        /// <summary>
        /// 字符串截断
        /// </summary>
        /// <param name="str"></param>
        /// <param name="Length">以汉字计算，比如Length为100表示取200个字符，100个汉字</param>
        /// <returns></returns>
        protected string GetCutString(string str, int Length)
        {
            Length *= 2;
            byte[] bs = System.Text.Encoding.Default.GetBytes(str);//请勿随意改编码，否则计算有误
            if (bs.Length <= Length)
            {
                return str;
            }
            else
            {
                return System.Text.Encoding.Default.GetString(bs, 0, Length);//请勿随意改编码，否则计算有误
            }
        }

     
        public System.DateTime ConvertIntDateTime(double d)
        {
            System.DateTime time = System.DateTime.MinValue;
            System.DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1));
            time = startTime.AddMilliseconds(d);
            return time;
        }
        /// <summary>
        /// 将c# DateTime时间格式转换为Unix时间戳格式
        /// </summary>
        /// <param name="time">时间</param>
        /// <returns>long</returns>
        public long ConvertDateTimeInt(System.DateTime time)
        {
            return JumbotPay.Utils.DateTimeHelp.ConvertDateTimeInt(time);
        }
        public void SaveAPIErrorLogs(string _url, string _data, string _json)
        {
            System.IO.StreamWriter sw = new System.IO.StreamWriter(HttpContext.Current.Server.MapPath("/Logs/api_error.log"), true, System.Text.Encoding.UTF8);
            sw.WriteLine(System.DateTime.Now.ToString());
            sw.WriteLine("\t访    问：" + _url);
            //sw.WriteLine("\t数    据：" + _data);
            sw.WriteLine("\t返    回：" + _json);
            sw.WriteLine("---------------------------------------------------------------------------------------------------");
            sw.Close();
            sw.Dispose();
        }

        /// <summary>
        /// 控制访问当前页面的周期，请勿滥用，会增加session负担
        /// </summary>
        /// <param name="_pagename"></param>
        /// <param name="_seconds"></param>
        /// <param name="_hasseconds"></param>
        /// <param name="_showinfo"></param>
        public string LimitGetPage(string _pagename, int _seconds, string _showinfo)
        {
            #region 这段代码控制用户频繁下载
            string _errorinfo = "";
            long _thistime = ConvertDateTimeInt(System.DateTime.Now) / 1000;
            if (JumbotPay.Utils.Session.Get("lastvisit_" + _pagename) == null)
            {
                JumbotPay.Utils.Session.Add("lastvisit_" + _pagename, _thistime.ToString());
            }
            else
            {
                long _lastdowntime = Str2Long(JumbotPay.Utils.Session.Get("lastvisit_" + _pagename));
                if (_lastdowntime + _seconds > _thistime)
                {
                    int _hasseconds = (int)(_lastdowntime + _seconds - _thistime);
                    CYQ.Data.Log.WriteLogToTxt("\r\n----------------------FastVisit-----------------\r\n客户机IP:" + Request.UserHostAddress + "\r\n浏览器:" + Request.Browser.Browser + " " + Request.Browser.Version);
                    _errorinfo = string.Format(_showinfo, _hasseconds.ToString());
                }
                else
                    JumbotPay.Utils.Session.Add("lastvisit_" + _pagename, _thistime.ToString());
            }
            #endregion
            return _errorinfo;
        }
    }
}
